<?php

namespace App;

use App\Expenditure;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['name'];

    public function expenditures()
    {
        return $this->hasMany(Expenditure::class);
    }
}
