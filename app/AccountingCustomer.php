<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCustomer extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'rfc', 'curp',
    'regimen', 'tax_regime_id', 'is_active', 'show_to_auxiliar'];

    public function employees()
    {
        return $this->hasMany(AccountingCustomerEmployee::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    function getFiles($folder)
    {
        $collection = $this->images->filter(function ($item) use ($folder) {
            return strpos($item->path, "/$folder/") !== false;
        });
        if(empty($collection)) return null;
        return $collection;
    }

    function getFile($folder)
    {
        $collection = $this->getFiles($folder);
        if(empty($collection)) return null;
        return $collection->last();
    }

    public function fileRegistroPatronal()
    {
        return $this->getFile("registro_patronal");
    }

    public function filesIdentificacion()
    {
        return $this->getFiles("identificacion");
    }

    public function filesAddress()
    {
        return $this->getFiles("address");
    }

    public function filesRFC()
    {
        return $this->getFiles("rfc");
    }

    public function filesCURP()
    {
        return $this->getFiles("curp");
    }

    public function fileActaNacimiento()
    {
        return $this->getFile("acta_nacimiento");
    }

    public function fileConstancia($year, $month)
    {
        return $this->getFile("constancias/$year/$month");
    }

    public function fileConstancias()
    {
        return $this->getFiles("constancias");
    }

    public function fileExpediente($year, $month)
    {
        return $this->getFile("expedientes/$year/$month");
    }

    public function fileExpedientes()
    {
        return $this->getFiles("expedientes");
    }

    public function fileExpedientesClinicos()
    {
        return $this->getFiles("exp_clinicos");
    }

    public function fileActas()
    {
        return $this->getFiles("actas");
    }

    public function fileDiot($year, $month, $type)
    {
        return $this->getFile("diots/$year/$month/$type");
    }

    public function fileDiots()
    {
        return $this->getFiles("diots");
    }

    public function fileSua($year, $month, $type)
    {
        return $this->getFile("suas/$year/$month/$type");
    }

    public function fileSuas()
    {
        return $this->getFiles("suas");
    }

    public function fileMonthlyDeclaration($year, $month, $type)
    {
        return $this->getFile("monthly_declarations/$year/$month/$type");
    }

    public function fileMonthlyDeclarations()
    {
        return $this->getFiles("monthly_declarations");
    }

    public function fileYearlyDeclaration($year, $date, $type)
    {
        return $this->getFile("yearly_declarations/$year/$date/$type");
    }

    public function fileYearlyDeclarations()
    {
        return $this->getFiles("yearly_declarations");
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }


}
