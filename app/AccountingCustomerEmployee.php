<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountingCustomerEmployee extends Model
{
    protected $fillable = ['name', 'email', 'date', 'accounting_customer_id', 'is_active'];

    public function accountingCustomer()
    {
        return $this->belongsTo(AccountingCustomer::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
