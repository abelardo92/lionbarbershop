<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'key', 'name', 'type', 'buy_price'
    ];

    public function scopeActives($query)
    {
        return $query->where('type', 'activo');
    }

    public function scopeInsumos($query)
    {
        return $query->where('type', 'insumo');
    }

    public function subsidiaries()
    {
        return $this->belongsToMany(Subsidiary::class)->withPivot('existence');
    }
}
