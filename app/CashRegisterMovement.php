<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashRegisterMovement extends Model
{
    protected $fillable = [
        'type' ,'cash_register_id' ,'user_id' ,'fiftycents',
        'onepeso' ,'twopesos' ,'fivepesos' ,'tenpesos',
        'twentypesos' ,'fiftypesos' ,'hundredpesos' ,'twohundredpesos',
        'fivehundredpesos' ,'onethousandpesos' ,'total' , 'employee_id', 'usd', 'minutes_taken', 'cash_register_cashier_id'
    ];

    public function cashRegister()
    {
        return $this->belongsTo(CashRegister::class);
    }

    public function cashierMovement()
    {
        return $this->belongsTo(CashRegisterCashierMovement::class, 'cash_register_cashier_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

     public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function getType()
    {
        if ($this->type == 'abrir') {
            return 'Caja abierta';
        }

        if ($this->type == 'mini-corte') {
            return 'Mini corte';
        }

        if ($this->type == 'corte') {
            return 'Corte caja';
        }
    }
}
