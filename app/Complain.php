<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $fillable = [
        'date', 'name', 'reason', 'subsidiary_id', 'folio',
        'phone', 'tracing', 'user_id', 'opened_by'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function openedBy()
    {
        return $this->belongsTo(User::class, 'opened_by', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sale()
    {
        return Sale::where("subsidiary_id", $this->subsidiary_id)->where("folio", $this->folio)->get()->first();
    }

    public function scopeUnreadedComplains($query)
    {
        return $query->where('opened_by', null);
    }
}
