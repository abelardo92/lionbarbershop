<?php

namespace App\Console\Commands;

use App\Customer;
use App\Mail\NextBirthday;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendBirthdayEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email with the next day birthday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->addDay()->format('Y-m-d');
        $customers = Customer::where('birthday', $date)->get();
        if ($customers->count() > 0) {
            Mail::to('arguellesyasociadossc@live.com.mx')
                ->send(new NextBirthday($customers, $date));
            Mail::to('abelardo-0@hotmail.com')
                ->send(new NextBirthday($customers, $date));
            Mail::to('abelardinii@gmail.com')
                ->send(new NextBirthday($customers, $date));
        }
    }
}
