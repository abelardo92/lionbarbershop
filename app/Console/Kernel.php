<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\DisableDayliEmployeeChecks::class,
         Commands\SendBirthdayEmail::class,
         Commands\NoticeDiaryCommand::class,
         Commands\BirthdayMessage::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $file = storage_path('logs/schedules.txt');
        $email = 'hi@imyorch.xyz';
        $day = Carbon::now()->format('l');
        $schedule->command('disable:checks')
             ->dailyAt('8:46')->when(function () use ($day){
                return $day != 'Sunday';
            })->appendOutputTo($file);
        $schedule->command('disable:checks')
             ->dailyAt('15:46')->when(function () use ($day) {
                return $day != 'Sunday';
            })->appendOutputTo($file);

        $schedule->command('disable:checks')
             ->dailyAt('11:16')->when(function () use ($day){
                return $day == 'Sunday';
            })->appendOutputTo($file);
        $schedule->command('disable:checks')
             ->dailyAt('16:16')->when(function () use ($day) {
                return $day == 'Sunday';
            })->appendOutputTo($file);

        $schedule->command('email:send')->dailyAt('10:00')->appendOutputTo($file);
        $schedule->command('messages:birthday')->dailyAt('10:00')->appendOutputTo($file);

        $schedule->command('email:diaries')->everyMinute()->appendOutputTo($file);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
