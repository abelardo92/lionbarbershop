<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomFolder extends Model
{
    protected $fillable = [
        'name', 'tab_name', 'active'
    ];

    public function subFolders()
    {
        return $this->hasMany(CustomSubFolder::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
