<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomSubFolder extends Model
{
    protected $fillable = [
        'name', 'tab_name', 'custom_folder_id', 'active',
    ];

    public function folder()
    {
        return $this->belongsTo(CustomFolder::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
