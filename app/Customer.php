<?php

namespace App;

use App\Referral;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'phone', 'email', 'facebook', 'twitter', 'wallet_number', 'birthday', 'residence_place', 'visits', 'services_free', 'identify_number', 'last_wallet_number', 'created_by', 'created_by_class', 'is_laundry', 'ironing_visits', 'is_active'
    ];

    public function referrals()
    {
        return $this->hasMany(Referral::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function creator()
    {
        $class = "\App\\$this->created_by_class";

        return $this->belongsTo(get_class((new $class)), 'created_by');
    }

    public function addVisit()
    {
        $this->increment('visits');
        if ($this->visits >= 10) {
            $this->addServicesFree();
            $this->resetVisits();
        }
    }

    public function removeVisit()
    {
        if($this->visits > 0){
            $this->decrement('visits');
            $this->save();
        }
    }

    public function resetVisits()
    {
        $this->visits = 0;
        $this->save();
    }

    public function setVisits($visitNumber)
    {
        $this->visits = $visitNumber;
        $this->save();
    }

    public function addServicesFree()
    {
        $this->increment('services_free');
        $this->save();
    }

    public function removeServicesFree()
    {
        if($this->services_free > 0){
            $this->decrement('services_free');
            $this->save();
        }
    }

    public function findAndUpdateParentReferral()
    {
        $referral = Referral::where('wallet_number', $this->wallet_number)->first();
        if ($referral) {
            $referral->used = true;
            $referral->owned_by = $this->id;
            $referral->save();
            $customer = $referral->customer;
            $referrals_count = $customer->referrals_count + 1;
            $customer->referrals_count = $referrals_count;
            $customer->save();
            if ($referrals_count % 10 == 0) {
                $customer->increment('services_free');
            }
        }
    }

    public function routeNotificationForNexmo()
    {
        return '52'.$this->phone;
    }

    public function validPhone()
    {
        return strlen($this->phone) == 10;
    }

    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNotGeneral($query)
    {
        return $query->where('id', '!=', 7);
    }

    public function scopeWithPhoneOrEmail($query)
    {
        return $query->where(function ($q) {
            $q->where('email','!=', '')
            ->orWhere('phone', '!=', '');
        });
    }
}
