<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPackageService extends Model
{
    protected $fillable = ['customer_package_id', 'service_id', 'quantity', 'price'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function customerPackage()
    {
        return $this->belongsTo(SalePackage::class);
    }
}
