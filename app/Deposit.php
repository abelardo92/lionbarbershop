<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = [
        'period', 'start', 'end', 'employee_id', 'amount'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}