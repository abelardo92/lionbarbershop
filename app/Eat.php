<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Eat extends Model
{
    protected $fillable = [
        'subsidiary_id', 'employee_id', 'date', 'start', 'end', 'user_id', 'cashier_id',
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cashier()
    {
        return $this->belongsTo(Employee::class, 'cashier_id');
    }

    public function scopeDate($query, $date)
    {
        return $query->where('date', $date);
    }

    public function scopeToday($query)
    {
        return $query->where('date', Carbon::now()->format('Y-m-d'));
    }
}
