<?php

namespace App;

use App\SaleService;
use App\Surveys\Survey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    protected $fillable = [
        'key', 'name', 'short_name', 'entry_date', 'rfc', 'curp', 'nss', 'address', 'marital_status', 'gender', 'salary', 
        'commission', 'imss_salary', 'infonavit_number', 'email', 'phone', 'subsidiary_id', 'has_rest', 'rest_minutes', 
        'job', 'code', 'password', 'active', 'inactived_at', 'commission_two', 'commission_number', 'account_number', 
        'monto_infonavit', 'no_banco', 'no_tarjeta', 'has_excellence', 'imss_date', 'bank_name', 'employee_code', 'user_id', 
        'birthday', 'fingerprint'
    ];

    const DOCUMENTS = [
        'id', 'curp', 'rfc', 'imss', 'rd', 'an', 'cap', 'cms',
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    public function getFingerprintAttribute($value)
    {
        return base64_encode($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function exchanges()
    {
        return $this->hasMany(ScheduleExchange::class);
    }

    public function currentExchanges()
    {
        /*return $this->exchanges()->orWhere('employee2_id',$this->id)->where('date','>=',date('Y-m-d'));*/
        $id = $this->id;
        $date = date('Y-m-d');
        //return $this->exchanges()->with(['exchanges'])
        return ScheduleExchange::with(['exchanges','employee','employee2'])
        ->where(function ($query) use ($id) {
            $query->where('employee_id',$id)
            ->orWhere('employee2_id',$id);
        })
        ->orWhereHas('exchanges', function ($q) use ($id) {
            $q->where('employee_id', $id)->orWhere('employee2_id', $id);
        })
        ->where('date','>=',$date);
    }

    public function disabilities()
    {
        return $this->hasMany(Disability::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function lastSale()
    {
        return $this->sales()->where('date', date('Y-m-d'))->finished()->notCanceled()->get()->last();
        /*return $this->sales()->where('date', $date)->where(function ($query) {
        $query->where('status','paid')
        ->orWhere('status','finalizado');
        })->notCanceled();*/
    }

    public function salesByMovement($cash_register_movement)
    {
        return Sale::where('cash_register_id', $cash_register_movement)
        ->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function weeklySales($date)
    {
        return Sale::where('date', $date)->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function weeklySalesBySubsidiary($date, $subsidiary_id)
    {
        return Sale::where('date', $date)->where('subsidiary_id', $subsidiary_id)->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function salesByToday()
    {
        return Sale::with(['services'])->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->today()->finished()->notCanceled();
    }

    public function weeklySalesRange($start,$end)
    {
        return Sale::with(['services'])->whereBetween('date', [$start, $end])->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function repairs()
    {
        return $this->hasMany(Sale::class, 'repaired_by');
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

    public function todayAttendances()
    {
        return $this->hasMany(Attendance::class)->today();
    }

    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    public function manuals()
    {
        return $this->hasMany(Manual::class, 'cashier_id');
    }

    public function diaries()
    {
        return $this->hasMany(Diary::class);
    }

    public function percepciones()
    {
        return $this->hasMany(Percepcione::class);
    }

    public function deducciones()
    {
        return $this->hasMany(Deduccione::class);
    }

    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }

    public function scopeWorking($query) {
        return $query->whereDoesntHave('attendances', function($q) {
            $q->where('type','salida');
        });
    }

    public function isEating() {
        $entradaDescanso = $this->todayAttendances->where('type','entrada descanso')->first();
        $salidaDescanso = $this->todayAttendances->where('type','salida descanso')->first();
        return !$entradaDescanso && $salidaDescanso;
    }

    public function scopeNoInSubsidiary($query)
    {
        return $query->whereNull('subsidiary_id');
    }

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }

    public function scopeBarbers($query)
    {
        return $query->whereJob('Barbero');
    }

    public function scopeBarberOrInCharge($query) {
        return $query->where(function ($q) {
            $q->where('job', 'Barbero')
            ->orWhere('job', 'Encargado');
        });
    }

    public function createAttendance($subsidiary_id, $type)
    {
        $now = Carbon::now();
        return $this->attendances()->create([
            'type' => $type,
            'subsidiary_id' => $subsidiary_id,
            'date' => $now->format('Y-m-d'),
            'time' => $now->format('H:i:s'),
        ]);
    }

    public function getSalaryByDateAndSubsidiary($date, $subsidiary_id, $has_punctuality)
    {
        $sales = $this->weeklySalesBySubsidiary($date, $subsidiary_id)->get();
        return $this->getSalary($sales, $has_punctuality);
    }

    public function getSalaryByDate($date, $has_punctuality) 
    {
        $sales = $this->weeklySales($date)->get();
        return $this->getSalary($sales, $has_punctuality);
    }

    public function getCommissionByDate($sales) {
        $salary = 0;
        $sale_ids = $sales->pluck('id');
        $services = SaleService::with('sale')->whereIn('sale_id', $sale_ids)->get();
        foreach ($services as $service) {
            $salary += $service->getCommission();
        }
        return $salary;
    }

    public function getSalary($sales, $has_punctuality) 
    {
        $salary = $this->getCommissionByDate($sales);
        // if(!$has_punctuality) {
        //     return $this->salary;
        // } else {
        //     if($salary < $this->salary)
        //         return $this->salary;
        // }
        // dd($salary);
        if($salary < $this->salary) {
            return $this->salary;
        }
        return $salary;
    }

    public function getServicesNumber($start, $end)
    {
        $sale_ids = $this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get()->pluck('id');
        return SaleService::whereIn('sale_id', $sale_ids)->get()->count();
    }

    public function getAttendedTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get() as $sale) {
            $time[] = $sale->attended_at->diffInMinutes($sale->finished_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function getWaitTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get() as $sale) {
            $time[] = $sale->created_at->diffInMinutes($sale->attended_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function getServicesReport()
    {
        $sales = $this->salesByToday()->get();
        $sale_ids = $sales->pluck('id');
        $kids_promotion_qty = 0;
        foreach ($sales as $sale) {
            $contains_kids = false;
            $contains_haircut = false;
            if($payments = $sale->services->where('service_id', 1)->first()) {
                $contains_haircut = true;
            }
            if($payments = $sale->services->where('service_id', 16)->first()) {
                $contains_kids = true;
            }
            if($contains_kids && $contains_haircut) {
                $kids_promotion_qty += 1;
            }
        }
        $services = SaleService::select(DB::raw('*, sum(qty) as services_count'))->whereIn('sale_id', $sale_ids)->groupBy('service_id')->get();
        foreach ($services as &$service) {
            if($service->service_id == 1) {
                $service->services_count -= $kids_promotion_qty;
            }
        }
        return $services;
    }

    public function fingers()
    {
        return $this->hasMany(Finger::class, 'user_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

    public function getName()
    {
        return $this->short_name;
    }

    public function correctDocument($doc)
    {
        foreach ($this->getDoctos() as $document) {
            if ($doc == $document) {
                return true;
            }
        }
        return false;
    }

    public function getDoctos()
    {
        $doctos = [];
        foreach (static::DOCUMENTS as $document) {
            $doctos[] = strtolower(trim($this->short_name)) . "_{$document}";
        }

        return $doctos;
    }

    public function hasDocument($doc)
    {
        foreach ($this->images->sortByDesc('id') as $document) {
            $name_arr = explode('.', $document->name);

            if (strtolower(trim($this->short_name)) . "_{$doc}" == $name_arr[0]) {
                return $document;
            }
        }

        return false;
    }

    public function getProfileimage()
    {
        foreach ($this->images as $document) {
            $name_arr = explode('.', $document->name);
            if (!in_array($name_arr[0], static::DOCUMENTS) || $name_arr[0] == 'foto') {
                return $document;
            }
        }

        return false;
    }

}
