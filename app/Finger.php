<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finger extends Model
{
    protected $fillable = [
        'data',
        'user_id'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'user_id');
    }
}
