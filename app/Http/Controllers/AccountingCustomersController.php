<?php namespace App\Http\Controllers;

use App\AccountingCustomer;
use App\AccountingCustomerEmployee;
use App\CustomFolder;
use App\CustomSubFolder;
use App\Image;
use App\TaxRegime;
use App\Traits\ImageableTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountingCustomersController extends Controller
{
    use ImageableTrait;

    public function index()
    {
        $user = Auth::user();
        // if($user->isA('auxiliar-contable')) {
        //     $customers = AccountingCustomer::where('show_to_auxiliar', true)->active()->get();
        // } else {
            $customers = AccountingCustomer::active()->get();
        //}
        return view('admin.accounting_customers.index', compact('customers', 'user'));
    }

    public function create()
    {
        $tax_regimes = TaxRegime::get();
        return view('admin.accounting_customers.create', compact('tax_regimes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $customer = AccountingCustomer::create($data);
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/registro_patronal", "registro_patronal");
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/identificacion", "identificacion");
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/acta_nacimiento", "acta_nacimiento");
        $request->session()->flash('success', 'Cliente creado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function storeFolder(Request $request)
    {
        $data = $request->all();
        $tab_name = $data['folder_name'];
        $name = strtolower(str_replace(" ", "_", $tab_name));
        $folder = CustomFolder::create([
            'name' => $name,
            'tab_name' => $tab_name,
        ]);
        $request->session()->flash('success', "Carpeta \"{$folder->name}\" creada con éxito.");
        return redirect()->back();
    }

    public function storeSubFolder(Request $request)
    {
        $data = $request->all();
        $folder_id = $data['folder_id'];
        $tab_name = $data['folder_name'];
        $name = strtolower(str_replace(" ", "_", $tab_name));
        $subfolder = CustomSubFolder::create([
            'name' => $name,
            'tab_name' => $tab_name,
            'custom_folder_id' => $folder_id,
        ]);
        $request->session()->flash('success', "Sub Carpeta \"{$subfolder->name}\" creada con éxito.");
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $customer = AccountingCustomer::with(['images', 'employees'])->find($id);
        $tax_regimes = TaxRegime::get();
        $custom_folders = CustomFolder::active()->get();

        $months = [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        ];

        $currentMonth = date("m");
        $currentYear = date("Y");

        return view('admin.accounting_customers.edit', compact('customer', 'tax_regimes', 'months', 'currentMonth', 'currentYear', 'custom_folders'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $customer->update($data);

        $file_registro = $customer->fileRegistroPatronal();
        $file_acta = $customer->fileActaNacimiento();

        //* Registro patronal
        if($request->hasFile("registro_patronal") && !empty($file_registro)) {
            $file_registro->deleteWithFile();
        }
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/registro_patronal", "registro_patronal");

        //* Acta de nacimiento
        if($request->hasFile("acta_nacimiento") && !empty($file_acta)) {
            $file_acta->deleteWithFile();
        }
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/acta_nacimiento", "acta_nacimiento");

        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/address", "address_file");
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/rfc", "rfc_file");
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/curp", "curp_file");
        $this->CreateFiles($customer, $request, "accountingCustomers/{$customer->id}/identificacion", "identificacion");

        $request->session()->flash('success', 'Cliente actualizado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateConstancia(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['constancia_year'];
        $month = $data['constancia_month'];

        $file_constancia = $customer->fileConstancia($year, $month);
        if($request->hasFile("constancia") && !empty($file_constancia)) {
            $file_constancia->deleteWithFile();
        }

        $path = "accountingCustomers/{$customer->id}/constancias/$year/$month";
        $this->CreateFiles($customer, $request, $path, "constancia", $year, $month);

        $request->session()->flash('success', 'Constancia guardada con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);

    }

    public function updateCustomFolderFile(Request $request, $id, $custom_folder_id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $folder = CustomFolder::with('subFolders')->active()->find($custom_folder_id);
        $folder_name = $folder->name;

        $year = $data["folder_{$folder_name}_year"];
        $month = $data["folder_{$folder_name}_month"];
        $subfolder_id = intval($data["subfolder_id"]);

        if($subfolder_id > 0) {
            $subfolder = CustomSubFolder::find($subfolder_id);
            $subfolder_name = $subfolder->name;
            $path = "accountingCustomers/{$customer->id}/{$folder_name}_{$subfolder->name}/$year/$month";
        } else {
            $subfolder_name = $folder_name;
            $path = "accountingCustomers/{$customer->id}/$folder_name/$year/$month";
        }

        $this->CreateFiles($customer, $request, $path, "file_$folder_name", $year, $month);
        $request->session()->flash('success', "Archivo de $subfolder_name guardado con éxito.");
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateExpediente(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['expediente_year'];
        $month = $data['expediente_month'];

        $path = "accountingCustomers/{$customer->id}/expedientes/$year/$month";
        $this->CreateFiles($customer, $request, $path, "expediente", $year, $month);

        $request->session()->flash('success', 'Expediente guardado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateExpedienteClinico(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['expediente_clinico_year'];
        $month = $data['expediente_clinico_month'];

        $path = "accountingCustomers/{$customer->id}/exp_clinicos/$year/$month";
        $this->CreateFiles($customer, $request, $path, "expediente_clinico", $year, $month);

        $request->session()->flash('success', 'Expediente clinico guardado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateActa(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['acta_year'];
        $month = $data['acta_month'];

        $path = "accountingCustomers/{$customer->id}/actas/$year/$month";
        $this->CreateFiles($customer, $request, $path, "acta", $year, $month);

        $request->session()->flash('success', 'Acta guardada con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateDiot(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['diot_year'];
        $month = $data['diot_month'];

        $file_diot = $customer->fileDiot($year, $month);
        // if($request->hasFile("diot") && !empty($file_diot)) {
        //     $file_diot->deleteWithFile();
        // }

        $path = "accountingCustomers/{$customer->id}/diots/$year/$month";
        $this->CreateFiles($customer, $request, $path, "diot", $year, $month);

        $request->session()->flash('success', 'Diot guardado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);

    }

    public function updateSua(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['sua_year'];
        $month = $data['sua_month'];
        $type = $data['sua_type'];

        // $file_sua = $customer->fileSua($year, $month, $type);
        // if($request->hasFile("sua") && !empty($file_sua)) {
        //     $file_sua->deleteWithFile();
        // }

        $path = "accountingCustomers/{$customer->id}/suas/$year/$month/$type";
        $this->CreateFiles($customer, $request, $path, "sua", $year, $month, $type);

        $request->session()->flash('success', 'Sua/Imss guardado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);

    }

    public function updateMonthlyDeclaration(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['monthly_declaration_year'];
        $month = $data['monthly_declaration_month'];
        $type = $data['monthly_declaration_type'];

        // $file_monthly_declaration = $customer->fileMonthlyDeclaration($year, $month, $type);
        // if($request->hasFile("monthly_declaration") && !empty($file_monthly_declaration)) {
        //     $file_monthly_declaration->deleteWithFile();
        // }

        $path = "accountingCustomers/{$customer->id}/monthly_declarations/$year/$month/$type";
        $this->CreateFiles($customer, $request, $path, "monthly_declaration", $year, $month, $type);

        $request->session()->flash('success', 'Declaracion mensual guardada con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);

    }

    public function updateYearlyDeclaration(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $year = $data['yearly_declaration_year'];
        $date = $data['yearly_declaration_date'];
        $type = $data['yearly_declaration_type'];

        // $file_yearly_declaration = $customer->fileYearlyDeclaration($year, $date, $type);
        // if($request->hasFile("yearly_declaration") && !empty($file_yearly_declaration)) {
        //     $file_yearly_declaration->deleteWithFile();
        // }

        $path = "accountingCustomers/{$customer->id}/yearly_declarations/$year/$date/$type";
        $this->CreateFiles($customer, $request, $path, "yearly_declaration", $year, null, $type, $date);

        $request->session()->flash('success', 'Declaracion anual guardada con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function updateEmployee(Request $request, $id)
    {
        $data = $request->all();
        $customer = AccountingCustomer::find($id);
        $employee_id = $data['expediente_employee_id'];
        $name = $data['expediente_employee_name'];
        $date = $data['expediente_employee_date'];
        $email = $data['expediente_employee_email'];

        // $file1 = $data['expediente_employee_file1'];
        $date1 = $data['expediente_employee_file1_date'];
        // $file2 = $data['expediente_employee_file2'];
        $date2 = $data['expediente_employee_file2_date'];
        // $file3 = $data['expediente_employee_file3'];
        $date3 = $data['expediente_employee_file3_date'];
        // $file4 = $data['expediente_employee_file4'];
        $date4 = $data['expediente_employee_file4_date'];

        $employee = new AccountingCustomerEmployee();

        if(!empty($employee_id)) {
            $employee = AccountingCustomerEmployee::find($employee_id);
            $message = "Empleado actualizado con éxito.";
        } else {
            $employee = new AccountingCustomerEmployee();
            $message = "Empleado guardado con éxito.";
        }
        $employee->accounting_customer_id = $customer->id;
        $employee->name = $name;
        $employee->date = $date;
        $employee->email = $email;
        $employee->save();

        $path = "accountingCustomers/{$customer->id}/expedientes/empleados/{$employee->id}";

        $this->CreateFiles($employee, $request, $path, "expediente_employee_file1", null, null, null, $date1);
        $this->CreateFiles($employee, $request, $path, "expediente_employee_file2", null, null, null, $date2);
        $this->CreateFiles($employee, $request, $path, "expediente_employee_file3", null, null, null, $date3);
        $this->CreateFiles($employee, $request, $path, "expediente_employee_file4", null, null, null, $date4);

        $request->session()->flash('success', $message);
        return redirect()->route('accounting_customers.edit', [$customer]);

    }

    public function destroyImage(Request $request, $customer_id, $image_id)
    {
        $customer = AccountingCustomer::find($customer_id);
        $image = Image::find($image_id);
        $image->deleteWithFile();

        $request->session()->flash('success', 'Archivo eliminado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function destroyImage2(Request $request, $customer_id)
    {
        $customer = AccountingCustomer::find($customer_id);
        $image = Image::find($request->file_id);
        $image->deleteWithFile();

        $request->session()->flash('success', 'Archivo eliminado con éxito.');
        return redirect()->route('accounting_customers.edit', [$customer]);
    }

    public function destroy(Request $request, $id)
    {
        $customer = AccountingCustomer::find($id);
        $customer->is_active = 0;
        $customer->save();

        $request->session()->flash('success', 'Cliente eliminado con éxito.');
        return redirect()->route('accounting_customers.index');
    }
}
