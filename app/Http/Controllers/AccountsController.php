<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    public function index(Request $request)
    {
        $accounts = Account::all();
        return view('admin.accounts.index', compact('accounts'));
    }

    public function create(Request $request)
    {
        return view('admin.accounts.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $account = Account::create($data);

        $request->session()->flash('success', 'Cuenta dada de alta con exito.');

        return redirect()->route('accounts.edit', $account->id);
    }

    public function edit($id)
    {
        $account = Account::find($id);
        return view('admin.accounts.edit', compact('account'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $account = Account::find($id);
        $account->update($data);

        $request->session()->flash('success', 'Cuenta actualizado con exito.');
        return back();
    }

}
