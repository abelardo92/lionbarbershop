<?php
namespace App\Http\Controllers;

use App\Attendance;
use App\Device;
use App\Employee;
use App\Finger;
use App\Subsidiary;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function getUrl(Request $request, $subsidiary_code)
    {
        if (!$employee = Employee::whereCode(trim($request->code))->first()) {
            return $this->responseNotEmployee();
        }
        $url = route('employees.reg.validate');

        $registerUrl = base64_encode("{$url}?user_id={$employee->id}&subsidiary_code={$subsidiary_code}");

        return $this->makeResponse([
            'url' => "finspot:FingerspotVer;{$registerUrl}",
        ], 202);
    }

    public function storeDevice(Request $request, $subsidiary_code, $employee_id)
    {
        if(!$employee = Employee::find($employee_id)) {
            return $this->responseNotEmployee(200);
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if (!$schedule = $this->getSchedule($subsidiary, $employee->id)) {
            return $this->responseNotSchedule(200);
        }

        $current_attendances = $employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->count();

        // if (!$schedule->can_check && !$schedule->canCheck() && $current_attendances == 0) {
        //     return $this->responseCanCheck($schedule, $employee);
        // }

        if ($schedule->turn->is_rest) {
            return $this->responseRestDay(200);
        }
        
        if ($current_attendances == 0) {
            $attendance = $employee->createAttendance($subsidiary->id, 'entrada');
            return $this->responseFirstCheck($employee, $attendance, $schedule);
        }

        if ($schedule->has_rest) {
            if ($current_attendances == 1) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida descanso');
                return $this->responseSecondCheck($employee, $attendance, $schedule);
            }

            if ($current_attendances == 2) {
                $attendance = $employee->createAttendance($subsidiary->id, 'entrada descanso');
                return $this->responseThirdCheck($employee, $attendance, $schedule);
            }

            if ($current_attendances == 3) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida');
                return $this->responseLastCheck($employee, $attendance, $schedule);
            }
        } else {
            if ($current_attendances == 1) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida');
                return $this->responseLastCheck($employee, $attendance, $schedule);
            }
        }
        // dd($employee);

        // $message = 'Registro inválido.';
        // return $this->makeResponse(compact('message', 'employee'), 200);

        return response()->json([
            'message' => 'Registro invalido',
            'employee' => $employee,
        ]);

    }

    public function store(Request $request, $subsidiary_code)
    {
        if (!$employee = Employee::with('image')->whereCode(trim($request->code))->first()) {
            return $this->responseNotEmployee();
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if (!$schedule = $this->getSchedule($subsidiary, $employee->id)) {
            return $this->responseNotSchedule();
        }

        $current_attendances = $employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->count();

        // if (!$schedule->can_check && !$schedule->canCheck() && $current_attendances == 0) {
        //     return $this->responseCanCheck($schedule, $employee);
        // }

        if ($schedule->turn->is_rest) {
            return $this->responseRestDay();
        }

        if (!$this->canAfternoonCheck($schedule)) {
            $message = 'no puedes checar antes de las 3:30';
            return $this->makeResponse(compact('message'), 403);
        }
        
        if ($current_attendances == 0) {
            $attendance = $employee->createAttendance($subsidiary->id, 'entrada');
            return $this->responseFirstCheck($employee, $attendance, $schedule);
        }

        if ($schedule->has_rest) {
            if ($current_attendances == 1) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida descanso');
                return $this->responseSecondCheck($employee, $attendance, $schedule);
            }

            if ($current_attendances == 2) {
                $attendance = $employee->createAttendance($subsidiary->id, 'entrada descanso');
                return $this->responseThirdCheck($employee, $attendance, $schedule);
            }

            if ($current_attendances == 3) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida');
                return $this->responseLastCheck($employee, $attendance, $schedule);
            }
        }

        if (!$schedule->has_rest) {
            if ($current_attendances == 1) {
                $attendance = $employee->createAttendance($subsidiary->id, 'salida');
                return $this->responseLastCheck($employee, $attendance, $schedule);
            }
        }

        return response()->json([
            'message' => 'Registro invalido',
            'employee' => $employee,
        ]);
    }

    protected function getSchedule($subsidiary, $employee_id)
    {
        $time = Carbon::now()->format('H:i:s');
        if ($time < '15:00:00') {
            $turns_id = Turn::whereIn('identifier', [1, 3])
                ->get()->pluck('id');
        } else {
            $turns_id = Turn::where('identifier', [1, 3])
                ->get()->pluck('id');
        }

        return $subsidiary->schedules()->with(['employee', 'turn'])
            ->where('employee_id', $employee_id)
            ->where('date', Carbon::now()->format('Y-m-d'))
        // ->whereIn('turn_id', $turns_id)
            ->first();
    }

    protected function makeResponse($data = [], $code = 200)
    {
        return response()->json($data, $code);
    }

    protected function responseNotEmployee($code = 404)
    {
        $message = 'No encontramos ni un empleado con esta clave.';
        return $this->makeResponse(compact('message'), $code);
    }

    protected function responseNotSchedule($code = 404)
    {
        $message = 'No tienes horario para el dia de hoy.';
        return $this->makeResponse(compact('message'), $code);
    }

    protected function canAfternoonCheck($schedule)
    {
        if ($schedule->turn->identifier != 2) {
            return true;
        }
        return Carbon::now()->format('H:i:s') > '15:30:00';
    }

    protected function responseCanCheck($schedule, $employee, $code = 403)
    {
        if($employee->job == "Barbero") {
            $hora = $schedule->turn->barber_start;
        } else {
            $hora = $schedule->turn->start;
        }
        $message = 'Se ha bloqueado tu entrada, te recuerdo que el tiempo de tolerancia es de 15 min.';
        $min = Carbon::parse($hora)->diffInMinutes(Carbon::now());
        $message .= " Llegaste {$min} minutos tarde";

        return $this->makeResponse(compact('message', 'employee', $code));
    }

    protected function responseRestDay($code = 503)
    {
        $message = 'Hoy es tu dia de descanso.';
        return $this->makeResponse(compact('message'), $code);
    }

    protected function responseFirstCheck($employee, $attendance, $schedule)
    {
        if($employee->job == "Barbero") {
            $hora = $schedule->turn->barber_start;
        } else {
            $hora = $schedule->turn->start;
        }

        $message = 'Gracias por llegar a tiempo.';
        if ($attendance->time > $hora) {
            $min = Carbon::parse($attendance->time)->diffInMinutes(Carbon::parse($hora));
            $message = "Gracias por llegar, llegaste {$min} minutos tarde";
        };

        return $this->makeResponse(compact('message', 'employee'));
    }

    protected function responseSecondCheck($employee, $attendance, $schedule)
    {
        $hora = $attendance->created_at->addMinutes($schedule->rest_minutes)->format('h:i a');
        $message = "Te recuerdo que debes volver a mas tardar a las {$hora}";
        return $this->makeResponse(compact('message', 'employee'));
    }

    protected function responseThirdCheck($employee, $attendance, $schedule)
    {
        $message = 'Gracias por regresar a tiempo.';
        $second_check_attendance = Attendance::where('date',date("Y-m-d"))->where('type','salida descanso')
        ->where('employee_id',$employee->id)->first();
        if($second_check_attendance != null){
            $time = $second_check_attendance->created_at->addMinutes($schedule->rest_minutes)->format('h:i a');
            $currentTime = $attendance->created_at->format('h:i a'); 
            if($currentTime > $time){
                $min = Carbon::parse($time)->diffInMinutes(Carbon::parse($currentTime));
                $message = "Gracias por llegar, llegaste {$min} minutos tarde";
            }
        }
        return $this->makeResponse(compact('message', 'employee'));
    }

    protected function responseLastCheck($employee, $attendance, $schedule)
    {
        $hora = $schedule->turn->end;
        $message = 'Gracias por colaborar el dia de hoy, ¡Que descanses!.';
        if ($attendance->time < $hora) {
            $min = Carbon::parse($hora)->diffInMinutes(Carbon::parse($attendance->time));
            $hora = Carbon::parse($hora)->format('h:i a');
            $message .= "Te estas yendo {$min} minutos antes, tu hora de salida es {$hora}";
        };
        $url = route('attendances.print', [
            $schedule->subsidiary->key, $attendance->id,
        ]);
        return $this->makeResponse(compact('message', 'employee', 'url'));
    }

    function print(Request $request, $subsidiary_code, $id) {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $attendance = Attendance::find($id);

        $employee = $attendance->employee;

        return view('admin.employees.attendance_ticket', compact('employee', 'attendance', 'subsidiary'));
    }

    public function getLink($subsidiary_code, $id)
    {
        if (!$employee = Employee::whereCode(trim($id))->first()) {
            return $this->responseNotEmployee();
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        $attendance = $employee->attendances()->today()->get()->last();
        if ($attendance->type == 'salida') {
            $url = route('attendances.print', [
                $subsidiary_code, $attendance->id,
            ]);
            return response()->json([
                'url' => $url,
            ]);

        }

        return response()->json([
            'message' => 'No podemos imprimirlo porque no ha checado salida.',
        ], 422);
    }
}
