<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\CashRegister;
use App\Employee;
use App\Sale;
use App\Subsidiary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogsController extends Controller
{
    public function employees(Request $request)
    {
        if ($request->has('status')) {
            $status = $request->status;
            if ($status == 'active') {
                $employees = Employee::whereActive(true)->get();
            }
            if ($status == 'inactive') {
                $employees = Employee::whereActive(false)->get();
            }
            if ($status == 'all') {
                $employees = Employee::all();
            }
        }
        else {
            $status = 'all';
            $employees = Employee::all();
        }

        return view('admin.catalogs.employees', compact('employees', 'status'));
    }
}
