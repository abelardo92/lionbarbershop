<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{
    use SubDomainHelper;

    public function index(Request $request)
    {
        $data = $request->all();
        $customers = Customer::active();
        $filter = '';
        if($request->has('filter')) {
            $filter = $data['filter'];
            $customers = $customers->where('name', 'LIKE', '%'.$filter.'%'); 
        }

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $customers = $customers->laundry()->paginate(10);
                return view('admin.customers.index', compact('customers'));
            }
        }
        $customers = $customers->notLaundry()->paginate(10);
        return view('admin.customers.index', compact('customers', 'filter'));
    }

    public function byFilter(Request $request)
    {
        $data = $request->all();
        $customers = Customer::active();
        $filter = '';
        if($request->has('filter') && strlen($request->filter) >= 3) {

            $filter = $data['filter'];

            //$customers = $customers->where('name', 'LIKE', '%'.$filter.'%'); 
            $customers = $customers->where(function($query) use ($filter){
                return $query->where('name', 'LIKE', '%'.$filter.'%')
                ->orWhere('phone', 'LIKE', '%'.$filter.'%')
                ->orWhere('wallet_number', 'LIKE', '%'.$filter.'%');
            });


            if ($subsidiary_code = $this->hasSubdomain()) {
                $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
                if($subsidiary->is_laundry){
                    $customers = $customers->laundry();//->paginate(10);
                    return response()->json($customers->get());
                }
            }

            $customers = $customers->notLaundry();//->paginate(10);
            return response()->json($customers->get());
        }

        return response()->json([]);
    }

    public function create(Request $request)
    {
        $redirect = false;
        if ($request->redirect) {
            $redirect = $request->redirect;
        }
        return view('admin.customers.create', compact('redirect'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $data['is_laundry'] = true;
            }
            if ($cash_register = $subsidiary->cashRegisters()->with('movements')->open()->today()->first()) {
                $data['created_by_class'] = 'Employee';
                $data['created_by'] = $cash_register->employee->id;
            } else {
                $data['created_by_class'] = 'User';
                $data['created_by'] = $request->user()->id;
            }

        } else {
            $data['created_by_class'] = 'User';
            $data['created_by'] = $request->user()->id;
        }

        if($data['birthday'] == "" || $data['birthday'] == "0000-00-00") $data['birthday'] = null; 

        if($customerr = Customer::where('wallet_number', $request->wallet_number)->get()->count() > 0) {
            $request->session()->flash('error', 'El número de monedero ya esta en uso, escriba uno diferente.');  
            return back(); 
        }

        $customer = Customer::create($data);
        $customer->findAndUpdateParentReferral();

        $request->session()->flash('success', 'Cliente dado de alta con exito.');

        if (isset($data['redirect'])) {
            return redirect()->to("{$data['redirect']}?customer_id={$customer->id}");
        }
        return redirect()->route('customers.edit', $customer->id);
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        $user = Auth::user();
        return view('admin.customers.edit', compact('customer', 'user'));
    }

    public function show($id)
    {
        return Customer::find($id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($data['birthday'] == "" || $data['birthday'] == "0000-00-00") $data['birthday'] = null; 

        if($customerr = Customer::where('wallet_number', $request->wallet_number)->where('id', '!=', $id)->get()->count() > 0) {
            $request->session()->flash('error', 'El número de monedero ya esta en uso, escriba uno diferente.');  
            return back(); 
        }

        $customer = Customer::find($id);
        $customer->update($data);
        $customer->findAndUpdateParentReferral();

        $request->session()->flash('success', 'Cliente actualizado con exito.');
        return back();
    }

    protected function createReferrals($customer)
    {
        foreach (range(1, 10) as $val) {
            $customer->referrals()->create([
                'wallet_number' => mt_rand(10000000, 99999999),
            ]);
        }
    }

    public function replaceWallet(Request $request, $id)
    {
        $data = $request->all();
        $customer = Customer::find($id);
        $customer->update($data);
        $customer->findAndUpdateParentReferral();

        $request->session()->flash('success', 'Monedero reemplazado con exito.');
        return response()->json([
            'success' => true,
        ]);
    }

    public function verifyBirthday($id)
    {
        $customer = Customer::find($id);
        $birthday = Carbon::parse($customer->birthday)->format('d-m');
        $now = Carbon::now()->format('d-m');
        if ($customer->birthday != null && $customer->birthday != "0000-00-00" && $now == $birthday) {
            $sales = $customer->sales()->today()->notCanceled()->get();

            return response()->json([
                'success' => true,
                'able_for_discount' => $sales->count() > 0 ? false : true,
                'customer' => $customer,
                'message' => $sales->count() > 0 ? "Ya aplicamos tu descuento en {$sales->first()->subsidiary->name}" : '',
            ]);
        }

        return response()->json([
            'success' => false,
        ]);
    }

    public function deactivate(Request $request, $customer_id)
    {
        $customer = Customer::find($customer_id);
        $customer->is_active = false;
        $customer->save();
        $request->session()->flash('success', 'Cliente eliminado con éxito.');
        return back();
    }
}
