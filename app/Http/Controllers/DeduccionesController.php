<?php

namespace App\Http\Controllers;

use App\Deduccione;
use App\Paysheet;
use Illuminate\Http\Request;

class DeduccionesController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        $deduccione = Deduccione::create($data);
        foreach (Paysheet::where('employee_id', $data['employee_id'])->get() as $paysheet) {
            $total= Deduccione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otras = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Deduccion dada de alta con exito.');
        return redirect()->back();
    }

    public function destroy(Request $request, $id)
    {
        $deduccione = Deduccione::find($id);
        $deduccione->delete();
        foreach (Paysheet::where('employee_id', $deduccione->employee_id)->get() as $paysheet) {
            $total= Deduccione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otras = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Deduccion eliminada con exito');
        return redirect()->back();
    }

}
