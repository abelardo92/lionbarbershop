<?php
namespace App\Http\Controllers;

use App\Eat;
use App\Employee;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EatsController extends Controller
{
    use SubDomainHelper;

    public function index($subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $start = Carbon::now()->format('Y-m-d');
        if (request()->has('start')) {
            $start = request()->start;
        }
        $eats = $subsidiary->eats()->date($start)->get();

        return view('admin.eats.index', compact('eats', 'start'));
    }

    public function create(Request $request, $subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $turns = Turn::where('identifier', 3)->get()->pluck('id');
        $barbers = $subsidiary->schedules()
            ->whereIn('turn_id', $turns)
            ->today()
            ->groupBy('employee_id')
            ->get()
            ->where('employee.job', 'Barbero')
            ->map(function ($key) {
                return $key->employee;
            });

        return view('admin.eats.create', compact('subsidiary', 'barbers'));
    }

    public function store(Request $request, $subsidiary_code)
    {
        $data = $request->all();
        $data['date'] = Carbon::now();
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if ($request->user()->isA('super-admin')) {
            $data['user_id'] = $request->user()->id;
        } else {
            $subsidiary_code = $this->hasSubdomain();
            $subsi = Subsidiary::whereKey($subsidiary_code)->first();
            if ($subsi) {
                if ($cash_register = $subsi->cashRegisters()->open()->today()->first()) {
                    $data['cashier_id'] = $cash_register->employee->id;
                } else {
                    $data['user_id'] = $request->user()->id;
                }
            } else {
                $data['user_id'] = $request->user()->id;
            }
        }
        $subsidiary->eats()->create($data);

        $request->session()->flash('success', 'Comida dada de alta con exito');
        return redirect()->route('eats.index', $subsidiary->key);
    }

    public function edit($subsidiary_code, $id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $turns = Turn::where('identifier', 3)->get()->pluck('id');
        $barbers = $subsidiary->schedules()
            ->whereIn('turn_id', $turns)
            ->today()
            ->groupBy('employee_id')
            ->get()
            ->where('employee.job', 'Barbero')
            ->map(function ($key) {
                return $key->employee;
            });
        $eat = Eat::find($id);

        return view('admin.eats.edit', compact('subsidiary', 'barbers', 'eat'));
    }

    public function update(Request $request, $subsidiary_code, $id)
    {
        $data = $request->all();

        $eat = Eat::find($id);
        $eat->update($data);

        $request->session()->flash('success', 'Comida actualizada con exito');
        return redirect()->route('eats.index', $subsidiary_code);
    }

    public function destroy(Request $request, $subsidiary_code, $id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $eat = Eat::find($id);
        $eat->delete();

        $request->session()->flash('success', 'Comida eliminada con exito.');
        return back();
    }

}
