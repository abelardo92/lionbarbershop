<?php

namespace App\Http\Controllers;

use App\Configuration;
use App\Employee;
use App\User;
use App\Job;
use App\Manual;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use App\Turn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\Database\Role;

class EmployeesController extends Controller
{
    use ImageableTrait;

    public function index(Request $request)
    {
        $employees = Employee::where('active', true)->get();
        $inactive_employees = Employee::where('active', false)->get();
        return view('admin.employees.index', compact('employees', 'inactive_employees'));
    }

    public function create()
    {
        $roles = Role::all();
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $jobs = Job::all();
        return view('admin.employees.create', compact('roles', 'subsidiaries', 'jobs'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
              
        if($employee = Employee::where('key', $data['key'])->get()->first()) {
            $request->session()->flash('error', 'La clave seleccionada ya existe, escriba una clave diferente.');
            return redirect()->back();
        }

        if($user = User::where('email', $data['email'])->get()->first()) {
            $request->session()->flash('error', 'El correo escrito ya existe, escriba un correo diferente.');
            return redirect()->back();
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['nss']),
            'subsidiary_id' => $data['subsidiary_id'],
            'api_token' => str_random(60),
        ]);
        $user->assign('employee');
        $data['user_id'] = $user->id;
        if($data['imss_date'] == "" || $data['imss_date'] == "0000-00-00") $data['imss_date'] = null;
        if($data['birthday'] == "" || $data['birthday'] == "0000-00-00") $data['birthday'] = null;
        $data['inactive_at'] = null;
        
        $employee = Employee::create($data);
        $this->CreateFile($employee, $request, "avatars/{$employee->id}");
        
        $request->session()->flash('success', 'Empleado dado de alta con exito.');
        return redirect()->route('employees.edit', $employee->id);
    }

    public function edit(Request $request, $id)
    {
        $employee = Employee::find($id);
        $roles = Role::all();
        $role = Role::where('name', 'employee')->first();
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $turns = Turn::all();
        $jobs = Job::all();
        $url = route('employees.reg.register');
        $registerUrl = base64_encode("{$url}?user_id={$employee->id}");

        return view('admin.employees.edit', compact('employee', 'roles', 'subsidiaries', 'turns', 'jobs', 'registerUrl', 'role'));
    }

    public function show(Request $request)
    {
        if ($employee = Employee::whereCode($request->code)->first()) {
            $schedules = $employee->schedules()->with(['subsidiary', 'turn']);
            if ($request->has('more')) {
                $schedules = $schedules->nextWeek();
            } else {
                $schedules = $schedules->thisWeek();
            }
            $schedules = $schedules->get()
                ->each(function ($schedule) {
                    $schedule->day = trans("dates.{$schedule->day}");
                });
            return response()->json([
                'schedules' => $schedules,
                'url' => route('employees.schedules.print', $employee->id),
            ]);
        }

        return response()->json([
            'error' => 'No encontramos ni un empleado con este codigo',
        ], 404);
    }

    public function getByCode($code) 
    {
        if($employee = Employee::whereCode($code)->first()) {
            return $employee;
        }
        return response()->json([
            'message' => 'No se encontró ningun empleado con esta clave'
        ]);
    }

    public function setFingerprint(Request $request) 
    {
        if($employee = Employee::whereCode($code)->first()) {
            $employee->fingerprint = $request->fingerprint;
            $employee->save();
            return true;
        }
        return false;
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $employee = Employee::find($id);
        if (isset($data['password']) && empty($data['password'])) {
            unset($data['password']);
        }
        if(!Auth::user()->isA('horarios')){
            if(isset($data['imss_date']) && ($data['imss_date'] == "" || $data['imss_date'] == "0000-00-00")) $data['imss_date'] = null;
            if( isset($data['birthday']) && ($data['birthday'] == "" || $data['birthday'] == "0000-00-00")) $data['birthday'] = null;
        }
        $employee->update($data);

        $this->CreateFile($employee, $request, "avatars/{$employee->id}");
        $request->session()->flash('success', 'Empleado actualizado con exito.');
        return redirect()->route('employees.edit', $employee->id);
    }

    public function addDocuments(Request $request, $id)
    {
        $data = $request->all();
        $employee = Employee::find($id);

        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $name_arr = explode('.', $file->getClientOriginalName());
                if (!$employee->correctDocument($name_arr[0])) {
                    $request->session()->flash('error', "El documento '{$name_arr[0]}' no tiene la nomenclatura correcta.");
                    return redirect()->route('employees.edit', $employee->id);
                }
            }
        }
        $this->CreateFiles($employee, $request, "documents/{$employee->id}");
        $request->session()->flash('success', 'Documentos actualizados con exito.');
        return redirect()->route('employees.edit', $employee->id);
    }

    public function destroy(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->active = false;
        $employee->inactived_at = $request->inactived_at;
        $employee->save();

        $request->session()->flash('success', 'Empleado dado de baja con exito');
        return redirect()->route('employees.index');
    }

    public function contrato($id)
    {
        $employee = Employee::find($id);
        $contract = Configuration::first()->contract;

        $configuration = Configuration::find($employee->subsidiary()->first()->configuration_id);

        $contrato = $contract;
        $contrato = str_replace('%razon_social%', $configuration->razon_social, $contrato);
        $contrato = str_replace('%configuration_rfc%', $configuration->rfc, $contrato);
        $contrato = str_replace('%configuration_address%', $configuration->address, $contrato);
        $contrato = str_replace('%configuration_curp%', $configuration->curp, $contrato);
        $contrato = str_replace('%configuration_created_in%', $configuration->created_in, $contrato);
        $contrato = str_replace('%configuration_regimen%', $configuration->regimen, $contrato);

        $contrato = str_replace('%name%', $employee->name, $contrato);
        $contrato = str_replace('%short_name%', $employee->short_name, $contrato);
        $contrato = str_replace('%job%', $employee->job, $contrato);
        $contrato = str_replace('%entry_date%', $employee->entry_date, $contrato);
        $contrato = str_replace('%rfc%', $employee->rfc, $contrato);
        $contrato = str_replace('%curp%', $employee->curp, $contrato);
        $contrato = str_replace('%nss%', $employee->nss, $contrato);
        $contrato = str_replace('%address%', $employee->address, $contrato);
        $contrato = str_replace('%gender%', $employee->gender, $contrato);
        $contrato = str_replace('%salary%', $employee->salary, $contrato);
        $contrato = str_replace('%salary_text%', $this->num2letras($employee->salary), $contrato);
        $contrato = str_replace('%code%', $employee->key, $contrato);
        $contrato = str_replace('%marital_status%', $employee->marital_status, $contrato);
        $contrato = str_replace('%begin%',"<table width='100%'><tr><td align='center'>", $contrato);
        $contrato = str_replace('%middle%',"</td><td align='center'>", $contrato);
        $contrato = str_replace('%end%',"</td></tr></table>", $contrato);

        $manuals = Manual::where('type', $employee->job)->get();
        $reglamento = Manual::where('type', 'Reglamento')->get();

        return view('admin.employees.contrato', compact('employee', 'contrato', 'manuals', 'reglamento'));
    }

    public function active(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->update(['active' => true]);

        $request->session()->flash('success', 'Empleado dado de alta con exito.');
        return back();
    }

    private function num2letras($num, $fem = false, $dec = true) { 
        $matuni[2]  = "dos"; 
        $matuni[3]  = "tres"; 
        $matuni[4]  = "cuatro"; 
        $matuni[5]  = "cinco"; 
        $matuni[6]  = "seis"; 
        $matuni[7]  = "siete"; 
        $matuni[8]  = "ocho"; 
        $matuni[9]  = "nueve"; 
        $matuni[10] = "diez"; 
        $matuni[11] = "once"; 
        $matuni[12] = "doce"; 
        $matuni[13] = "trece"; 
        $matuni[14] = "catorce"; 
        $matuni[15] = "quince"; 
        $matuni[16] = "dieciseis"; 
        $matuni[17] = "diecisiete"; 
        $matuni[18] = "dieciocho"; 
        $matuni[19] = "diecinueve"; 
        $matuni[20] = "veinte"; 
        $matunisub[2] = "dos"; 
        $matunisub[3] = "tres"; 
        $matunisub[4] = "cuatro"; 
        $matunisub[5] = "quin"; 
        $matunisub[6] = "seis"; 
        $matunisub[7] = "sete"; 
        $matunisub[8] = "ocho"; 
        $matunisub[9] = "nove"; 

        $matdec[2] = "veint"; 
        $matdec[3] = "treinta"; 
        $matdec[4] = "cuarenta"; 
        $matdec[5] = "cincuenta"; 
        $matdec[6] = "sesenta"; 
        $matdec[7] = "setenta"; 
        $matdec[8] = "ochenta"; 
        $matdec[9] = "noventa"; 
        $matsub[3]  = 'mill'; 
        $matsub[5]  = 'bill'; 
        $matsub[7]  = 'mill'; 
        $matsub[9]  = 'trill'; 
        $matsub[11] = 'mill'; 
        $matsub[13] = 'bill'; 
        $matsub[15] = 'mill'; 
        $matmil[4]  = 'millones'; 
        $matmil[6]  = 'billones'; 
        $matmil[7]  = 'de billones'; 
        $matmil[8]  = 'millones de billones'; 
        $matmil[10] = 'trillones'; 
        $matmil[11] = 'de trillones'; 
        $matmil[12] = 'millones de trillones'; 
        $matmil[13] = 'de trillones'; 
        $matmil[14] = 'billones de trillones'; 
        $matmil[15] = 'de billones de trillones'; 
        $matmil[16] = 'millones de billones de trillones'; 
       
        //Zi hack
        $float=explode('.',$num);
        $num=$float[0];

        $num = trim((string)@$num); 
        if ($num[0] == '-') { 
            $neg = 'menos '; 
            $num = substr($num, 1); 
        }else 
            $neg = ''; 
            while ($num[0] == '0') $num = substr($num, 1); 
            if ($num[0] < '1' or $num[0] > 9) 
                $num = '0' . $num; 
            $zeros = true; 
            $punt = false; 
            $ent = ''; 
            $fra = ''; 
            for ($c = 0; $c < strlen($num); $c++) { 
                $n = $num[$c]; 
                if (! (strpos(".,'''", $n) === false)) { 
                    if ($punt) break; 
                    else{ 
                        $punt = true; 
                        continue; 
                    } 
                }else if (! (strpos('0123456789', $n) === false)) { 
                    if ($punt) { 
                        if ($n != '0') $zeros = false; 
                            $fra .= $n; 
                    }else 
                    $ent .= $n; 
                }else 
            break; 
            } 
            $ent = '     ' . $ent; 
            if ($dec and $fra and ! $zeros) { 
                $fin = ' coma'; 
                for ($n = 0; $n < strlen($fra); $n++) { 
                    if (($s = $fra[$n]) == '0') 
                        $fin .= ' cero'; 
                    else if ($s == '1') 
                        $fin .= $fem ? ' una' : ' un'; 
                    else 
                        $fin .= ' ' . $matuni[$s]; 
                } 
            }else 
                $fin = ''; 
            if ((int)$ent === 0) return 'Cero ' . $fin; 
            $tex = ''; 
            $sub = 0; 
            $mils = 0; 
            $neutro = false; 
            while ( ($num = substr($ent, -3)) != '   ') { 
                $ent = substr($ent, 0, -3); 
                if (++$sub < 3 and $fem) { 
                    $matuni[1] = 'una'; 
                    $subcent = 'as'; 
                }else{ 
                    $matuni[1] = $neutro ? 'un' : 'uno'; 
                    $subcent = 'os'; 
                } 
                $t = ''; 
                $n2 = substr($num, 1); 
                if ($n2 == '00') { 
                }else if ($n2 < 21) 
                $t = ' ' . $matuni[(int)$n2]; 
                else if ($n2 < 30) { 
                    $n3 = $num[2]; 
                    if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
                    $n2 = $num[1]; 
                    $t = ' ' . $matdec[$n2] . $t; 
                }else{ 
                    $n3 = $num[2]; 
                    if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
                    $n2 = $num[1]; 
                    $t = ' ' . $matdec[$n2] . $t; 
                } 
                $n = $num[0]; 
                if ($n == 1) { 
                    $t = ' ciento' . $t; 
                }else if ($n == 5){ 
                    $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
                }else if ($n != 0){ 
                    $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
                } 
                if ($sub == 1) { 
                }else if (! isset($matsub[$sub])) { 
                    if ($num == 1) { 
                        $t = ' mil'; 
                    }else if ($num > 1){ 
                        $t .= ' mil'; 
                    } 
                }else if ($num == 1) { 
                    $t .= ' ' . $matsub[$sub] . '?n'; 
                }else if ($num > 1){ 
                    $t .= ' ' . $matsub[$sub] . 'ones'; 
                }   
                    if ($num == '000') $mils ++; 
                    else if ($mils != 0) { 
                        if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
                        $mils = 0; 
                    } 
            $neutro = true; 
            $tex = $t . $tex; 
            } 
       $tex = $neg . substr($tex, 1) . $fin; 
       //Zi hack --> return ucfirst($tex);
       $end_num=ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
       return $end_num; 
    } 
}
