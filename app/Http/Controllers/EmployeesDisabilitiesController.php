<?php 

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Schedule;
use App\Subsidiary;
use App\Turn;
use App\Disability;
use Carbon\Carbon;
use App\Traits\ImageableTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use Auth;

class EmployeesDisabilitiesController extends Controller
{
    use ImageableTrait;

    public function create($employee_id)
    {
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $turns = Turn::all();
        $employee = Employee::find($employee_id);
        return view('admin.employees.disabilities.create', compact('employee_id', 'subsidiaries', 'turns', 'employee'));
    }

    public function store(Request $request, $employee_id)
    {
        $data = $request->all();
        $employee = Employee::find($employee_id);
        $dates = [];
        $start = new Carbon($data['start_date']);
        $end = new Carbon($data['end_date']);
        
        while ($start->lte($end)) {
            $date = $start->copy();
            $dates[] = $date;
            $start->addDay();
        }
        
        $sched = [];
        
        $employee->schedules()->whereBetween('date', [$data['start_date'], $data['end_date']])->delete();

        foreach ($dates as $date) {
            $sched[] = $employee->schedules()->create([
                'day' => $date->format('l'),
                'turn_id' => 11, //INCAPACIDAD
                'subsidiary_id' => $employee->subsidiary_id,
                'start' => $data['start_date'],
                'end' => $data['end_date'],
                'extra_day' => 0,
                'has_rest' => 0,
                'rest_minutes' => 0,
                'date' => $date->format('Y-m-d')
            ]);
        }
        
        $disability = $employee->disabilities()->create([
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'user_id' => Auth::user()->id
            ]);
        $this->CreateFile($disability, $request, "disabilities/{$disability->id}");
        $request->session()->flash('success', 'Incapacidad dada de alta con exito.');
        if(Auth::user()->isA('employee')){
            return redirect('/home');
        }
        
        return redirect()->route('employees.edit', [$employee_id]);
    }

    public function edit(Request $request, $employee_id, $id)
    {
        $disability = Disability::find($id);
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $turns = Turn::all();
        $employee = Employee::find($employee_id);
        return view('admin.employees.disabilities.edit', compact('employee_id', 'disability', 'subsidiaries', 'turns', 'employee'));
    }

    public function update(Request $request, $employee_id, $id)
    {
        $data = $request->all();
        $employee = Employee::find($employee_id);
        $dates = [];
        $start = new Carbon($data['start_date']);
        $end = new Carbon($data['end_date']);
        
        while ($start->lte($end)) {
            $date = $start->copy();
            $dates[] = $date;
            $start->addDay();
        }
        
        $sched = [];
        
        $employee->schedules()->whereBetween('date', [$data['start_date'], $data['end_date']])->delete();

        foreach ($dates  as $date) {
            $sched[] = $employee->schedules()->create([
                'day' => $date->format('l'),
                'turn_id' => 11, //INCAPACIDAD
                'subsidiary_id' => $employee->subsidiary_id,
                'start' => $data['start_date'],
                'end' => $data['end_date'],
                'extra_day' => 0,
                'has_rest' => 0,
                'rest_minutes' => 0,
                'date' => $date->format('Y-m-d')
            ]);
        }

        $disability = Disability::find($id);
        $disability->update($data);

        $this->CreateFile($disability, $request, "disabilities/{$disability->id}");
        $request->session()->flash('success', 'Incapacidad actualizada con exito.');
        if(Auth::user()->isA('employee')){
            return redirect('/home');
        }
        return redirect()->route('employees.disabilities.edit', [$employee_id, $disability->id]);
    }

    public function destroy(Request $request, $employee_id, $id)
    {
        $disability = Disability::find($id);
        if($image = $disability->image()->get()->first()){
            $filePath = $image->path;
            $filePath = storage_path($filePath);
            $filePath = str_replace("/storage","app\public",$filePath);
            $filePath = str_replace("/","\\",$filePath);        
            @unlink($filePath);
            $disability->image()->delete();
        }
        $disability->delete();
        $request->session()->flash('success', 'Incapacidad eliminada con exito.');
        if(Auth::user()->isA('employee')){
            return redirect('/home');
        }
        return redirect()->route('employees.edit', $employee_id);
    }

    public function print($id)
    {
        $employee = Employee::find($id);
        $now = Carbon::now();
        $start = $now->startOfWeek()->format('d-m-Y');
        $end = $now->endOfWeek()->format('d-m-Y');
        $disabilities = $employee->disabilities()->with(['subsidiary', 'turn'])
                    ->thisWeek()->get()
                    ->each(function ($schedule){
                        $schedule->day = trans("dates.{$schedule->day}");
                    });

        return view('admin.employees.disabilities.print', compact('employee', 'disabilities', 'start', 'end'));
    }
}
