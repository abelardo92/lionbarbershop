<?php namespace App\Http\Controllers;

use App\Employee;
use App\Issue;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use App\Traits\SubDomainHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class EmployeesIssuesController extends Controller
{
    use ImageableTrait;
    use SubDomainHelper;

    public function index($subsidiary_code, $id)
    {
        if (!$employee = Employee::whereCode(trim($id))->first()) {
            return $this->responseNotEmployee();
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $issues = $employee->issues()->with('images')->dates([request('start'), request('end')])->get();
        return response()->json([
            'issues' => $issues,
        ]);
    }

    public function sentMessages()
    {
        $issues = Issue::with('originSubsidiary', 'destinySubsidiary','originUser.employee', 'images')->whereNotNull('origin_user_id')->cashierMessages();
        return $this->sentMessagesByRoute($issues, 'admin.issues.sent', 2);        
    }

    public function sentEmployeeMessages()
    {
        $issues = Issue::with('originSubsidiary', 'destinySubsidiary','originUser.employee', 'images')->whereNotNull('origin_user_id')->employeeMessages();
        return $this->sentMessagesByRoute($issues, 'admin.issues.employees.sent', 3);        
    }

    private function sentMessagesByRoute($issues, $route, $type) {
        $user = Auth::user();

        if($type == 3) {
            if($user->isA('employee')) {
                $issues = $issues->where('origin_user_id', $user->id);
            } else {
                $issues = $issues->where('origin_subsidiary_id', 7);
            }
            $issues = $issues->orderBy('id','desc')->paginate(8);
            return view($route, compact('user', 'issues'));
        }

        if($user->isA('cashier', 'barbero')) {
            if($subsidiary_code = $this->hasSubdomain()) {
                $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
                $issues = $issues->where('origin_subsidiary_id',$subsidiary->id);
            } else {
                $issues = $issues->where('origin_user_id', $user->id);
            }   
        } else if ($type == 2) {
            $issues = $issues->where('origin_subsidiary_id', 7);
        }
        $issues = $issues->orderBy('id','desc')->paginate(8);
        return view($route, compact('user', 'issues'));
    }

    public function receivedMessages()
    {
        return $this->receivedMessagesByRoute('admin.issues.received', 2);
    }

    public function receivedEmployeeMessages()
    {
        return $this->receivedMessagesByRoute('admin.issues.employees.received', 3);   
    }

    private function receivedMessagesByRoute($route, $type) {

        $user = Auth::user();
        $subsidiary_id = 7;

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->isAdmon() && !$user->isA('super-admin', 'supervisor', 'accountant')) {
                return redirect()->back();
            }
            if($user->isA('cashier')) {
                $subsidiary_id = $subsidiary->id;
            }
        } 
        else if($type == 2 && !$user->isA('super-admin', 'supervisor', 'accountant', 'cashier-admin')) {
            return redirect()->back();
        }
        
        $issues = Issue::with('originSubsidiary', 'destinySubsidiary','originUser.employee', 'images');
        if($type == 2) {
            $issues = $issues->where('destiny_subsidiary_id', $subsidiary_id)->where('origin_user_id', '!=', $user->id)
            ->cashierMessages();
        } else if ($type == 3) {
            if($user->isA('employee', 'cashier', 'barbero')) {
                $issues = $issues->employeeMessages()->where('destiny_user_id', $user->id);
            } else {
                $issues = $issues->employeeMessages()->where('destiny_subsidiary_id', $subsidiary_id);
            }
            
        }
        $issues = $issues->orderBy('id','desc')->paginate(8);

        return view($route, compact('user', 'issues'));
    }

    protected function responseNotEmployee()
    {
        $message = 'No encontramos ni un empleado con esta clave.';
        return $this->makeResponse(compact('message'), 404);
    }

    protected function makeResponse($data = [], $code = 200)
    {
        return response()->json($data, $code);
    }

    public function create($employee_id)
    {
        $employee = Employee::find($employee_id);
        $currentDate = Carbon::now()->format('Y-m-d');
        return view('admin.employees.issues.create', compact('employee_id', 'employee', 'currentDate'));
    }

    public function create2()
    {
        $employees = Employee::active()->get();
        $currentDate = Carbon::now()->format('Y-m-d');
        return view('admin.issues.create2', compact('employees', 'currentDate'));
    }

    public function createMessage()
    {
        return $this->createMessageByRoute('admin.issues.create');
    }

    public function createEmployeeMessage()
    {
        return $this->createMessageByRoute('admin.issues.employees.create');
    }

    private function createMessageByRoute($route) {
        $user = Auth::user();
        $subsidiaries = Subsidiary::active()->notLaundry()->get();
        return view($route, compact('user', 'subsidiaries'));
    }

    public function createAnswerMessage($related_issue_id)
    {
        return $this->createAnswerMessageByRoute($related_issue_id, 'admin.issues.create');
    }

    public function createAnswerEmployeeMessage($related_issue_id)
    {
        return $this->createAnswerMessageByRoute($related_issue_id, 'admin.issues.employees.create');
    }

    private function createAnswerMessageByRoute($related_issue_id, $route) {
        $user = Auth::user();
        $subsidiaries = Subsidiary::active()->notLaundry()->get();
        $related_issue = Issue::find($related_issue_id);
        return view($route, compact('user', 'subsidiaries', 'related_issue'));   
    }

    public function store(Request $request, $employee_id)
    {
        $data = $request->all();
        $data['type'] = 1;
        $employee = Employee::find($employee_id);
        $issue = $employee->issues()->create($data);
        $this->CreateFiles($issue, $request, "issues/{$issue->id}");

        $request->session()->flash('success', 'Incidencia creada con exito.');
        return redirect()->route('employees.edit', [$employee_id]);
    }

    public function store2(Request $request)
    {
        $data = $request->all();
        $data['type'] = 1;
        $employee = Employee::find($request->employee_id);
        $issue = $employee->issues()->create($data);
        $this->CreateFiles($issue, $request, "issues/{$issue->id}");

        $request->session()->flash('success', 'Incidencia creada con éxito.');
        return redirect()->back();
    }

    public function showMessage($id)
    {
        return $this->showMessageByRoute($id, 'admin.issues.show');
    }

    public function showEmployeeMessage($id)
    {
        return $this->showMessageByRoute($id, 'admin.issues.employees.show');
    }

    private function showMessageByRoute($id, $route) {
        $user = Auth::user();
        $issue = Issue::with('originSubsidiary', 'destinySubsidiary', 'originUser.employee', 'images')->find($id);
        if($user->id != $issue->origin_user_id && $issue->opened_by == null) {
            $issue->opened_by = $user->id;
            $issue->update(); 
        }
        return view($route, compact('issue', 'user'));
    } 

    public function storeMessage(Request $request)
    {
        return $this->storeMessageByRoute($request, 'issues.messages.sent', 2);
    }

    public function storeEmployeeMessage(Request $request)
    {
        return $this->storeMessageByRoute($request, 'issues.messages.employees.sent', 3);
    }

    private function storeMessageByRoute($request, $route, $type) {

        $data = $request->all();
        $user = Auth::user();
        $data['origin_user_id'] = $user->id;
        $data['type'] = $type;
        $data['date'] = Carbon::now()->format('Y-m-d');

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $data['origin_subsidiary_id'] = $subsidiary->id;
            if(!$user->isA('super-admin', 'supervisor', 'accountant')) {
                $data['destiny_subsidiary_id'] = 7;
            }
        } else {
            if($user->isA('super-admin', 'supervisor', 'accountant')) {
                $data['origin_subsidiary_id'] = 7;
            } else {
                $data['destiny_subsidiary_id'] = 7;
            }
        }

        if(isset($data['destiny_subsidiary_id']) && $data['destiny_subsidiary_id'] == "") {
            $data['destiny_subsidiary_id'] = null;
        }

        if(isset($data['destiny_user_id']) && $data['destiny_user_id'] == "") {
            $data['destiny_user_id'] = null;
        }
        
        $issue = Issue::create($data);
        $this->CreateFiles($issue, $request, "issues/{$issue->id}");

        $request->session()->flash('success', 'Mensaje enviado con éxito.');
        return redirect()->route($route);
    }

    public function edit(Request $request, $employee_id, $id)
    {
        $issue = Issue::find($id);
        $employee = Employee::find($employee_id);
        return view('admin.employees.issues.edit', compact('employee_id', 'issue', 'employee'));
    }

    public function update(Request $request, $employee_id, $id)
    {
        $data = $request->all();
        $issue = Issue::find($id);
        $issue->update($data);

        $this->CreateFiles($issue, $request, "issues/{$issue->id}");

        $request->session()->flash('success', 'Incidencia actualizada con exito.');
        return redirect()->route('employees.issues.edit', [$employee_id, $issue->id]);
    }

    public function destroy(Request $request, $employee_id, $id)
    {
        $issue = Issue::find($id);
        $issue->delete();

        $request->session()->flash('success', 'Incidencia eliminada con exito.');
        return redirect()->route('employees.edit', $employee_id);
    }
}
