<?php
namespace App\Http\Controllers;

use App\Device;
use App\Employee;
use App\Finger;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FingersController extends Controller 
{
    public function register(Request $request)
    {
        if ($request->has('user_id') && !empty($request->has('user_id'))) {
            $user_id = $request->user_id;
            $url = route('employees.reg.process');
            $urlac = route('employees.reg.getac');

            return "$user_id;SecurityKey;20;{$url};{$urlac}";

        }

        return 'empty';
    }

    public function getac(Request $request)
    {
        if ($request->has('vc') && !empty($request->has('vc'))) {
            $device = Device::vc($request->vc)->first();
            return $device->ac.$device->sn;
        }

        session()->flash('success', 'no se encuentra el parametro vc');
        return route('employees.index');
    }

    public function registerProcess(Request $request)
    {
        $data       = explode(";", $request->input('RegTemp'));
        $vStamp     = $data[0];
        $sn         = $data[1];
        $user_id    = $data[2];
        $regTemp    = $data[3];
        $employee = Employee::find($user_id);
        $device = Device::sn($sn)->first();
        $res = [];

        $salt = md5("{$device->ac}{$device->vkey}{$regTemp}{$sn}{$user_id}");

        // if (strtoupper($vStamp) == strtoupper($salt)) {
            Finger::create([
                'user_id' => $user_id,
                'data' => $regTemp
            ]);
            $res['result'] = true; 
            session()->flash('success', 'Huella registrada con exito');
            return route('employees.edit', [$employee->id]);
            
        // } else {
        //     $res['result'] = false;
        //     session()->flash('success', 'Ocurrio un error al registrar la huella');
        //     return route('employees.edit', [$employee->id]);
        // }

    }

    public function validat(Request $request)
    {
        if ($request->has('user_id') && !empty($request->has('user_id'))) {
            $user_id = $request->user_id;
            $finger = Finger::where('user_id', $user_id)->first();
            $url = route('employees.validate.process', $request->subsidiary_code);
            $urlac = route('employees.reg.getac');

            return "{$user_id};{$finger->data};SecurityKey;10;{$url}?subsidiary_code={$request->subsidiary_code};{$urlac};extraParams";

        }

        return 'empty';
    }

    public function validateProcess(Request $request)
    {
        $data       = explode(";",$_POST['VerPas']);
        $user_id    = $data[0];
        $vStamp     = $data[1];
        $time       = $data[2];
        $sn         = $data[3];
        $device = Device::sn($sn)->first();
        $employee = Employee::find($user_id);
        $finger = Finger::where('user_id', $user_id)->first();
        
        $user_name  = $data['user_name'];
            
        $salt = md5($sn.$finger->data.$device->vc.$time.$user_id.$device->vkey);
        $res = [];

        if (strtoupper($vStamp) == strtoupper($salt)) {
            $log = createLog($user_name, $time, $sn);
            if ($log == 1) {
                echo $base_path."messages.php?user_name=$user_name&time=$time";
            } else {
                echo $base_path."messages.php?msg=$log";
            }
        } else {
            $msg = "Parameter invalid..";
            echo $base_path."messages.php?msg=$msg";
        }
        return route('employees.edit', [$employee->id]);

    }
}