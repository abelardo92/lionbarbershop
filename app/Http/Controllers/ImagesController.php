<?php namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    public function destroy(Request $request, $id)
    {
        $image = Image::find($id);
        $image->delete();

        $request->session()->flash('success', 'Imagen eliminada con exito.');
        return back();
    }
}
