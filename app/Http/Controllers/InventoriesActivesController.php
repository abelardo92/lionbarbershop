<?php 
namespace App\Http\Controllers;

use App\Article;
use App\InventoryArticle;
use App\Subsidiary;
use Illuminate\Http\Request;

class InventoriesActivesController extends Controller
{
    public function index(Request $request)
    {
        $type = request('type', 'activo');
        $entries = InventoryArticle::where('type', $type)->entries()->get();
        $outs = InventoryArticle::where('type', $type)->outs()->get();
        $adjusts = InventoryArticle::where('type', $type)->adjusts()->get();
        
        return view('admin.inventories_actives.index', compact('type', 'entries','outs','adjusts'));
    }

    public function create()
    {
        $type = request('type', 'activo');
        $kardex_type = request('kardex_type', 'entrada');
        $articles = Article::where('type', $type)->get();
        $subsidiaries = Subsidiary::all();
        if ($kardex_type == 'entrada') {
            $typeto = 'entries';
        }elseif ($kardex_type == 'salida') {
            $typeto = 'outs';
        }else {
            $typeto = 'adjusts';
        }

        return view("admin.inventories_actives.{$typeto}.create", compact('articles', 'subsidiaries', 'type', 'kardex_type'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $folio = InventoryArticle::getFolio($data['kardex_type']);
        $tipo = ucfirst($data['kardex_type']);
        $subsidiary = Subsidiary::find($data['subsidiary_id']);
        $method = "saveInventory{$tipo}";
        $this->{$method}($data, $folio, $subsidiary);
        
        $request->session()->flash('success', 'Inventario dado de alta con exito.');
        return redirect()->route('inventories.activos.insumos', ["type" => $data['type']]);
    }

    public function show($kardex_type, $folio, $type)
    {
        $types = InventoryArticle::where('kardex_type', $kardex_type)
                        ->where('type', $type)
                        ->where('folio', $folio)
                        ->get();
        if ($kardex_type == 'entrada') {
            $title = 'Ticket de entrada.';
        }elseif ($kardex_type == 'salida') {
            $title = 'Ticket de salida.';
        }else {
            $title = 'Ticket de ajuste.';
        }
        if ($kardex_type == 'entrada') {
            $tipi = 'entries';
        }
        if ($kardex_type == 'salida') {
            $tipi = 'outs';
        }
        if ($kardex_type == 'ajuste') {
            $tipi = 'adjusts';
        }
        return view("admin.inventories_actives.{$tipi}.ticket", compact('title', 'types'));
    }

    public function saveInventoryEntrada($data, $folio, $subsidiary)
    {
        foreach ($data['articles'] as $article) {
            InventoryArticle::create([
                'folio' => $folio,
                'type' => $data['type'],
                'kardex_type' => $data['kardex_type'],
                'subsidiary_id' => $data['subsidiary_id'],
                'article_id' => $article['id'],
                'qty' => $article['qty'],
            ]);

            $existence = $article['qty'];
            $pid = $article['id'];
            if ($article_find = $subsidiary->articles()->find($pid)) {
                $existence = $article_find->pivot->existence + $article['qty'];
            }
            $subsidiary->articles()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);
        }
    }

    public function saveInventorySalida($data, $folio, $subsidiary)
    {
        foreach ($data['articles'] as $article) {
            InventoryArticle::create([
                'folio' => $folio,
                'type' => $data['type'],
                'kardex_type' => $data['kardex_type'],
                'subsidiary_id' => $data['subsidiary_id'],
                'article_id' => $article['id'],
                'qty' => $article['qty'],
                'concepto' => $data['concepto'],
            ]);

            $existence = $article['qty'];
            $pid = $article['id'];
            if ($article_find = $subsidiary->articles()->find($pid)) {
                $existence = $article_find->pivot->existence - $article['qty'];
            }
            $subsidiary->articles()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);
        }
    }

    public function saveInventoryAjuste($data, $folio, $subsidiary)
    {
        foreach ($data['articles'] as $article) {
            InventoryArticle::create([
                'folio' => $folio,
                'type' => $data['type'],
                'kardex_type' => $data['kardex_type'],
                'subsidiary_id' => $data['subsidiary_id'],
                'article_id' => $article['id'],
                'qty' => $article['qty'],
                'current_existence' => $article['current_existence'],
                'real_existence' => $article['real_existence'],
            ]);
            $pid = $article['id'];
            $subsidiary->articles()->syncWithoutDetaching([
                $pid => [
                    'existence' => $article['real_existence'],
                ],
            ]);
        }
    }
}
