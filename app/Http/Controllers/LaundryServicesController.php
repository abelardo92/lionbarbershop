<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subsidiary;
use App\LaundryService;
use App\Turn;
use Illuminate\Support\Facades\Auth;
use App\CashRegister;
use App\ExchangeRate;
use Carbon\Carbon;

class LaundryServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $now = Carbon::now()->toDateString();
        return view('admin.laundry-services.create', compact('subsidiaries', 'now'));
    }

    public function create2()
    {
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        $turns = Turn::all();
        $now = Carbon::now()->toDateString();
        return view('admin.laundry-services.create2', compact('subsidiaries', 'now', 'turns'));
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $folio = 1;

        $subsidiary = Subsidiary::find($data['subsidiary_id']);
        if ($last = $subsidiary->originLaundryServices()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        $data['folio'] = $folio;
        $cash_register = $subsidiary->cashRegisters()->open()->today()->first();
        $laundry_service = LaundryService::create([
            'subsidiary_id' => $data['subsidiary_id'],
            'customer_subsidiary_id' => $data['customer_subsidiary_id'],
            'date' => $data['date'],
            'amount' => $data['amount'],
            'folio' => $data['folio'],
            'cash_register_id' => $cash_register->id,
            'customer_name' => $data['customer_name'],
        ]);

        $cash_register->total = $cash_register->total + $laundry_service->amount;
        $cash_register->save();  

        $request->session()->flash('success', 'Servicio creado con éxito');
        return redirect()->to('/home');
    }

    public function store2(Request $request)
    {
        $data = $request->all();
        $folio = 1;
        $now = Carbon::now();
        $date = Carbon::parse($data['date']);
        if ($date >= $now->subDays(1)){
            $request->session()->flash('error', 'Solo se permiten fechas anteriores a la actual');
            return redirect()->back();   
        }
        $subsidiary = Subsidiary::find($data['subsidiary_id']);
        if(!($cash_register = CashRegister::where('subsidiary_id',$subsidiary->id)->where('turn_id',$data['turn_id'])->where('date',$data['date'])->first())){
            $cash_register = CashRegister::create([
                'date' => $data['date'],
                'time' => "00:00:00",
                'subsidiary_id' => $subsidiary->id,
                'close' => 1,
                'imprest' => 1000,
                'employee_id' => $request->user()->id,
                'turn_id' => $data['turn_id'],
                'total' => 0,
                'exchange_rate_id' => ExchangeRate::latest()->first()->id,
            ]);
        } 
        if ($last = $subsidiary->originLaundryServices()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        $data['folio'] = $folio;

        $laundry_service = LaundryService::create([
            'subsidiary_id' => $data['subsidiary_id'],
            'customer_subsidiary_id' => $data['customer_subsidiary_id'],
            'date' => $data['date'],
            'amount' => $data['amount'],
            'folio' => $data['folio'],
            'cash_register_id' => $cash_register->id,
            'customer_name' => $data['customer_name'],
        ]);

        $cash_register->total = $cash_register->total + $laundry_service->amount;
        $cash_register->save();

        $request->session()->flash('success', 'Servicio creado con éxito');
        return redirect()->to('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laundry_service = LaundryService::find($id);
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        return view('admin.laundry-services.edit', compact('subsidiaries', 'laundry_service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $laundry_service = LaundryService::find($id);
        $laundry_service->update($data);
        $request->session()->flash('success', 'Servicio actualizado con éxito');
        return redirect()->back();
    }

    public function destroy(Request $request, $id)
    {
        $laundry_service = LaundryService::find($id);
        $laundry_service->delete();
        $request->session()->flash('success', 'Servicio eliminado con éxito.');
        return redirect()->back();
    }

    public function cancel($id){
        $laundry_service = LaundryService::find($id);
        if($laundry_service->canceled_at != null || $laundry_service->canceled_by != null){
            return response()->json([
            'message' => 'La nota ' .$laundry_service->folio. ' ya está cancelada.',
            'laundry_service' => $laundry_service,
            ]);
        }
        $laundry_service->canceled_at = Carbon::now();
        $laundry_service->canceled_by = Auth::user()->id;
        $laundry_service->save();
        return response()->json([
            'message' => 'Nota cancelada con éxito.',
            'laundry_service' => $laundry_service,
            ]);
    }

    public function print($id) 
    {
        $laundry_service = LaundryService::find($id);
        $subsidiary = $laundry_service->subsidiary()->get()->first();
        $customer_subsidiary = $laundry_service->customerSubsidiary()->get()->first();
        return view('admin.laundry-services.tickets.sale', compact('subsidiary', 'customer_subsidiary', 'laundry_service'));
    }

    public function search($folio, $subsidiary_id)
    {     
        if (!$laundry_service = LaundryService::with(['subsidiary', 'customerSubsidiary'])->where('folio', $folio)->where('subsidiary_id',$subsidiary_id)->get()->first()) {
            return response()->json(['message' => 'No encontramos ventas con este folio.'], 404);
        }
        return response()->json([
            'message' => '',
            'laundry_service' => $laundry_service,
        ]);
    }
}
