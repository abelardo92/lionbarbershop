<?php

namespace App\Http\Controllers;

use App\Paysheet;
use App\Percepcione;
use Illuminate\Http\Request;

class PercepcioneController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $percepcione = Percepcione::create($data);
        foreach (Paysheet::where('employee_id', $data['employee_id'])->get() as $paysheet) {
            $total= Percepcione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otros_ingresos = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Percepción dada de alta con exito.');
        return redirect()->back();
    }

    public function destroy(Request $request, $id)
    {
        $percepcione = Percepcione::find($id);
        $percepcione->delete();

        foreach (Paysheet::where('employee_id', $percepcione->employee_id)->get() as $paysheet) {
            $total= Percepcione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otros_ingresos = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Percepción eliminada con exito');
        return redirect()->back();
    }

}
