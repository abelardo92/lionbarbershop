<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Image;
use App\Product;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use App\Traits\SubDomainHelper;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;

class ProductsController extends Controller
{
    use ImageableTrait;
    use SubDomainHelper;

    public function index(Request $request)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $products = Product::laundry()->active()->paginate(15);
                return view('admin.products.index', compact('products'));
            }
        }
        $products = Product::notLaundry()->active()->orderBy('key', 'asc')->paginate(15);
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry) {
                $data['is_laundry'] = true;
                $subsidiaries = Subsidiary::active()->laundry()->get();
            } else {
                $subsidiaries = Subsidiary::active()->notLaundry()->get();
            }
        } else {
            $subsidiaries = Subsidiary::active()->notLaundry()->get();
        }
        $product = Product::create($data);

        $this->CreateFile($product, $request, "products/{$product->id}");

        foreach ($subsidiaries as $subsidiary) {
            $pid = $product->id;
            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => 0
                ]
            ]);
        }
        
        $request->session()->flash('success', 'Producto dado de alta con exito.');
        return redirect()->route('products.edit', $product->id);
    }

    public function edit(Request $request, $id)
    {
        $product = Product::find($id);
        return view('admin.products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::find($id);
        $product->update($data);
        $this->CreateFile($product, $request, "products/{$product->id}");

        $request->session()->flash('success', 'Producto actualizado con éxito.');
        return redirect()->route('products.edit', $product->id);
    }

    public function getByKey($key)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->active()->firstOrFail();
            $product = $subsidiary->products()->whereKey($key)->first();
            if(!$product){
                $product = Product::whereKey($key)->first();
                $product->existence = 0;
            }
        }else {
            $product = Product::whereKey($key)->first();     
        }
        
        if ($product) {
            if ($subsidiary_code = $this->hasSubdomain()) {
                if($productPivot = $product->pivot){
                    $product->existence = $productPivot->existence;
                }            
            }
            return response()->json([
                'success' => true,
                'product' => $product,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No encontramos ni un producto con esta clave.',
        ], 404);
        
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->is_active = false;
        $product->save();
        session()->flash('success', "El producto ha sido eliminado con éxito");
        return back();
    }

    public function search($subsidiary_code, $id)
    {
        $product = Product::active()->where('id', $id)->first();
        return $product;
    }
}
