<?php

namespace App\Http\Controllers;

use App\Http\Requests\Surveys\CreateQuestion;
use App\Subsidiary;
use App\Surveys\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::orderBy('order')->paginate(20);
        return view('admin.surveys.questions.index', compact('questions'));
    }

    public function create()
    {
        $subsidiaries = Subsidiary::all();
        return view('admin.surveys.questions.create', compact('subsidiaries'));
    }

    public function store(CreateQuestion $request)
    {
        $data = $request->all();
        if (isset($data['options'])) {
            $data['options'] = implode('|', $data['options']);
        }
        $question = Question::create($data);

        $request->session()->flash('success', 'Pregunta creada con exito.');
        return redirect()->route('questions.edit', $question->id);
    }

    public function edit($question_id)
    {
        $subsidiaries = Subsidiary::all();
        $question = Question::find($question_id);
        return view('admin.surveys.questions.edit', compact('subsidiaries', 'question'));
    }

    public function update(CreateQuestion $request, $question_id)
    {
        $question = Question::find($question_id);

        $data = $request->all();
        if (isset($data['options'])) {
            $data['options'] = implode('|', $data['options']);
        }
        $question->update($data);

        $request->session()->flash('success', 'Pregunta actualizada con exito.');
        return redirect()->route('questions.edit', $question->id);
    }
}
