<?php

namespace App\Http\Controllers;

use App\Account;
use App\Attendance;
use App\CashRegister;
use App\Diary;
use App\Employee;
use App\Paysheet;
use App\Product;
use App\Customer;
use App\Sale;
use App\Service;
use App\LaundryService;
use App\Subsidiary;
use App\WarehouseRequest;
use App\WarehouseProduct;
use App\WarehouseMovement;
use App\WarehouseAdjustment;
use App\Surveys\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use League\Csv\Writer;
use SplTempFileObject;

class ReportsController extends Controller
{
    public function attendances(Request $request)
    {
        $now = Carbon::now();
        $today = $now->format('Y-m-d');
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $start = $now->format('Y-m-d');
        }

        $attendances = Attendance::date($start)
            ->orderBy('subsidiary_id', 'asc')
            ->orderBy('employee_id', 'asc')
            ->orderBy('time', 'asc')
            ->get();

        return view('admin.reports.attendances', compact('attendances', 'start', 'today'));
    }

    public function attendancesTime(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        $attendance->time = $request->time;
        $attendance->save();
        $request->session()->flash('success', 'Hora actualizada con exito');
        return back();
    }

    public function destroyAttendances(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        $attendance->delete();
        $request->session()->flash('success', 'Checada eliminada con exito');
        return back();
    }

    public function cashCutsByDate(Request $request) {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $start = $now->format('Y-m-d');
        }

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.cashCutsByDate', compact('subsidiaries', 'start'));
    }

    public function cashierMovementsResume(Request $request) {
        $oneWeekBefore = Carbon::now()->subDays(7);
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $oneWeekBefore->format('Y-m-d');
            $end = Carbon::now()->format('Y-m-d');
        }

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.cashierMovementsResume', compact('subsidiaries', 'start', 'end'));
    }

    public function issues(Request $request)
    {
        $now = Carbon::now();
        $barbers = Employee::all();
        if ($request->has('start')) {
            $barber = Employee::find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $barber = $barbers->first();
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        $issues = $barber->issues()->dates([$start, $end])->get();
        return view('admin.reports.issues', compact('issues', 'start', 'end', 'barber', 'barbers'));
    }

    public function schedules(Request $request)
    {
        $now = Carbon::now();
        $subsidiaries = $this->getSubsidiaries();
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfWeek();
            $end = Carbon::parse($request->end)->endOfWeek();
        } else {
            $start = Carbon::now()->startOfWeek();
            $end = Carbon::now()->endOfWeek();
            $dates = [];
        }
        $copy = $start->copy();
        while ($copy->lte($end)) {
            $dates[] = $copy->copy();
            $copy->addDay();
        }
        return view('admin.reports.schedules', compact('subsidiaries', 'dates', 'start', 'end'));
    }

    public function attendanceCard(Request $request)
    {
        $employees = Employee::with(['attendances','schedules'])->active()->get();
        if ($request->has('employee_id')) {
            $employee = Employee::with(['attendances','schedules'])->find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $employee = $employees->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        if($start > $end) {
            $request->session()->flash('error', 'La fecha inicial no puede ser posterior a la final');
            return redirect()->back();
        }

        return view('admin.reports.attendance_card', compact('employees', 'employee', 'start', 'end'));
    }

    public function notFrequentCustomers(Request $request)
    {
        $oneMonthBefore = Carbon::now()->subDays(30);
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $oneMonthBefore->format('Y-m-d');
            $end = $oneMonthBefore->format('Y-m-d');
        }

        $endCarbon = Carbon::createFromFormat('Y-m-d', $end);
        //dd($endCarbon);

        $customers = Customer::active()->notLaundry()->withPhoneOrEmail()->
        whereIn('id', function($q) use ($start, $end) {
            $q->select('customer_id')->from('sales')->whereBetween('date', [$start, $end]);
        })->
        whereNotIn('id', function($q) use ($endCarbon) {
            $q->select('customer_id')->from('sales')->where('created_at', '>=', $endCarbon);
            //$q->select('customer_id')->from('sales')->where('date', '>=', $end);
        })->paginate(8);
        //->toSql();
        //dd($oneMonthBefore);
        return view('admin.reports.notFrequentCustomers', compact('customers', 'start', 'end'));
    }

    public function salesByCustomer(Request $request)
    {
        $customer = null;
        //$customer_id = 0;
        $customers = Customer::active()->get();
        if ($request->has('customer_id')) {
        //    $customer_id = $request->customer_id;
            $customer = Customer::find($request->customer_id);
        } 
        return view('admin.reports.salesByCustomer', compact('customers', 'customer'));
    }

    public function customersByBarber(Request $request)
    {
        $employee_id = 0;
        $customers = null;
        $barbers = Employee::active()->barbers()->get();
        if ($request->has('employee_id')) {
            $employee_id = $request->employee_id;

            $customers = Customer::select('customers.id', 'customers.name', 'customers.phone', 'customers.email', DB::raw('count(s1.id) as `sales_count`'))
            ->join('sales as s1',function($q) use ($employee_id) {
                $q->on('customers.id', '=', 's1.customer_id')->where('s1.employee_id', $employee_id)
                ->where('s1.paid', true)->where('s1.finish', true)->whereNull('s1.canceled_by');
            })
            ->active()->withPhoneOrEmail()->where('customers.id', '!=', 7)
            ->groupBy('customers.id')
            ->orderBy('sales_count', 'desc')
            ->paginate(8);
        }

        return view('admin.reports.customersByBarber', compact('customers', 'barbers', 'employee_id'));   
    }

    public function subsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $subsidiary->sales()->select(DB::raw('id, date, SUM(subtotal) as sumtotal'))
        ->dates([$start, $end])->finished()->notCanceled()->groupBy('date')->get();
        return view('admin.reports.subsidiary', compact('subsidiaries', 'subsidiary', 'start', 'end', 'sales'));
    }

    public function subsidiaries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.subsidiaries', compact('subsidiaries', 'start', 'end'));
    }

    public function unsuppliedOrders(Request $request)
    {
        $subsidiaries = Subsidiary::active()->notLaundry()->get();
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        if($request->has('subsidiary_id')) {
            $subsidiary_id = $request->subsidiary_id;
        } else {
            $subsidiary_id = "";
        }

        $requests = WarehouseRequest::with(['subsidiary','products.product','movements.products', 'createdBy'])
        ->where('created_at','>=', $start." 00:00:00")
        ->where('created_at','<=', $end." 23:59:59");

        if($request->subsidiary_id != "") {
            $requests = $requests->where('subsidiary_id', $subsidiary_id);            
        }

        $requests = $requests->whereHas('movements', function($q) {
            $q->where('is_confirmed', true);
        })->orderBy('id','DESC')->get();

        //identifica los pedidos que no fueron surtidos completamente y acumula en "filteresRequests"
        $original_requests = $requests;
        $filteredRequests = [];
        foreach ($requests as $index => $req) {
            $removeRequest = false;
            $movement = $req->movements->first();
            foreach ($req->products as $product) {
                if($movementProduct = $movement->products->where('warehouse_product_id', $product->warehouse_product_id)->first()) {
                    if($product->quantity > $movementProduct->quantity) {
                        $removeRequest = true;
                    }
                } else {
                    $removeRequest = true;
                }
            }
            if($removeRequest) {
                $filteredRequests[] = $requests->pull($index);
            }
        }

        return view('admin.reports.unsupplied_orders', compact('filteredRequests','requests', 'subsidiaries', 'start', 'end', 'subsidiary_id'));
    }

    public function barber(Request $request)
    {
        $barbers = Employee::whereJob('Barbero')->active()->get();
        if ($request->has('employee_id')) {
            $barber = Employee::find($request->employee_id);
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $barber = $barbers->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $barber->sales()->select(DB::raw('id, subsidiary_id, date, SUM(total) as sumtotal, SUM(tip) as sumtip'));
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->whereBetween('date', [$start, $end])->finished()->notCanceled()->groupBy('date')->get();
        return view('admin.reports.barber', compact('barbers', 'barber', 'start', 'end', 'sales'));
    }

    public function turns(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $cash_regiters = CashRegister::where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $cash_regiters = $cash_regiters->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $cash_regiters = $cash_regiters->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });

        return view('admin.reports.turns', compact('cash_regiters', 'start', 'end'));
    }

    public function daily(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $cash_regiters = CashRegister::with(['sales','subsidiary'])->where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $cash_regiters = $cash_regiters->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $cash_regiters = $cash_regiters->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });
        return view('admin.reports.daily', compact('cash_regiters', 'start', 'end'));
    }

    public function accumulatedDaily(Request $request)
    {
        $now = Carbon::now();
        $start = $now->format('Y-m-d');
        $end = $now->format('Y-m-d');

        if ($request->has('start')) {
            $start = $request->start;
        }
        if ($request->has('end')) {
            $end = $request->end;
        } 

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.accumulated_daily', compact('subsidiaries', 'start', 'end'));
    }

    public function dailyLaundryServices(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $laundry_services = LaundryService::where('date', $start)->get();
        return view('admin.reports.daily_laundry_services', compact('laundry_services', 'start'));
    }

    public function servicesSubsidiaries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('start')) {
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.subsidiaries', compact('subsidiaries', 'start', 'end'));
    }

    public function servicesEmployees(Request $request)
    {
        $barbers = Employee::whereJob('Barbero')->active()->get();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.employees', compact('barbers', 'start', 'end'));
    }

    public function servicesSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $subsidiary->sales()
            ->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get();

        return view('admin.reports.services.subsidiary', compact('sales', 'subsidiary', 'subsidiaries', 'start', 'end'));
    }

    public function productsSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.products', compact('subsidiary', 'subsidiaries', 'start', 'end'));
    }

    public function dailyTurns(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $subsidiary = $subsidiaries->first();
            $start = $now->format('Y-m-d');
        }

        $cash_regiters = $subsidiary->cashRegisters()->where('date', $start)->get();

        return view('admin.reports.daily_turns', compact('cash_regiters', 'start', 'subsidiaries', 'subsidiary'));
    }

    public function entries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $entries = $subsidiary->entries()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $entries = $subsidiary->entries()->orderBy('created_at', 'desc')->get();
            }
        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $entries = $subsidiary->entries()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.entries', compact('entries', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function departures(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $departures = $subsidiary->departures()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $departures = $subsidiary->departures()->orderBy('created_at', 'desc')->get();
            }
        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $departures = $subsidiary->departures()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.departures', compact('departures', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function adjustments(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $adjustments = $subsidiary->adjustments()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $adjustments = $subsidiary->adjustments()->orderBy('created_at', 'desc')->get();
            }

        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $adjustments = $subsidiary->adjustments()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.adjustments', compact('adjustments', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function weeklyBarber(Request $request)
    {
        $barbers = Employee::barberOrInCharge()->active()->get();
        if ($request->has('employee_id')) {
            $barber = Employee::find($request->employee_id);
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $barber = $barbers->first();
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $sales = [];
        while ($copy->lte($end)) {
            $sales[] = $barber->weeklySales($copy->format('Y-m-d'))->select(DB::raw('id, subsidiary_id, date, SUM(subtotal) as sumtotal'))->groupBy('subsidiary_id')->get();
            $copy->addDay();
        }

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.weekly-barber', compact('barbers', 'barber', 'start', 'end', 'sales'));
    }

    public function weeklyServicesBarber(Request $request)
    {
        $barbers = Employee::whereJob('Barbero')->active()->get();
        $now = Carbon::now();
        if ($request->has('employee_id')) {
            $barber = Employee::find($request->employee_id);
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $barber = $barbers->first();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $sales = [];
        while ($copy->lte($end)) {
            $sales[] = $barber->weeklySales($copy->format('Y-m-d'))->select(DB::raw('id, subsidiary_id, date, SUM(subtotal) as sumtotal'))->groupBy('subsidiary_id')->get();
            $copy->addDay();
        }

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');
        return view('admin.reports.services.weekly-barbers', compact('barbers', 'barber', 'start', 'end', 'sales'));
    }

    public function promotionsByRange(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = Sale::with(['subsidiary', 'customer', 'cashRegister.employee'])
        ->dates([$start, $end])->withPromotion()->paginate(8);
        return view('admin.reports.services.with-promotion', compact('sales', 'start', 'end'));
    }

    public function attendancesControl(Request $request)
    {
        $employee = Employee::find($request->employee_id);
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end);
        return view('admin.reports.attendances.attendances-control', compact('employee', 'start', 'end'));
    }

    public function weeklyPaysheetBarber(Request $request)
    {
        $user = Auth::user();
        $barbers = Employee::whereJob('Barbero')->active()->get();
        
        if ($request->has('employee_id')) {
            $barber = Employee::find($request->employee_id);
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $barber = $barbers->first();
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-barber', compact('barbers', 'barber', 'start', 'end', 'dates', 'user'));
    }

    public function weeklyPaysheetBarberPrint($barber_id, $start, $end)
    {
        $user = Auth::user();
        $barbers = Employee::whereJob('Barbero')->active()->get();
        
        if ($barber_id) {
            $barber = Employee::find($barber_id);
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $barber = $barbers->first();
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-barber-print', compact('barbers', 'barber', 'start', 'end', 'dates', 'user'));
    }

    public function saleStatus(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->format('Y-m-d');
        }

        $sales = Sale::where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->finished()->orderBy('subsidiary_id')->get();
        return view('admin.reports.status', compact('sales', 'start'));
    }

    public function commissionStatus(Request $request)
    {
        $employees = Employee::where('job', 'Barbero')->get();
        if ($request->has('employee_id')) {
            $employee = Employee::find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $now = Carbon::now();
            $employee = $employees->first();
            $start = $now->format('Y-m-d');
            $end = $start;
        }

        $sales = $employee->sales()->whereBetween('date', [$start, $end]);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->finished()->notCanceled()->orderBy('subsidiary_id')->get();
        return view('admin.reports.commission_status', compact('sales', 'start', 'end', 'employees', 'employee'));
    }

    public function stocks()
    {
        $products = Product::all();
        $subsidiaries = Subsidiary::with(['products'])->active()->get();
        return view('admin.reports.inventories.stocks', compact('products', 'subsidiaries'));
    }

    public function warehouseStocks()
    {
        $products = WarehouseProduct::active()->orderBy('code')->get();
        return view('admin.reports.inventories.warehouse_stocks', compact('products'));
    }

    public function productsToOrder()
    {
        $products = WarehouseProduct::active()->orderBy('code')->get();
        return view('admin.reports.inventories.warehouse_products_to_order', compact('products'));
    }

    public function movementsByProduct(Request $request)
    {
        $products = WarehouseProduct::active()->orderBy('code', 'asc')->get();
        $product = $products->first();
        if ($request->has('product_id')) {
            $product = $products->where('id', $request->product_id)->first();
        }
        
        $product_id = $product->id;
        $movements = WarehouseMovement::with(['products'])->notCanceled()->confirmed()
            ->whereHas('products', function($q) use ($product_id) {
                $q->where('warehouse_product_id', $product_id);
            })->orderBy('id','desc')->limit(10)->get();

        $adjustments = WarehouseAdjustment::with(['product'])->where('warehouse_product_id', $product_id)->limit(10)->get();
        $types = WarehouseMovement::getEnum('types');

        return view('admin.reports.inventories.warehouse_movements_by_product', compact('movements', 'product', 'products', 'adjustments', 'types'));
    }

    public function weeklyPaysheetResume(Request $request)
    {
        $employees = Employee::active()
            ->orderByRaw('job = ? desc', ['Cajero'])
            ->orderByRaw('job = ? desc', ['Barbero'])
            ->orderByRaw('job = ? desc', ['Supervisor'])
            ->orderByRaw('job = ? desc', ['Auxiliar'])
            ->orderBy('short_name', 'asc')
            ->paginate(8);
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-resume', compact('employees', 'start', 'end', 'dates'));
    }

    public function weeklyPaysheetResumePost(Request $request)
    {
        ini_set('max_input_vars', 1000000);
        $data = $request->all();
        foreach ($data['employee'] as $paysheet) {
            if (Paysheet::where('key', $data['start'] . '-' . $data['end'])->where('employee_id', $paysheet['id'])->first()) {
                continue;
            }
           
            $tt = Paysheet::create([
                'key' => $data['start'] . '-' . $data['end'],
                'start' => $data['start'],
                'end' => $data['end'],
                'employee_id' => $paysheet['id'] ?? 0,
                'total' => $paysheet['total'] ?? 0,
                'asistencia' => $paysheet['asistencia'] ?? 0,
                'asistencia_pay' => (($paysheet['asistencia'] ?? 0) > 0 ? true : false),
                'puntualidad' => $paysheet['puntualidad'] ?? 0,
                'puntualidad_pay' => (($paysheet['puntualidad'] ?? 0) > 0 ? true : false),
                'productividad' => $paysheet['productividad'] ?? 0,
                'productividad_pay' => (($paysheet['productividad'] ?? 0) > 0 ? true : false),
                'excellence' => $paysheet['excellence'] ?? 0,
                'excellence_pay' => (($paysheet['excellence'] ?? 0) > 0 ? true : false),
                'extra' => $paysheet['extra'] ?? 0,
                'extra_pay' => (($paysheet['extra'] ?? 0) > 0 ? true : false),
                'otros_ingresos' => $paysheet['otros_ingresos'] ?? 0,
                'repairs' => $paysheet['repairs'] ?? 0,
                'cardtips' => $paysheet['cardtips'] ?? 0,
                'products' => $paysheet['products'] ?? 0,
                'total_ingreso' => $paysheet['total_ingresos'] ?? 0,
                'prestamo' => $paysheet['prestamo'] ?? 0,
                'faltas' => $paysheet['total_faltas'] ?? 0,
                'faltas_pay' => ($paysheet['total_faltas'] > 0 ? true : false),
                'uniforme' => $paysheet['uniforme'] ?? 0,
                'infonavit' => $paysheet['monto_infonavit'] ?? 0,
                'otras' => $paysheet['otras'] ?? 0,
                'discount_repairs' => $paysheet['discount-repairs'] ?? 0,
                'total_deducciones' => $paysheet['desctotal'] ?? 0,
                'neto' => $paysheet['neto'] ?? 0,
                'depositado' => $paysheet['depositado'] ?? 0,
                'diferencia' => $paysheet['diferencia'] ?? 0,
            ]);
             if ($paysheet['id'] == 25) {
                # code...
                // dd($tt);
            }
        }

        return redirect()->route('paysheets.index');
    }

    public function weeklyPaysheetEmployee(Request $request)
    {
        $employees = Employee::where('job', '<>', 'Barbero')->get();
        if ($request->has('employee_id')) {
            $employee = Employee::find($request->employee_id);
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $employee = $employees->first();
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-employee', compact('employees', 'employee', 'start', 'end', 'dates'));
    }

    public function weeklyPaysheetEmployeePrint($employee_id, $start, $end)
    {
        $employees = Employee::where('job', '<>', 'Barbero')->get();
        if (!empty($employee_id)) {
            $employee = Employee::find($employee_id);
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $employee = $employees->first();
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-employee-print', compact('employees', 'employee', 'start', 'end', 'dates'));
    }

    public function servicesSubsidiariesResume(Request $request)
    {
        $services = Service::notLaundry()->get();
        
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');;
            $end = Carbon::parse($request->end)->format('Y-m-d');;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }
        $subsidiaries = Subsidiary::with(['sales' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end])
            ->where('paid', true)
            ->where('finish', true)
            ->whereNull('canceled_by');
        }])->active()->get();

        return view('admin.reports.services.subsidiaries-services', compact('services', 'subsidiaries', 'start', 'end'));
    }

    public function productsComisiones(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        $sales = Sale::whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->orderBy('employee_id')
            ->get()->groupBy('employee_id');

        return view('admin.reports.comisiones-products', compact('sales', 'start', 'end'));
    }

    public function salesTips(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        $sales = Sale::whereBetween('date', [$start, $end]);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->finished()->notCanceled()->orderBy('employee_id')->get()->groupBy('employee_id');
        return view('admin.reports.sales-tips', compact('sales', 'start', 'end'));
    }

    public function weeklyCommisionsResume(Request $request)
    {
        $employees = Employee::whereJob('Barbero')->active()->orderBy('short_name', 'asc')->get();
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.weekly-comission', compact('employees', 'start', 'end', 'dates'));
    }

    public function weeklyCommisionsResumePost(Request $request)
    {
        $res = $request->all();
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Nombre', 'Comision', 'Puntualidad', 'Productos', 'propinas', 'Total']);
        foreach ($res['employee'] as $data) {
            $employee = Employee::find($data['id']);
            $csv->insertOne([
                $employee->name,
                number_format($data['commission'], 2, '.', ''),
                number_format($data['appoint'], 2, '.', ''),
                number_format($data['products'], 2, '.', ''),
                number_format($data['tip'], 2, '.', ''),
                number_format($data['bigtotal'], 2, '.', ''),
            ]);

        }
        $csv->output("Resumen-comisiones-ventas-{$request->start}-{$request->end}.csv");
        die;
    }

    public function diaries(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $diaries_query = Diary::dates([$start, $end]); 
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $diaries_query = $diaries_query->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $subsidiary_total_atend = $diaries_query->bySubsidiary()->isAttend()->count();
        $call_center_total_atend = $diaries_query->callCenter()->isAttend()->count();
        $subsidiary_total_active = $diaries_query->bySubsidiary()->active()->count();
        $call_center_total_active = $diaries_query->callCenter()->active()->count();
        $subsidiary_total_cancel = $diaries_query->bySubsidiary()->cancel()->count();
        $call_center_total_cancel = $diaries_query->callCenter()->cancel()->count();
        
        $subsidiary_total = $subsidiary_total_atend + $subsidiary_total_active;
        $call_center_total = $call_center_total_atend + $call_center_total_active;
        
        $data = compact('start', 'end', 'subsidiary_total', 'call_center_total', 'subsidiary_total_cancel', 'call_center_total_cancel', 'subsidiary_total_atend', 'call_center_total_atend', 'subsidiary_total_active', 'call_center_total_active');
        return view('admin.reports.diaries', $data);
    }

    public function promos(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales_dates_finished = Sale::dates([$start, $end])->where('diary_id', '>', '0')->finished()->get();

        $dates_total = $sales_dates_finished->count();
        $dates_total_atend = 0;
        foreach ($sales_dates_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $dates_total_atend += (is_null($services_max_price) ? 0 : $services_max_price) * .10;
        }

        $sales_birthday_finished = Sale::dates([$start, $end])->where('has_birthday', 1)->finished()->get();
        $birthday_total = $sales_birthday_finished->count();
        $birthday_total_atend = 0;
        foreach ($sales_birthday_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $birthday_total_atend += is_null($services_max_price) ? 0 : $services_max_price;
        }

        $sales_courtesy_finished = Sale::dates([$start, $end])->finished()
        ->whereIn('id', function($q) {
            $q->select('sale_id')->from('sale_payments')->where('method', 'Cortesia');
        })->get();
        /*
        ->whereHas('payments', function ($query) {
            $query->where('method', 'Cortesia');
        })->finished()->get();
        */
        $cortesy_total = $sales_courtesy_finished->count();

        $cortesy_total_atend = 0;
        foreach ($sales_courtesy_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $cortesy_total_atend += is_null($services_max_price) ? 0 : $services_max_price;
        }

        $data = compact('start', 'end', 'dates_total', 'dates_total_atend', 'birthday_total', 'birthday_total_atend', 'cortesy_total', 'cortesy_total_atend');
        return view('admin.reports.promos', $data);
    }

    public function calendar(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        return view('admin.reports.calendar', compact('subsidiaries'));
    }

    public function getCalendar(Request $request)
    {
        if ($request->subsidiary_id == 'all') {
            $sales_object = new Sale;
        }else {
            $sales_object = Subsidiary::find($request->subsidiary_id);
        }
        $sales = [];
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end);
        $copy = $start->copy();
        while ($copy->lte($end)) {
            if ($request->subsidiary_id == 'all') {
                $total = $sales_object->where('date', $copy->format('Y-m-d'))->finished()->notCanceled()->sum('subtotal');
            }else {
                $total = $sales_object->sales()->where('date', $copy->format('Y-m-d'))->finished()->notCanceled()->sum('subtotal');
            }
            $sales[] = [
                'start' => $copy->format('Y-m-d'),
                'title' => number_format($total, 2, '.', ',')
            ];
            $copy->addDay();
        }
        return response()->json($sales);
    }

    public function salesRepairs(Request $request)
    {
        $now = Carbon::now();
        $barbers = Employee::whereJob('Barbero')->active()->get();
        if ($request->has('start')) {
            $barber = Employee::find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $barber = $barbers->first();
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        };

        return view('admin.reports.repairs', compact('barbers', 'barber', 'start', 'end'));
    }

    public function surveyBarber(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $questions = Question::satisfactions()->active()->get();
        $question = $questions->first();
        $barbers = Employee::whereJob('Barbero')->active()->get();
        $barber_id = $request->get('barber_id', 'all');
        if ($barber_id == 'all') {
            $barbersReport = $barbers;
        }else {
            $barbersReport = Employee::whereJob('Barbero')->active()->whereIn('id', [$barber_id])->get();
        }

        if ($request->has('question_id')) {
            $question = Question::find($request->get('question_id'));
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d  H:i:s');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d  H:i:s');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d  H:i:s');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d  H:i:s');
        }
        return view(
            'admin.reports.surveys.barber',
            compact('subsidiaries', 'questions', 'question', 'barbers', 'start', 'end', 'barber_id', 'barbersReport')
        );
    }

    public function surveySubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $questions = Question::satisfactions()->active()->get();
        $question = $questions->first();
        $subsidiary_id = $request->get('subsidiary_id', 'all');
        if ($subsidiary_id == 'all') {
            $subsidiariesReport = $subsidiaries;
        }else {
            $subsidiariesReport = Subsidiary::where('is_active', true)->whereIn('id', [$subsidiary_id])->get();
        }

        if ($request->has('question_id')) {
            $question = Question::find($request->get('question_id'));
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d H:i:s');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d H:i:s');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d H:i:s');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d H:i:s');
        }
        return view(
            'admin.reports.surveys.subsidiary',
            compact('subsidiaries', 'questions', 'question', 'start', 'end', 'subsidiary_id', 'subsidiariesReport')
        );
    }

    public function expenditureSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $accounts = Account::all();
        $subsidiary_id = $request->get('subsidiary_id', 'all');
        if ($subsidiary_id == 'all') {
            $subsidiariesReport = $subsidiaries;
        }else {
            $subsidiariesReport = Subsidiary::whereIn('id', [$subsidiary_id])->get();
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        return view(
            'admin.reports.expenditures.subsidiary',
            compact('subsidiaries', 'start', 'end', 'subsidiary_id', 'subsidiariesReport', 'accounts')
        );
    }

    public function expenditureMonthly(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $accounts = Account::all();

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        return view(
            'admin.reports.expenditures.monthly',
            compact('subsidiaries', 'start', 'end', 'accounts')
        );
    }

    public function expenditureAccount(Request $request, $account_id)
    {
        $subsidiaries = $this->getSubsidiaries();
        $account = Account::find($account_id);
        $user = Auth::user();

        if($account == null) {
            $request->session()->flash('error', 'La cuenta deseada no existe.');
            return redirect()->to("/home/");
        }

        $expenditures = $account->expenditures()->dates([$request->start, $request->end]);
        if ($request->has('subsidiary_id')) {
            $expenditures = $expenditures->where('subsidiary_id', $request->subsidiary_id);
        }else {
            $expenditures->whereIn('subsidiary_id', $subsidiaries->pluck('id'));
        }
        $expenditures = $expenditures->orderBy('date', 'desc')->get();

        return view('admin.reports.expenditures.account', compact('expenditures', 'account', 'user')
        );
    }

    public function flow(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        $laundry_services = LaundryService::whereBetween('date', [$start, $end])->get();
        return view('admin.reports.flow', compact('subsidiaries', 'laundry_services', 'start', 'end'));
    }

    private function getSubsidiaries() {
        $subsidiaries = Subsidiary::active();
        $user = Auth::user();
        if($user->access_subsidiary_id != null) {
            $subsidiaries = $subsidiaries->where('id',$user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }
}
