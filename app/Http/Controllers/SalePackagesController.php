<?php

namespace App\Http\Controllers;

use App\Sale;
use App\SalePackage;
use Illuminate\Http\Request;

class SalePackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $subsidiary_code, $sale_id, $sale_package_id)
    {
        $data = $request->all();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary'])->find($sale_id);
        $sale_package = SalePackage::with('sale','products','services')->find($sale_package_id);
        if($sale_package->products){
            $sale_package->products()->delete();
        }
        if($sale_package->services){
            $sale_package->services()->delete();
        }
        $sale_package->delete();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary', 'payments'])->find($sale_id);
        $sale->updateTotals();
        $message = 'Haz eliminado el paquete.';
        return response()->json(compact('sale', 'message'));
    }
}
   