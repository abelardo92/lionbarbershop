<?php
namespace App\Http\Controllers;

use App\Attendance;
use App\Customer;
use App\Diary;
use App\Employee;
use App\Events\MakeSurvey;
use App\ExchangeRate;
use App\MiniCutPetition;
use App\Sale;
use App\SalePayment;
use App\SalePackage;
use App\SaleProduct;
use App\SaleService;
use App\Service;
use App\Subsidiary;
use App\Promotion;
use App\Product;
use App\Package;
use App\CashRegister;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\SubDomainHelper;
use App\Image;
use App\Traits\ImageableTrait;
use Illuminate\Support\Facades\Hash;
use App\Notifications\SaleMessage;

class SalesController extends Controller
{
    use SubDomainHelper;
    use ImageableTrait;

    private function passwordCorrect($suppliedPassword, $user)
    {
        return Hash::check($suppliedPassword, $user->password, []);
    }
    public function index($subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if(!$subsidiary->isAdmon()) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }

        $sales = [];
        $finished_sales = [];
        $finished_sales_count = 0;
        $finished_sales_total = 0;
        if ($cash_register) {
            //$cash_register
            if(!$subsidiary->isAdmon()) {
                $sales = $cash_register->subsidiary->sales()->with(['employee', 'employee2', 'customer', 'services.service', 'products.product', 'packages.package.packageServices', 'customerSubsidiary','payments', 'subsidiary', 'image', 'services.service.subsidiaryPrices'])->today()->active()->get();
            } else {
                $sales = $cash_register->sales()->with(['employee', 'employee2', 'customer', 'services.service', 'products.product', 'packages.package.packageServices', 'customerSubsidiary','payments', 'subsidiary', 'image', 'services.service.subsidiaryPrices'])->active()->get();
            }
            
            foreach ($sales as $sale) {
                $sale->setOrder();
            }
            if(!$subsidiary->isAdmon()) {
                $finished_sales = $cash_register->sales()->today()->finished()->notCanceled();
            } else {
                $finished_sales = $cash_register->sales()->finished()->notCanceled();
            }
            

            $finished_sales_count = $finished_sales->get()->count();
            $finished_sales_total = $finished_sales->sum('total');
        }
        $not_finished_sales = null;
        if($subsidiary->is_laundry) {
            $not_finished_sales = Sale::with(['employee', 'employee2', 'customer', 'services.service', 'products.product', 'packages.package.packageServices', 'customerSubsidiary','payments', 'subsidiary'])->beforeToday()->notFinished()->where('subsidiary_id',$cash_register->subsidiary->id)->get();
            foreach ($not_finished_sales as $sale) {
                $sale->setOrder();
            }
        }
        return response()->json(compact('sales', 'not_finished_sales', 'finished_sales_count', 'finished_sales_total'));
    }

    public function currentSales()
    {
        $subsidiary_code = $this->hasSubdomain();
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if(!$subsidiary->isAdmon()) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }
        $sales = [];
        $diaries = Diary::with(['employee','customer'])->where('subsidiary_id',$subsidiary->id)
        ->today()->notCancel()->afterCurrentTime()->doesntHave('sale')->orderBy('time', 'asc')->get();
        if ($cash_register) {
            $sales = $cash_register->sales()->with(['employee', 'employee2', 'customer', 'diary']);
            if(!$subsidiary->isAdmon()) {
                $sales = $sales->today();
            }
            $sales = $sales->active()->notFinished()->orderBy('created_at', 'asc')->get();
            foreach ($sales as $sale) {
                $sale->setOrder();
            }
        }
        return response()->json(compact('sales', 'diaries'));
    }

    public function activeSales()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            return view('admin.sales.activeSales', compact('subsidiary'));
        }
        return redirect()->back();
    }

    public function create(Request $request, $subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if(!$subsidiary->isAdmon()) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }

        if ($cash_register == null) {
            session()->flash('success', 'No puedes crear una venta hasta que abras tu caja.');
            return redirect()->back();
        }

        $employee_ids = $cash_register->subsidiary->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->today()->active()->finish(false)->get()->map(function ($sale) {
            return $sale->employee_id;
        })->toArray();

        if(!$subsidiary->isAdmon()) {
            $barbers = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'));
            $barbers = $barbers->where('subsidiary_id', $subsidiary->id)->today();
            $barbers = $barbers->groupBy('employee_id')->get();
            $barbers = $barbers->whereIn('count_attend', [1, 3]);
            $barbers = $barbers->where('employee.job', 'Barbero')
            ->map(function ($key) {
                return $key->employee;
            })->filter(function ($employee) use ($employee_ids) {
                return !in_array($employee->id, $employee_ids);
            });
            $barbers = $barbers->merge([46 => Employee::find(46)]);

            $suggestedBarber = $barbers[0];

            //obtener barbero mas inactivo.
            foreach ($barbers as $barber) {
                if($barber->id != 46) {
                    $lastSale = $barber->lastSale();
                    $suggestedLastSale = $suggestedBarber->lastSale();
                    $barberTime = Carbon::now();
                    $suggestedBarberTime = Carbon::now();

                    if($lastSale != null) {
                        $barberTime = $lastSale->updated_at;
                    } else {
                        if($attendance = $barber->attendances()->where('type','entrada')->where('subsidiary_id',$subsidiary->id)->where('date',date('Y-m-d'))->get()->first()){
                        $barberTime = $attendance->updated_at;
                        }
                    }

                    if($suggestedLastSale != null){
                        $suggestedBarberTime = $suggestedLastSale->updated_at;
                    } else {
                        if($attendance = $suggestedBarber->attendances()->where('type','entrada')->where('subsidiary_id',$subsidiary->id)->where('date',date('Y-m-d'))->get()->first()){
                            $suggestedBarberTime = $attendance->updated_at;
                        }
                    }
                    if($barberTime != null && $suggestedBarberTime != null && $barberTime < $suggestedBarberTime) {
                        $suggestedBarber = $barber;
                    }
                }
            }
        } else {
            $barbers = Employee::where('job', 'Barbero')->get();
            $suggestedBarber = $barbers[0];
        }

        //$customers = Customer::notLaundry()->active()->get();
        $services = Service::where('subsidiary_id', null)->orWhere('subsidiary_id', $subsidiary->id)->notLaundry()->get();
        $products = $cash_register->subsidiary->products;
        $diaries = $cash_register->subsidiary->diaries()->with('customer')->today()->active()
        ->orderBy('time', 'asc')->get();

        $customer_id = $request->customer_id ? $request->customer_id: 0;
        
        return view('admin.sales.create', compact('subsidiary', 'barbers', 'services', 'customer_id', 'diaries', 'products', 'suggestedBarber'));
    }

    public function createLaundry(Request $request)
    {
        $data = $request->all();
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        
        if (!$cash_register = $subsidiary->cashRegisters()->open()->today()->first()) {
            session()->flash('success', 'No puedes crear una venta hasta que abras tu caja.');
            return redirect()->back();
        }
        
        $subsidiaries = Subsidiary::active()->notLaundry()->get(); 
        $employee_ids = $subsidiary->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->today()->active()->finish(false)->get()->map(function ($sale) {
            return $sale->employee_id;
        })->toArray();

        $washers = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
            ->where('subsidiary_id', $subsidiary->id)
            ->today()
            ->groupBy('employee_id')
            ->get()
            ->whereIn('count_attend', [1, 3])
            ->where('employee.job', 'Lavador')
            ->map(function ($key) {
                return $key->employee;
            })->filter(function ($employee) use ($employee_ids) {
            return !in_array($employee->id, $employee_ids);
        });

        $washers = $washers->merge([46 => Employee::find(46)]);
        
        $customers = Customer::Laundry()->active()->get();
        $services = Service::where('subsidiary_id', null)->orWhere('subsidiary_id', $subsidiary->id)->laundry()->get();
        $products = $subsidiary->products;
        $packages = Package::all();
        $diaries = $subsidiary->diaries()->with('customer')->today()->active()->orderBy('time', 'asc')->get();

        $customer_id = $request->customer_id ? $request->customer_id : 0;

        return view('admin.sales.createLaundry', compact('subsidiary', 'customers', 'washers', 'services', 'customer_id', 'diaries', 'products', 'packages', 'subsidiaries'));
    }

    public function edit($subsidiary_code, $id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $sale = Sale::find($id);
        $attendances = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
            ->where('subsidiary_id', $subsidiary->id)
            ->today()
            ->groupBy('employee_id')
            ->get()
            ->whereIn('count_attend', [1, 3]);
        $customers = Customer::notLaundry()->active()->get();
        $services = Service::where('subsidiary_id', null)->orWhere('subsidiary_id', $subsidiary->id)->get();

        return view('admin.sales.edit', compact('subsidiary', 'customers', 'attendances', 'services', 'sale'));
    }

    public function editPayments($id)
    {
        $sale = Sale::with(['customer','payments'])->find($id);
        $barbers = Employee::barbers()->get();
        return view('admin.sales.editPayments', compact('sale', 'barbers'));
    }

    public function updatePayments(Request $request, $id)
    {
        $data = $request->all();
        $sale = Sale::find($id);
        $sale->update($data);
        
        $methods = $data['methods'];
        $coins = $data['coins']; 
        $totals = $data['totals']; 
        foreach ($data['payment_ids'] as $index => $id) {
            if($id != 0) {
                $payment = SalePayment::find($id);
                $payment->method = $methods[$index];
                $payment->coin = $coins[$index]; 
                $payment->total = $totals[$index]; 
                $payment->save();
            } else {
                $sale->payments()->create([
                    'method' => $methods[$index],
                    'coin' => $coins[$index],
                    'total' => $totals[$index],
                ]);
            }
        }
        $request->session()->flash('success', 'Pagos actualizados con éxito');
        return redirect()->back();
    }

    public function editImage($id) 
    {
        $sale = Sale::with(['image'])->find($id);
        return view('admin.sales.editImage', compact('sale'));
    }

    public function updateImage(Request $request, $id)
    {
        $sale = Sale::find($id);
        $this->CreateFile($sale, $request, "sales/{$sale->id}");
        $request->session()->flash('success', 'Imagen actualizada con éxito');
        return redirect()->back();
    }

    public function store(Request $request, $subsidiary_code)
    {
        $data = $request->all();
        $data['exchange_rate_id'] = ExchangeRate::latest()->first()->id;
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if (!empty($data['diary_id'])) {
            $diary = Diary::find($data['diary_id']);
            $data['customer_id'] = $diary->customer->id;
            $data['has_discount'] = $diary->has_discount;
            $diary->attend = true;
            $diary->save();
        } else {
            $data['diary_id'] = 0;
        }

        if (empty($data['customer_id'])) {
            $request->session()->flash('error', 'Necesitas seleccionar un cliente o una cita.');
            return redirect()->back();
        }

        if(!$subsidiary->isAdmon()) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first(); 
            $data['date'] = Carbon::now()->format('Y-m-d');          
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
            $data['date'] = $cash_register->date;
        }

        $data['cash_register_id'] = $cash_register->id;

        $folio = 1;
        if ($last = $cash_register->subsidiary->sales()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        $data['folio'] = $folio;
        if($data['employee2_id'] == "0") $data['employee2_id'] = null;
        $sale = $cash_register->subsidiary->sales()->create($data);

        $activePromotion = $this->activePromotion();
        $employee_id = $data['employee_id'];
        $employee = Employee::find($employee_id);

        if (!empty($data['service_id'])) {
            foreach ($data['service_id'] as $service_id) {
                
                $service = Service::with('subsidiaryPrices')->find($service_id);
                $cost = $service->getCost($employee->commission_number);
                $currentPrice = $service->currentPrice($cash_register->subsidiary->id);

                $sale->services()->create([
                    'service_id' => $service_id,
                    'price' => $currentPrice,
                    'qty' => 1,
                    'has_promotion' => $service->hasPromotion($cash_register->subsidiary->id),
                    'original_price' => $service->originalPrice($cash_register->subsidiary->id),
                    'cost' => $cost
                ]);
            }
        }

        if (!empty($data['product_id'])) {
            foreach ($data['product_id'] as $product_id) {
                $product = Product::find($product_id);
                $productPrice = $product->sell_price;

                if($activePromotion && $activePromotion->must_buy_a_service && !empty($data['service_id']) && !$activePromotion->promotionProductExceptions()->where('product_id',$product_id)->get()->first() && ($subsidiary->id == $activePromotion->subsidiary_id || $activePromotion->subsidiary_id == null)){
                    //Si existe una promocióm, requiere la compra de un servicio, existe un servicio, es una sucursal válida y no está excento de la promoción ...
                    $productPrice = ($productPrice * (100 - $activePromotion->products_discount_percentaje)) / 100;
                }

                $sale->products()->create([
                    'product_id' => $product_id,
                    'qty' => $data['product_qty'][$product_id],
                    'price' => $productPrice,
                    'original_price' => $product->sell_price
                ]);
            }
        }
        $sale->updateTotals();
        $this->CreateFile($sale, $request, "sales/{$sale->id}");
        
        $request->session()->flash('success', 'Servicio creado con exito');
        return redirect()->to('/home');
    }

    public function storeLaundry(Request $request)
    {
        $data = $request->all();
        $data['date'] = Carbon::now()->format('Y-m-d');
        $data['exchange_rate_id'] = ExchangeRate::latest()->first()->id;

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }

        $cash_register = $subsidiary->cashRegisters()->open()->today()->first();
        $data['cash_register_id'] = $cash_register->id;

        if (empty($data['employee_id'])) {
            $request->session()->flash('error', 'Necesitas seleccionar un empleado(a).');
            return redirect()->back();
        }

        if (!isset($data['customer_subsidiary_id']) || 
            (isset($data['customer_subsidiary_id']) && $data['customer_subsidiary_id'] == 0 && !isset($data['customer_id'])) || 
            (isset($data['customer_id']) && $data['customer_id'] == 0 && empty($data['comments'])) ) {
            $request->session()->flash('error', 'Necesitas seleccionar un cliente');
            return redirect()->back();
        }

        if(!$request->has('product_ids') && !$request->has('service_ids') && !$request->has('package_ids')){
            $request->session()->flash('error', "La venta debe tener un servicio, producto o paquete");
            return redirect()->back();
        }

        $folio = 1;
        if ($last = $subsidiary->sales()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        $data['folio'] = $folio;
        if($data['customer_subsidiary_id'] == 0) {
            $data['customer_subsidiary_id'] = null;
        } 
        $sale = $subsidiary->sales()->create($data);

        $activePromotion = $this->activePromotion();

        if (!empty($data['service_ids'])) {
            foreach ($data['service_ids'] as $index => $service_id) {
                $service = Service::find($service_id);
                $currentPrice = $service->currentPrice();
                $has_promotion = false;
                if($service->sell_price != $currentPrice){
                    $has_promotion = true;
                }
                $sale->services()->create([
                    'service_id' => $service_id,
                    'price' => $currentPrice,
                    'qty' => $data['service_quantities'][$index],
                    'has_promotion' => $has_promotion,
                    'original_price' => $service->sell_price
                ]);
            }
        }

        if (!empty($data['product_ids'])) {
            foreach ($data['product_ids'] as $index => $product_id) {
                $product = Product::find($product_id);
                $productPrice = $product->sell_price;

                if($activePromotion && $activePromotion->must_buy_a_service && !empty($data['service_id']) && !$activePromotion->promotionProductExceptions()->where('product_id',$product_id)->get()->first() && ($subsidiary->id == $activePromotion->subsidiary_id || $activePromotion->subsidiary_id == null)){
                    //Si existe una promoción, requiere la compra de un servicio, existe un servicio, es una sucursal válida y no está excento de la promoción ...
                    $productPrice = ($productPrice * (100 - $activePromotion->products_discount_percentaje)) / 100;
                }

                $sale->products()->create([
                    'product_id' => $product_id,
                    'price' => $productPrice,
                    'qty' => $data['product_quantities'][$index],
                    'original_price' => $product->sell_price
                ]);
            }
        }

        if (!empty($data['package_ids'])) {
            foreach ($data['package_ids'] as $index => $package_id) {
                
                $package = package::with(['packageProducts.product','packageServices.service'])->find($package_id);
                $salePackage = $sale->packages()->create([
                    'package_id' => $package_id,
                    'quantity' => $data['package_quantities'][$index],
                    'price' => $package->price,
                    'original_price' => $package->price
                ]);

                foreach ($package->packageProducts as $index => $packageProduct) {
                   $salePackageProduct = $salePackage->products()->create([
                        'product_id' => $packageProduct->product_id,
                        'quantity' => $packageProduct->quantity,
                        'price' => $packageProduct->product->sell_price,
                   ]); 
                }

                foreach ($package->packageServices as $index => $packageService) {
                   $salePackageService = $salePackage->services()->create([
                        'service_id' => $packageService->service_id,
                        'quantity' => $packageService->quantity,
                        'price' => $packageService->service->sell_price,
                   ]); 
                }
            }
        } 

        $sale->updateTotals();

        $request->session()->flash('success', 'Servicio creado con exito');
        return redirect()->to('/home');
    }

    public function storePaidAdvances(Request $request)
    {
        $data = $request->all();
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

            if(!$subsidiary->isAdmon()) {
                $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
            } else {
                $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
            }
            
            $sale = Sale::find($data['sale_id']);

            if ($data['payment_gateways']['efectivo']) {
                $money_total = 0;
                if ($data['coin_mxn']) {
                    $money_total += $data['qty_money_mxn'];
                    $sale->payments()->create([
                        'method' => 'Efectivo',
                        'coin' => 'mxn',
                        'total' => $data['qty_money_mxn'],
                        'type' => 1,
                        'cash_register_id' => $cash_register->id
                    ]);
                }
                if ($data['coin_usd']) {
                    $usd_total = $data['qty_money_usd'] * $cash_register->exchangeRate->rate;
                    $money_total += $usd_total;

                    $sale->payments()->create([
                        'method' => 'Efectivo',
                        'coin' => 'usd',
                        'total' => $usd_total,
                        'type' => 1,
                        'cash_register_id' => $cash_register->id
                    ]);
                }

                $money_total -= $data['money_change'];
                $cash_register->total = $cash_register->total + $money_total;
                $cash_register->save();
            }
            if ($data['payment_gateways']['tarjeta']) {
                $card_total = $data['qty_card'];
                $sale->payments()->create([
                    'method' => 'Tarjeta',
                    'total' => $card_total,
                    'comment' => "{$data['card_brand']} - {$data['last_four']}",
                    'type' => 1,
                    'cash_register_id' => $cash_register->id
                ]);
            }
        }
    }

    public function show($subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $cash_register = $subsidiary->cashRegisters()->open()->today()->first();
        return response()->json([
            'message' => 'Caja abierta',
            'cash_register' => $cash_register,
        ]);
    }

    public function update(Request $request, $subsidiary_code, $id)
    {
        $data = $request->all();
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if(!$subsidiary->isAdmon()) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }

        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary'])->find($id);

        if(isset($data['barber2_id'])) {
            $barber2_id = $data['barber2_id'];
        } else {
            $barber2_id = "";
        }

        if ($data['status'] == 'attend') {
            $sale->markAsAttend($data['barber_id'], $barber2_id);
            $message = 'Marcado como atendiendo';
            $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->find($id);
            $employee_ids = $cash_register->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->today()->active()->finish(false)->get()->map(function ($sale) {
                return $sale->employee_id;
            })->toArray();
            $attendances = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
                ->where('subsidiary_id', $cash_register->subsidiary->id)
                ->today()
                ->groupBy('employee_id')
            // ->whereNotIn('employee_id', $employee_ids)
                ->get()
                ->whereIn('count_attend', [1, 3]);
            $barbers = $attendances->where('employee.job', 'Barbero');
            $barbers = $barbers->map(function ($key) {
                return $key->employee;
            })->filter(function ($employee) use ($employee_ids) {
                return !in_array($employee->id, $employee_ids);
            });
            $barbers = $barbers->merge([46 => Employee::find(46)]);
            return response()->json(compact('sale', 'message', 'barbers'));
        }

        if ($data['status'] == 'finish') {
            $sale->markAsFinish($data['barber_id'], $barber2_id);
            $message = 'Marcado como finalizado';

            $employee_ids = $cash_register->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->today()->active()->finish(false)->get()->map(function ($sale) {
                return $sale->employee_id;
            })->toArray();
            $attendances = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
                ->where('subsidiary_id', $cash_register->subsidiary->id)
                ->today()
                ->groupBy('employee_id')
            // ->whereNotIn('employee_id', $employee_ids)
                ->get()
                ->whereIn('count_attend', [1, 3]);
            $barbers = $attendances->where('employee.job', 'Barbero');
            $barbers = $barbers->map(function ($key) {
                return $key->employee;
            })->filter(function ($employee) use ($employee_ids) {
                return !in_array($employee->id, $employee_ids);
            });
            $barbers = $barbers->merge([46 => Employee::find(46)]);
            $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->find($id);
            return response()->json(compact('sale', 'message', 'barbers'));
        }

        if ($sale->paid) {
            $already_paid = true;
            $message = 'Ya haz realizado el pago.';
            return response()->json(compact('already_paid', 'message'));
        }

        if($sale->has_birthday && isset($data['payment_gateways']['birthday'])) {
            if($data['payment_gateways']['birthday']) {
                $sale->has_discount = false;
            } else {
                $sale->has_birthday = false;
            }
        }

        $sale->update($data);
        $sale->markAsPaid($data['barber_id'], $barber2_id);

        if($subsidiary->is_laundry) {
            $now = Carbon::now();
            $created_at = Carbon::parse($sale->created_at);
            $service_almacenaje = Service::find(21);
            if($created_at->diffInDays($now) >= 7) {
                $sale->services()->create([
                    'qty' => 1,
                    'service_id' => $service_almacenaje->id,
                    'price' => $service_almacenaje->sell_price,
                    'has_promotion' => false,
                    'original_price' => $service_almacenaje->sell_price
                ]);
                $sale->updateTotals();
            }
        }

        //acumula si no es "PUBLICO EN GENERAL"
        if($sale->customer != null) {
            if(!(isset($data['payment_gateways']['wallet']) && $data['payment_gateways']['wallet']) && $sale->services->where('id', '!=', 16)->count() && $sale->customer->id != 7) {
                $sale->customer->addVisit();
            }
        }

        if(isset($data['payment_gateways']['cortesia']) && $data['payment_gateways']['cortesia']){
            foreach ($sale->services as $sale_service) {
                $sale_service->price = 0;
                $sale_service->save();
            }
            $sale->is_courtesy = true;
            $sale->save();
        }

        foreach ($sale->products as $sale_product) {
            $pid = $sale_product->product_id;
            if(isset($data['payment_gateways']['cortesia']) && $data['payment_gateways']['cortesia']) {
                $sale_product->price = 0;
                $sale_product->save();
            }
            $departure = $cash_register->subsidiary->departures()->create([
                'user_id' => $request->user()->id,
                'concept_id' => 1,
                'product_id' => $pid,
                'qty' => $sale_product->qty,
                'folio' => $cash_register->subsidiary->getDeparturesFolio(),
                'sale_id' => $sale->id,
            ]);

            $product_find = $cash_register->subsidiary->products()->find($pid);
            $existence = $product_find->pivot->existence - $sale_product->qty;
            $cash_register->subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);

            $departure->product->updateExistence($sale_product->qty, 'decrement');
        }
        //$cash_register = $subsidiary->cashRegisters()->open()->today()->first();

        $sales = $cash_register->sales()->with(['customer'])->active()->get();
        $finished_sales = $cash_register->sales()->finished()->notCanceled();
        $finished_sales_count = $finished_sales->get()->count();
        $finished_sales_total = $finished_sales->sum('total');

        if ($data['payment_gateways']['efectivo'] == true) {
            $money_total = 0;

            if ($data['coin_mxn'] == true) {
                $money_total += $data['qty_money_mxn'];
                $sale->payments()->create([
                    'method' => 'Efectivo',
                    'coin' => 'mxn',
                    'total' => $data['qty_money_mxn'],
                    'cash_register_id' => $cash_register->id
                ]);
            }
            if ($data['coin_usd'] == true) {
                $usd_total = $data['qty_money_usd'] * $cash_register->exchangeRate->rate;
                $money_total += $usd_total;

                $sale->payments()->create([
                    'method' => 'Efectivo',
                    'coin' => 'usd',
                    'total' => $usd_total,
                    'cash_register_id' => $cash_register->id
                ]);
            }

            $money_total -= $data['money_change'];

            $cash_register->total = $cash_register->total + $money_total;
            $cash_register->save();

            //Si se pagó la venta se verifica si la caja tiene 1000 pesos:
            $minicut = $subsidiary->miniCutPetitions()->latest()->first();
            if($minicut == null && $cash_register->total > 1000){
                MiniCutPetition::create([
                    'subsidiary_id' => $subsidiary->id,
                    'datetime' => date('Y-m-d H:i:s')
                ]);
            }
        }
        if ($data['payment_gateways']['tarjeta'] == true) {
            $card_total = $data['qty_card'];
            $sale->payments()->create([
                'method' => 'Tarjeta',
                'total' => $card_total,
                'comment' => "{$data['card_brand']} - {$data['last_four']}",
                'cash_register_id' => $cash_register->id
            ]);
        }
        if ($data['payment_gateways']['wallet'] == true) {
            $wallet_total = $data['qty_wallet'];

            $sale->payments()->create([
                'method' => 'Monedero',
                'total' => $wallet_total,
                'comment' => "",
                'cash_register_id' => $cash_register->id
            ]);
            $data['payment_gateway'] = 'Monedero';
            $sale->customer->removeServicesFree();
        }
        $sale->turn_id = $cash_register->turn_id;
        $sale->finished_cash_register_id = $cash_register->id;
        $data['tip_in'] = 'tarjeta';
        $sale->update($data);
        $message = 'Servicio pagado con exito.';

        $employee_ids = $cash_register->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->today()->active()->get()->map(function ($sale) {
            return $sale->employee_id;
        })->toArray();
        $attendances = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
            ->where('subsidiary_id', $subsidiary->id)
            ->today()
            ->groupBy('employee_id')
        // ->whereNotIn('employee_id', $employee_ids)
            ->get()
            ->whereIn('count_attend', [1, 3]);
        $barbers = $attendances->where('employee.job', 'Barbero');
        $barbers = $barbers->map(function ($key) {
            return $key->employee;
        })->filter(function ($employee) use ($employee_ids) {
            return !in_array($employee->id, $employee_ids);
        });
        $barbers = $barbers->merge([46 => Employee::find(46)]);

        return response()->json(compact('sales', 'finished_sales_count', 'finished_sales_total', 'message', 'cash_register', 'barbers'));
    }

    function print($subsidiary_code, $id) {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $sale = Sale::with(['payments','packages','products','services','customer','subsidiary', 'image'])->find($id);
        $user = Auth::user();
        return view('admin.sales.tickets.sale', compact('subsidiary', 'sale', 'user'));
    }

    public function printAdmin($subsidiary_code, $id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $sale = Sale::find($id);

        return view('admin.sales.tickets.sale', compact('subsidiary', 'sale'));
    }

    function printCustomer($sale_id) {
        $sale = Sale::with(['payments','employee', 'packages','products','services','customer','subsidiary', 'image'])->find(base64_decode($sale_id));
        $customer_print = true;
        return view('admin.sales.tickets.sale', compact('subsidiary', 'sale', 'customer_print'));
    }

    public function  printAdvances($id)
    {
        $subsidiary = null;
        $sale = null;
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $sale = Sale::find($id);
        }
        $is_advance = true;
        return view('admin.sales.tickets.sale', compact('subsidiary', 'sale', 'is_advance'));
    }

    public function barber($subsidiary_code, $id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $sale = Sale::find($id);

        return view('admin.sales.tickets.barber', compact('subsidiary', 'sale'));
    }

    public function destroy(Request $request, $subsidiary_code, $folio)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        if (!$sale = $subsidiary->sales()->with(['customer'])->where('folio', $folio)->first()) {
            return response()->json(['message' => 'No encontramos ventas con este folio.'], 503);
        }

        if($sale->canceled_at != null || $sale->canceled_by != null){
            return response()->json([
            'message' => 'La nota '.$sale->folio. ' ya está cancelada.',
            'sale' => $sale,
            ], 503);
        }

        if (!$user = User::where('email', $request->email)->first()) {
            return response()->json([
                'message' => 'La cuenta de correo especificada no existe.'
            ], 503);
        }

        if (!$this->passwordCorrect($request->password, $user)) {
            return response()->json([
                'message' => 'La contraseña es incorrecta.'
            ], 503);
        }

        if(!$user->isA('super-admin', 'super-admin-restringido', 'cashier', 'cashier-admin', 'manager', 'supervisor')) {
            return response()->json([
                'message' => 'No tienes permiso para cancelar',
            ], 503);
        }

        $sale->canceled_at = Carbon::now();
        $sale->canceled_by = $user->id;
        
        if($sale->customer->visits == 0 && $sale->customer->services_free > 0){
            $sale->customer->setVisits(9);
            $sale->customer->removeServicesFree();
        } else{
            $sale->customer->removeVisit();
        }

        if($sale->payments()->where('method','Monedero')->first()){
            $sale->customer->addServicesFree();
        }
        
        $sale->save();

        return response()->json([
            'message' => 'Nota cancelada con éxito',
            'sale' => $sale,
        ]);
    }

    public function search($subsidiary_code, $folio)
    {
        $subsi_folio = explode('-', $folio);
        if (count($subsi_folio) == 2) {
            $subsidiary = Subsidiary::whereKey($subsi_folio[0])->firstOrFail();
            $folio = $subsi_folio[1];
        } else {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        if (!$sale = $subsidiary->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary', 'subsidiary'])->where('folio', $folio)->first()) {
            return response()->json(['message' => 'No encontramos ventas con este folio.'], 404);
        }

        return response()->json([
            'message' => '',
            'sale' => $sale,
        ]);
    }

    public function updateBarber(Request $request, $subsidiary_code, $folio)
    {
        if (!$sale = Sale::find($folio)) {
            return response()->json(['message' => 'No encontramos ventas con este folio.'], 404);
        }

        $sale->repair = true;
        $sale->repaired_by = $request->barber_id;
        $sale->repaired_at = Carbon::now();
        $sale->save();

        return response()->json([
            'message' => 'Hemos marcado el servicio como reparado.',
            'sale' => $sale,
        ]);
    }

    public function showSurveyForm(Request $request, $subsidiary_code, $folio)
    {
        if (!$sale = Sale::find($folio)) {
            return response()->json(['message' => 'No encontramos ventas con este folio.'], 404);
        }
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        broadcast(new MakeSurvey($sale, $subsidiary));
        return response()->json([]);
    }

    public function activePromotion()
    {
        return Promotion::where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true)->get()->first();
    }

    public function sendTicket(Request $request)
    {
        $error = '';
        $status = 'error';
        if($sale = Sale::with(['services','products','customer','subsidiary'])->find($request->sale_id)) {
            if ($sale->customer && $sale->customer->validPhone()) {
                try {
                    $sale->customer->phone = $request->phone;
                    $sale->customer->notify(new SaleMessage($sale));
                    $status = 'success';
                } catch (\Exception $e) {
                    $error = 'Error al enviar mensaje de prueba: '.$e->getMessage();
                }
            } else {
                $error = 'Numero no válido';
            }
        } else {
            $error = 'Venta no encontrada';
        }
        return response()->json(['status' => $status, 'error' => $error]);

    }

    public function addSaleContents(Request $request, $sale_id)
    {
        $data = $request->all();

        if (empty($data['sale_products']) && empty($data['sale_services']) && empty($data['sale_packages'])) {
            return response()->json([
            'error' => 'Debe seleccionar al menos un producto, servicio o paquete',
            ], 404);
        }

        $sale = Sale::find($data['sale']['id']);

        if(isset($data['sale_products']) && !empty($data['sale_products'])){
            foreach ($data['sale_products'] as $index => $product) {
                $currentProduct = Product::find($product['product_id']);
                if($sale_product = SaleProduct::where('product_id', $currentProduct->id)->where('sale_id', $sale->id)->get()->first()){
                    $sale_product->qty = $sale_product->qty + intval($product['qty']);
                    $sale_product->save(); 
                } else {
                    $productPrice = $currentProduct->sell_price;
                    $sale->products()->create([
                        'product_id' => $currentProduct->id,
                        'price' => $productPrice,
                        'qty' => $product['qty'],
                        'original_price' => $currentProduct->sell_price
                    ]);
                }
            }
        }

        if (isset($data['sale_services']) && !empty($data['sale_services'])) {

            foreach ($data['sale_services'] as $index => $service) {

                if($sale_service = SaleService::where('service_id', $service['service_id'])->where('sale_id', $sale->id)->get()->first()) {

                    $sale_service->qty = $sale_service->qty + intval($service['qty']);
                    $sale_service->save(); 
                } else {
                    $currentService = Service::with(['subsidiaryPrices'])->find($service['service_id']);
                    $currentPrice = $currentService->currentPrice($sale->subsidiary_id);
                    
                    $sale->services()->create([
                        'service_id' => $currentService->id,
                        'price' => $currentPrice,
                        'qty' => $service['qty'],
                        'has_promotion' => $currentService->hasPromotion($sale->subsidiary_id),
                        'original_price' => $currentService->originalPrice($sale->subsidiary_id)
                    ]);
                }
            }
        }

        if (isset($data['sale_packages']) && !empty($data['sale_packages'])) {
            foreach ($data['sale_packages'] as $index => $package) {
                if($sale_package = SalePackage::where('package_id', $package['package_id'])->where('sale_id', $sale->id)->get()->first()){
                    $sale_package->quantity = $sale_package->quantity + intval($package['qty']);
                    $sale_package->save(); 
                } else {
                    $currentPackage = Package::with(['packageProducts.product','packageServices.service'])->find($package['package_id']);
                    $salePackage = $sale->packages()->create([
                        'package_id' => $currentPackage->id,
                        'quantity' => $package['qty'],
                        'price' => $currentPackage->price,
                        'original_price' => $currentPackage->price
                    ]);

                    foreach ($currentPackage->packageProducts as $index => $packageProduct) {
                       $salePackageProduct = $salePackage->products()->create([
                            'product_id' => $packageProduct->product_id,
                            'quantity' => $packageProduct->qty,
                            'price' => $packageProduct->product->sell_price,
                        ]); 
                    }

                    foreach ($currentPackage->packageServices as $index => $packageService) {
                       $salePackageService = $salePackage->services()->create([
                            'service_id' => $packageService->service_id,
                            'quantity' => $packageService->qty,
                            'price' => $packageService->service->sell_price,
                        ]); 
                    }
                }
            }
        }
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary'])->find($data['sale']['id']);
        $sale->updateTotals();
        $message = 'Elementos agregados a la venta';
        return response()->json(compact('sale', 'message'));
    }
}
