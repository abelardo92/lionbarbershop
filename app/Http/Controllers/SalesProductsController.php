<?php
namespace App\Http\Controllers;

use App\Product;
use App\Promotion;
use App\Sale;
use App\Subsidiary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesProductsController extends Controller 
{
    public function index($subsidiary_code, $sale_id)
    {
        return response()->json(Product::all());
    }

    public function store(Request $request, $subsidiary_code, $sale_id)
    {
        $data = $request->all();
        $sale = Sale::find($sale_id);
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $product = $sale->products()->where('product_id', $data['id'])->first();
        if ($product) {
            $product->increment('qty');
        }

         if ( !$product) {

            $productPrice = $data['sell_price'];
            $activePromotion = $this->activePromotion();
            
            if($activePromotion && $activePromotion->must_buy_a_service 
                && $sale->services()->first() 
                && !$activePromotion->promotionProductExceptions()->where('product_id',$data['id'])->get()->first() 
                && ($subsidiary->id == $activePromotion->subsidiary_id || $activePromotion->subsidiary_id == null)){
                    //Si existe una promoción, requiere la compra de un servicio, existe un servicio, es una sucursal válida y no está excento de la promoción ...
                    $productPrice = ($productPrice * (100 - $activePromotion->products_discount_percentaje)) / 100;
            }
            
            $product = $sale->products()->create([
                'product_id' => $data['id'],
                'qty' => 1,
                'price' => $productPrice,
                'original_price' => $data['sell_price']
            ]);
        }

        $sale->updateTotals();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->find($sale_id);
        $message = 'Producto agregado a la nota con exito.';
        $products = $subsidiary->products();
        return response()->json(compact('sale', 'message', 'products'));
    }

    public function destroy(Request $request, $subsidiary_code, $sale_id, $sale_product_id)
    {
        $data = $request->all();
        $sale = Sale::find($sale_id);
        $product = $sale->products()->find($sale_product_id);
        $product->product()->increment('existence', $product->qty);
        $product->delete();
        $sale->updateTotals();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary', 'payments'])->find($sale_id);
        $message = 'Haz eliminado el producto.';
        return response()->json(compact('sale', 'message'));
    }

    public function activePromotion()
    {
        return Promotion::where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true)->get()->first();
    }
}