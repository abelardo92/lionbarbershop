<?php
namespace App\Http\Controllers;

use App\Sale;
use App\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SalesServicesController extends Controller 
{
    public function index($subsidiary_code, $sale_id)
    {
        return response()->json(Service::all());
    }

    public function store(Request $request, $subsidiary_code, $sale_id)
    {
        $data = $request->all();
        $sale = Sale::with('employee')->find($sale_id);
        $service = $sale->services()->where('service_id', $data['id'])->first();
        if ($service) {
            $service->increment('qty');
        } else {

            $currentService = Service::with(['subsidiaryPrices'])->find($data['id']);
            $commission_number = $sale->employee != null ? $sale->employee->commission_number : 1;

            $service = $sale->services()->create([
                'service_id' => $data['id'],
                'price' => $currentService->currentPrice($sale->subsidiary_id),
                'qty' => 1,
                'has_promotion' => $currentService->hasPromotion($sale->subsidiary_id),
                'original_price' => $currentService->originalPrice($sale->subsidiary_id),
                'cost' => $currentService->getCost($commission_number)
            ]);
        }
        $sale->updateTotals();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->find($sale_id);
        $message = 'Servicio agregado a la nota con éxito.';
        return response()->json(compact('sale', 'message'));
    }

    public function destroy(Request $request, $subsidiary_code, $sale_id, $sale_service_id)
    {
        $data = $request->all();
        $sale = Sale::find($sale_id);
        $service = $sale->services()->find($sale_service_id);
        $service->delete();
        $sale->updateTotals();
        $sale = Sale::with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary', 'payments'])->find($sale_id);
        $message = 'Haz eliminado el servicio.';
        return response()->json(compact('sale', 'message'));
    }

}