<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Schedule;
use App\ScheduleExchange;
use App\ScheduleExchangeChild;
use App\Subsidiary;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleExchangesController extends Controller
{
	public function index(Request $request)
    {
    	$exchanges = ScheduleExchange::with(['employee','employee2','exchanges'])->where('date','>=',date('Y-m-d'))->get();
        return view('admin.exchanges.index', compact('exchanges'));
    }

    public function create($employee_id)
    {
        $employee = Employee::find($employee_id);
        $employees = Employee::where('job',$employee->job)->active()->get();
        return view('admin.employees.exchanges.create', compact('employee', 'employees'));
    }

    public function store(Request $request, $employee_id)
    {
        $data = $request->all();
        $data['employee_id'] = $employee_id;
        $exchange = ScheduleExchange::create($data);
        if(isset($data['employee_ids'])){
            foreach ($data['employee_ids'] as $index => $id) {
                if(!empty($id)) {
                    $exchangeChild = ScheduleExchangeChild::create([
                    'date' => $data['dates'][$index],
                    'employee_id' => $id,
                    'employee2_id' => $data['employee2_ids'][$index],
                    'schedule_exchange_id' => $exchange->id
                    ]);
                }
            }
        }

        $request->session()->flash('success', 'Solicitud creada con éxito');
        return redirect("home");
    }

    public function edit($employee_id, $id)
    {
        $exchange = ScheduleExchange::with(['exchanges'])->find($id);
        $employee = Employee::find($employee_id);
        $employees = Employee::where('job',$employee->job)->active()->get();
        return view('admin.employees.exchanges.edit', compact('employee', 'exchange', 'employees'));
    }

    public function update(Request $request, $employee_id, $id)
    {
        $data = $request->all();
        $exchange = ScheduleExchange::find($id);
        $exchange->update($data);
        foreach ($data['exchange_child_id'] as $index => $id) {
            $exchangeChild = ScheduleExchangeChild::find($id);

            if($exchangeChild->date != $data['dates'][$index]){
                $exchangeChild->date = $data['dates'][$index];
                $exchangeChild->employee_authorized = false;
                $exchangeChild->employee2_authorized = false;
            }

            if($exchangeChild->employee_id != $data['employee_ids'][$index]){
                $exchangeChild->employee_id = $data['employee_ids'][$index];
                $exchangeChild->employee_authorized = false;
            }

            if($exchangeChild->employee2_id != $data['employee2_ids'][$index]){
                $exchangeChild->employee2_id = $data['employee2_ids'][$index];
                $exchangeChild->employee2_authorized = false;
            }
            $exchangeChild->save();
        }
        $request->session()->flash('success', 'Solicitud actualizada con éxito.');
        return redirect()->back();
    }

    public function destroy(Request $request, $employee_id, $id)
    {
        $exchange = ScheduleExchange::find($id);
        $exchange->exchanges()->delete();
        $exchange->delete();
        $request->session()->flash('success', 'Solicitud eliminada con éxito.');
        return redirect("home");
    }

    public function authorizee(Request $request, $id)
    {
        $data = $request->all();
        $employee_id = $data['employee_id'];
        $exchange = ScheduleExchange::with(['exchanges'])->find($id);
        foreach ($exchange->exchanges as $exchangeChild) {
            if($exchangeChild->employee_id == $employee_id){
                $exchangeChild->employee_authorized = true;
            }
            if($exchangeChild->employee2_id == $employee_id){
                $exchangeChild->employee2_authorized = true;
            }
            $exchangeChild->save();
        }

        if($exchange->employee2_id == $employee_id){
            $exchange->employee2_authorized = true;
            $exchange->save();
        }
        $request->session()->flash('success', 'Solicitud autorizada con éxito');
        return redirect()->back();
    }

    public function unauthorize(Request $request, $id)
    {
        $data = $request->all();
        $employee_id = $data['employee_id'];
        $exchange = ScheduleExchange::with(['exchanges'])->find($id);
        foreach ($exchange->exchanges as $exchangeChild) {
            if($exchangeChild->employee_id == $employee_id){
                $exchangeChild->employee_authorized = false;
            }
            if($exchangeChild->employee2_id == $employee_id){
                $exchangeChild->employee2_authorized = false;
            }
            $exchangeChild->save();
        }

        if($exchange->employee2_id == $employee_id){
            $exchange->employee2_authorized = false;
            $exchange->save();
        }
        $request->session()->flash('success', 'Solicitud rechazada con éxito');
        return redirect()->back();
    }

    public function authorizeadmin(Request $request, $id)
    {
        $exchange = ScheduleExchange::with(['exchanges'])->find($id);
        $schedule1 = $exchange->schedule1();
        $schedule2 = $exchange->schedule2();
        
        if(!$exchange->isAuthorizedByAll()){
            $request->session()->flash('error', 'Alguno de los empleados no autorizaron el intercambio');
            return redirect()->back();
        }

        if(!$schedule1 || !$schedule2){
        	$request->session()->flash('error', 'Alguno de los empleados no tiene horario en la fecha correspondiente');
            return redirect()->back();
        }

        /*
        if($schedule1->subsidiary_id != $schedule2->subsidiary_id && $schedule1->subsidiary_id != 7 && $schedule2->subsidiary_id != 7){
        	$request->session()->flash('error', 'los horarios deben pertenecer a la misma sucursal');
            return redirect()->back();
        }*/ 

        foreach ($exchange->exchanges as $exchangeChild) {
            $scheduleChild1 = $exchangeChild->schedule1();
            $scheduleChild2 = $exchangeChild->schedule2();

            if($scheduleChild1 != null && $scheduleChild2 != null){
                $scheduleChild1->employee_id = $exchangeChild->employee2_id;
                $scheduleChild2->employee_id = $exchangeChild->employee_id;
                $scheduleChild1->save();
                $scheduleChild2->save();
            }   
        }

        //update schedules
        $schedule1->employee_id = $exchange->employee2_id;
        $schedule2->employee_id = $exchange->employee_id;

        $exchange->admin_authorized = true;
        $exchange->user_id = Auth::user()->id;
        $exchange->save();
        $schedule1->save();
        $schedule2->save();
        $request->session()->flash('success', 'Solicitud abrobada con éxito, horarios intercambiados');
        return redirect()->back();
    }

    public function unauthorizeadmin(Request $request, $id)
    {
        $exchange = ScheduleExchange::find($id);
        $exchange->admin_authorized = false;
        $exchange->user_id = Auth::user()->id;
        $exchange->save();
        $request->session()->flash('success', 'Solicitud rechazada con éxito');
        return redirect()->back();
    }
}
