<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Service;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use Illuminate\Http\Request;
use App\Traits\SubDomainHelper;

class ServicesController extends Controller
{
    use ImageableTrait;
    use SubDomainHelper;

    public function index(Request $request)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $services = Service::laundry()->paginate(15);
                return view('admin.services.index', compact('services'));
            }
        }
        $services = Service::notLaundry()->orderBy('key', 'asc')->paginate(15);
        return view('admin.services.index', compact('services'));
    }

    public function create()
    {
        $subsidiaries = $this->getSubsidiaries();
        $currentSubsidiary = null;
        if ($subsidiary_code = $this->hasSubdomain()) {
            $currentSubsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return view('admin.services.create', compact('subsidiaries', 'currentSubsidiary'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry) {
                $data['is_laundry'] = true;
                $subsidiaries = Subsidiary::active()->laundry()->get();
            } else {
                $subsidiaries = Subsidiary::active()->notLaundry()->get();
            }
        } else {
            $subsidiaries = Subsidiary::active()->notLaundry()->get();
        }
        
        if($data['subsidiary_id'] == "") $data['subsidiary_id'] = null;
        $service = Service::create($data);
        $this->CreateFile($service, $request, "services/{$service->id}");

        $request->session()->flash('success', 'Servicio dado de alta con exito.');
        return redirect()->route('services.edit', $service->id);
    }

    public function edit(Request $request, $id)
    {
        $service = Service::find($id);
        $subsidiaries = $this->getSubsidiaries();
        $currentSubsidiary = null;
        if ($subsidiary_code = $this->hasSubdomain()) {
            $currentSubsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return view('admin.services.edit', compact('service', 'subsidiaries', 'currentSubsidiary'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $service = Service::find($id);
        if($data['subsidiary_id'] == "") $data['subsidiary_id'] = null; 
        $service->update($data);
        $this->CreateFile($service, $request, "services/{$service->id}");
        
        $request->session()->flash('success', 'Servicio actualizado con exito.');
        return redirect()->route('services.edit', $service->id);
    }

    private function getSubsidiaries(){
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                return Subsidiary::active()->laundry()->get();
            }
        }
        return Subsidiary::active()->notLaundry()->get();
    }
}
