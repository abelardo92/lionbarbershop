<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Subsidiary;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubsidiariesTurnsController extends Controller
{
    public function create($subsidiary_id)
    {
        return view('admin.subsidiaries.turns.create', compact('subsidiary_id'));
    }

    public function store(Request $request, $subsidiary_id)
    {
        $data = $request->all();
        $data['start'] = Carbon::parse($data['start'])->format('H:i:s');
        $data['end'] = Carbon::parse($data['end'])->format('H:i:s');
        
        $subsidiary = Subsidiary::find($subsidiary_id);
        $turn = $subsidiary->turns()->create($data);

        
        $request->session()->flash('success', 'Turno de sucursal dado de alta con exito.');
        return redirect()->route('subsidiaries.turns.edit', [$subsidiary->id, $turn->id]);
    }

    public function edit(Request $request, $subsidiary_id, $id)
    {
        $turn = Turn::find($id);
        return view('admin.subsidiaries.turns.edit', compact('turn', 'subsidiary_id'));
    }

    public function update(Request $request, $subsidiary_id, $id)
    {
        $data = $request->all();
        $data['start'] = Carbon::parse($data['start'])->format('H:i:s');
        $data['end'] = Carbon::parse($data['end'])->format('H:i:s');

        $turn = Turn::find($id);
        $turn->update($data);

        $request->session()->flash('success', 'Turno de sucursal actualizado con exito.');
        return redirect()->route('subsidiaries.turns.edit', [$subsidiary_id, $turn->id]);
    }
}
