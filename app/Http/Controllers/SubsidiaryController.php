<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Subsidiary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SubsidiaryController extends Controller
{
    private function passwordCorrect($suppliedPassword, $user)
    {
        return Hash::check($suppliedPassword, $user->password, []);
    }

    public function auth(Request $request, $subsidiary_code)
    {
        if (!$employee = Employee::whereCode($request->code)->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 404);
        }

        if (in_array($employee->job, ['Supervisor', 'Encargado', 'Cajero'])) {
            session()->put('employee_id', $employee->id);
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No tienes permiso para acceder',
        ], 403);
    }

    public function authDelete(Request $request, $subsidiary_code)
    {
        if (!$user = User::where('email', $request->email)->first()) {
            return response()->json([
                'message' => 'La cuenta de correo especificada no existe.'
            ], 404);
        }

        if (!$this->passwordCorrect($request->password, $user)) {
            return response()->json([
                'message' => 'La contraseña es incorrecta.'
            ], 503);
        }

        if($user->isA('super-admin', 'super-admin-restringido', 'cashier', 'cashier-admin', 'manager', 'supervisor')) {
            return response()->json([
                'success' => true,
            ]);
        }

        // if (($request->type == 'product' && in_array($employee->job, ['Supervisor', 'Encargado', 'Cajero'])) || 
        //     ($request->type == 'service' && in_array($employee->job, ['Supervisor', 'Encargado']))) {
        //     session()->put('employee_id', $employee->id);
        //     return response()->json([
        //         'success' => true,
        //     ]);
        // }

        return response()->json([
            'success' => false,
            'message' => 'No tienes permiso para acceder',
        ], 403);
    }

    public function authAdmin(Request $request, $subsidiary_code)
    {
        $data = $request->all();
        if (!$user = User::where('email', $request->email)->first()) {
            return response()->json([
                'message' => 'La cuenta de correo especificada no existe.'
            ], 404);
        }

        if (!$this->passwordCorrect($request->password, $user)) {
            return response()->json([
                'message' => 'La contraseña es incorrecta.'
            ], 503);
        }

        // request->action contains which url the user is trying to access
        if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'supervisor', 'cashier-admin')) {
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No tienes permiso para acceder',
        ], 403);
    }
}
