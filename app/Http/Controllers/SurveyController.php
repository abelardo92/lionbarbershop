<?php

namespace App\Http\Controllers;

use App\Surveys\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SurveyController extends Controller
{
    public function index()
    {
        $surveys = Survey::latest();
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $surveys = $surveys->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $surveys = $surveys->paginate(20);
        return view('admin.surveys.surveys.index', compact('surveys'));
    }

    public function show($surveyId)
    {
        $survey = Survey::find($surveyId);
        return view('admin.surveys.surveys.show', compact('survey'));
    }
}
