<?php

namespace App\Http\Controllers;

use App\AccountingCustomer;
use App\Employee;
use App\Http\Requests;
use App\Subsidiary;
use App\User;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();

        if($request->has('filter')) {
            $filter = $data['filter'];
            $users = User::where('name', 'LIKE', '%'.$filter.'%')->orWhere('email', 'LIKE', '%'.$filter.'%')->active()->paginate(15);
        }
        else {
            $users = User::active()->paginate(15);
        }
        return view('admin.users.index', compact('users', 'currentUser'));
    }

    public function create()
    {
        $currentUser = Auth::user();
        if($currentUser->isA('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::where('name', '!=', 'super-admin')->get();
        }
        $subsidiaries = Subsidiary::where('is_active', true)->get();
        return view('admin.users.create', compact('roles', 'subsidiaries'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if(!$user = $this->findAnUser($data)){

            $user = $this->createUser($data);
            $user->assign($data['role_id']);
            $request->session()->flash('success', 'Usuario dado de alta con exito.');

            if ($request->has('from_employee')) {
                $employee = Employee::find($data['employee_id']);
                $employee->user_id = $user->id;
                $employee->save();
                return back();
            }

            return redirect()->route('users.edit', $user->id);
        }
        $request->session()->flash('error', 'El correo escrito ya existe, escriba un correo diferente');
        return back();
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $subsidiaries = Subsidiary::active()->get();

        $currentUser = Auth::user();
        if($currentUser->isA('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::where('name', '!=', 'super-admin')->get();
        }

        $accounting_customers = AccountingCustomer::active()->get();

        if($currentUser->isA('manager','super-admin-restringido') && $user->isA('super-admin')) {
            $request->session()->flash('error', 'No puedes editar la informacion de un super administrador');
            return back();
        }
        return view('admin.users.edit', compact('user', 'roles', 'subsidiaries', 'accounting_customers'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (empty($data['password'])) {
            $key = array_search('password', $data);
            unset($key);
        }else{
            $data['password'] = bcrypt($data['password']);
        }
        $user = User::find($id);
        if (!isset($data['from_employee'])) {
            $rol = Role::whereName($user->roles->first()->name)->first();
            $user->retract($rol->name);
            $user->assign($data['role_id']);
        }

        $data['allowed_accounting_customers'] = implode(",", $data['allowed_accounting_customers']);

        $user->update($data);
        $request->session()->flash('success', 'Usuario actualizado con éxito.');
        return redirect()->back();
    }

    protected function createUser($data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'subsidiary_id' => $data['subsidiary_id'] ?? null,
            'api_token' => str_random(60),
        ]);
    }

    protected function findAnUser($data)
    {
        if(!$user = User::where('email', $data['email'])->first()) {
            return false;
        }
        $user->update([
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
        return $user;
    }

    public function deactivate(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $user->is_active = false;
        $user->save();
        $request->session()->flash('success', 'Usuario eliminado con éxito.');
        return back();
    }
}
