<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WarehouseProduct;
use App\Product;
use App\Subsidiary;

class WarehouseProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = WarehouseProduct::active();
        if ($request->has('filter')) {
            $filter = $request->filter;
            $products = $products->where(function ($query) use ($filter) {
                $query->where('name', 'LIKE', '%'.$filter.'%')
                ->orWhere('code', 'LIKE', '%'.$filter.'%');
            });
        }
        $products = $products->orderBy('code')->paginate(15);
        $uses = WarehouseProduct::getEnum('uses');
        $accountings = WarehouseProduct::getEnum('accountings');
        return view('admin.warehouse.products.index', compact('products', 'uses', 'accountings'));
    }

    public function adjustMustHaves()
    {
        $warehouseProducts = WarehouseProduct::with(['existences'])->active()->orderBy('code')->get();
        $subsidiaries = Subsidiary::active()->where('id', '!=', 1)->get();
        return view('admin.warehouse.products.adjust-must-haves', compact('warehouseProducts', 'subsidiaries'));
    }

    public function storeMustHaves(Request $request)
    {
        $warehouseProducts = WarehouseProduct::with(['existences'])->active()->orderBy('code')->get();
        $subsidiaries = Subsidiary::active()->where('id', '!=', 1)->get();
        //dd($request->mustHave);
        foreach ($warehouseProducts as $product) {

            if(isset($request->existence[$product->id])) {
                if($product->warehouse_desired_existence != $request->existence[$product->id]){
                $product->warehouse_desired_existence = $request->existence[$product->id];
                $product->save();
                }
            }
            
            foreach ($subsidiaries as $subsidiary) {

                if(isset($request->mustHave[$product->id][$subsidiary->id])) {
                    $currentExistence = $request->mustHave[$product->id][$subsidiary->id];

                    if($existence = $product->existences->where('subsidiary_id', $subsidiary->id)->first()) {
                        if($existence->must_have != $currentExistence) {
                            $existence->must_have = $currentExistence;
                            $existence->save();
                        }
                    } else {
                        $product->existences()->create([
                            'subsidiary_id' => $subsidiary->id,
                            'must_have' => $currentExistence
                        ]);
                    }
                }
            }
        }

        $request->session()->flash('success', 'Información actualizada con éxito.');
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::notLaundry()->get();
        return view('admin.warehouse.products.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        if($productByCode = WarehouseProduct::where('code', $data['code'])->get()->first()) {
            $request->session()->flash('error', 'El código de producto ya existe');
            return redirect()->back();
        }
        if($data['product_id'] == "") $data['product_id'] = null;
        $product = WarehouseProduct::create($data);
        $request->session()->flash('success', 'Producto dado de alta con éxito.');
        return redirect()->route('warehouse.products.edit', $product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $product = WarehouseProduct::find($id);
        $products = Product::notLaundry()->get();
        $user = Auth::user();
        return view('admin.warehouse.products.edit', compact('product', 'products', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($data['product_id'] == "") $data['product_id'] = null;
        if($request->has('code') && $data['prev_code'] != $data['code'] && $productByCode = WarehouseProduct::where('code', $data['code'])->get()->first()) {
            $request->session()->flash('error', 'El código de producto ya existe');
            return redirect()->back();
        }
        $product = WarehouseProduct::find($id);
        $product->update($data);
        $request->session()->flash('success', 'Producto actualizado con éxito.');
        return redirect()->route('warehouse.products.edit', $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $product = WarehouseProduct::find($id);
        $product->is_active = false;
        $product->save();
        $request->session()->flash('success', 'Producto eliminado con éxito.');
        return redirect()->back();
    }
}
