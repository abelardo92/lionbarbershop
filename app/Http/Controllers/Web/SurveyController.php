<?php

namespace App\Http\Controllers\Web;

use App\Events\AcceptSurvey;
use App\Http\Controllers\Controller;
use App\Sale;
use App\Subsidiary;
use App\Surveys\Question;
use App\Surveys\Survey;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function index($subsidiary_code)
    {
        return view('surveys.index');
    }

    public function store(Request $request, $subsidiary_code)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $sale = Sale::find($request->sale_id);
        $survey = Survey::create([
            'code' => Survey::generateUniqueCode(),
            'cashier_id' => $sale->cashRegister->employee->id,
            'employee_id' => $sale->employee->id,
            'subsidiary_id' => $request->subsidiary_id,
            'sale_id' => $request->sale_id,
            'started_at' => Carbon::now(),
        ]);

        broadcast(new AcceptSurvey($sale, $subsidiary));

        return response()->json([
            'survey' => $survey
        ]);
    }

    public function edit($subsidiary_code, $survey_id)
    {
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $survey = Survey::find($survey_id);
        $questions = Question::active()->noSubsidiary()->get();
        $questions = $questions->merge($subsidiary->questions);

        // if ($survey->completed) {
        //    return redirect()->route('surveys.index', $subsidiary_code);
        // }

        return view('surveys.edit', compact('subsidiary', 'survey', 'questions'));
    }

    public function update(Request $request, $subsidiary_code, $survey_id)
    {
        $survey = Survey::find($survey_id);
        foreach ($request->question as $questionId => $data) {
            $survey->questions()->attach($questionId, [
                'answer' => $this->buildAnswer($data['answer'])
            ]);
        }

        $survey->update([
            'finished_at' => Carbon::now(),
            'completed' => true
        ]);

        return response()->json([
            'survey' => $survey
        ]);
    }

    protected function buildAnswer($answer)
    {
        if (is_array($answer)) {
            return implode('|', $answer);
        }

        return $answer;
    }
}
