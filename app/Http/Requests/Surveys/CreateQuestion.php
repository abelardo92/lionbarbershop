<?php

namespace App\Http\Requests\Surveys;

use Illuminate\Foundation\Http\FormRequest;

class CreateQuestion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'input_type' => 'required',
            'options' => 'required_if:input_type,checkbox,radio',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo Texto es obligatorio',
            'input_type.required' => 'El campo Tipo de Pregunta es obligatorio',
            'options.required_if' => 'Obligatorio minimo 1 opción si el campo Tipo de Pregunta es de tipo opcion multipe'
        ];
    }
}
