<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = [
        'name', 'path', 'full_path', 'month', 'year', 'type', 'date'
    ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function getPathAttribute($value)
    {
        return Storage::url($value);
    }

    public function isMp3()
    {
        $arr = explode('.', $this->path);
        $ext = end($arr);
        return !!in_array($ext, ['mp3', 'mpga']);
    }

    public function deleteWithFile()
    {
        $path = str_replace("/storage/", "public/", $this->path);
        if(Storage::exists($path)){
            Storage::delete($path);
        }
        $this->delete();
    }

    public function scopeBySection($query, $section)
    {
        return $query->where('path', 'LIKE', "%/$section/%");
    }

    public function scopeByMonth($query, $month)
    {
        return $query->where('month', $month);
    }

    public function scopeByYear($query, $year)
    {
        return $query->where('year', $year);
    }

    public function isInSection($section = "", $month = "", $year = "")
    {
        return str_contains($this->path, "$section/$month/$year");
    }
}
