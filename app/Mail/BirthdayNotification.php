<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BirthdayNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;
    public $date;
    public $subject = "Feliz cumpleaños te desea Lion Barbershop!";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $date)
    {
        $this->customer = $customer;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.birthday-customer2');
    }
}
