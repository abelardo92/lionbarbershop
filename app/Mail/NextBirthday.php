<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NextBirthday extends Mailable
{
    use Queueable, SerializesModels;

    public $customers;
    
    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customers, $date)
    {
        $this->customers = $customers;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Estos son los clientes que cumplen años el {$this->date}")
            ->view('emails.birthday');
    }
}
