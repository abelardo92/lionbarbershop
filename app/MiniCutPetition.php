<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiniCutPetition extends Model
{
    protected $fillable = [
        'subsidiary_id', 'datetime'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }
}
