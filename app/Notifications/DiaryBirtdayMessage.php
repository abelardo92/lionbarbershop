<?php

namespace App\Notifications;

use App\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class DiaryBirtdayMessage extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        return (new NexmoMessage)
                // ->content("Feliz cumpleaños {$notifiable->name} te desea Lion Barbershop, te recordamos que el dia de hoy te regalamos un servicio gratis presentando una identificacion con tu fecha de nacimiento. Aplica solo el dia de hoy");
                ->content("Feliz cumpleaños ".$notifiable->name." te desea Lion Barbershop, te regalamos un servicio gratis presentando una identificacion. Aplica solo el dia de hoy");
    }
}
