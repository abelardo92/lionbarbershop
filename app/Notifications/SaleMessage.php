<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class SaleMessage extends Notification
{
    use Queueable;

    public $sale;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sale)
    {
        $this->sale = $sale;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        $ticket_link = "www.lionbarbershop.com/ticket/".base64_encode($this->sale->id);
        $note_content = "Gracias por venir a LION {$this->sale->subsidiary->name}, consulta tu nota en $ticket_link";        
        return (new NexmoMessage)->content($note_content);
    }
}