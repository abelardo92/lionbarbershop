<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Enums;

class Pending extends Model
{
	use Enums;

    protected $fillable = [
        'subsidiary_id', 'subject', 'description_pending', 'description_finished', 'date', 'created_by', 'finished_by',
        'opened_by', 'status', 'finished_at', 'is_admin',
    ];

    protected $enumStatuses = [
        1 => 'Pendiente',
        2 => 'Trabajando',
        3 => 'Finalizado',
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id', 'id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function openedBy()
    {
        return $this->belongsTo(User::class, 'opened_by', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function finishedBy()
    {
        return $this->belongsTo(User::class, 'finished_by', 'id');
    }

    public function scopeNonFinished($query)
    {
        return $query->where('status', 1);
    }

    public function scopeFinished($query)
    {
        return $query->where('status', 3);
    }

    public function scopeAdmin($query)
    {
        return $query->where('is_admin', true);
    }

    public function scopeNotAdmin($query)
    {
        return $query->where('is_admin', false);
    }

    public function scopeUnreadedMessagesBySubsidiary($query, $subsidiary_id, $user)
    {
        return $query->where('subsidiary_id', $subsidiary_id)->where('created_by', '!=', $user->id)->where('status', 1);
    }

    public function scopeUnreadedMessages($query, $user)
    {
        return $query->where('created_by', '!=', $user->id)->where('status', 1);
    }
}
