<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Percepcione extends Model
{
    protected $fillable = [
        'employee_id', 'text', 'amount', 'date'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }
}