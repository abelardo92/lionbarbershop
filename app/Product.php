<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'key', 'name', 'stock_max', 'stock_min', 'sell_price', 'buy_price', 'commission', 'has_commission', 'is_laundry', 'is_active'
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function subsidiaries()
    {
        return $this->belongsToMany(Subsidiary::class)->withPivot('existence');
    }

    public function updateExistence($qty, $type = 'increment')
    {
        $this->{$type}('existence', $qty);
    }

    public function addExistence($qty)
    {
        $this->existence = $this->existence + $qty;
        $this->save();
    }

    public function subtrackExistence($qty)
    {
        $this->existence = $this->existence - $qty;
        $this->save();
    }
    
    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
