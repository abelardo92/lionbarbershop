<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotions';
    protected $fillable = [
        'name', 'start_date', 'end_date', 'start_time', 'end_time', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'apply_on_other_promotions', 'is_active', 'must_buy_a_service', 'must_buy_a_product', 'services_discount_percentaje', 'products_discount_percentaje'
    ];

    public function promotionservices()
    {
        return $this->hasMany(PromotionServices::class);
    }

    public function promotionProductExceptions()
    {
    	return $this->hasMany(PromotionProductException::class);
    }
}
