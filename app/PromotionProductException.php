<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionProductException extends Model
{
    protected $fillable = [
        'promotion_id', 'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
