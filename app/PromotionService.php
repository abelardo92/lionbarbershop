<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionService extends Model
{
    protected $table = 'promotion_services';
    protected $fillable = [
        'promotion_id', 'service_id', 'price'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    
    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
