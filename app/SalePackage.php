<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalePackage extends Model
{
    protected $fillable = ['sale_id', 'package_id', 'quantity', 'price', 'original_price', 'package_content_price', 'is_disabled', 'disabled_by'];

    public function sale()
    {
        return $this->belongsTo(Sale::class, 'sale_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function disabler()
    {
        return $this->belongsTo(Employee::class, 'disabled_by');
    }

    public function services()
    {
        return $this->hasMany(SalePackageService::class);
    }

    public function products()
    {
        return $this->hasMany(SalePackageProduct::class);
    }

    public function contentPrice()
    {
        $total = 0;
        foreach ($this->services as $service){
            $total += $service->price;
        }
        foreach ($this->products as $product){
            $total += $product->price;
        }
        return $total;
    }
}
