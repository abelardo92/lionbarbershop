<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalePackageProduct extends Model
{
    protected $fillable = ['sale_package_id', 'product_id', 'quantity', 'price'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function salePackage()
    {
        return $this->belongsTo(SalePackage::class);
    }
}
