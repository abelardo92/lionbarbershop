<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalePayment extends Model
{
    protected $fillable = ['method', 'total', 'comment', 'sale_id', 'coin', 'type', 'created_by', 'cash_register_id'];

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function cashRegister()
    {
        return $this->belongsTo(CashRegister::class, 'cash_register_id', 'id');
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeEfectivo($query)
    {
        return $query->where('method', 'Efectivo');
    }

    public function scopeCard($query)
    {
        return $query->where('method', 'Tarjeta');
    }

    public function scopeMonedero($query)
    {
        return $query->where('method', 'Monedero');
    }

    public function scopeUsd($query)
    {
        return $query->where('coin', 'usd');
    }

    public function scopeMxn($query)
    {
        return $query->where('coin', 'mxn');
    }

    public function scopeAdvance($query)
    {
        return $query->where('type', 1);
    }
}
