<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleService extends Model
{
    protected $fillable = ['qty', 'service_id', 'sale_id', 'price', 'has_promotion', 'original_price', 'cost'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function getCommission() 
    {
    	$qty = $this->qty;
        $currentSale = $this->sale;

        if(strpos($this->name,'2x1') || strpos($this->name,'2X1')) {
            if($currentSale->employee_id == $currentSale->employee2_id) {
                $qty++;
            }  
        }

        if($qty > 0 && $currentSale->hasTwoBarbers() && $currentSale->containsHaircutAndKids()) {
            $qty = $qty - 1;
        }
        return $this->cost * $qty;
    }
}
