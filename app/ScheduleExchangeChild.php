<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleExchangeChild extends Model
{
    protected $fillable = [
        'date', 'employee_id', 'employee2_id', 'employee_authorized', 'employee2_authorized', 'schedule_exchange_id'
    ];

    public $schedule1 = null;
    public $schedule2 = null;

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function employee2()
    {
        return $this->belongsTo(Employee::class, 'employee2_id', 'id');
    }

    public function schedule1()
    {
        return Schedule::with(['turn', 'subsidiary', 'employee'])->where('date',$this->date)->where('employee_id',$this->employee_id)->get()->first();
    }

    public function schedule2()
    {
        return Schedule::with(['turn', 'subsidiary', 'employee'])->where('date',$this->date)->where('employee_id',$this->employee2_id)->get()->first();
    }

    public function parentExchange()
    {
        return $this->belongsTo(ScheduleExchange::class);
    }
}
