<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'key', 'name', 'sell_price', 'second_sell_price', 
        'third_sell_price', 'commission', 'cost', 'cost2', 'cost3',
        'kids_promotion', 'subsidiary_id', 'is_laundry', 'type'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }
    
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getCost($commission_number) {
        switch ($commission_number) {
            case 1:
                return $this->cost;
                break;
            case 2:
                if($this->cost2 > 0)
                    return $this->cost2;
                break;
            case 3:
                if($this->cost3 > 0)
                    return $this->cost3;
                break;
        }
        return $this->cost;
    }

    public function promotionservices()
    {
        //return $this->hasMany(PromotionService::class);
        $promotionservices = $this->hasMany(PromotionService::class)->whereHas('promotion', function ($query) {
            $query->where('is_active', true)
                ->whereDate('start_date','<=', date('Y-m-d'))
                ->whereDate('end_date','>=', date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true);
        });
        return $promotionservices;
    }

    public function activePromotion()
    {
        return $this->promotionservices()->whereHas('promotion', function ($query) {
            $query->where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true);
        })->get()->first();
    }

    public function currentPrice($subsidiary_id)
    {
        if($activePromotion = $this->activePromotion()){
            return $activePromotion->price;
        } else {
            $subsidiaryPrice = $this->subsidiaryPrice($subsidiary_id);
            if($subsidiaryPrice != null){
                return $subsidiaryPrice;
            }
            return $this->sell_price;
        }
    }

    public function subsidiaryPrice($subsidiary_id)
    {
        foreach ($this->subsidiaryPrices as $subsidiaryPrice) {
            if($subsidiaryPrice->subsidiary_id == $subsidiary_id){
                return $subsidiaryPrice->price;
            }
        }
        return null;
    }

    public function originalPrice($subsidiary_id) {
        $subsidiaryPrice = $this->subsidiaryPrice($subsidiary_id);
        if($subsidiaryPrice != null) {
            return $subsidiaryPrice; 
        }
        return $this->sell_price; 
    }

    public function hasPromotion($subsidiary_id)
    {
        return $this->originalPrice($subsidiary_id) != $this->currentPrice($subsidiary_id);
    } 

    public function subsidiaryPrices()
    {
        return $this->hasMany(SubsidiaryServicePrice::class);
    }

    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeKilo($query)
    {
        return $query->where('type', 1);
    }

    public function scopePiece($query)
    {
        return $query->where('type', 2);
    }

    public function scopeRent($query)
    {
        return $query->where('type', 3);
    }
}
