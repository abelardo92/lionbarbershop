<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsidiaryWarehouseProduct extends Model
{
    protected $fillable = [
        'warehouse_product_id', 'subsidiary_id', 'existence', 'must_have' 
    ];

    public function product()
    {
        return $this->belongsTo(WarehouseProduct::class, 'warehouse_product_id', 'id');
    }

    public function subsidiary()
    {
        return $this->belongsTo(WarehouseProduct::class, 'subsidiary_id', 'id');
    }
}
