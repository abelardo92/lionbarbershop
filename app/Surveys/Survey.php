<?php

namespace App\Surveys;

use App\Surveys\Question;
use App\{Employee, Subsidiary, Sale};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Survey extends Model
{
    protected $fillable = [
        'code', 'cashier_id', 'employee_id', 'subsidiary_id', 'sale_id', 'started_at', 'finished_at', 'completed'
    ];

    protected $dates = [
        'started_at', 'finished_at'
    ];

    public function cashier()
    {
        return $this->belongsTo(Employee::class, 'cashier_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class)->withTimestamps()->withPivot('answer');
    }

    static public function generateUniqueCode()
    {
        do {
            $code = Str::random(16);
            $found = (new static)->whereCode($code)->first();
        } while ($found);

        return $code;
    }

    public function scopeCompleted($query, $bool = true)
    {
        return $query->whereCompleted($bool);
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('created_at', $dates);
    }
}