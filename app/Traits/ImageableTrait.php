<?php namespace App\Traits;

use App\Image;

trait ImageableTrait
{
    protected function CreateFile($model, $request, $path, $field_name = 'image')
    {
        if ($request->hasFile($field_name)) {
            $file = $request->file($field_name);

            if($image2 = $model->image()->get()->first()){
                $image2->path = $file->store("{$path}", 'public');
                $image2->name = $file->getClientOriginalName();
                $image2->save();
            } else {
                $image = Image::create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store("{$path}", 'public'),
                ]);
                $model->image()->save($image);
            }
        }
    }

    protected function CreateFiles($model, $request, $path, $field_name = 'image', $year = null, $month = null, $type = null, $date = null)
    {
        if (!$request->hasFile($field_name)) return;

        $files = $request->file($field_name);

        if(!is_array($files)) {
            $files = [$files];
        }

        foreach ($files as $file) {
            $image = Image::create([
                'name' => $file->getClientOriginalName(),
                'path' => $file->store("{$path}", 'public'),
                'year' => $year,
                'month' => $month,
                'type' => $type,
                'date' => $date,
            ]);
            $model->images()->save($image);
        }
    }
}
