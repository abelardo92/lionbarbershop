<?php namespace App\Traits;

trait SubDomainHelper
{
    public function hasSubdomain()
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $arr = explode(".", $_SERVER['HTTP_HOST']);
            $domain = array_shift($arr);
            if (in_array($domain, ['lionbarbershop', 'www'])) {
                return false;
            }
            return $domain; 
        }
        return false;
    }
}
