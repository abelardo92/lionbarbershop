<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
    protected $fillable = [
        'subsidiary_id', 'is_rest', 'name', 'start', 'end', 'identifier', 'is_payed', 'barber_start',
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function isDouble() {
        return $this->id == 3 || $this->id == 8;
    }
}
