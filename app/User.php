<?php

namespace App;

use App\Employee;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
    use Notifiable, HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token', 'subsidiary_id', 'access_subsidiary_id', 'is_active',
        'allowed_accounting_customers',
    ];

    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    public function employee()
    {
        // return $this->hasOne(Employee::class);
        return Employee::where('user_id', $this->id)->get()->first();
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function accessSubsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'access_subsidiary_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNoInSubsidiary($query)
    {
        return $query->whereNull('subsidiary_id');
    }

    public function scopeNotInAccessSubsidiary($query)
    {
        return $query->whereNull('access_subsidiary_id');
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function departures()
    {
        return $this->hasMany(Departure::class);
    }

    public function disabilities()
    {
        return $this->hasMany(Disability::class);
    }

    public function manuals()
    {
        return $this->hasMany(Manual::class);
    }

    public function getName()
    {
        return $this->name;
    }

    public function containsAccountingCustomer($accounting_customer_id) {
        $accounting_customers = explode(",", $this->allowed_accounting_customers);
        return in_array($accounting_customer_id, $accounting_customers);
    }
}
