<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Enums;

class WarehouseProduct extends Model
{
	use Enums;

    protected $fillable = [
        'code', 'name', 'use', 'amount', 'accounting', 'in_storage', 'product_id', 'is_active', 'type', 'warehouse_desired_existence'
    ];

    protected $enumUses = [
        1 => 'Para servicio',
        2 => 'Aseo y limpieza',
        3 => 'Area de café',
        4 => 'Ventas',
        5 => 'Papelería',
        6 => 'Botiquin',
    ];

    protected $enumAccountings = [
        1 => 'Suministros',
        2 => 'Aseo y limpieza',
        4 => 'Ventas',
        5 => 'Papelería'
    ];

    protected $enumTypes = [
        1 => 'Barberia',
        2 => 'Lavanderia',
        3 => 'Todos'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function existences()
    {
        //return $this->belongsToMany(Subsidiary::class)->withPivot('existence', 'must_have');
        return $this->hasMany(SubsidiaryWarehouseProduct::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('is_active', false);
    }

    public function initExistence()
    {
        $this->{"existence"} = "";
    }
}
