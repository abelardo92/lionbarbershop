<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseRequest extends Model
{
    protected $fillable = [
        'subsidiary_id', 'folio', 'date', 'created_by', 'employee_id', 'is_confirmed', 'confirmed_by', 'confirmed_at' 
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function confirmedBy()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }

    public function products()
    {
    	return $this->hasMany(WarehouseRequestProduct::class);
    }

    public function movements()
    {
        return $this->hasMany(WarehouseMovement::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('is_confirmed', true);
    }

    public function scopeNotConfirmed($query)
    {
        return $query->where('is_confirmed', false);
    }

    public function containsProduct($id){
        $product_ids = $this->products->pluck('warehouse_product_id')->toArray();
        return in_array($id, $product_ids);
    }
}
