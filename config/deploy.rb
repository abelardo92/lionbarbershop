# config valid only for current version of Capistrano
lock '3.11.0'

set :application, 'lionbarbershop'
set :repo_url, 'git@bitbucket.org:abelardo92/lionbarbershop.git'
# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/lionbarbershop'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('.env')
set :laravel_dotenv_file, "./.env.#{fetch(:stage)}"

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :file_permissions_paths, [
  # 'bootstrap/cache',
  # 'storage',
  # 'storage/app',
  # # 'storage/app/public',
  # 'storage/framework',
  # 'storage/framework/cache',
  # 'storage/framework/sessions',
  # 'storage/framework/views',
  # 'storage/logs'
]
# set :file_permissions_chmod_mode, "0777"
# set :file_permissions_users, ["www-data"]

namespace :deploy do
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      within release_path do
        execute :php, "artisan db:seed --force"
        execute :php, "artisan bouncer:seed "
        execute :php, "artisan storage:link"
      end
    end
  end

  after :finishing, 'laravel:migrate'
  after :finishing, 'deploy:set_permissions:acl'
  after :finishing, 'deploy:restart'

end