<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->string('name');
            $table->string('short_name');
            $table->date('entry_date');
            $table->string('rfc');
            $table->string('curp');
            $table->string('nss');
            $table->string('address');
            $table->string('marital_status');
            $table->string('gender');
            $table->decimal('salary', 16, 2)->default(0.00);
            $table->decimal('commission', 16, 2)->default(0.00);
            $table->decimal('imss_salary', 16, 2)->default(0.00);
            $table->string('infonavit_number')->nullable();
            $table->integer('user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
