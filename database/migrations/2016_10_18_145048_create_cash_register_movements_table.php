<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegisterMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_register_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->integer('cash_register_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('fiftycents')->nullable();
            $table->integer('onepeso')->default(0);
            $table->integer('twopesos')->default(0);
            $table->integer('fivepesos')->default(0);
            $table->integer('tenpesos')->default(0);
            $table->integer('twentypesos')->default(0);
            $table->integer('fiftypesos')->default(0);
            $table->integer('hundredpesos')->default(0);
            $table->integer('twohundredpesos')->default(0);
            $table->integer('fivehundredpesos')->default(0);
            $table->integer('onethousandpesos')->default(0);
            $table->decimal('total', 16, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_register_movements');
    }
}
