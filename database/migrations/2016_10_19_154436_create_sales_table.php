<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable();
            $table->integer('subsidiary_id');
            $table->integer('employee_id')->nullable();
            $table->integer('turn_id')->nullable();
            $table->integer('exchange_rate_id')->nullable();
            $table->decimal('subtotal', 16, 2)->nullable();
            $table->decimal('tip', 16, 2)->nullable();
            $table->decimal('total', 16, 2)->nullable();
            $table->string('status')->default('en espera');
            $table->boolean('paid')->default(false);
            $table->string('payment_gateway')->nullable();
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
