<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->boolean('attend')->default(false);
            $table->timestamp('attended_at')->nullable();
            $table->boolean('finish')->default(false);
            $table->timestamp('finished_at')->nullable();
            $table->timestamp('pay_at')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('last_four')->nullable();
            $table->string('coin')->default('mxn');
            $table->decimal('money_change', 16, 2)->nullable();
            $table->decimal('qty', 16, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn([
                'attend',
                'attended_at',
                'finish',
                'finished_at',
                'pay_at',
                'card_brand',
                'last_four',
                'coin',
                'money_change',
            ]);
        });
    }
}
