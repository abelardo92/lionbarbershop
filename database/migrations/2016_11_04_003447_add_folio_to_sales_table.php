<?php

use App\Subsidiary;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFolioToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->integer('folio')->nullable();
        });

        $this->seedCurrentFolios();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('folio');
        });
    }

    public function seedCurrentFolios()
    {
        foreach (Subsidiary::all() as $subsidiary) {
            $folio = 1;
            foreach ($subsidiary->sales as $sale) {
                $sale->folio = $folio;
                $sale->save();
                $folio++;
            }
        }
    }
}
