<?php

use App\CashRegister;
use App\Schedule;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRestColumnsToSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->date('date')->nullable();
            $table->boolean('has_rest')->default(false);
            $table->integer('rest_minutes')->default(0);
        });
        $schedules = Schedule::with('turn')->get();
        if($schedules->count() > 0) {
            $this->convertSchedules();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('has_rest');
            $table->dropColumn('rest_minutes');
        });
    }

    protected function convertSchedules()
    {
        $schedules = Schedule::with('turn')->get();
        $cash_registers = CashRegister::with('turn')->get();
        Schedule::truncate();
        $matutino = Turn::where('identifier', 1)->first();
        $vespertino = Turn::where('identifier', 2)->first();
        $doble = Turn::where('identifier', 3)->first();
        $domingo = Turn::where('identifier', 4)->first();

        Turn::truncate();
        $matutino = Turn::create([
            'is_rest' => $matutino->is_rest,
            'name' => $matutino->name,
            'start' => $matutino->start,
            'end' => $matutino->end,
            'identifier' => 1
        ]);
        $vespertino = Turn::create([
            'is_rest' => $vespertino->is_rest,
            'name' => $vespertino->name,
            'start' => $vespertino->start,
            'end' => $vespertino->end,
            'identifier' => 2
        ]);
        $doble = Turn::create([
            'is_rest' => $doble->is_rest,
            'name' => $doble->name,
            'start' => $doble->start,
            'end' => $doble->end,
            'identifier' => 3
        ]);
        $domingo = Turn::create([
            'is_rest' => $domingo->is_rest,
            'name' => $domingo->name,
            'start' => $domingo->start,
            'end' => $domingo->end,
            'identifier' => 4
        ]);
        
        foreach ($schedules as $schedule) {
            $dates = [];
            $start = new Carbon($schedule->start);
            $end = new Carbon($schedule->end);
            while ($start->lte($end)) {
                $date = $start->copy();
                if ($date->format('l') == $schedule->day) {
                     $dates[] = $date;
                }
                $start->addDay();
            }

            if ($schedule->turn->identifier == 1) {
                $turn_id = $matutino->id;
            }
            if ($schedule->turn->identifier == 2) {
                $turn_id = $vespertino->id;
            }
            if ($schedule->turn->identifier == 3) {
                $turn_id = $doble->id;
            }
            if ($schedule->turn->identifier == 4) {
                $turn_id = $domingo->id;
            }

            foreach ($dates as $date) {
                Schedule::create([
                    'employee_id' => $schedule->employee_id,
                    'day' => $schedule->day,
                    'turn_id' => $turn_id,
                    'subsidiary_id' => $schedule->subsidiary_id,
                    'start' => $schedule->start,
                    'end' => $schedule->end,
                    'date' => $date->format('Y-m-d')
                ]);
            }

            foreach ($cash_registers as $cash_register) {
                if ($cash_register->turn->identifier == 1) {
                    $cash_register->turn_id = $matutino->id;
                }
                if ($cash_register->turn->identifier == 2) {
                    $cash_register->turn_id = $vespertino->id;
                }
                if ($cash_register->turn->identifier == 3) {
                    $cash_register->turn_id = $doble->id;
                }
                if ($cash_register->turn->identifier == 4) {
                    $cash_register->turn_id = $domingo->id;
                }
                $cash_register->save();
            }
        }
    }
}
