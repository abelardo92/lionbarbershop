<?php

use App\Sale;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class CreateSalePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('method')->default('Efectivo');
            $table->decimal('total');
            $table->text('comment')->nullable();
            $table->integer('sale_id');
            $table->timestamps();
        });

        $this->importSalePayments();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_payments');
    }

    public function importSalePayments()
    {
        $total = 0;
        foreach (Sale::all() as $sale) {
            $data = [
                'method' => $sale->payment_gateway ?: 'Efectivo',
                'total' => $sale->subtotal
            ];
            if ($sale->payment_gateway == 'Tarjeta') {
                $data['comment'] = "{$sale->card_brand} - {$sale->last_four}";
            }
            $sale->payments()->create($data);
            $total++;
        }

        Log::info("{$total} sales payments imported");
    }
}
