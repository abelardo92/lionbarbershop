<?php

use App\Sale;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipInToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->string('tip_in')->default('efectivo')->after('tip');
        });

        $this->setTipInColumn();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropColumn('tip_in');
        });
    }

    public function setTipInColumn()
    {
        foreach (Sale::all() as $sale) {
            $tip_in = null;
            if ((bool) !$sale->payment_gateway) {
                $tip_in = strtolower($sale->payment_gateway);
            }
            if ((bool) !$tip_in) {
                $tip_in = 'efectivo';
            }
            $sale->tip_in = $tip_in;
            $sale->save();
        }
    }
}
