<?php

use App\Product;
use App\Subsidiary;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSubsidiaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_subsidiary', function (Blueprint $table) {
            $table->integer('product_id');
            $table->integer('subsidiary_id');
            $table->integer('existence')->default(0);
        });

        $this->uptadeInventory();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_subsidiary');
    }

    public function uptadeInventory()
    {
        foreach (Subsidiary::all() as $subsidiary) {
            foreach (Product::all() as $product) {
                $entry = $subsidiary->entries()->where('product_id', $product->id)->sum('qty');
                $departure = $subsidiary->departures()->where('product_id', $product->id)->sum('qty');
                $adjustment = $subsidiary->adjustments()->where('product_id', $product->id)->sum('real_existence');

                $total = $entry - $departure;
                if ($adjustment > 0) {
                    $total = $adjustment;
                }

                $subsidiary->products()->attach($product, ['existence' => $total]);
                
            }
        }
    }
}
