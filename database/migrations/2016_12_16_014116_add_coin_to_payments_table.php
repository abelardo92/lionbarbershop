<?php

use App\Sale;
use App\SalePayment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoinToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_payments', function (Blueprint $table) {
            $table->string('coin')->default('mxn');
        });

        $this->updatePayments();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_payments', function (Blueprint $table) {
            $table->dropColumn('coin');
        });
    }

    public function updatePayments()
    {
        // foreach (Sale::efectivo()->where('coin', 'usd')->get() as $sale) {
        //     foreach ($sale->payments()->efectivo()->get() as $payment) {
        //         $payment->coin = 'usd';
        //         $payment->save();
        //     }
        // }

        foreach (SalePayment::efectivo()->get() as $payment) {
            if($payment->sale->coin == 'usd'){
                $payment->coin = 'usd';
                $payment->save();
            }
        }
    }
}
