<?php

use App\Customer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServicesFreeToCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('services_free')->default(0);
        });

        $this->migrateVisits();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('services_free');
        });
    }

    protected function migrateVisits()
    {
        foreach (Customer::where('visits', '>=', 10)->get() as $customer) {
            $customer->addServicesFree();
            $customer->resetVisits();
        }
    }
}
