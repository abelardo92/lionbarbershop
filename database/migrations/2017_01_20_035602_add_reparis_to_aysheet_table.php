<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReparisToAysheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paysheets', function (Blueprint $table) {
            $table->decimal('repairs', 10, 2)->nullable();
            $table->decimal('discount_repairs', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paysheets', function (Blueprint $table) {
            $table->dropColumn('repairs');
            $table->dropColumn('discount_repairs');
        });
    }
}
