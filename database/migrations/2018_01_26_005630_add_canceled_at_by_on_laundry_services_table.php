<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanceledAtByOnLaundryServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laundry_services', function (Blueprint $table) {
            $table->timestamp('canceled_at')->nullable();
            $table->integer('canceled_by')->unsigned()->nullable();
            $table->foreign('canceled_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laundry_services', function (Blueprint $table) {
            $table->dropForeign(['canceled_by']);
            $table->dropColumn('canceled_by');
            $table->dropColumn('canceled_at');
        });
    }
}
