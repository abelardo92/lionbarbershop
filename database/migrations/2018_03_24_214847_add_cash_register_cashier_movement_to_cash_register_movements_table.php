<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashRegisterCashierMovementToCashRegisterMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_register_movements', function (Blueprint $table) {
            $table->integer('cash_register_cashier_id')->unsigned()->nullable();
            $table->foreign('cash_register_cashier_id')->references('id')->on('cash_register_cashier_movements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_register_movements', function (Blueprint $table) {
            $table->dropForeign(['cash_register_cashier_id']);
            $table->dropColumn('cash_register_cashier_id');
        });
    }
}
