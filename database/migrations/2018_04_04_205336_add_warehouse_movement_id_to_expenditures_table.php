<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWarehouseMovementIdToExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenditures', function (Blueprint $table) {
            $table->integer('warehouse_movement_id')->unsigned()->nullable();
            $table->foreign('warehouse_movement_id')->references('id')->on('warehouse_movements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenditures', function (Blueprint $table) {
            $table->dropForeign(['warehouse_movement_id']);
            $table->dropColumn('warehouse_movement_id');
        });
    }
}
