<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTaxRegimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_regimes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $query = "INSERT INTO
        tax_regimes (name)
        VALUES
        ('Régimen Simplificado de Confianza'),
        ('Sueldos y salarios e ingresos asimilados a salarios'),
        ('Régimen de Actividades Empresariales y Profesionales'),
        ('Régimen de Incorporación Fiscal'),
        ('Enajenación de bienes'),
        ('Régimen de Actividades Empresariales con ingresos a través de Plataformas Tecnológicas'),
        ('Régimen de Arrendamiento'),
        ('Intereses'),
        ('Obtención de premios'),
        ('Dividendos'),
        ('Demás ingresos')";

        DB::statement($query);

        Schema::table('accounting_customers', function (Blueprint $table) {
            $table->integer('tax_regime_id')->unsigned();
            $table->foreign('tax_regime_id')->references('id')->on('tax_regimes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_customers', function (Blueprint $table) {
            $table->dropForeign(['tax_regime_id']);
            $table->dropColumn('tax_regime_id');
        });
        Schema::dropIfExists('tax_regimes');
    }
}
