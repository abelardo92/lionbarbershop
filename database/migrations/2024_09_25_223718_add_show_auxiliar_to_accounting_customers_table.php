<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowAuxiliarToAccountingCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounting_customers', function (Blueprint $table) {
            $table->boolean('show_to_auxiliar')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounting_customers', function (Blueprint $table) {
            $table->dropColumn('show_to_auxiliar');
        });
    }
}
