<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_folders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tab_name');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('custom_sub_folders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tab_name');
            $table->boolean('active')->default(true);
            $table->integer('custom_folder_id')->unsigned()->nullable();
            $table->foreign('custom_folder_id')->references('id')->on('custom_folders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_sub_folders');
        Schema::dropIfExists('custom_folders');
    }
}
