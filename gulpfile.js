const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

elixir(mix => {
    mix.sass('app.scss')
       .webpack('app.js')
       .webpack('surveys/questions_handler.js')
       .webpack('surveys/respond_survey_handler.js')
       .sass('survey.scss')
       .webpack('survey.js');
       // .webpack('paysheet.js');
});
