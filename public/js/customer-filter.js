$(function () {    
        $('.customer_id').select2({
            ajax: {
                url: "/home/customers/byFilter",
                dataType: 'json',
                delay: 500,
                data: function (params) {
                  return {
                    filter: params.term, // search term
                    page: params.page
                  };
                },
                
                processResults: function (data, params) {
                  return {
                    results: data
                    };
                },
                cache: true
              },
              placeholder: 'Busca un cliente...',
              escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
              minimumInputLength: 3,
              templateResult: formatCustomer,
              templateSelection: formatCustomerSelection
        });

        function formatCustomer (customer) {

            if (customer.loading) {
                return customer.name;
            }

            var markup = "<div class='select2-result-customersitory clearfix'>" +
            "<div class='select2-result-customersitory__meta'>" +
            "<div class='select2-result-customersitory__title'>" + customer.name; 
            if(customer.wallet_number) {
                markup += " (" + customer.wallet_number + ")";
            }
            markup += "</div>";

            if (customer.phone) {
                markup += "<div class='select2-result-customersitory__description'>cel: " + customer.phone + "</div>";
            }
            "</div></div>";

            return markup;
        }

        function formatCustomerSelection (customer) {
          if(customer == null) {
            return "Busca un cliente...";
          }
          return customer.name || customer.phone;
        }
    });