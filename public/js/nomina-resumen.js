
function updatePaysheet(paysheet_id) {
    $('#paysheet-error').hide();
    $('#paysheet-success').hide();
    var paysheetData = getPaysheetData(paysheet_id);
    
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: paysheetData,
        url: '/home/paysheets/updateById',
        success: function(data) {
            if(data.success) {
                $('#paysheet-success').show();    
                $('#paysheet-success').html(data.message);
            }    
        },
        error: function (error) {
            $('#paysheet-error').show();
            $('#paysheet-error').html(JSON.stringify(error));
        }
    });   
}

function archivePaysheet(paysheet_id) {
    $('#paysheet-error').hide();
    $('#paysheet-success').hide();
    var paysheetData = getPaysheetData(paysheet_id);
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: paysheetData,
        url: '/home/paysheets/archiveById',
        success: function(data) {
            if(data.success) {
                $('#paysheet-success').show();    
                $('#paysheet-success').html(data.message);
            }    
        },
        error: function (error) {
            $('#paysheet-error').show();
            $('#paysheet-error').html(JSON.stringify(error));
        }
    });
}

function getPaysheetData(paysheet_id) {
    var data = { 
        _token: $('input[name="_token"]').val(),
        id: paysheet_id,
        asistencia: Number($('#attendance-text').html().replace(',','')),
        asistencia_pay: $('#check-attendance').is(':checked') ? 1 : 0,
        puntualidad: Number($('#punctuality-text').html().replace(',','')),
        puntualidad_pay: $('#check-punctuality').is(':checked') ? 1 : 0,
        productividad: Number($('#productivity-text').html().replace(',','')), 
        productividad_pay: $('#check-productivity').is(':checked') ? 1 : 0,
        excellence: Number($('#excellence-text').html().replace(',','')), 
        excellence_pay: $('#check-excellence').is(':checked') ? 1 : 0,
        extra: Number($('#extraday-text').html().replace(',','')), 
        extra_pay: $('#check-extraday').is(':checked') ? 1 : 0,
        prestamo: Number($('#prestamo').val()),
        uniforme: Number($('#uniform').val()),
        infonavit: Number($('#infonavit').val()),
        depositado: Number($('#depositado').val()),
    };
    return data;
}

$(function(){

    function updateTotales() {
        var total = Number($('#total-text').html().replace(',',''));
        var totaldeducciones = Number($('#total-deducciones-text').html().replace(',',''));
        var neto = total - totaldeducciones; 
        var depositado = Number($('#depositado').val());
        $('#neto-text').text(parseFloat(neto).toFixed(2));
        $('#diferencia-text').text(parseFloat(neto - depositado).toFixed(2));
    }

    function updateIngresos() {
        var total = Number($('#salarios-text').html().replace(',',''));
        var asistencia = Number($('#attendance-text').html().replace(',',''));
        var puntualidad = Number($('#punctuality-text').html().replace(',',''));
        var productividad = Number($('#productivity-text').html().replace(',',''));
        var excellence = Number($('#excellence-text').html().replace(',',''));
        var extra = Number($('#extraday-text').html().replace(',',''));
        var tips = Number($('#tips-text').html().replace(',',''));
        var commission = Number($('#commission-text').html().replace(',',''));
        var perceptions = Number($('#perceptions-text').html().replace(',',''));
        var sum = total + asistencia + puntualidad + productividad + excellence + extra + tips + commission + perceptions;
        //alert(sum);
        $('#total-text').text(parseFloat(sum).toFixed(2));
        updateTotales();
    }

    function updateDeducciones() {
        var prestamo = Number($('#prestamo').val());
        var faltas = Number($('#faults-text').html().replace(',',''));
        var uniforme = Number($('#uniform').val());
        var infonavit = Number($('#infonavit').val());
        var comision = Number($('#commissiontip-text').html().replace(',',''));
        var deductions = Number($('#deductions-text').html().replace(',',''));
        var sum = prestamo + faltas + uniforme + infonavit + comision + deductions;
        $('#total-deducciones-text').text(parseFloat(sum).toFixed(2));
        updateTotales();
    }

    $('#check-attendance, #check-punctuality, #check-productivity, #check-excellence, #check-extraday').on('change', function (e) {
        var $this = $(e.currentTarget);
        var input = $this.attr('id').split('-')[1];
        var val = 0;
        if ($this.is(':checked')) {
            val = $('#'+input).val();
        } 
        $('#'+input+'-text').text(val+'.00');
        updateIngresos();
    });

    $('#check-faults').on('change', function (e) {
        var $this = $(e.currentTarget);
        var input = $this.attr('id').split('-')[1];
        var val = 0;
        if ($this.is(':checked')) {
            val = $('#'+input).val();
        } 
        $('#'+input+'-text').text(val+'.00');
        updateDeducciones();
    });

    $('#prestamo, #uniform, #infonavit').on('change', function (e) {
        updateDeducciones();
    });

    $('#depositado').on('change', function (e) {
        updateTotales();
    });
});