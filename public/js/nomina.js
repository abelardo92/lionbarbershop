$(function(){
    function updateTotales() {
        var ingresos = Number($('#total_ingreso').val());
        var deducciones = Number($('#total_deducciones').val());
        var depositado = Number($('#depositado').val());
        var sum = ingresos - deducciones;
        $('#neto').val(sum);
        $('#neto-text').text(sum);
        $('#diferencia').val(sum - depositado)
        $('#diferencia-text').text(sum - depositado)
    }

    function updateIngresos() {
        var total = Number($('#total').val());
        var asistencia = Number($('#asistencia').val());
        var puntualidad = Number($('#puntualidad').val());
        var productividad = Number($('#productividad').val());
        var excellence = Number($('#excellence').val());
        var extra = Number($('#extra').val());
        var otros_ingresos = Number($('#otros_ingresos').val());
        var sum = total + asistencia + puntualidad + productividad + excellence + extra + otros_ingresos;
        $('#total_ingreso').val(sum);
        $('#total-text').text(sum);
        updateTotales();
    }
    $('#asistencia_pay, #puntualidad_pay, #productividad_pay, #excellence_pay, #extra_pay').on('change', function (e) {
        var $this = $(e.currentTarget);
        var input = $this.attr('id').split('_')[0];
        var val = 0;
        if ($this.is(':checked')) {
            val = $('#'+input).data('val');
        }
        $('#'+input).val(val);
        $('#'+input+'-text').text(val);
        updateIngresos()
    });

    function updateDeducciones() {
        var prestamo = Number($('#prestamo').val());
        var faltas = Number($('#faltas').val());
        var uniforme = Number($('#uniforme').val());
        var infonavit = Number($('#infonavit').val());
        var otras = Number($('#otras').val());
        var sum = prestamo + faltas + uniforme + infonavit + otras;
        $('#total_deducciones').val(sum);
        $('#total-deducciones-text').text(sum);
        updateTotales()
    }

    $('#prestamo, #uniforme, #infonavit').on('change', function (e) {
        updateDeducciones()
    });
});