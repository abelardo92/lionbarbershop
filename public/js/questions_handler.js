/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

"use strict";
eval("'use strict';\n\nwindow.QuestionsHandler = {\n    init: function init() {\n        this.initEvents();\n    },\n    initEvents: function initEvents() {\n        this.addInputTypeSelectEvent();\n        this.addOptionButtonEvent();\n        this.removeOptionButtonEvent();\n    },\n    addInputTypeSelectEvent: function addInputTypeSelectEvent() {\n        $('.js-input-type-select').on('change', function (e) {\n            var select = $(e.target);\n            switch (select.val()) {\n                case \"checkbox\":\n                case \"radio\":\n                    $('.js-options').show();\n                    break;\n                default:\n                    $('.js-options').hide();\n            }\n        }.bind(this));\n    },\n    addOptionButtonEvent: function addOptionButtonEvent() {\n        $('.js-add-option').on('click', function (e) {\n            $('.js-options-container').append(this.getHtmlOptionMarkup());\n            this.removeOptionButtonEvent();\n        }.bind(this));\n    },\n    getHtmlOptionMarkup: function getHtmlOptionMarkup() {\n        var uuid = this.generateOptionId();\n        return '\\n            <div class=\"form-group\" id=\"option-' + uuid + '\">\\n                <label for=\"' + uuid + '\" class=\"col-md-4 text-right\">\\n                    <button type=\"button\" class=\"btn btn-link btn-xs js-remove-option\" data-id=\"option-' + uuid + '\">x</button>\\n                    Opci\\xF3n\\n                </label>\\n                <div class=\"col-md-8\">\\n                    <input type=\"text\" class=\"form-control\" name=\"options[]\" id=\"' + uuid + '\" required>\\n                </div>\\n            </div>\\n        ';\n    },\n    generateOptionId: function generateOptionId() {\n        var d = new Date().getTime();\n        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {\n            var r = (d + Math.random() * 16) % 16 | 0;\n            d = Math.floor(d / 16);\n            return (c == 'x' ? r : r & 0x3 | 0x8).toString(16);\n        });\n        return uuid;\n    },\n    removeOptionButtonEvent: function removeOptionButtonEvent() {\n        $('.js-remove-option').off().on('click', function (e) {\n            var id = $(e.target).data('id');\n            $('#' + id).remove();\n        }.bind(this));\n    }\n};//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL3N1cnZleXMvcXVlc3Rpb25zX2hhbmRsZXIuanM/NzM5MyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbndpbmRvdy5RdWVzdGlvbnNIYW5kbGVyID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoKSB7XG4gICAgICAgIHRoaXMuaW5pdEV2ZW50cygpO1xuICAgIH0sXG4gICAgaW5pdEV2ZW50czogZnVuY3Rpb24gaW5pdEV2ZW50cygpIHtcbiAgICAgICAgdGhpcy5hZGRJbnB1dFR5cGVTZWxlY3RFdmVudCgpO1xuICAgICAgICB0aGlzLmFkZE9wdGlvbkJ1dHRvbkV2ZW50KCk7XG4gICAgICAgIHRoaXMucmVtb3ZlT3B0aW9uQnV0dG9uRXZlbnQoKTtcbiAgICB9LFxuICAgIGFkZElucHV0VHlwZVNlbGVjdEV2ZW50OiBmdW5jdGlvbiBhZGRJbnB1dFR5cGVTZWxlY3RFdmVudCgpIHtcbiAgICAgICAgJCgnLmpzLWlucHV0LXR5cGUtc2VsZWN0Jykub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICB2YXIgc2VsZWN0ID0gJChlLnRhcmdldCk7XG4gICAgICAgICAgICBzd2l0Y2ggKHNlbGVjdC52YWwoKSkge1xuICAgICAgICAgICAgICAgIGNhc2UgXCJjaGVja2JveFwiOlxuICAgICAgICAgICAgICAgIGNhc2UgXCJyYWRpb1wiOlxuICAgICAgICAgICAgICAgICAgICAkKCcuanMtb3B0aW9ucycpLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgJCgnLmpzLW9wdGlvbnMnKS5oaWRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0uYmluZCh0aGlzKSk7XG4gICAgfSxcbiAgICBhZGRPcHRpb25CdXR0b25FdmVudDogZnVuY3Rpb24gYWRkT3B0aW9uQnV0dG9uRXZlbnQoKSB7XG4gICAgICAgICQoJy5qcy1hZGQtb3B0aW9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICQoJy5qcy1vcHRpb25zLWNvbnRhaW5lcicpLmFwcGVuZCh0aGlzLmdldEh0bWxPcHRpb25NYXJrdXAoKSk7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZU9wdGlvbkJ1dHRvbkV2ZW50KCk7XG4gICAgICAgIH0uYmluZCh0aGlzKSk7XG4gICAgfSxcbiAgICBnZXRIdG1sT3B0aW9uTWFya3VwOiBmdW5jdGlvbiBnZXRIdG1sT3B0aW9uTWFya3VwKCkge1xuICAgICAgICB2YXIgdXVpZCA9IHRoaXMuZ2VuZXJhdGVPcHRpb25JZCgpO1xuICAgICAgICByZXR1cm4gJ1xcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwXCIgaWQ9XCJvcHRpb24tJyArIHV1aWQgKyAnXCI+XFxuICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCInICsgdXVpZCArICdcIiBjbGFzcz1cImNvbC1tZC00IHRleHQtcmlnaHRcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1saW5rIGJ0bi14cyBqcy1yZW1vdmUtb3B0aW9uXCIgZGF0YS1pZD1cIm9wdGlvbi0nICsgdXVpZCArICdcIj54PC9idXR0b24+XFxuICAgICAgICAgICAgICAgICAgICBPcGNpXFx4RjNuXFxuICAgICAgICAgICAgICAgIDwvbGFiZWw+XFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtOFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBuYW1lPVwib3B0aW9uc1tdXCIgaWQ9XCInICsgdXVpZCArICdcIiByZXF1aXJlZD5cXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAnO1xuICAgIH0sXG4gICAgZ2VuZXJhdGVPcHRpb25JZDogZnVuY3Rpb24gZ2VuZXJhdGVPcHRpb25JZCgpIHtcbiAgICAgICAgdmFyIGQgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgdmFyIHV1aWQgPSAneHh4eHh4eHgteHh4eC00eHh4LXl4eHgteHh4eHh4eHh4eHh4Jy5yZXBsYWNlKC9beHldL2csIGZ1bmN0aW9uIChjKSB7XG4gICAgICAgICAgICB2YXIgciA9IChkICsgTWF0aC5yYW5kb20oKSAqIDE2KSAlIDE2IHwgMDtcbiAgICAgICAgICAgIGQgPSBNYXRoLmZsb29yKGQgLyAxNik7XG4gICAgICAgICAgICByZXR1cm4gKGMgPT0gJ3gnID8gciA6IHIgJiAweDMgfCAweDgpLnRvU3RyaW5nKDE2KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB1dWlkO1xuICAgIH0sXG4gICAgcmVtb3ZlT3B0aW9uQnV0dG9uRXZlbnQ6IGZ1bmN0aW9uIHJlbW92ZU9wdGlvbkJ1dHRvbkV2ZW50KCkge1xuICAgICAgICAkKCcuanMtcmVtb3ZlLW9wdGlvbicpLm9mZigpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICB2YXIgaWQgPSAkKGUudGFyZ2V0KS5kYXRhKCdpZCcpO1xuICAgICAgICAgICAgJCgnIycgKyBpZCkucmVtb3ZlKCk7XG4gICAgICAgIH0uYmluZCh0aGlzKSk7XG4gICAgfVxufTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2Fzc2V0cy9qcy9zdXJ2ZXlzL3F1ZXN0aW9uc19oYW5kbGVyLmpzIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9");

/***/ }
/******/ ]);