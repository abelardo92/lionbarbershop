/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

"use strict";
eval("'use strict';\n\nwindow.RespondSurveyHandler = {\n    total_questions: 0,\n    current_question: 1,\n    init: function init() {\n        this.initEvents();\n    },\n    initEvents: function initEvents() {\n        this.addFormEvent();\n        this.addNextButtonEvent();\n        this.addRadioCheckboxEvent();\n        this.showHideButtonsContent();\n    },\n    addFormEvent: function addFormEvent() {\n        $('.js-form-respond').on('submit', function (e) {\n            e.preventDefault();\n            $(e.target).hide();\n            $('.js-show-on-submit').show();\n            $.ajax({\n                type: 'put',\n                dataType: 'json',\n                data: $(e.target).serialize(),\n                url: $(e.target).attr('action'),\n                headers: {\n                    'X-CSRF-TOKEN': Laravel.csrfToken\n                },\n                success: function success(survey) {\n                    $('.js-message').text('Gracias por responder la encuesta, que tengas un excelente dia.');\n                    setTimeout(function () {\n                        window.location.href = '/surveys';\n                    }, 2000);\n                }\n            });\n            return false;\n        });\n    },\n    addNextButtonEvent: function addNextButtonEvent() {\n        var _this = this;\n\n        $('.js-next-button').on('click', function () {\n            _this.nextEvent();\n        });\n    },\n    nextEvent: function nextEvent() {\n        var old = this.current_question;\n        if ($('[data-index=\"' + old + '\"]').data('aswered') == '0') {\n            return false;\n        }\n        this.current_question++;\n        if (this.total_questions == this.current_question) {\n            $('.js-next-button').hide();\n            $('.js-submit-button').show();\n        }\n        $('[data-index=\"' + this.current_question + '\"]').show();\n        $('[data-index=\"' + old + '\"]').hide();\n        this.showHideButtonsContent();\n    },\n    addRadioCheckboxEvent: function addRadioCheckboxEvent() {\n        var _this2 = this;\n\n        $('.js-radio-change, .js-checkbox-change').on('click', function (e) {\n            var index = $(e.target).data('index');\n            $('[data-index=\"' + index + '\"]').data('aswered', '1');\n            if ($(e.target).hasClass('js-radio-change')) {\n                if ($('.js-submit-button').is(':visible')) {\n                    $('.js-submit-button').trigger('click');\n                } else {\n                    _this2.nextEvent();\n                }\n            }\n        });\n    },\n    showHideButtonsContent: function showHideButtonsContent() {\n        var questionType = $('.js-question-container:visible').data('type');\n        if (questionType == 'satisfaction') {\n            $('.js-buttons-content').hide();\n        } else {\n            $('.js-buttons-content').show();\n        }\n        if (this.total_questions == this.current_question) {\n            $('.js-buttons-content').show();\n            $('.js-next-button').hide();\n            $('.js-submit-button').show();\n        }\n    }\n};//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL3N1cnZleXMvcmVzcG9uZF9zdXJ2ZXlfaGFuZGxlci5qcz9lNzg0Il0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0Jztcblxud2luZG93LlJlc3BvbmRTdXJ2ZXlIYW5kbGVyID0ge1xuICAgIHRvdGFsX3F1ZXN0aW9uczogMCxcbiAgICBjdXJyZW50X3F1ZXN0aW9uOiAxLFxuICAgIGluaXQ6IGZ1bmN0aW9uIGluaXQoKSB7XG4gICAgICAgIHRoaXMuaW5pdEV2ZW50cygpO1xuICAgIH0sXG4gICAgaW5pdEV2ZW50czogZnVuY3Rpb24gaW5pdEV2ZW50cygpIHtcbiAgICAgICAgdGhpcy5hZGRGb3JtRXZlbnQoKTtcbiAgICAgICAgdGhpcy5hZGROZXh0QnV0dG9uRXZlbnQoKTtcbiAgICAgICAgdGhpcy5hZGRSYWRpb0NoZWNrYm94RXZlbnQoKTtcbiAgICAgICAgdGhpcy5zaG93SGlkZUJ1dHRvbnNDb250ZW50KCk7XG4gICAgfSxcbiAgICBhZGRGb3JtRXZlbnQ6IGZ1bmN0aW9uIGFkZEZvcm1FdmVudCgpIHtcbiAgICAgICAgJCgnLmpzLWZvcm0tcmVzcG9uZCcpLm9uKCdzdWJtaXQnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJChlLnRhcmdldCkuaGlkZSgpO1xuICAgICAgICAgICAgJCgnLmpzLXNob3ctb24tc3VibWl0Jykuc2hvdygpO1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB0eXBlOiAncHV0JyxcbiAgICAgICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgICAgIGRhdGE6ICQoZS50YXJnZXQpLnNlcmlhbGl6ZSgpLFxuICAgICAgICAgICAgICAgIHVybDogJChlLnRhcmdldCkuYXR0cignYWN0aW9uJyksXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAnWC1DU1JGLVRPS0VOJzogTGFyYXZlbC5jc3JmVG9rZW5cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIHN1Y2Nlc3Moc3VydmV5KSB7XG4gICAgICAgICAgICAgICAgICAgICQoJy5qcy1tZXNzYWdlJykudGV4dCgnR3JhY2lhcyBwb3IgcmVzcG9uZGVyIGxhIGVuY3Vlc3RhLCBxdWUgdGVuZ2FzIHVuIGV4Y2VsZW50ZSBkaWEuJyk7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnL3N1cnZleXMnO1xuICAgICAgICAgICAgICAgICAgICB9LCAyMDAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgfSxcbiAgICBhZGROZXh0QnV0dG9uRXZlbnQ6IGZ1bmN0aW9uIGFkZE5leHRCdXR0b25FdmVudCgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgICAgICAkKCcuanMtbmV4dC1idXR0b24nKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy5uZXh0RXZlbnQoKTtcbiAgICAgICAgfSk7XG4gICAgfSxcbiAgICBuZXh0RXZlbnQ6IGZ1bmN0aW9uIG5leHRFdmVudCgpIHtcbiAgICAgICAgdmFyIG9sZCA9IHRoaXMuY3VycmVudF9xdWVzdGlvbjtcbiAgICAgICAgaWYgKCQoJ1tkYXRhLWluZGV4PVwiJyArIG9sZCArICdcIl0nKS5kYXRhKCdhc3dlcmVkJykgPT0gJzAnKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jdXJyZW50X3F1ZXN0aW9uKys7XG4gICAgICAgIGlmICh0aGlzLnRvdGFsX3F1ZXN0aW9ucyA9PSB0aGlzLmN1cnJlbnRfcXVlc3Rpb24pIHtcbiAgICAgICAgICAgICQoJy5qcy1uZXh0LWJ1dHRvbicpLmhpZGUoKTtcbiAgICAgICAgICAgICQoJy5qcy1zdWJtaXQtYnV0dG9uJykuc2hvdygpO1xuICAgICAgICB9XG4gICAgICAgICQoJ1tkYXRhLWluZGV4PVwiJyArIHRoaXMuY3VycmVudF9xdWVzdGlvbiArICdcIl0nKS5zaG93KCk7XG4gICAgICAgICQoJ1tkYXRhLWluZGV4PVwiJyArIG9sZCArICdcIl0nKS5oaWRlKCk7XG4gICAgICAgIHRoaXMuc2hvd0hpZGVCdXR0b25zQ29udGVudCgpO1xuICAgIH0sXG4gICAgYWRkUmFkaW9DaGVja2JveEV2ZW50OiBmdW5jdGlvbiBhZGRSYWRpb0NoZWNrYm94RXZlbnQoKSB7XG4gICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgICQoJy5qcy1yYWRpby1jaGFuZ2UsIC5qcy1jaGVja2JveC1jaGFuZ2UnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyIGluZGV4ID0gJChlLnRhcmdldCkuZGF0YSgnaW5kZXgnKTtcbiAgICAgICAgICAgICQoJ1tkYXRhLWluZGV4PVwiJyArIGluZGV4ICsgJ1wiXScpLmRhdGEoJ2Fzd2VyZWQnLCAnMScpO1xuICAgICAgICAgICAgaWYgKCQoZS50YXJnZXQpLmhhc0NsYXNzKCdqcy1yYWRpby1jaGFuZ2UnKSkge1xuICAgICAgICAgICAgICAgIGlmICgkKCcuanMtc3VibWl0LWJ1dHRvbicpLmlzKCc6dmlzaWJsZScpKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJy5qcy1zdWJtaXQtYnV0dG9uJykudHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBfdGhpczIubmV4dEV2ZW50KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9LFxuICAgIHNob3dIaWRlQnV0dG9uc0NvbnRlbnQ6IGZ1bmN0aW9uIHNob3dIaWRlQnV0dG9uc0NvbnRlbnQoKSB7XG4gICAgICAgIHZhciBxdWVzdGlvblR5cGUgPSAkKCcuanMtcXVlc3Rpb24tY29udGFpbmVyOnZpc2libGUnKS5kYXRhKCd0eXBlJyk7XG4gICAgICAgIGlmIChxdWVzdGlvblR5cGUgPT0gJ3NhdGlzZmFjdGlvbicpIHtcbiAgICAgICAgICAgICQoJy5qcy1idXR0b25zLWNvbnRlbnQnKS5oaWRlKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKCcuanMtYnV0dG9ucy1jb250ZW50Jykuc2hvdygpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnRvdGFsX3F1ZXN0aW9ucyA9PSB0aGlzLmN1cnJlbnRfcXVlc3Rpb24pIHtcbiAgICAgICAgICAgICQoJy5qcy1idXR0b25zLWNvbnRlbnQnKS5zaG93KCk7XG4gICAgICAgICAgICAkKCcuanMtbmV4dC1idXR0b24nKS5oaWRlKCk7XG4gICAgICAgICAgICAkKCcuanMtc3VibWl0LWJ1dHRvbicpLnNob3coKTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHJlc291cmNlcy9hc3NldHMvanMvc3VydmV5cy9yZXNwb25kX3N1cnZleV9oYW5kbGVyLmpzIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=");

/***/ }
/******/ ]);