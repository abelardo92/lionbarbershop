require('./bootstrap');
require('./paysheet');

Vue.component('customers', require('./components/Customers.vue').default);
Vue.component('warehouse-request-table', require('./components/WarehouseRequestTable.vue').default);
Vue.component('warehouse-movement-table', require('./components/WarehouseMovementTable.vue').default);
Vue.component('warehouse-movement-table-form', require('./components/WarehouseMovementTableForm.vue').default);
Vue.component('sale-payments-form', require('./components/SalePaymentsForm.vue').default);
Vue.component('entradas', require('./components/Entradas.vue').default);
Vue.component('departures', require('./components/Departures.vue').default);
Vue.component('active-sales', require('./components/ActiveSales.vue').default);
Vue.component('adjustments', require('./components/Adjustments.vue').default);
Vue.component('warehouse-adjustments', require('./components/WarehouseAdjustments.vue').default);
Vue.component('entradas-articles', require('./components/ArticlesEntradas.vue').default);
Vue.component('ajustes-articles', require('./components/ArticlesAdjustments.vue').default);
Vue.component('attendance', require('./components/Attendance.vue').default);
Vue.component('attendance-device', require('./components/AttendanceDevice.vue').default);
Vue.component('cash-register', require('./components/CashRegister.vue').default);
Vue.component('laundry-cash-register', require('./components/LaundryCashRegister.vue').default);
Vue.component('sale-detail', require('./components/SaleDetail.vue').default);
Vue.component('sale-detail-laundry', require('./components/SaleDetailLaundry.vue').default);
Vue.component('add-product-service', require('./components/AddProductService.vue').default);
Vue.component('pay-advance-laundry', require('./components/PayAdvanceLaundry.vue').default);
Vue.component('add-laundry-product-service', require('./components/AddLaundryProductService.vue').default);
Vue.component('show-exchange-children', require('./components/ShowExchangeChildren.vue').default);
Vue.component('mini-cut', require('./components/MiniCut.vue').default);
Vue.component('cash-cut', require('./components/CashCut.vue').default);
Vue.component('cash-cut-button', require('./components/CashCutButton.vue').default);
Vue.component('cash-cut-modal', require('./components/CashCutModal.vue').default);
Vue.component('schedules', require('./components/Schedules.vue').default);
Vue.component('cancel-sale', require('./components/CancelSale.vue').default);
Vue.component('cancel-sale-modal', require('./components/CancelSaleModal.vue').default);
Vue.component('cancel-admin-sale', require('./components/CancelAdminSale.vue').default);
Vue.component('cancel-admin-sale-modal', require('./components/CancelAdminSaleModal.vue').default);
Vue.component('log-in-to-subsidiary', require('./components/LogInToSubsidiary.vue').default);
Vue.component('log-in-to-subsidiary-modal', require('./components/LogInToSubsidiaryModal.vue').default);
Vue.component('reprint-cuts', require('./components/RePrintCuts.vue').default);
Vue.component('reprint-cuts-modal', require('./components/ReprintCutsModal.vue').default);
Vue.component('reprint-turn-sale', require('./components/ReprintTurnSale.vue').default);
Vue.component('reprint-turn-sale-modal', require('./components/ReprintTurnSaleModal.vue').default);
Vue.component('reprint-cash-cut', require('./components/RePrintCashCut.vue').default);
Vue.component('reprint-cash-cut-modal', require('./components/ReprintCashCutModal.vue').default);
Vue.component('repair-service', require('./components/RepairService.vue').default);
Vue.component('repair-service-modal', require('./components/RepairServiceModal.vue').default);
Vue.component('replace-wallet', require('./components/ReplaceWallet.vue').default);
Vue.component('referral-modal', require('./components/ReferralModal.vue').default);
Vue.component('reprint-out-ticket', require('./components/ReprintOutTicket.vue').default);
Vue.component('reprint-out-ticket-modal', require('./components/ReprintOutTicketModal.vue').default);
Vue.component('reprint-cash-movements', require('./components/ReprintCashMovements.vue').default);
Vue.component('reprint-cash-movements-modal', require('./components/ReprintCashMovementsModal.vue').default);
Vue.component('finished-services-modal', require('./components/FinishedServicesModal.vue').default);
Vue.component('see-issues', require('./components/SeeIssues.vue').default);
Vue.component('see-issues-modal', require('./components/SeeIssuesModal.vue').default);
Vue.component('schedules-barber', require('./components/SchedulesBarber.vue').default);
Vue.component('alert-diary', require('./components/AlertDiary.vue').default);

import Datepicker from 'vuejs-datepicker';

const app = new Vue({
    el: '#app',
    components: {
        Datepicker
    },
    data: {
        'subsidiary_id': 0,
        'date': '',
        'redirect': '',
        'auth': {
            'code': '',
            'password': ''
        },
        'admin': {
            'email': '',
            'password': '',
            'action': ''
        },
        'auth_error': false,
        'concept_id': '',
        'employee': {
            'has_rest': 0,
            'rest_minutes': 0
        },
        'times': [],
        'time': '',
        'time_error': '',
        'barber_id': 0
    },
    created: function () {
        Vue.prototype.$eventHub = new Vue();
        // console.log("eventhub");
    },
    computed: {
        hasRest: function () {
            return this.employee.has_rest == 0
        },
        canCreateDiary: function () {
            return this.time != '';
        }
    },
    methods: {
        dateFormatter(date) {
            return date ? moment(date).format('YYYY-MM-DD') : "";
        },
        showAdminAuthModal: function (e) {
            e.preventDefault();
            this.redirect = $(e.target).attr('href');
            this.admin.action = $(e.target).attr("href");
            $('#modal-admin-auth').modal('show');
        },
        showAuthModal: function (e) {
            e.preventDefault();
            this.redirect = $(e.target).attr('href');
            $('#modal-auth').modal('show');
        },
        makeAuth: function (e) {
            e.preventDefault();
            this.auth_error = false;
            this.$http.post($(e.target).attr('action'), this.auth)
            .then(function (response){
                window.location.href = this.redirect;
            },function (error){
                this.auth_error = error.data.message
            });
        },
        makeAdminAuth: function (e) {
            e.preventDefault();
            this.auth_error = false;
            this.$http.post($(e.target).attr('action'), this.admin)
            .then(function (response){
                window.location.href = this.redirect;
            },function (error){
                this.auth_error = error.data.message
            });
        },
        searchTimes: function (e) {
            e.preventDefault();
            this.time_error = false;
            $("#barber_id").select2('data', null);
            var data = {
                params: {
                    subsidiary_id: this.subsidiary_id,
                    date: this.dateFormatter(this.date)
                }
            }
            this.$http.get('/home/search/times', data)
            .then(function (response) {
                this.times = response.data.times;
                $("#barber_id").select2('data', null, true);
                $("#barber_id").select2({
                  data: response.data.barbers
                });
            }, function (error) {
                this.time_error = error.data.message;
            });
        },
        setTimeActive(time) {
            for (var i = 0; i < this.times.length; i++) {
                for (var j = 0; j < this.times[i].length; j++) {
                    this.times[i][j].active = false;
                }
            }
            time.active = true;
            this.time = time.time;
            this.searchFroBarbers();
        },
        searchFroBarbers: function () {
            this.time_error = false;
            $("#barber_id").empty();
            var data = {
                params: {
                    subsidiary_id: this.subsidiary_id,
                    date: this.dateFormatter(this.date),
                    time: this.time
                }
            }
            this.$http.get('/home/search/barbers', data)
            .then(function (response) {
                $("#barber_id").select2({
                  data: [{id: 0, text: 'Seleccione uno por favor.'}].concat(response.data.barbers)
                });
            }, function (error) {
                this.time_error = error.data.message;
            });
        },
    }
});
