module.exports = $(function () {

    var updateTotals = function (employee_id) {
        var $diferencia = $('#diferencia-'+employee_id+'-hidden');
        var $depositado = $('#depositado-'+employee_id+'-hidden');
        var $diferencia_text = $('#diferencia-'+employee_id+'-text');
        var $neto = $('#neto-'+employee_id+'-hidden');
        var val = Number($neto.val()) - Number($depositado.val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));
    };
    if($('.disable').data('disable') == 1){
        $('.disable').find($('input')).each(function(){
            $(this).prop('disabled', true)
        })
    }
    $('.check-attendance').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');
        var $asistencia_text = $('#asistencia-'+$employee_id+'-text');

        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');
        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');
        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');
        var val= 0;
        if( $(this).is(':checked') ){
            val = Number($total.val()) * 0.10;
            $asistencia.val(val);
            $asistencia_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $asistencia.val(0);
            $asistencia_text.text(0);
        }
        var total = Number($total.val()) + val + Number($puntualidad.val()) + Number($productividad.val()) + Number($extra.val()) + Number($otros_ingresos.val()) + Number($repairs.val()) + Number($cardtips.val()) + Number($products.val())  + Number($excellence.val())
        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');

        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    $('.check-puntualidad').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');

        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $puntualidad_text = $('#puntualidad-'+$employee_id+'-text');

        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');
        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');
        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');
        var val= 0;
        if( $(this).is(':checked') ){
            val = Number($total.val()) * 0.10;
            $puntualidad.val(val);
            $puntualidad_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $puntualidad.val(0);
            $puntualidad_text.text(0);
        }
        var total = Number($total.val()) + val + Number($asistencia.val()) + Number($productividad.val()) + Number($extra.val()) + Number($otros_ingresos.val()) + Number($repairs.val())  + Number($cardtips.val()) + Number($products.val())  + Number($excellence.val())
        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');
        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    $('.check-productividad').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');

        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $porcen_productividad = $('#porcen-productividad-'+$employee_id+'-hidden');
        var $productividad_text = $('#productividad-'+$employee_id+'-text');
        
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');
        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');
        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');
        var val= 0;
        if( $(this).is(':checked') ){
            val = Number($total.val()) * Number($porcen_productividad.val());
            $productividad.val(val);
            $productividad_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $productividad.val(0);
            $productividad_text.text(0);
        }
        var total = Number($total.val()) + val + Number($puntualidad.val()) + Number($asistencia.val()) + Number($extra.val()) + Number($otros_ingresos.val()) + Number($repairs.val())  + Number($cardtips.val()) + Number($products.val())  + Number($excellence.val())
        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');
        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    $('.check-extra').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');

        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');
        var $extra_text = $('#extra-'+$employee_id+'-text');

        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');

        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');
        
        var val= 0;
        if( $(this).is(':checked') ){
            val = $extra.data('val');
            $extra.val(val);
            $extra_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $extra.val(0);
            $extra_text.text(0);
        }
        var total = Number($total.val()) + Number(val) + Number($asistencia.val()) + Number($puntualidad.val()) + Number($productividad.val()) + Number($otros_ingresos.val()) + Number($repairs.val()) + Number($cardtips.val()) + Number($products.val())  + Number($excellence.val())

        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');
        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    $('.check-excellence').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');

        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');
        var $extra_text = $('#extra-'+$employee_id+'-text');

        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');
        var $excellence_text = $('#excellence-'+$employee_id+'-text');

        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');
        
        var val= 0;
        if( $(this).is(':checked') ){
            val = $excellence.data('val');
            $excellence.val(val);
            $excellence_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $excellence.val(0);
            $excellence_text.text(0);
        }
        var total = Number($total.val()) + Number(val) + Number($asistencia.val()) + Number($puntualidad.val()) + Number($productividad.val()) + Number($extra.val()) + Number($otros_ingresos.val()) + Number($repairs.val()) + Number($cardtips.val()) + Number($products.val())
        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');
        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    $('.otros_ingresos').on('change', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#total-'+$employee_id+'-hidden');
        var $asistencia = $('#asistencia-'+$employee_id+'-hidden');
        var $puntualidad = $('#puntualidad-'+$employee_id+'-hidden');
        var $productividad = $('#productividad-'+$employee_id+'-hidden');
        var $repairs = $('#repairs-'+$employee_id+'-hidden');
        var $extra = $('#extra-'+$employee_id+'-hidden');
        var $otros_ingresos = $('#otros_ingresos-'+$employee_id+'-hidden');

        var $cardtips = $('#cardtips-'+$employee_id+'-hidden');
        var $products = $('#products-'+$employee_id+'-hidden');
        var $excellence = $('#excellence-'+$employee_id+'-hidden');

        var $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $total_ingresos_text = $('#total_ingresos-'+$employee_id+'-text');

        var total = Number($total.val()) + Number($asistencia.val()) + Number($otros_ingresos.val()) + Number($puntualidad.val()) + Number($productividad.val()) + Number($extra.val()) + Number($repairs.val())  + Number($cardtips.val()) + Number($products.val())  + Number($excellence.val())
        $total_ingresos.val(total);
        $total_ingresos_text.text('$ '+ total.toFixed(2));

        var $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text');
        var val_total = Number($total_ingresos.val()) - Number($deducciones.val());
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));
        updateTotals($employee_id);
    });

    // descuentos

    $('.prestamo').on('change', function (e) {
        e.preventDefault()
        var $employee_id = $(this).data('employee-id'),
            $total_faltas = $('#total_faltas-'+$employee_id+'-hidden'),
            $uniforme = $('#uniforme-'+$employee_id+'-hidden'),
            $monto_infonavit = $('#monto_infonavit-'+$employee_id+'-hidden'),
            $otras = $('#otras-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text'),
            $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $deducciones_text = $('#deducciones-'+$employee_id+'-text'),
            $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $discount_repairs = $('#discount-repairs-'+$employee_id+'-hidden');

        var val = Number($(this).val()) + Number($total_faltas.val()) + Number($uniforme.val()) + Number($monto_infonavit.val()) + Number($otras.val()) + Number($discount_repairs.val());
        $deducciones.val(val);
        $deducciones_text.text('$ '+ Number(val).toFixed(2));
        var val_total = Number($total_ingresos.val()) - val;
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));

        var $diferencia = $('#diferencia-'+$employee_id+'-hidden'),
        $depositado = $('#depositado-'+$employee_id+'-hidden'),
        $diferencia_text = $('#diferencia-'+$employee_id+'-text');
        var val = Number($neto.val()) - Number($depositado.val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));

        updateTotals($employee_id);

        return false;
    });

    $('.check-faltas').on('click', function() {
        var $employee_id = $(this).data('employee-id'),
            $total_faltas = $('#total_faltas-'+$employee_id+'-hidden'),
            $total_faltas_text = $('#total_faltas-'+$employee_id+'-text'),
            $prestamo = $('#prestamo-'+$employee_id+'-hidden'),
            $monto_infonavit = $('#monto_infonavit-'+$employee_id+'-hidden'),
            $uniforme = $('#uniforme-'+$employee_id+'-hidden'),
            $otras = $('#otras-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text'),
            $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $deducciones_text = $('#desctotal-'+$employee_id+'-text'),
            $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $discount_repairs = $('#discount-repairs-'+$employee_id+'-hidden');
        var val= 0;
        if( $(this).is(':checked') ){
            val = $total_faltas.data('val');
            $total_faltas.val(val);
            $total_faltas_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $total_faltas.val(0);
            $total_faltas_text.text('$ 0.00');
        }

        var val = Number($uniforme.val()) + Number($total_faltas.val()) + Number($prestamo.val()) + Number($monto_infonavit.val()) + Number($otras.val()) + Number($discount_repairs.val());
        $deducciones.val(val);
        $deducciones_text.text('$ '+ Number(val).toFixed(2));
        var val_total = Number($total_ingresos.val()) - val;
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));

        var $diferencia = $('#diferencia-'+$employee_id+'-hidden'),
        $depositado = $('#depositado-'+$employee_id+'-hidden'),
        $diferencia_text = $('#diferencia-'+$employee_id+'-text');
        var val = Number($neto.val()) - Number($depositado.val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));

        updateTotals($employee_id);
    });

    $('.uniforme').on('change', function (e) {
        e.preventDefault()
        var $employee_id = $(this).data('employee-id'),
            $total_faltas = $('#total_faltas-'+$employee_id+'-hidden'),
            $prestamo = $('#prestamo-'+$employee_id+'-hidden'),
            $monto_infonavit = $('#monto_infonavit-'+$employee_id+'-hidden'),
            $otras = $('#otras-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text'),
            $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $deducciones_text = $('#deducciones-'+$employee_id+'-text'),
            $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $discount_repairs = $('#discount-repairs-'+$employee_id+'-hidden');


        var val = Number($(this).val()) + Number($total_faltas.val()) + Number($prestamo.val()) + Number($monto_infonavit.val()) + Number($otras.val()) + Number($discount_repairs.val());
        $deducciones.val(val);
        $deducciones_text.text('$ '+ Number(val).toFixed(2));
        var val_total = Number($total_ingresos.val()) - val;
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));

        var $diferencia = $('#diferencia-'+$employee_id+'-hidden'),
        $depositado = $('#depositado-'+$employee_id+'-hidden'),
        $diferencia_text = $('#diferencia-'+$employee_id+'-text');
        var val = Number($neto.val()) - Number($depositado.val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));

        updateTotals($employee_id);
        return false;
    });

    $('.otras').on('change', function (e) {
        e.preventDefault()
        var $employee_id = $(this).data('employee-id'),
            $total_faltas = $('#total_faltas-'+$employee_id+'-hidden'),
            $uniforme = $('#uniforme-'+$employee_id+'-hidden'),
            $monto_infonavit = $('#monto_infonavit-'+$employee_id+'-hidden'),
            $prestamo = $('#prestamo-'+$employee_id+'-hidden'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $neto_text = $('#neto-'+$employee_id+'-text'),
            $deducciones = $('#desctotal-'+$employee_id+'-hidden'),
            $deducciones_text = $('#deducciones-'+$employee_id+'-text'),
            $total_ingresos = $('#total_ingresos-'+$employee_id+'-hidden');
        var $discount_repairs = $('#discount-repairs-'+$employee_id+'-hidden');


        var val = Number($(this).val()) + Number($total_faltas.val()) + Number($uniforme.val()) + Number($monto_infonavit.val()) + Number($prestamo.val()) + Number($discount_repairs.val());
        $deducciones.val(val);
        $deducciones_text.text('$ '+ Number(val).toFixed(2));
        var val_total = Number($total_ingresos.val()) - val;
        $neto.val(val_total);
        $neto_text.text('$ '+ Number(val_total).toFixed(2));

        var $diferencia = $('#diferencia-'+$employee_id+'-hidden'),
        $depositado = $('#depositado-'+$employee_id+'-hidden'),
        $diferencia_text = $('#diferencia-'+$employee_id+'-text');
        var val = Number($neto.val()) - Number($depositado.val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));

        updateTotals($employee_id);

        return false;
    });

    $('.depositado').on('change', function (e) {
        e.preventDefault()
        var $employee_id = $(this).data('employee-id'),
            $neto = $('#neto-'+$employee_id+'-hidden'),
            $diferencia = $('#diferencia-'+$employee_id+'-hidden'),
            $diferencia_text = $('#diferencia-'+$employee_id+'-text');
        var val = Number($neto.val()) - Number($(this).val());
        $diferencia.val(val);
        $diferencia_text.text('$ '+ Number(val).toFixed(2));

        updateTotals($employee_id);

        return false;
    });

    $('.check-puntu').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#hidden-commision-'+$employee_id);

        var $puntualidad_text = $('#text-appoint-'+$employee_id);
        var $puntualidad = $('#hidden-appoint-'+$employee_id);

        var $productos_text = $('#text-products-'+$employee_id);
        var $productos = $('#hidden-products-'+$employee_id);

        var $propinas_text = $('#text-tip-'+$employee_id);
        var $propinas = $('#hidden-tip-'+$employee_id);

        var $neto_text = $('#text-bigtotal-'+$employee_id);
        var $neto = $('#hidden-bigtotal-'+$employee_id);

        var val= 0;
        if( $(this).is(':checked') ){
            val = Number($total.val()) * 0.10;
            $puntualidad.val(val);
            $puntualidad_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $puntualidad.val(0);
            $puntualidad_text.text(0);
        }
        var total = Number($total.val()) + Number(val) + Number($productos.val()) + Number($propinas.val())

        $neto.val(total);
        $neto_text.text('$ '+ Number(total).toFixed(2));

        updateTotals($employee_id);
    });

    $('.check-prod').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#hidden-commision-'+$employee_id);

        var $puntualidad_text = $('#text-appoint-'+$employee_id);
        var $puntualidad = $('#hidden-appoint-'+$employee_id);

        var $productos_text = $('#text-products-'+$employee_id);
        var $productos = $('#hidden-products-'+$employee_id);

        var $propinas_text = $('#text-tip-'+$employee_id);
        var $propinas = $('#hidden-tip-'+$employee_id);

        var $neto_text = $('#text-bigtotal-'+$employee_id);
        var $neto = $('#hidden-bigtotal-'+$employee_id);

        var val= 0;
        if( $(this).is(':checked') ){
            val = $(this).data('total');
            $productos.val(val);
            $productos_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $productos.val(0);
            $productos_text.text(0);
        }
        var total = Number($total.val()) + Number(val) + Number($puntualidad.val()) + Number($propinas.val())

        $neto.val(total);
        $neto_text.text('$ '+ Number(total).toFixed(2));

        updateTotals($employee_id);
    });

    $('.check-tip').on('click', function() {
        var $employee_id = $(this).data('employee-id');
        var $total = $('#hidden-commision-'+$employee_id);

        var $puntualidad_text = $('#text-appoint-'+$employee_id);
        var $puntualidad = $('#hidden-appoint-'+$employee_id);

        var $productos_text = $('#text-products-'+$employee_id);
        var $productos = $('#hidden-products-'+$employee_id);

        var $propinas_text = $('#text-tip-'+$employee_id);
        var $propinas = $('#hidden-tip-'+$employee_id);

        var $neto_text = $('#text-bigtotal-'+$employee_id);
        var $neto = $('#hidden-bigtotal-'+$employee_id);

        var val= 0;
        if( $(this).is(':checked') ){
            val = $(this).data('total');
            $propinas.val(val);
            $propinas_text.text('$ '+ Number(val).toFixed(2));
        } else {
            val = 0
            $propinas.val(0);
            $propinas_text.text(0);
        }
        var total = Number($total.val())+ Number($puntualidad.val()) + Number($productos.val()) + Number(val)

        $neto.val(total);
        $neto_text.text('$ '+ Number(total).toFixed(2));

        updateTotals($employee_id);
    });
})