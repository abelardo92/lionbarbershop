window.RespondSurveyHandler = {
    total_questions: 0,
    current_question: 1,
    init: function () {
        this.initEvents();
    },
    initEvents: function () {
        this.addFormEvent();
        this.addNextButtonEvent();
        this.addRadioCheckboxEvent();
        this.showHideButtonsContent();
    },
    addFormEvent: function () {
        $('.js-form-respond').on('submit', (e) => {
            e.preventDefault();
            $(e.target).hide();
            $('.js-show-on-submit').show();
            $.ajax({
                type: 'put',
                dataType: 'json',
                data: $(e.target).serialize(),
                url: $(e.target).attr('action'),
                headers: {
                    'X-CSRF-TOKEN': Laravel.csrfToken
                },
                success: (survey) => {
                    $('.js-message').text('Gracias por responder la encuesta, que tengas un excelente dia.');
                    setTimeout(function () {
                        window.location.href = '/surveys';
                    }, 2000);
                } 
            });
            return false;
        });
    },
    addNextButtonEvent() {
        $('.js-next-button').on('click', () => {this.nextEvent()});
    },
    nextEvent() {
        let old = this.current_question;
        if ($(`[data-index="${old}"]`).data('aswered') == '0') {
            return false;
        }
        this.current_question++;
        if (this.total_questions == this.current_question) {
            $('.js-next-button').hide();
            $('.js-submit-button').show();
        }
        $(`[data-index="${this.current_question}"]`).show();
        $(`[data-index="${old}"]`).hide();
        this.showHideButtonsContent();
    },
    addRadioCheckboxEvent() {
        $('.js-radio-change, .js-checkbox-change').on('click', (e) => {
            let index = $(e.target).data('index');
            $(`[data-index="${index}"]`).data('aswered', '1');
            if ($(e.target).hasClass('js-radio-change')) {
                if ($('.js-submit-button').is(':visible')) {
                    $('.js-submit-button').trigger('click');
                }else {
                    this.nextEvent();
                }
            }
        })
    },
    showHideButtonsContent() {
        const questionType = $('.js-question-container:visible').data('type');
        if (questionType == 'satisfaction') {
            $('.js-buttons-content').hide();
        }else {
            $('.js-buttons-content').show();
        }
        if (this.total_questions == this.current_question) {
            $('.js-buttons-content').show();
            $('.js-next-button').hide();
            $('.js-submit-button').show();
        }
    }
};