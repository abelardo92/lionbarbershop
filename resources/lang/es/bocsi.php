<?php
return [
    'input_types' => [
        'text' => 'Campo de texto',
        'textarea' => 'Caja de texto',
        'checkbox' => 'Opción multiple, multiple selección',
        'radio' => 'Opción multiple, una selección',
        'satisfaction' => 'Opcion multiple de satisfacción'
    ]
];