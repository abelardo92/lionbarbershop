<form action="{{ route('accounting_customers.updateActa', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-4">
            <label for="acta_month">Mes</label>
            <select name="acta_month" id="acta_month" class="form-control">
                @foreach($months as $key => $month)
                    <option value="{{$key}}" {{ $key == $currentMonth ? 'selected' : '' }}>{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-4">
            <label for="acta_year">Año</label>
            <select name="acta_year" id="acta_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-4">
            <label for="acta">Acta</label>
            <input type="file" name="acta" id="acta" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre del archivo</th>
                    <th>Año - Mes</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->fileActas() as $acta)
                <tr>
                    <td width="60%">{{ $acta->name }}</td>
                    <td width="20%">{{ $acta->year }} - {{ $acta->month }}</td>
                    <td width="20%" class="d-flex">
                        <a href="{{$acta->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                        @if(!Auth::user()->isA('auxiliar-contable'))
                        <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $acta->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
