<form action="{{ route('accounting_customers.updateConstancia', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-4">
            <label for="constancia_month">Mes</label>
            <select name="constancia_month" id="constancia_month" class="form-control">
                @foreach($months as $key => $month)
                    <option value="{{$key}}" {{ $key == $currentMonth ? 'selected' : '' }}>{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-4">
            <label for="constancia_year">Año</label>
            <select name="constancia_year" id="constancia_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-4">
            <label for="address">Constancia de situacion fiscal</label>
            <input type="file" name="constancia" id="constancia" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre del archivo</th>
                    <th>Año - Mes</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->fileConstancias() as $constancia)
                <tr>
                    <td width="60%">{{ $constancia->name }}</td>
                    <td width="20%">{{ $constancia->year }} - {{ $constancia->month }}</td>
                    <td width="20%" class="d-flex">
                        <a href="{{$constancia->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                        @if(!Auth::user()->isA('auxiliar-contable'))
                        <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $constancia->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
