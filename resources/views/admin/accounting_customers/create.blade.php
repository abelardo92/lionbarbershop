@extends('layouts.app')

@section('css')
<style>
    .mt-3, .my-3 {
        margin-top: 1rem !important;
    }

    .ml-1 {
        margin-left: 0.5rem !important;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    .mt-0, .my-0 {
        margin-top: 0 !important;
    }

    .ml-auto, .mx-auto {
        margin-left: auto !important;
    }
    .mr-auto, .mx-auto {
        margin-right: auto !important;
    }

    .mt-auto, .my-auto {
        margin-top: auto !important;
    }
    .mb-auto, .my-auto {
        margin-bottom: auto !important;
    }

    .d-flex {
        display: flex;
    }

    .btn-delete-file {
        border: none;
        background: transparent;
    }

    .file-link {
        padding-top: 2px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Create nuevo cliente
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('accounting_customers.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                        {{{ csrf_field() }}}

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="name">Nombre</label>
                                <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <label for="phone">Teléfono</label>
                                <input type="text" name="phone" id="phone" value="{{old('phone')}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="curp">REGIMEN FISCAL</label>
                                <select name="tax_regime_id" id="tax_regime_id" class="form-control">
                                    @foreach($tax_regimes as $tax_regime)
                                        <option value="{{$tax_regime->id}}" {{ old('tax_regime_id') == $tax_regime->id ? 'selected' : '' }}>{{$tax_regime->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <label for="registro_patronal">
                                    Registro patronal
                                </label>
                                <input type="file" name="registro_patronal" id="registro_patronal" class="form-control">
                            </div>
                            <div class="col-sm-6 mt-3">
                                <label for="acta_nacimiento">
                                    Acta de nacimiento
                                </label>
                                <input type="file" name="acta_nacimiento" id="acta_nacimiento" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="address">Dirección</label>
                                <div class="d-flex">
                                    <input type="text" name="address" id="address" value="{{old('address')}}" class="form-control">
                                    <input type="file" name="address_file[]" id="address_file" class="form-control ml-1" multiple>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <b>Dirección (Archivos):</b><br>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="rfc">RFC</label>
                                <div class="d-flex">
                                    <input type="text" name="rfc" id="rfc" value="{{old('rfc')}}" class="form-control">
                                    <input type="file" name="rfc_file[]" id="rfc_file" class="form-control ml-1" multiple>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <b>RFC (Archivos):</b><br>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="curp">CURP</label>
                                <div class="d-flex">
                                    <input type="text" name="curp" id="curp" value="{{old('curp')}}" class="form-control">
                                    <input type="file" name="curp_file[]" id="curp_file" class="form-control ml-1" multiple>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <b>CURP (Archivos):</b><br>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mt-3">
                                <label for="identificacion">
                                    Identificación
                                </label>
                                <input type="file" name="identificacion[]" id="identificacion" class="form-control" multiple>
                            </div>
                            <div class="col-sm-6 mt-3">
                                <b>Identificación (Archivos):</b><br>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 mt-3">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <a href="{{route('accounting_customers.index')}}" class="btn btn-danger">Cancelar</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
