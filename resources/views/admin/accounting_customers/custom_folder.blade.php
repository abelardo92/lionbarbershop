<form action="{{ route('accounting_customers.updateCustomFolderFile', [$customer->id, $folder->id]) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-4">
            <label for="folder_{{$folder->name}}_month">Mes</label>
            <select name="folder_{{$folder->name}}_month" id="folder_{{$folder->name}}_month" class="form-control">
                @foreach($months as $key => $month)
                    <option value="{{$key}}" {{ $key == $currentMonth ? 'selected' : '' }}>{{$month}}</option>
                @endforeach
            </select>
            <input type="hidden" name="subfolder_id" id="{{$folder->name}}_subfolder_id" class="form-control">
        </div>
        <div class="col-sm-4">
            <label for="folder_{{$folder->name}}_year">Año</label>
            <select name="folder_{{$folder->name}}_year" id="folder_{{$folder->name}}_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-4">
            <label for="file_{{$folder->name}}">Archivo</label>
            <input type="file" name="file_{{$folder->name}}" id="file_{{$folder->name}}" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="modal fade" tabindex="-1" role="dialog" id="add_subfolder_{{$folder->id}}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Agregar subcarpeta para "{{$folder->tab_name}}"</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('accounting_customers.storeSubFolder') }}" method="post" autocomplete="off">
                        {{{ csrf_field() }}}
                        <div class="modal-container">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="folder_name">Nombre de la subcarpeta</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" name="folder_id" id="folder_id" value="{{$folder->id}}" class="form-control">
                                    <input type="name" name="folder_name" id="folder_name" class="form-control">
                                </div>
                                <div class="col-sm-4">
                                    <input type="submit" id="add_folder_button" class="btn btn-primary" value="Agregar subcarpeta">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->isA('super-admin'))
    <div class="col-sm-12 mt-3 d-flex">
        <button type="button" class="btn btn-link ml-auto" data-toggle="modal" data-target="#add_subfolder_{{$folder->id}}">Agregar subcarpeta <i class="fa fa-folder"></i></button>
    </div>
    @endif
    <div class="col-sm-12 mt-3">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="tab active subfolder-tab" id="table_{{$folder->name}}_li">
                <a href="#table_{{$folder->name}}" data-subfolder-field = "{{$folder->name}}_subfolder_id" data-subfolder-id="" class="subfolder-link" aria-controls="table_{{$folder->name}}" role="tab" data-toggle="tab">{{$folder->tab_name}} (RAIZ)</a>
            </li>
            @foreach ($folder->subfolders->where('active', true) as $subfolder)
                <li role="presentation" class="tab subfolder-tab" id="table_{{$folder->name}}_{{$subfolder->name}}_li">
                    <a href="#table_{{$folder->name}}_{{$subfolder->name}}" data-subfolder-field = "{{$folder->name}}_subfolder_id" data-subfolder-id="{{$subfolder->id}}" class="subfolder-link" aria-controls="table_{{$folder->name}}_{{$subfolder->name}}" role="tab" data-toggle="tab">{{$subfolder->tab_name}}</a>
                </li>
            @endforeach
        </ul>
        <br>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane subfolder-container active" id="table_{{$folder->name}}">
                <table class="table table-striped datatables">
                    <thead>
                        <tr>
                            <th>Nombre del archivo</th>
                            <th>Año - Mes</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customer->getFiles($folder->name) as $file)
                        <tr>
                            <td width="60%">{{ $file->name }}</td>
                            <td width="20%">{{ $file->year }} - {{ $file->month }}</td>
                            <td width="20%" class="d-flex">
                                <a href="{{$file->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                                @if(!Auth::user()->isA('auxiliar-contable'))
                                <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $file->id])}}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                                </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @foreach ($folder->subFolders->where('active', true) as $subfolder)
                <div role="tabpanel" class="tab-pane subfolder-container" id="table_{{$folder->name}}_{{$subfolder->name}}">
                    <table class="table table-striped datatables">
                        <thead>
                            <tr>
                                <th>Nombre del archivo</th>
                                <th>Año - Mes</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                // dd("{$folder->name}_{$subfolder->name}");
                            @endphp
                            @foreach ($customer->getFiles("{$folder->name}_{$subfolder->name}") as $file)
                            <tr>
                                <td width="60%">{{ $file->name }}</td>
                                <td width="20%">{{ $file->year }} - {{ $file->month }}</td>
                                <td width="20%" class="d-flex">
                                    <a href="{{$file->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                                    @if(!Auth::user()->isA('auxiliar-contable'))
                                    <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $file->id])}}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>
</div>
