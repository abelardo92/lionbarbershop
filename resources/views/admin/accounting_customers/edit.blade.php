@extends('layouts.app')

@section('css')
<style>
    .mt-3, .my-3 {
        margin-top: 1rem !important;
    }

    .ml-1 {
        margin-left: 0.5rem !important;
    }

    .mt-1 {
        margin-top: 0.5rem !important;
    }

    .mb-0, .my-0 {
        margin-bottom: 0rem !important;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    .mt-0, .my-0 {
        margin-top: 0 !important;
    }

    .ml-auto, .mx-auto {
        margin-left: auto !important;
    }
    .mr-auto, .mx-auto {
        margin-right: auto !important;
    }

    .mt-auto, .my-auto {
        margin-top: auto !important;
    }
    .mb-auto, .my-auto {
        margin-bottom: auto !important;
    }

    .d-flex {
        display: flex;
    }

    .btn-delete-file {
        border: none;
        background: transparent;
    }

    .file-link {
        padding-top: 2px;
    }

    .modal-container {
        margin-left: auto;
        margin-right: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$customer->name}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <div class="modal fade" tabindex="-1" role="dialog" id="add_folder">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Agregar carpeta</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('accounting_customers.storeFolder') }}" method="post" autocomplete="off">
                                            {{{ csrf_field() }}}
                                            <div class="modal-container">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label for="folder_name">Nombre de la carpeta</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="name" name="folder_name" id="folder_name" class="form-control">
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="submit" id="add_folder_button" class="btn btn-primary" value="Agregar Carpeta">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->isA('super-admin'))
                        <div class="col-sm-12 mt-3 d-flex">
                            <button type="button" class="btn btn-link ml-auto" data-toggle="modal" data-target="#add_folder">Agregar carpeta <i class="fa fa-folder"></i></button>
                        </div>
                        @endif
                        <div class="col-sm-12 mt-1">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="tab active" id="general_li"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">GENERAL</a></li>
                                <li role="presentation" class="tab" id="constancia_li"><a href="#constancia" aria-controls="constancia" role="tab" data-toggle="tab">CONST. SITUACION FISCAL</a></li>
                                <li role="presentation" class="tab" id="expediente_li"><a href="#expediente" aria-controls="expediente" role="tab" data-toggle="tab">EXPEDIENTE</a></li>
                                <li role="presentation" class="tab" id="expediente_clinico_li"><a href="#expediente_clinico" aria-controls="expediente_clinico" role="tab" data-toggle="tab">EXP. CLINICO</a></li>
                                <li role="presentation" class="tab" id="actas_li"><a href="#actas" aria-controls="actas" role="tab" data-toggle="tab">ACTAS</a></li>
                                <li role="presentation" class="tab" id="expediente_empleados_li"><a href="#expediente_empleados" aria-controls="expediente_empleados" role="tab" data-toggle="tab">EXP. EMPLEADOS</a></li>
                                <li role="presentation" class="tab" id="imss_li"><a href="#imss" aria-controls="imss" role="tab" data-toggle="tab">SUA E IMSS</a></li>
                                <li role="presentation" class="tab" id="diots_li"><a href="#diots" aria-controls="diots" role="tab" data-toggle="tab">DIOTS</a></li>
                                <li role="presentation" class="tab" id="mensuales_li"><a href="#mensuales" aria-controls="mensuales" role="tab" data-toggle="tab">DEC. MENSUALES Y PAGOS</a></li>
                                <li role="presentation" class="tab" id="anuales_li"><a href="#anuales" aria-controls="anuales" role="tab" data-toggle="tab">DEC. ANUALES</a></li>
                                @foreach ($custom_folders as $folder)
                                    <li role="presentation" class="tab" id="{{$folder->name}}_li"><a href="#{{$folder->name}}" aria-controls="{{$folder->name}}" role="tab" data-toggle="tab">{{strtoupper($folder->tab_name)}}</a></li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="general">
                                    <form action="{{ route('accounting_customers.update', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                                        {{{ csrf_field() }}}
                                        {{{ method_field('PUT') }}}

                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <label for="name">Nombre</label>
                                                <input type="text" name="name" id="name" value="{{$customer->name}}" class="form-control">
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="phone">Teléfono</label>
                                                <input type="text" name="phone" id="phone" value="{{$customer->phone}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mt-3">
                                                <label for="curp">REGIMEN FISCAL</label>
                                                <select name="tax_regime_id" id="tax_regime_id" class="form-control">
                                                    @foreach($tax_regimes as $tax_regime)
                                                        <option value="{{$tax_regime->id}}" {{ $customer->tax_regime_id == $tax_regime->id ? 'selected' : '' }}>{{$tax_regime->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <label for="registro_patronal">
                                                    Registro patronal
                                                    @if(!empty($customer->fileRegistroPatronal()))
                                                    <a href="{{$customer->fileRegistroPatronal()->path}}" target="_blank">(Ver archivo)</a>
                                                    @endif
                                                </label>
                                                <input type="file" name="registro_patronal" id="registro_patronal" class="form-control">
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <label for="acta_nacimiento">
                                                    Acta de nacimiento
                                                    @if(!empty($customer->fileActaNacimiento()))
                                                    <a href="{{$customer->fileActaNacimiento()->path}}" target="_blank">(Ver archivo)</a>
                                                    @endif
                                                </label>
                                                <input type="file" name="acta_nacimiento" id="acta_nacimiento" class="form-control">
                                            </div>
                                            @if(!Auth::user()->isA('auxiliar-contable'))
                                            <div class="col-sm-6 mt-3">
                                                <label for="acta_nacimiento">
                                                    Mostrar a auxiliar
                                                </label>
                                                <select name="show_to_auxiliar" id="show_to_auxiliar" class="form-control" required="required">
                                                    <option @if($customer->show_to_auxiliar) selected="selected" @endif value="1">SI</option>
                                                    <option @if(!$customer->show_to_auxiliar) selected="selected" @endif value="0">NO</option>
                                                </select>
                                            </div>
                                            @else
                                                <input type="hidden" name="show_to_auxiliar" id="show_to_auxiliar" value="{{$customer->show_to_auxiliar}}" />
                                            @endif
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mt-3">
                                                <label for="address">Dirección</label>
                                                <div class="d-flex">
                                                    <input type="text" name="address" id="address" value="{{$customer->address}}" class="form-control">
                                                    <input type="file" name="address_file[]" id="address_file" class="form-control ml-1" multiple>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <b>Dirección (Archivos):</b><br>
                                                @foreach ($customer->filesAddress() as $file)
                                                <div class="d-flex">
                                                    <a href="{{$file->path}}" target="_blank" class="file-link">{{ $file->name }}</a>
                                                    @if(!Auth::user()->isA('auxiliar-contable'))
                                                    <button class="ml-1 btn-delete-file" type="button" data-file="{{ $file->id }}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mt-3">
                                                <label for="rfc">RFC</label>
                                                <div class="d-flex">
                                                    <input type="text" name="rfc" id="rfc" value="{{$customer->rfc}}" class="form-control">
                                                    <input type="file" name="rfc_file[]" id="rfc_file" class="form-control ml-1" multiple>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <b>RFC (Archivos):</b><br>
                                                @foreach ($customer->filesRFC() as $file)
                                                <div class="d-flex">
                                                    <a href="{{$file->path}}" target="_blank" class="file-link">{{ $file->name }}</a>
                                                    @if(!Auth::user()->isA('auxiliar-contable'))
                                                    <button class="ml-1 btn-delete-file" type="button" data-file="{{ $file->id }}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-6 mt-3">
                                                <label for="curp">CURP</label>
                                                <div class="d-flex">
                                                    <input type="text" name="curp" id="curp" value="{{$customer->curp}}" class="form-control">
                                                    <input type="file" name="curp_file[]" id="curp_file" class="form-control ml-1" multiple>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <b>CURP (Archivos):</b><br>
                                                @foreach ($customer->filesCURP() as $file)
                                                <div class="d-flex">
                                                    <a href="{{$file->path}}" target="_blank" class="file-link">{{ $file->name }}</a>
                                                    @if(!Auth::user()->isA('auxiliar-contable'))
                                                    <button class="ml-1 btn-delete-file" type="button" data-file="{{ $file->id }}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-6 mt-3">
                                                <label for="identificacion">
                                                    Identificación
                                                </label>
                                                <input type="file" name="identificacion[]" id="identificacion" class="form-control" multiple>
                                            </div>
                                            <div class="col-sm-6 mt-3">
                                                <b>Identificación (Archivos):</b><br>
                                                @foreach ($customer->filesIdentificacion() as $file)
                                                <div class="d-flex">
                                                    <a href="{{$file->path}}" target="_blank" class="file-link">{{ $file->name }}</a>
                                                    @if(!Auth::user()->isA('auxiliar-contable'))
                                                    <button class="ml-1 btn-delete-file" type="button" data-file="{{ $file->id }}">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12 mt-3">
                                                <input type="submit" class="btn btn-primary" value="Actualizar">
                                                <a href="{{route('accounting_customers.index')}}" class="btn btn-danger">Cancelar</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <form style="display: none;" id="delete_file_form" action="{{route('accounting_customers.image.destroy2', [$customer->id])}}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input type="hidden" id="file_id" name="file_id" value="" />
                                    <button id="delete_file">
                                        {{-- <i class="fa fa-trash"></i> --}}
                                    </button>
                                </form>

                                <div role="tabpanel" class="tab-pane" id="constancia">
                                    @include('admin.accounting_customers.constancias')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="expediente">
                                    @include('admin.accounting_customers.expedientes')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="expediente_clinico">
                                    @include('admin.accounting_customers.expedientes_clinicos')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="actas">
                                    @include('admin.accounting_customers.actas')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="expediente_empleados">
                                    @include('admin.accounting_customers.expedientes_empleados')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="imss">
                                    @include('admin.accounting_customers.sua')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="diots">
                                    @include('admin.accounting_customers.diots')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="mensuales">
                                    @include('admin.accounting_customers.monthly_declarations')
                                </div>
                                <div role="tabpanel" class="tab-pane" id="anuales">
                                    @include('admin.accounting_customers.yearly_declarations')
                                </div>
                                @foreach ($custom_folders as $folder)
                                <div role="tabpanel" class="tab-pane" id="{{$folder->name}}">
                                    @include('admin.accounting_customers.custom_folder')
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

    $('a[data-toggle="tab"]:not(.subfolder-link)').on('shown.bs.tab', function(event) {
        const activeTab = $(event.target), // activated tab
        id = activeTab.attr('href'); // active tab href

        // set id in html5 localstorage for later usage
        localStorage.setItem('activeTab', id);

    });

    window.onload = function() {
        const tabID = localStorage.getItem('activeTab');
        if(tabID) {
            $(".tab-pane:not(.subfolder-container), .tab").removeClass('active');
            $(tabID).addClass('active');
            $(`${tabID}_li`).addClass('active');
        }
    };

    $(".subfolder-link").on('click', function (e) {
        e.preventDefault();
        const subfolder_id = $(this).data('subfolder-id');
        const subfolder_field = $(this).data('subfolder-field');
        $(`#${subfolder_field}`).val(subfolder_id);
    });

    $(".btn-delete-file").on('click', function (e) {
        e.preventDefault();
        const file_id = $(this).data('file');
        $("#file_id").val(file_id);
        $("#delete_file_form").submit();
    });

    $(".delete-file, #delete_file_form").on('submit', function (e) {
        if (confirm("Desea eliminar este registro?") == false) {
            e.preventDefault();
        }
    });

    function editEmployee(id) {
        if(id) {
            $("#expediente_employee_id").val($(`#expediente_employee_id_${id}`).val());
            $("#expediente_employee_name").val($(`#expediente_employee_name_${id}`).val());
            $("#expediente_employee_date").val($(`#expediente_employee_date_${id}`).val());
            $("#expediente_employee_email").val($(`#expediente_employee_email_${id}`).val());
            $("#edit_employee_button").val("Editar empleado");
        } else {
            const today = new Date().toISOString().slice(0, 10)
            $("#expediente_employee_id").val(0);
            $("#expediente_employee_name").val("");
            $("#expediente_employee_date").val(today);
            $("#expediente_employee_email").val("");
            $("#edit_employee_button").val("Agregar empleado");

        }
    }

</script>
@endsection
