
<div class="modal fade" tabindex="-1" role="dialog" id="add_expediente_empleados">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Agregar/Editar empleado</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('accounting_customers.updateEmployee', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                    {{{ csrf_field() }}}
                    {{{ method_field('PUT') }}}

                    <div class="modal-container">

                        <div class="row">
                            <div class="col-sm-4">
                                <label for="expediente_employee_name">Nombre</label>
                                <input type="hidden" name="expediente_employee_id" id="expediente_employee_id" value="">
                                <input type="name" name="expediente_employee_name" id="expediente_employee_name" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="expediente_employee_date">Fecha</label>
                                <input type="date" name="expediente_employee_date" id="expediente_employee_date" value="<?= date('Y-m-d') ?>" class="form-control">
                            </div>
                            <div class="col-sm-4">
                                <label for="expediente_employee_email">Correo</label>
                                <input type="email" name="expediente_employee_email" id="expediente_employee_email" class="form-control">
                            </div>
                            <div class="col-sm-12 mt-3">
                                <table class="table table-striped mb-0">
                                    <thead>
                                        <tr>
                                            <th>Archivo 1</th>
                                            <th>Archivo 2</th>
                                            <th>Archivo 3</th>
                                            <th>Archivo 4</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="25%">
                                                <input type="file" name="expediente_employee_file1" id="expediente_employee_file1" class="form-control">
                                                <input type="date" name="expediente_employee_file1_date" id="expediente_employee_file1_date" value="<?= date('Y-m-d') ?>" class="form-control mt-1">
                                            </td>
                                            <td width="25%">
                                                <input type="file" name="expediente_employee_file2" id="expediente_employee_file2" class="form-control">
                                                <input type="date" name="expediente_employee_file2_date" id="expediente_employee_file2_date" value="<?= date('Y-m-d') ?>" class="form-control mt-1">
                                            </td>
                                            <td width="25%">
                                                <input type="file" name="expediente_employee_file3" id="expediente_employee_file3" class="form-control">
                                                <input type="date" name="expediente_employee_file3_date" id="expediente_employee_file3_date" value="<?= date('Y-m-d') ?>" class="form-control mt-1">
                                            </td>
                                            <td width="25%">
                                                <input type="file" name="expediente_employee_file4" id="expediente_employee_file4" class="form-control">
                                                <input type="date" name="expediente_employee_file4_date" id="expediente_employee_file4_date" value="<?= date('Y-m-d') ?>" class="form-control mt-1">
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-2 mt-3">
                                <input type="submit" id="edit_employee_button" class="btn btn-primary" value="Agregar Empleado">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="form-group">
    <div class="col-sm-12 mt-3">
        <button type="button" onclick="editEmployee(0)" class="btn btn-link" data-toggle="modal" data-target="#add_expediente_empleados">Agregar empleado</button>
    </div>
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Fecha</th>
                    <th>Correo</th>
                    <th>Archivo(s)</th>
                    <th>editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->employees as $employee)
                <tr>
                    <td width="20%">{{ $employee->name }}</td>
                    <td width="15%">{{ $employee->date }}</td>
                    <td width="25%">{{ $employee->email }}</td>
                    <td width="30%">
                        <div class="modal fade" tabindex="-1" role="dialog" id="exp_empleado_files_{{ $employee->id }}">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Archivos de {{ $employee->name }}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="modal-container">
                                            <table class="table table-striped datatables">
                                                <thead>
                                                    <tr>
                                                        <td><b>Nombre del archivo</b></td>
                                                        <td><b>Fecha</b></td>
                                                        <td><b>Eliminar</b></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($employee->images->sortBy('date')->reverse() as $file)
                                                    <tr>
                                                        <td>
                                                            <a href="{{$file->path}}" target="_blank" class="file-link">{{ $file->name }}</a>
                                                        </td>
                                                        <td>{{ $file->date }}</td>
                                                        <td>
                                                            @if(!Auth::user()->isA('auxiliar-contable'))
                                                            <button class="ml-1 btn-delete-file" type="button" data-file="{{ $file->id }}">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#exp_empleado_files_{{ $employee->id }}">Ver archivos</button>
                    </td>
                    <td width="10%">
                        <input type="hidden" id="expediente_employee_id_{{$employee->id}}" value="{{$employee->id}}">
                        <input type="hidden" id="expediente_employee_name_{{$employee->id}}" value="{{$employee->name}}">
                        <input type="hidden" id="expediente_employee_date_{{$employee->id}}" value="{{$employee->date}}">
                        <input type="hidden" id="expediente_employee_email_{{$employee->id}}" value="{{$employee->email}}">
                        <button type="button" onclick="editEmployee({{$employee->id}})" class="btn btn-link" data-toggle="modal" data-target="#add_expediente_empleados">Editar</button>
                    </td>
                    {{-- <td width="20%" class="d-flex">
                        <a href="{{$expediente->path}}" class="btn btn-info btn-xs">Editar</a>
                    </td> --}}
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
