@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Administrar clientes (Contabilidad)
                    <div class="pull-right">
                        <a class="header-link" href="{{route('accounting_customers.create')}}">Agregar cliente</a>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped datatables">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>RFC</th>
                                <th>CURP</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                                @php
                                    if(Auth::user()->isA('auxiliar-contable') && !Auth::user()->containsAccountingCustomer($customer->id)) continue;
                                @endphp
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->rfc }}</td>
                                    <td>{{ $customer->curp }}</td>
                                    <td>
                                        <a href="{{route('accounting_customers.edit', $customer->id)}}" class="btn btn-info btn-xs">Editar</a>
                                        @if(!Auth::user()->isA('auxiliar-contable'))
                                        <form action="{{route('accounting_customers.destroy', $customer->id)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
