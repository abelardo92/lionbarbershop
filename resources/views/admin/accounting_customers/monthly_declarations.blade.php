<form action="{{ route('accounting_customers.updateMonthlyDeclaration', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-3">
            <label for="monthly_declaration_month">Mes</label>
            <select name="monthly_declaration_month" id="monthly_declaration_month" class="form-control">
                @foreach($months as $key => $month)
                    <option value="{{$key}}" {{ $key == $currentMonth ? 'selected' : '' }}>{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="monthly_declaration_year">Año</label>
            <select name="monthly_declaration_year" id="monthly_declaration_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-3">
            <label for="monthly_declaration_type">Tipo</label>
            <select name="monthly_declaration_type" id="monthly_declaration_type" class="form-control">
                <option value="DECLARACION">DECLARACION</option>
                <option value="PAGO">PAGO</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label for="monthly_declaration">DECLARACION / PAGO</label>
            <input type="file" name="monthly_declaration" id="monthly_declaration" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre del archivo</th>
                    <th>Año - Mes</th>
                    <th>tipo</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->fileMonthlyDeclarations() as $monthly_declaration)
                <tr>
                    <td width="60%">{{ $monthly_declaration->name }}</td>
                    <td width="15%">{{ $monthly_declaration->year }} - {{ $monthly_declaration->month }}</td>
                    <td width="15%">{{ $monthly_declaration->type }}</td>
                    <td width="10%" class="d-flex">
                        <a href="{{$monthly_declaration->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                        @if(!Auth::user()->isA('auxiliar-contable'))
                        <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $monthly_declaration->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
