<form action="{{ route('accounting_customers.updateSua', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-3">
            <label for="sua_month">Mes</label>
            <select name="sua_month" id="sua_month" class="form-control">
                @foreach($months as $key => $month)
                    <option value="{{$key}}" {{ $key == $currentMonth ? 'selected' : '' }}>{{$month}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="sua_year">Año</label>
            <select name="sua_year" id="sua_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-3">
            <label for="sua_type">Tipo</label>
            <select name="sua_type" id="sua_type" class="form-control">
                <option value="SIPARE">SIPARE</option>
                <option value="SUA">SUA</option>
                <option value="PAGO">PAGO</option>
                <option value="ARCHIVO_DE_PAGO">ARCHIVO DE PAGO</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label for="sua">SUA</label>
            <input type="file" name="sua" id="sua" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre del archivo</th>
                    <th>Año - Mes</th>
                    <th>tipo</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->fileSuas() as $sua)
                <tr>
                    <td width="60%">{{ $sua->name }}</td>
                    <td width="15%">{{ $sua->year }} - {{ $sua->month }}</td>
                    <td width="15%">{{ $sua->type }}</td>
                    <td width="10%" class="d-flex">
                        <a href="{{$sua->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                        @if(!Auth::user()->isA('auxiliar-contable'))
                        <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $sua->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
