<form action="{{ route('accounting_customers.updateYearlyDeclaration', $customer->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {{{ csrf_field() }}}
    {{{ method_field('PUT') }}}

    <div class="form-group">
        <div class="col-sm-3">
            <label for="yearly_declaration_year">Año</label>
            <select name="yearly_declaration_year" id="yearly_declaration_year" class="form-control">
                @for($c = $currentYear-20; $c <= $currentYear; $c++)
                    <option value="{{$c}}" {{ $c == $currentYear ? 'selected' : '' }}>{{$c}}</option>
                @endfor
            </select>
        </div>
        <div class="col-sm-3">
            <label for="yearly_declaration_type">TIPO</label>
            <select name="yearly_declaration_type" id="yearly_declaration_type" class="form-control">
                <option value="DECLARACION">DECLARACION</option>
                <option value="ACUSE_ENVIO">ACUSE DE ENVIO</option>
                <option value="ACUSE_RECIBIDO">ACUSE DE RECIBIDO</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label for="yearly_declaration">FECHA PRESENTACION</label>
            <input type="date" name="yearly_declaration_date" id="yearly_declaration_date" value="<?= date('Y-m-d') ?>" class="form-control">
        </div>
        <div class="col-sm-3">
            <label for="yearly_declaration">ARCHIVO</label>
            <input type="file" name="yearly_declaration" id="yearly_declaration" class="form-control">
        </div>
        <div class="col-sm-2 mt-3">
            <input type="submit" class="btn btn-primary" value="Subir archivo">
        </div>
    </div>
</form>
<div class="form-group">
    <div class="col-sm-12 mt-3">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Nombre del archivo</th>
                    <th>Año</th>
                    <th>Fecha presentacion</th>
                    <th>tipo</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($customer->fileYearlyDeclarations() as $yearly_declaration)
                <tr>
                    <td width="50%">{{ $yearly_declaration->name }}</td>
                    <td width="15%">{{ $yearly_declaration->year }}</td>
                    <td width="15%">{{ $yearly_declaration->date }}</td>
                    <td width="15%">{{ $yearly_declaration->type }}</td>
                    <td width="20%" class="d-flex">
                        <a href="{{$yearly_declaration->path}}" class="btn btn-info btn-xs">Ver archivo</a>
                        @if(!Auth::user()->isA('auxiliar-contable'))
                        <form class="delete-file ml-1" action="{{route('accounting_customers.image.destroy', [$customer->id, $yearly_declaration->id])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
