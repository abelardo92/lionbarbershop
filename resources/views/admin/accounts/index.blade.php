@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cuentas
                    <div class="pull-right">
                        <a style="color: white;" href="{{route('accounts.create')}}">Agregar cuenta</a>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped datatables">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($accounts as $account)
                                <tr>
                                    <td>{{ $account->name }}</td>
                                    <td><a href="{{ route('accounts.edit', $account->id) }}">Editar</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
