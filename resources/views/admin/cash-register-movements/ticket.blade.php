@extends('layouts.tickets_internos')

@section('content')
    <p>
        @if($movement->type == 'corte')
            <strong>Corte:</strong>
        @else
            <strong>Mini corte:</strong>
        @endif
        {{ $cashRegister->movements()->count() - 1 }}
        @php
            $subsidiary = $cashRegister->subsidiary()->get()->first();
        @endphp
    </p>
    <p><strong>Fecha y hora:</strong> {{ $movement->created_at->format('d-m-Y h:i a')}}</p>
    <p>
        <strong>Cajero:</strong> 
        @if($cashRegister->employee)
            {{ $cashRegister->employee->short_name }}
        @else
            {{ $cashRegister->user->name }}
        @endif
    </p>
    <p><strong>Sucursal:</strong> {{ $subsidiary->name }}</p>
    <p><strong>Turno:</strong> {{ $cashRegister->turn->name }}</p>
    <p><strong>Tipo:</strong> {{ $type }}</p>
    @if($movement->type == 'mini-corte')
        <p><strong>Minutos tardados:</strong> {{ $movement->minutes_taken }}</p>
    @endif
    <br>

    <p><strong>$ .50 MXN</strong> x {{ $movement->fiftycents }} = {{ $movement->fiftycents * .50}}</p>
    <p><strong>$ 1 MXN</strong> x {{ $movement->onepeso }} = {{ $movement->onepeso * 1}}</p>
    <p><strong>$ 2 MXN</strong> x {{ $movement->twopesos }} = {{ $movement->twopesos * 2}}</p>
    <p><strong>$ 5 MXN</strong> x {{ $movement->fivepesos }} = {{ $movement->fivepesos * 5}}</p>
    <p><strong>$ 10 MXN</strong> x {{ $movement->tenpesos }} = {{ $movement->tenpesos * 10}}</p>
    <p><strong>$ 20 MXN</strong> x {{ $movement->twentypesos }} = {{ $movement->twentypesos * 20}}</p>
    <p><strong>$ 50 MXN</strong> x {{ $movement->fiftypesos }} = {{ $movement->fiftypesos * 50}}</p>
    <p><strong>$ 100 MXN</strong> x {{ $movement->hundredpesos }} = {{ $movement->hundredpesos * 100}}</p>
    <p><strong>$ 200 MXN</strong> x {{ $movement->twohundredpesos }} = {{ $movement->twohundredpesos * 200}}</p>
    <p><strong>$ 500 MXN</strong> x {{ $movement->fivehundredpesos }} = {{ $movement->fivehundredpesos * 500}}</p>
    <p><strong>$ 1000 MXN</strong> x {{ $movement->onethousandpesos }} = {{ $movement->onethousandpesos * 1000}}</p>
    <p><strong>1 USD = {{$exchange_rate->rate}} MXN</strong> x {{ $movement->usd ?: 0 }} = {{ ($movement->usd ?: 0) * $exchange_rate->rate}}</p>
    <p><strong>Total:</strong> {{ $movement->total }}</p>
    @if($cashRegister->total > 0)
        <p><strong>Diferencia:</strong> {{ $cashRegister->total }}</p>
    @endif

    @if($movement->type == 'corte')
        <h2>Mini cortes</h2>
        <table width="100%" border="1">
            @foreach($cashRegister->movements()->whereType('mini-corte')->get() as $mini_cut)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>$ {{ $mini_cut->total }}</td>
                </tr>
            @endforeach
        </table>

        <h2>Corte de caja</h2>
        <table width="100%" border="1">
            <tr>
                <td>EFECTIVO</td>
                @php
                    if($subsidiary->id != 11){
                        $sale_id = $cashRegister->sales()->finished()->notCanceled()->get()->pluck('id');
                        $cash_total = App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->mxn()->sum('total') - $cashRegister->sales()->finished()->notCanceled()->sum('money_change');
                    } else {
                        $cash_total = $cashRegister->laundryServices()->get()->sum('amount');
                    }
                @endphp
                <td>{{ $cash_total }}</td>
            </tr>
            <tr>
                <td>Monedero</td>
                @php
                    $sale_id = $cashRegister->sales()->finished()->notCanceled()->get()->pluck('id');
                @endphp
                <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->monedero()->sum('total') }}</td>
            </tr>
            <tr>
                <td>TARJETAS</td>
                <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->card()->sum('total') }}</td>
            </tr>
            <tr>
                <td>DLS</td>
                @php
                    $sale_id = $cashRegister->sales()->finished()->notCanceled()->get()->pluck('id');
                @endphp
                <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->usd()->sum('total') / $exchange_rate->rate }}</td>
            </tr>
            <tr>
                <td>T.C</td>
                <td>{{ $exchange_rate->rate }}</td>
            </tr>
            <tr>
                <td>Total</td>
                <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->usd()->sum('total') }}</td>
            </tr>
            <tr>
                <th>Total</th>
                @php
                    if($subsidiary->id != 11){
                        $sale_id = $cashRegister->sales()->finished()->notCanceled()->get()->pluck('id');
                        $total = App\SalePayment::whereIn('sale_id', $sale_id)->sum('total') - $cashRegister->sales()->finished()->notCanceled()->sum('money_change');
                    } else {
                        $total = $cash_total;
                    }
                @endphp
                <td>
                {{ $total }}
                </td>
            </tr>
        </table>

        <h2>Importe de servicios efectuados</h2>
        <table width="100%" border="0">
            <tr>
                <th>Puesto</th>
                <th>Nombre</th>
                <th>Importe</th>
                <th>Propina</th>
            </tr>
            <?php $stt = 0;?>
            @foreach($sales as $group_sales)
                <?php
                    if (!$employee = $group_sales->first()->employee) {
                        $employee = \App\Employee::find(46);
                    }
                    $ptot = 0;
                    $pcount = 0;
                    $producto_groups = App\SaleProduct::whereIn('sale_id', $group_sales->pluck('id'))->get()->groupBy('product_id');
                    foreach ($producto_groups as $producto_group) {
                        $pcount = $producto_group->sum('qty');
                        $ptot += $producto_group->first()->price * $pcount;
                        $stt += $producto_group->first()->price * $pcount;
                    }
                ?>
                <tr>
                    <td>{{ $employee->job }}</td>
                    <td>{{ $employee->short_name }}</td>
                    <td>
                        <?php $sale_ids = $employee->salesByMovement($movement->cash_register_id)->get()->pluck('id');?>
                        {{ App\SalePayment::whereIn('sale_id', $group_sales->pluck('id'))->sum('total') - $group_sales->sum('tip') - $ptot - $group_sales->sum('money_change')}}
                        ---
                        {{App\SaleService::whereIn('sale_id', $sale_ids)->where('service_id', '!=', '16')->sum('qty')}}
                    </td>
                    <td>{{ $group_sales->sum('tip') }}</td>
                </tr>
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td>{{ App\SalePayment::whereIn('sale_id', $cashRegister->sales()->finished()->notCanceled()->pluck('id'))->sum('total') - $cashRegister->sales()->finished()->notCanceled()->sum('tip') - $stt - $cashRegister->sales()->finished()->notCanceled()->sum('money_change')}}</td>
                <td>{{ $cashRegister->sales()->finished()->notCanceled()->sum('tip') }}</td>
            </tr>
        </table>

        <?php 
        $sale_ids = array();
        foreach($sales as $group_sales) {
            $sale_ids = array_merge($sale_ids, $group_sales->pluck('id')->toArray());
        }
        $service_groups = App\SaleService::select(DB::raw('*, sum(qty) as services_count'))->whereIn('sale_id', $sale_ids)->groupBy('service_id')->get(); 
        $services_count = 0;
        ?>
        <h2>Venta de servicios</h2>
        <table width="100%" border="0">
            <tr>
                <th>Nombre</th>
                <th>Cantidad</th>
            </tr>
            @foreach($service_groups as $service_group)
            <?php $service = App\Service::find($service_group->service_id); ?>
            <tr>
                <td>@if($service) {{ $service->name }} @endif</td>
                <td>{{ $service_group->services_count }}</td>
            </tr>
            <?php $services_count +=  $service_group->services_count; ?>
            @endforeach
            <tr>
                <td>Total:</td>
                <td>{{ $services_count }}</td>
            </tr>
        </table>

        <h2>Venta de productos</h2>
        <table width="100%" border="0">
            @foreach($productos as $sale_producto)
                <tr>
                    <td>{{ $sale_producto->sum('qty') }}</td>
                    <td>{{ $sale_producto->first()->product->name }}</td>
                    <td>{{ $sale_producto->sum('qty')}} *  {{$sale_producto->first()->price }}</td>
                    <td>{{ $sale_producto->sum('qty') * $sale_producto->first()->price }}</td>
                </tr>
            @endforeach

            <tr>
                <td colspan="3">Total venta de productos</td>
                <td>{{ $productos->sum(function ($sale_producto) {
                        return $sale_producto->sum('qty') * $sale_producto->first()->price;
                    }) }}</td>
            </tr>
        </table>
    @endif

    @if($cashRegister->sales()->repaireds()->count() > 0 )
        <h2>Servicios reparados</h2>
        <table width="100%" border="0">
            <tr>
                <th>Hecho por</th>
                <th>Reparado por</th>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Total</th>
            </tr>
            @foreach($cashRegister->sales()->repaireds()->get() as $sale)
                <tr>
                    <td>{{ $sale->employee->short_name }}</td>
                    <td>{{ $sale->repaired->short_name }}</td>
                    <td>{{ $sale->subsidiary->key }} - {{ $sale->folio }}</td>
                    <td>{{ $sale->date }}</td>
                    <td>{{ $sale->total }}</td>
                </tr>
            @endforeach

            <tr>
                <td colspan="2">Total en reparaciones</td>
                <td>{{ $cashRegister->sales()->repaireds()->sum('total') }}</td>
            </tr>
        </table>
    @endif
    @if($movement->cashierMovement != null)
    @php 
        $cashierMovement = $movement->cashierMovement;
    @endphp
    <p><strong>Estado de la caja</strong></p>
    <p><strong>$ .50 MXN</strong> x {{ $cashierMovement->fiftycents }} = {{ $cashierMovement->fiftycents * .50}}</p>
    <p><strong>$ 1 MXN</strong> x {{ $cashierMovement->onepeso }} = {{ $cashierMovement->onepeso * 1}}</p>
    <p><strong>$ 2 MXN</strong> x {{ $cashierMovement->twopesos }} = {{ $cashierMovement->twopesos * 2}}</p>
    <p><strong>$ 5 MXN</strong> x {{ $cashierMovement->fivepesos }} = {{ $cashierMovement->fivepesos * 5}}</p>
    <p><strong>$ 10 MXN</strong> x {{ $cashierMovement->tenpesos }} = {{ $cashierMovement->tenpesos * 10}}</p>
    <p><strong>$ 20 MXN</strong> x {{ $cashierMovement->twentypesos }} = {{ $cashierMovement->twentypesos * 20}}</p>
    <p><strong>$ 50 MXN</strong> x {{ $cashierMovement->fiftypesos }} = {{ $cashierMovement->fiftypesos * 50}}</p>
    <p><strong>$ 100 MXN</strong> x {{ $cashierMovement->hundredpesos }} = {{ $cashierMovement->hundredpesos * 100}}</p>
    <p><strong>$ 200 MXN</strong> x {{ $cashierMovement->twohundredpesos }} = {{ $cashierMovement->twohundredpesos * 200}}</p>
    <p><strong>$ 500 MXN</strong> x {{ $cashierMovement->fivehundredpesos }} = {{ $cashierMovement->fivehundredpesos * 500}}</p>
    <p><strong>$ 1000 MXN</strong> x {{ $cashierMovement->onethousandpesos }} = {{ $cashierMovement->onethousandpesos * 1000}}</p>
    <p><strong>1 USD = {{$exchange_rate->rate}} MXN</strong> x {{ $cashierMovement->usd ?: 0 }} = {{ ($cashierMovement->usd ?: 0) * $exchange_rate->rate}}</p>
    <p><strong>Total:</strong> {{ $cashierMovement->total() }}</p>
    @endif
    <div class="sign text-area">
        <h4>Firma</h4>
    </div>
@endsection