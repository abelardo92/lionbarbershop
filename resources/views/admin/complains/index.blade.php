@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Quejas y sugerencias</div>

                    <div class="panel-body">
                        <a href="{{route('complains.create')}}">Agregar queja</a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Nombre</th>
                                    <th>Sucursal</th>
                                    <th>Teléfono</th>
                                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                    <th>Creado por</th>
                                    <th>Visto por</th>
                                    @endif
                                    <th>Ver</th>
                                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                    @endif
                                    <th>Ticket</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($complains as $complain)
                                    <?php $sale = $complain->sale(); ?>
                                    <tr>
                                        <td>{{ $complain->date }}</td>
                                        <td>{{ $complain->name }}</td>
                                        <td>
                                            @if($subsidiary = $complain->subsidiary()->get()->first())
                                                {{ $subsidiary->name }}
                                            @endif
                                        </td>
                                        <td>{{ $complain->phone }}</td>
                                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                        <td>
                                            @if($user = $complain->user()->get()->first())
                                                {{ $user->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($complain->openedBy != null)
                                                {{ $complain->openedBy->name }}
                                            @endif
                                        </td>
                                        @endif
                                        <td>
                                            <a href="{{ route('complains.show', $complain->id) }}" class="btn btn-link">Ver</a>
                                        </td>
                                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                        <td>
                                            <a href="{{ route('complains.edit', $complain->id) }}" class="btn btn-link">Editar</a>
                                        </td>
                                        <td>
                                            <form action="{{route('complains.destroy', [$complain->id])}}" method="post">
                                                {{{csrf_field()}}}
                                                {{{method_field('DELETE')}}}
                                                <input type="submit" class="btn btn-link" value="Eliminar">
                                            </form>
                                        </td>
                                        @endif
                                        <td>
                                            @if($complain->subsidiary_id != null && $complain->folio != 0 && $sale != null)  
                                                <a href="{{route('sales.print.admin', [$subsidiary->key, $sale->id])}}" target="_blank">
                                                    {{ $subsidiary->key }}-{{ $sale->folio }}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
