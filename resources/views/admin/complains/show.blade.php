@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Quejas y sugerencias
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">

                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Fecha del incidente *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control datepicker" id="date" name="date" required="required" value="{{ $complain->date }}" disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $complain->name }}" disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Teléfono</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control" id="phone" name="phone" value="{{ $complain->phone }}" disabled>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Motivo *</label>
                                    <div class="col-sm-6">
                                      <textarea class="form-control" rows="8" id="reason" name="reason" disabled>{{ $complain->reason }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Folio</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="folio" name="folio" value="{{ $folio }}" disabled>
                                    </div>
                                </div>

                                @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Seguimiento</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="tracing" name="tracing" value="{{ $complain->tracing }}" disabled>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <a class="btn btn-primary" href="{{route('complains.index')}}">Regresar</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
