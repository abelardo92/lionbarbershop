@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tipos de cambio
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/configurations/rates')}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="rate" class="col-sm-4 control-label">Importe *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="rate" name="rate" required="required" value="{{ old('rate') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
