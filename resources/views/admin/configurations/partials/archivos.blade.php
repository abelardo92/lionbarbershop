<div role="tabpanel" class="tab-pane active" id="archivos">
    <div class="panel panel-default">
        <div class="panel-body">
            <form action="{{route('configurations.updateFiles', $configuration->id)}}" method="post" enctype="multipart/form-data">
                {{{ csrf_field() }}}
                {{{ method_field('PUT') }}}

                <div class="form-group">
                    <label for="image[]" class="control-label">Archivo(s) *</label>
                    <input type="file" class="form-control" id="image[]" name="image[]" multiple required="required">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Guardar">
                </div>
            </form>
                <br/>
                <?php $count = 0; ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>F. Creación</th>
                            @if($count > 0 && Auth::user()->isA('super-admin', 'accountant'))
                            <th>Opciones</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($configuration->images as $file)
                            <tr>
                                <td><a href="{{ $file->path }}" target="_blank">{{ $file->name }}</a></td>
                                <td>{{ $file->created_at }}</td>
                                <td>
                                    @if($count > 0 && Auth::user()->isA('super-admin', 'accountant'))
                                        <form action="{{route('destroy.images', $file->id)}}" method="post">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="btn btn-danger">Eliminar</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>           
                            <?php $count++; ?>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
