<div role="tabpanel" class="tab-pane" id="contrato">
    <div class="panel panel-default">
        <div class="panel-body">
            <form action="{{route('configurations.update', $configuration->id)}}" method="post">
                {{{ csrf_field() }}}
                {{{ method_field('PUT') }}}
                <div class="form-group">
                    <label>Empleado:</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%name%');return false;" class="label label-primary">%name%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%short_name%');return false;" class="label label-primary">%short_name%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%job%');return false;" class="label label-primary">%job%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%entry_date%');return false;" class="label label-primary">%entry_date%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%rfc%');return false;" class="label label-primary">%rfc%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%curp%');return false;" class="label label-primary">%curp%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%nss%');return false;" class="label label-primary">%nss%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%address%');return false;" class="label label-primary">%address%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%gender%');return false;" class="label label-primary">%gender%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%salary%');return false;" class="label label-primary">%salary%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%salary_text%');return false;" class="label label-primary">%salary_text%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%code%');return false;" class="label label-primary">%code%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%marital_status%');return false;" class="label label-primary">%marital_status%</label>
                    
                    <br/><label>Configuración:</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%razon_social%');return false;" class="label label-primary">%razon_social%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%configuration_rfc%');return false;" class="label label-primary">%configuration_rfc%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%configuration_address%');return false;" class="label label-primary">%configuration_address%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%configuration_curp%');return false;" class="label label-primary">%configuration_curp%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%configuration_created_in%');return false;" class="label label-primary">%configuration_created_in%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%configuration_regimen%');return false;" class="label label-primary">%configuration_regimen%</label>

                    <br/><label>Centrar texto:</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%begin%');return false;" class="label label-primary">%begin%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%middle%');return false;" class="label label-primary">%middle%</label>
                    <label for="@{{name}}" onclick="insertAtCaret('%end%');return false;" class="label label-primary">%end%</label>
                    <hr>

                    <textarea name="contract" id="contract" class="form-control" rows="15">{{old('contract', $configuration->contract)}}</textarea>
                </div>
                @if(Auth::user()->isA('super-admin'))
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Guardar">
                </div>
                @endif
            </form>
        </div>
    </div>
</div>
