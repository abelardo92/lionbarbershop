<div role="tabpanel" class="tab-pane" id="huella">
    <div class="panel panel-default">
        <div class="panel-heading">
        Dispositivos de huella
            <p class="pull-right">
                <a href="{{ url('/home/configurations/devices') }}">Agregar nuevo</a>
            </p>
        </div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>NS</th>
                        <th>Sucursal</th>
                        <th>opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($devices as $device)
                        <tr>
                            <td>
                                {{ $device->device_name }}
                            </td>
                            <td>{{ $device->sn }}</td>
                            <td>{{ $device->subsidiary->name }}</td>
                            <td>
                                <a href="{{url('/home/configurations/devices/'.$device->id)}}">
                                    Editar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $devices->links() }}
        </div>
    </div>
</div>
