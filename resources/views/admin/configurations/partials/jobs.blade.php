<div role="tabpanel" class="tab-pane" id="puestos">
    <div class="panel panel-default">
        <div class="panel-heading">
            Puestos
            <p class="pull-right">
                <a href="{{ url('/home/configurations/jobs') }}">Agregar nuevo</a>
            </p>
        </div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{{ $job->name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
