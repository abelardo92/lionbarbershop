@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clientes
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('customers.store')}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        @if($redirect)
                            <input type="hidden" name="redirect" value="{{$redirect}}">
                        @endif

                        <div class="form-group">
                            <label for="wallet_number" class="col-sm-4 control-label">Monedero *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="wallet_number" name="wallet_number" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="identify_number" class="col-sm-4 control-label">Numero de identificación</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="identify_number" name="identify_number" value="{{ old('identify_number') }}">
                              <span class="help-block">IFE, INE, Licencia conducir, Pasaporte, etc.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Telefono</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="birthday" class="col-sm-4 control-label">Cumpleaños</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="{{ old('birthday') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook" class="col-sm-4 control-label">Facebook</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="facebook" name="facebook" value="{{ old('facebook') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter" class="col-sm-4 control-label">Twitter</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="twitter" name="twitter" value="{{ old('twitter') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="residence_place" class="col-sm-4 control-label">Lugar de residencia</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="residence_place" name="residence_place"  value="{{ old('residence_place') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
