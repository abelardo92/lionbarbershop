@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clientes
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('customers.update', $customer->id)}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="wallet_number" class="col-sm-4 control-label">Monedero *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="wallet_number" name="wallet_number" value="{{ $customer->wallet_number }}" @if($customer->wallet_number) disabled @endif>
                                <replace-wallet
                                    current="{{ $customer->wallet_number }}"
                                    customer_id="{{ $customer->id }}"
                                ></replace-wallet>
                            </div>
                        </div>

                        @if($customer->last_wallet_number)
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Monedero anterior *</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control" value="{{ $customer->last_wallet_number }}" disabled>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $customer->name) }}" @if(!Auth::user()->isA('super-admin', 'super-admin-restringido')) disabled @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="identify_number" class="col-sm-4 control-label">Numero de identificación</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="identify_number" name="identify_number" value="{{ old('identify_number', $customer->identify_number) }}">
                              <span class="help-block">IFE, INE, Licencia conducir, Pasaporte, etc.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Telefono *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone', $customer->phone) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="birthday" class="col-sm-4 control-label">Cumpleaños *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="{{ old('birthday', $customer->birthday) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $customer->email) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook" class="col-sm-4 control-label">Facebook</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="facebook" name="facebook"  value="{{ old('facebook', $customer->facebook) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter" class="col-sm-4 control-label">Twitter</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="twitter" name="twitter"  value="{{ old('twitter', $customer->twitter) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="residence_place" class="col-sm-4 control-label">Lugar de residencia *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="residence_place" name="residence_place" required="required" value="{{ old('residence_place', $customer->residence_place) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="visits" class="col-sm-4 control-label">Numero de visitas *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="visits" name="visits" @if(!$user->isA('super-admin', 'super-admin-restringido')) disabled="disabled" @endif value="{{ $customer->visits }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Servicios gratis</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" @if(!$user->isA('super-admin')) disabled="disabled" @endif value="{{ $customer->services_free }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <div class="pull-right">
                                @if($customer->creator)
                                    Creado por: {{ $customer->creator->getName() }}<br/>
                                @endif
                                    Fecha de  creación: {{ $customer->created_at }}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Monederos para referencia
                    <referral-modal
                        customer_id="{{ $customer->id }}"
                    ></referral-modal>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Monedero</th>
                                <th>Cliente</th>
                                <th>Usado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customer->referrals()->paginate(10) as $referral)
                                <tr>
                                    <td>{{ $referral->wallet_number }}</td>
                                    <td>{{ $referral->owner ? $referral->owner->name : '' }}</td>
                                    <td>{{ $referral->used ? 'Si' : 'No' }}</td>
                                    <td>
                                        <form action="{{route('customers.referrals.destroy', [$customer->id, $referral->id])}}" method="post">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$customer->referrals()->paginate(10)->links()}}
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
