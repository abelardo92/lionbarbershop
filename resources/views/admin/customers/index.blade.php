@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clientes
                    <div class="pull-right">
                        <a class="header-link" href="{{route('customers.create')}}">Agregar cliente</a>
                    </div>
                </div>

                <div class="panel-body">
                <form method="GET" action="{{ url('home/customers') }}">
                <div class="pull-right">
                    <div class="input-group custom-search-form">
                    <input type="text" class="form-control" name="filter" placeholder="buscar...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
                </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>Correo</th>
                                <th>Facebook</th>
                                <th>Twitter</th>
                                <th>Cumpleaños</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $customer->facebook }}</td>
                                    <td>{{ $customer->twitter }}</td>
                                    <td>{{ $customer->birthday }}</td>
                                    <td>
                                        <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-link">Editar</a>
                                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                        <form action="{{route('customers.deactivate', $customer->id)}}" method="post">
                                            {{{csrf_field()}}}
                                            <input type="submit" class="btn btn-link" value="Eliminar">
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if(isset($filter))
                        {{ $customers->appends(['filter' => $filter])->links() }}
                    @else
                        {{ $customers->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
