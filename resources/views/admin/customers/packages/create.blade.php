@extends('layouts.app')

@section('css')
<style>
    .datepicker .dropdown-menu {
    position: relative;
    z-index: 10000 !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Asigna un cliente y un paquete (promoción especial)
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <div class="panel-body">   
                    <b>Esta sección permite asignar promociones especiales a un cliente, al terminar se generará un código, el cual se deberá proporcionar el cliente.</b>
                    <br/><br/><br/><br/>
                    <form action="{{route('customer.packages.store')}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">Cliente *</label>
                            <div class="col-sm-6">
                              <select name="customer_id" id="customer_id" class="customer_id form-control">
                              </select>
                                <!--<select class="js-data-example-ajax"></select>-->
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Paquete *</label>
                            <div class="col-sm-6">
                                <select name="package_id" id="package_id" class="select2 form-control">
                                    <option value="">Seleccionar uno por favor...</option>
                                    @foreach($packages as $package)
                                        <option @if($package_id == $package->id) selected="selected" @endif value="{{$package->id}}">{{$package->name}} ({{$package->wallet_number}})
                                            cel: {{ $package->phone }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="birthday" class="col-sm-4 control-label">Fecha de expiración</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="expiration_date" name="expiration_date" value="{{ old('expiration_date') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/customer-filter.js') }}"></script>
@endsection
