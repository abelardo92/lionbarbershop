@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Depostios
                </div>

                <div class="panel-body">
                    <form action="{{route('deposits.update', $deposit->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="employee_id">Empleado: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($employees as $employee)
                                    <option 
                                    @if($employee->id == $deposit->employee->id) selected="selected" @endif 
                                    value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{ old('start', $deposit->start) }}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{ old('end', $deposit->end) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount">Cantidad depositada: </label>
                            <input type="text" class="form-control" name="amount" value="{{ old('amount', $deposit->amount) }}">
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Guardar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
