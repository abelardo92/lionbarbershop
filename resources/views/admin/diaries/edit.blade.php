@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Citas <small>Editar cita</small>
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('diary.update', $diary->id)}}" method="POST">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        {{-- <input type="hidden" name="barber_id" v-model="barber_id" value="0"> --}}
                        <input type="hidden" name="time" v-model="time" value="{{$diary->time}}">

                        <div class="form-group">
                            <label for="customer_id" class="control-label">
                                Cliente *
                                @if(Auth::user()->isA('super-admin'))
                                    <a href="{{url('/home/customers/create?redirect='.route('diary.edit', $diary->id))}}" class="btn btn-xs btn-info">Nuevo cliente</a>
                                @endif
                            </label>
                            <select name="customer_id" id="customer_id" class="select2 form-control" required="required">
                                <option value="">Seleccionar uno por favor...</option>
                                @foreach($customers as $customer)
                                    <option @if($diary->customer_id == $customer->id) selected="selected" @endif value="{{$customer->id}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="control-label">Telefono *</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{$diary->customer->phone}}">
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="control-label">
                                Sucursal *
                            </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control" required="required" v-model="subsidiary_id">
                                <option value="">Seleccionar uno por favor...</option>
                                @foreach($subsidiaries as $subsi)
                                    <option @if($diary->subsidiary_id == $subsi->id) selected="selected" @endif value="{{$subsi->id}}">{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="date" class="control-label">Fecha *</label>
                            <input type="text" class="form-control datepicker" name="date" id="date" v-model="date" value="{{$diary->date}}">
                        </div>

                        <div class="form-group">
                            <label for="employee_id" class="control-label">
                                Barbero *
                            </label>
                            <select name="employee_id" id="barber_id" class="select2 form-control">
                                <option value="0">Seleccionar uno por favor...</option>
                                @if($diary->employee)
                                    <option selected="selected" value="{{$diary->employee->id}}">{{$diary->employee->short_name}}</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-default" :disabled="!canCreateDiary">Guardar</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-primary" @click="searchTimes">Buscar</button>
                                <p class="text-red" v-if="time_error">@{{time_error}}</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Horas disponibles
                </div>
                <div class="panel-body" v-for="timess in times">
                    <button class="btn btn-default" :class="{active: time.active}" :disabled="time.disabled" v-for="time in timess" style="margin-left: 10px; margin-top: 10px;" @click="setTimeActive(time)">@{{time.time_es}}</button>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Comidas del dia
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Barbero</th>
                                <th>Sucursal</th>
                                <th>Inicio</th>
                                <th>fin</th>
                                <th>Creado por</th>
                                <th>Creacion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($eats as $eat)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $eat->employee->short_name }}</td>
                                    <td>{{ $eat->subsidiary->name }}</td>
                                    <td>{{ $eat->start }}</td>
                                    <td>{{ $eat->end }}</td>
                                    @if($eat->user)
                                        <td>{{ $eat->user->name }}</td>
                                    @elseif($eat->cashier)
                                        <td>{{ $eat->cashier->short_name }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $eat->created_at->format('h:i a') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#customer_id').on('select2:select', function (){
                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: '/home/customers/'+$(this).val(),
                    success: function (res) {
                        $('#phone').val(res.phone)
                    }
                });
            });
        });
    </script>
@endsection
