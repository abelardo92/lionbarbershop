@extends('layouts.tickets_internos')


@section('content')
    <p><strong>FECHA:</strong> {{ $attendance->date }}</p>
    <p><strong>NOMBRE:</strong> {{ $employee->name }}</p>
    <p><strong>SUCURSAL:</strong> {{ $subsidiary->name }}</p>

    @foreach($employee->attendances()->today()->get() as $attendance)
        <p><strong>{{ strtoupper($attendance->type) }}:</strong> {{ $attendance->time }}</p>
    @endforeach
    <table>
        <tr>
            <th>No</th>
            <th>Servicio</th>
            <th>Importe</th>
        </tr>
    <?php $total = 0;?>
    @foreach($employee->getServicesReport() as $service)
        <tr>
            <td>{{ $service->service->name }}</td>
            <td>
                {{ $service->services_count }}
            </td>
            <td>
                <?php $total += $service->price * $service->services_count;?>
              $ {{ $service->price * $service->services_count }}
            </td>
        </tr>
    @endforeach
        <tr>
            <td colspan=>
               Total:
            </td>
            <td>
                $ {{ $total }}
            </td>
        </tr>
    </table>

    <div class="sign text-area">
        <h4>Firma</h4>
    </div>
@endsection