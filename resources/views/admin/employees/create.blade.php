@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Empleados
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('employees.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal" autocomplete="off">
                        {{{ csrf_field() }}}
                        <div class="form-group">
                            <label for="key" class="col-sm-4 control-label">Numero de empleado *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="key" name="key" value="{{ old('key') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employee_code" class="col-sm-4 control-label">Código de empleado *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="employee_code" name="employee_code" value="{{ old('employee_code') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre completo*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="name" name="name" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="short_name" class="col-sm-4 control-label">Nombre corto *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="short_name" name="short_name" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-4 control-label">Foto</label>
                            <div class="col-sm-6">
                              <input type="file" class="form-control" id="image" name="image">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="email" name="email" required="required" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Telefono *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="phone" name="phone" required="required" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entry_date" class="col-sm-4 control-label">Fecha de ingreso *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="entry_date" name="entry_date" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rfc" class="col-sm-4 control-label">RFC *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="rfc" name="rfc" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="curp" class="col-sm-4 control-label">CURP *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="curp" name="curp" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nss" class="col-sm-4 control-label">NSS *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="nss" name="nss" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-sm-4 control-label">Dirección *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="address" name="address" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="marital_status" class="col-sm-4 control-label">Estado civil *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="marital_status" name="marital_status" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option value="Soltero">Soltero</option>
                                  <option value="Casado">Casado</option>
                                  <option value="Divorciado">Divorciado</option>
                                  <option value="Viudo">Viudo</option>
                                  <option value="Union Libre">Union Libre</option>
                              </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-sm-4 control-label">Sexo *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="gender" name="gender" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option value="Masculino">Masculino</option>
                                  <option value="Femenino">Femenino</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="salary" class="col-sm-4 control-label">Salario *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="salary" name="salary" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="commission_number" class="col-sm-4 control-label">Costo pagado por servicio *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="commission_number" name="commission_number" required="required">
                                  <option value="1">Costo 1</option>
                                  <option value="2">Costo 2</option>
                                  <option value="3">Costo 3</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="imss_salary" class="col-sm-4 control-label">Salario del IMSS</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="imss_salary" name="imss_salary">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="infonavit_number" class="col-sm-4 control-label">No. de Credito Infonavit</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="infonavit_number" name="infonavit_number">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="monto_infonavit" class="col-sm-4 control-label">Monto descuento Infonavit</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="monto_infonavit" name="monto_infonavit">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account_number" class="col-sm-4 control-label">No. de Cuenta</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="account_number" name="account_number">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_banco" class="col-sm-4 control-label">No. empleado del banco</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="no_banco" name="no_banco">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_banco" class="col-sm-4 control-label">Nombre del banco</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="bank_name" name="bank_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_tarjeta" class="col-sm-4 control-label">No. de tarjeta</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="no_tarjeta" name="no_tarjeta">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  @foreach($subsidiaries as $subsidiary)
                                    <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="job" class="col-sm-4 control-label">Puesto *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="job" name="job" required="required">
                                  <option value="">Selecciona uno por favor...</option>
                                  @foreach($jobs as $job)
                                    <option value="{{ $job->name }}">{{ $job->name }}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="has_excellence" class="col-sm-4 control-label">Tiene bono de excelencia *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="has_excellence" name="has_excellence" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option value="1">Si</option>
                                  <option value="0">No</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">Clave empleado</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="code" name="code" value="{{ old('code') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entry_date" class="col-sm-4 control-label">Fecha de alta IMSS *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="imss_date" name="imss_date" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-sm-4 control-label">Contraseña empleado</label>
                            <div class="col-sm-6">
                              <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entry_date" class="col-sm-4 control-label">Fecha de nacimiento *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="{{ old('birthday') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <button class="btn btn-info" type="button" onclick="window.history.back()">regresar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
