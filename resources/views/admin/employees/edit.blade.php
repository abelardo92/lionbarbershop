@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($image = $employee->getProfileimage())
                        <img src="{{$image->path}}" alt="{{$image->name}}" class="img-square img-responsive">
                    @endif
                </div>
            </div>
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'horarios'))
              <div class="panel panel-default">
                  <div class="panel-heading">
                    Documentos 
                    <a href="{{route('employees.contrato', $employee->id)}}" target="_blank">Imprimir contrato</a>
                  </div>
                  <div class="panel-body">
                      <form action="{{route('employees.upload.documents', $employee->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                          {{{ csrf_field() }}}
                          {{{ method_field('PUT') }}}
                            <div class="form-group">
                                <label for="image[]" class="control-label">Imagenes</label>
                                <input type="file" class="form-control" id="image[]" multiple name="image[]">
                            </div>

                            <div class="form-group">
                              <button type="submit" class="btn btn-default">Subir</button>
                            </div>
                      </form>

                      <hr>
                      <ul class="list-unstyled">
                        @foreach(App\Employee::DOCUMENTS as $document)
                            <li>
                              @if($doc = $employee->hasDocument($document))
                                <a href="{{$doc->path}}" target="_blank">
                                  <i class="fa fa-check"></i> {{$document}}
                                </a>
                              @else
                                <i class="fa fa-close"></i> {{$document}}
                              @endif
                            </li>
                        @endforeach
                      </ul>
                  </div>
              </div>
            @endif

        </div>
        <div class="col-md-9">
          @if(!Auth::user()->isA('supervisor'))
            <div class="panel panel-default">
                <div class="panel-heading">
                    Empleados
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('employees.update', $employee->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal" autocomplete="off">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        <div class="form-group">
                            <label for="key" class="col-sm-4 control-label">Numero de empleado *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key', $employee->key) }}" @if(Auth::user()->isA('horarios')) disabled="disabled" @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employee_code" class="col-sm-4 control-label">Código de empleado *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="employee_code" name="employee_code" value="{{ old('employee_code', $employee->employee_code) }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre completo*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $employee->name) }}" @if(Auth::user()->isA('horarios')) disabled="disabled" @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="short_name" class="col-sm-4 control-label">Nombre corto*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="short_name" name="short_name" required="required" value="{{ old('short_name', $employee->short_name) }}" @if(Auth::user()->isA('horarios')) disabled="disabled" @endif>
                            </div>
                        </div>

                      @if(Auth::user()->isA('horarios'))

                        <div class="form-group">
                            <label for="image" class="col-sm-4 control-label">Foto</label>
                            <div class="col-sm-6">
                              <input type="file" class="form-control" id="image" name="image">
                            </div>
                        </div>
                      @endif
                      @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="email" name="email" required="required" value="{{ old('email', $employee->email) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Telefono *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="phone" name="phone" required="required" value="{{ old('phone', $employee->phone) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entry_date" class="col-sm-4 control-label">Fecha de ingreso *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="entry_date" name="entry_date" required="required" value="{{ old('entry_date', $employee->entry_date) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rfc" class="col-sm-4 control-label">RFC *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="rfc" name="rfc" required="required" value="{{ old('rfc', $employee->rfc) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="curp" class="col-sm-4 control-label">CURP *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="curp" name="curp" required="required" value="{{ old('curp', $employee->curp) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nss" class="col-sm-4 control-label">NSS *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="nss" name="nss" required="required" value="{{ old('NSS', $employee->nss) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-sm-4 control-label">Dirección *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="address" name="address" required="required"  value="{{ old('address', $employee->address) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="marital_status" class="col-sm-4 control-label">Estado civil *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="marital_status" name="marital_status" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option @if('Soltero' == $employee->marital_status) selected="selected" @endif value="Soltero">Soltero</option>
                                  <option @if('Casado' == $employee->marital_status) selected="selected" @endif value="Casado">Casado</option>
                                  <option @if('Divorciado' == $employee->marital_status) selected="selected" @endif value="Divorciado">Divorciado</option>
                                  <option @if('Viudo' == $employee->marital_status) selected="selected" @endif value="Viudo">Viudo</option>
                                  <option @if('Union Libre' == $employee->marital_status) selected="selected" @endif value="Union Libre">Union Libre</option>
                              </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gender" class="col-sm-4 control-label">Sexo *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="gender" name="gender" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option @if('Masculino' == $employee->gender) selected="selected" @endif value="Masculino">Masculino</option>
                                  <option @if('Femenino' == $employee->gender) selected="selected" @endif value="Femenino">Femenino</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="salary" class="col-sm-4 control-label">Salario *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="salary" name="salary" required="required"  value="{{ old('salary', $employee->salary) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="commission_number" class="col-sm-4 control-label">Costo pagado por servicio *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="commission_number" name="commission_number" required="required">
                                  <option @if('1' == $employee->commission_number) selected="selected" @endif value="1">Costo 1</option>
                                  <option @if('2' == $employee->commission_number) selected="selected" @endif value="2">Costo 2</option>
                                  <option @if('3' == $employee->commission_number) selected="selected" @endif value="3">Costo 3</option>
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="imss_salary" class="col-sm-4 control-label">Salario del IMSS</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="imss_salary" name="imss_salary"  value="{{ old('imss_salary', $employee->imss_salary) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="infonavit_number" class="col-sm-4 control-label">No. de Credito Infonavit</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="infonavit_number" name="infonavit_number" value="{{ old('infonavit_number', $employee->infonavit_number) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="monto_infonavit" class="col-sm-4 control-label">Monto descuento Infonavit</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="monto_infonavit" name="monto_infonavit" value="{{ old('monto_infonavit', $employee->monto_infonavit) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account_number" class="col-sm-4 control-label">No. de cuenta.</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="account_number" name="account_number" value="{{ old('account_number', $employee->account_number) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_banco" class="col-sm-4 control-label">No. de empleado del banco.</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="no_banco" name="no_banco" value="{{ old('no_banco', $employee->no_banco) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bank_name" class="col-sm-4 control-label">Nombre del banco</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="bank_name" name="bank_name" value="{{ old('bank_name', $employee->bank_name) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="no_tarjeta" class="col-sm-4 control-label">No. de tarjeta.</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="no_tarjeta" name="no_tarjeta" value="{{ old('no_tarjeta', $employee->no_tarjeta) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  @foreach($subsidiaries as $subsidiary)
                                    <option @if($subsidiary->id == $employee->subsidiary_id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="job" class="col-sm-4 control-label">Puesto *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="job" name="job" required="required">
                                  <option value="">Selecciona uno por favor...</option>
                                  @foreach($jobs as $job)
                                    <option @if($employee->job == $job->name) selected="selected" @endif alue="{{ $job->name }}">{{ $job->name }}</option>
                                  @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="has_excellence" class="col-sm-4 control-label">Tiene bono de excelencia *</label>
                            <div class="col-sm-6">
                              <select class="form-control" id="has_excellence" name="has_excellence" required="required">
                                  <option value="">Seleccione uno por favor...</option>
                                  <option @if($employee->has_excellence) selected="selected" @endif value="1">Si</option>
                                  <option @if(!$employee->has_excellence) selected="selected" @endif value="0">No</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">Clave empleado</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="code" name="code" value="{{ old('code', $employee->code) }}" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imss_date" class="col-sm-4 control-label">Fecha de alta IMSS *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="imss_date" name="imss_date" value="{{ $employee->imss_date }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="entry_date" class="col-sm-4 control-label">Fecha de nacimiento *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="{{ old('birthday', $employee->birthday) }}" required="required">
                            </div>
                        </div>
                      @endif
                      @if(Auth::user()->isA('super-admin', 'super-admin-restringido','horarios'))
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <button class="btn btn-info" type="button" onclick="window.history.back()">regresar</button>
                            </div>
                        </div>
                      @endif
                    </form>
                </div>
            </div>
          @endif
          @if(Auth::user()->isA('super-admin','super-admin-restringido'))
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clave de acceso
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('employees.update', $employee->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal" autocomplete="off">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="code" class="col-sm-4 control-label">Clave empleado</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="code" name="code" value="{{ old('code', $employee->code) }}" required="required">
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="password" class="col-sm-4 control-label">Huella digital</label>
                            <div class="col-sm-6">
                              @if($employee->fingers->count() == 0)
                                <a href="finspot:FingerspotReg;{{$registerUrl}}">
                                  Registrar
                                </a>
                              @else
                                Registrado
                              @endif
                            </div>
                        </div> -->

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <button class="btn btn-info" type="button" onclick="window.history.back()">regresar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
          @endif

          @include('admin.employees.partials.scheluds')
          @if(!Auth::user()->isA('supervisor'))
            @include('admin.employees.partials.disabilities')
          @endif

          @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'cashier-admin'))
            @include('admin.employees.partials.issues')
          @endif

          @if(!Auth::user()->isA('supervisor'))
            @include('admin.employees.partials.user')
          @endif
        </div>
    </div>
</div>
@endsection
