@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitud por: <strong>{{$employee->name}}</strong>
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('employees.exchanges.store', $employee->id) }}" method="post" autocomplete="off">
                      {{{ csrf_field() }}}

                      <div class="form-group">
                        
                        <div class="col-md-6">
                          <label for="employee2_id" class="control-label">Empleado a intercambiar *</label>
                          <select class="form-control" id="employee2_id" name="employee2_id" required="required" v-model="employee2_id">
                              <option value="" selected>Seleccione uno por favor...</option>
                              @foreach($employees as $emp)
                                <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                              @endforeach
                          </select>
                        </div>
        
                        <div class="col-md-6">
                          <label for="date" class="control-label">Fecha *</label>
                          <input type="text" class="form-control datepicker" id="date" name="date"/>
                        </div>

                      </div>
                      @if(Auth::user()->isA('super-admin'))
                      <div class="form-group>">
                        <div class="col-md-12">
                          <label style="margin-top:20px; margin-bottom:20px;">Si desea realizar mas de un intercambio a la vez se deberá utilizar los siguientes campos</label>
                        </div>    
                      </div>

                      <div class="form-group">
                        
                        <div class="col-md-12">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>
                                  <label class="control-label">Empleado</label>
                                </th>
                                <th>
                                  <label class="control-label">Empleado 2</label>
                                </th>
                                <th>
                                  <label class="control-label">Fecha</label>
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <select class="form-control" name="employee_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" name="employee2_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <input type="text" class="form-control datepicker" name="dates[]"/>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="form-control" name="employee_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" name="employee2_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <input type="text" class="form-control datepicker" name="dates[]"/>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="form-control" name="employee_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" name="employee2_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <input type="text" class="form-control datepicker" name="dates[]"/>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <select class="form-control" name="employee_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <select class="form-control" name="employee2_ids[]">
                                    <option value="" selected>Seleccione uno por favor...</option>
                                    @foreach($employees as $emp)
                                      <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                    @endforeach
                                  </select>
                                </td>
                                <td>
                                  <input type="text" class="form-control datepicker" name="dates[]"/>
                                </td>
                              </tr>
                            </tbody>   
                          </table>
                        </div>
                      </div>
                      @endif
                      <div class="form-group col-md-offset-8">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                         <a href="{{ URL::previous() }}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        /*
        $(document).ready(function () {
          var date = new Date();
          date.setDate(date.getDate()+2);
          //alert(date);
          $('#date').datetimepicker({  
            format: 'dd/mm/yyyy',
            //defaultDate: "11/1/2013",
            startDate: date
          });
        });
        */
    </script>
@endsection