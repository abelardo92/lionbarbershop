@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitud por: <strong>{{$employee->name}}</strong>
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                
                <div class="panel-body">
                    <form action="{{ route('employees.exchanges.update', [$employee->id, $exchange->id]) }}" method="post" autocomplete="off">
                      {{{ csrf_field() }}}
                      {{{ method_field('PUT') }}}
                      <div class="form-group">
                        
                        <div class="col-md-6">
                          <label for="employee2_id" class="control-label">Empleado a intercambiar *</label>
                          <select class="form-control" id="employee2_id" name="employee2_id" required="required" v-model="employee2_id">
                              <option value="" selected>Seleccione uno por favor...</option>
                              @foreach($employees as $emp)
                                <option value="{{$emp->id}}" @if($exchange->employee2_id == $emp->id) selected @endif>{{$emp->short_name}}</option>
                              @endforeach
                          </select>
                        </div>

                        <div class="col-md-6">
                          <label for="date" class="control-label">Fecha *</label>
                          <input type="text" class="form-control datepicker" id="date" name="date" value="{{ $exchange->date }}"/>
                        </div>

                      </div>

                      <div class="form-group>">
                        <div class="col-md-12">
                          <label style="margin-top:20px; margin-bottom:20px;">Si desea realizar mas de un intercambio a la vez se deberá utilizar los siguientes campos</label>
                        </div>    
                      </div>

                      <div class="form-group">
                        
                        <div class="col-md-12">
                          <table class="table">
                            <tr>
                              <th>
                                <label class="control-label">Empleado</label>
                              </th>
                              <th>
                                <label class="control-label">Empleado 2</label>
                              </th>
                              <th>
                                <label class="control-label">Fecha</label>
                              </th>
                            </tr>
                            @php
                              $cont = 0;
                            @endphp
                            @foreach($exchange->exchanges as $exchange_child)
                            <tr>
                              <td>
                                <input type="hidden" name="exchange_child_id[]" value="{{ $exchange_child->id }}"/>
                                <select class="form-control" name="employee_ids[]">
                                  <option value="" selected>Seleccione uno por favor...</option>
                                  @foreach($employees as $emp)
                                    <option value="{{$emp->id}}" @if($exchange_child->employee_id == $emp->id) selected @endif>{{$emp->short_name}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <select class="form-control" name="employee2_ids[]">
                                  <option value="" selected>Seleccione uno por favor...</option>
                                  @foreach($employees as $emp)
                                    <option value="{{$emp->id}}" @if($exchange_child->employee2_id == $emp->id) selected @endif>{{$emp->short_name}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <input type="text" class="form-control datepicker" name="dates[]" value="{{ $exchange_child->date }}"/>
                              </td>
                            </tr>
                              @php
                                $cont++;
                              @endphp
                            @endforeach
                            @for($i = $cont; $i < 4; $i++)
                              <!--<tr>
                              <td>
                                <select class="form-control" name="employee_ids[]">
                                  <option value="" selected>Seleccione uno por favor...</option>
                                  @foreach($employees as $emp)
                                    <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <select class="form-control" name="employee2_ids[]">
                                  <option value="" selected>Seleccione uno por favor...</option>
                                  @foreach($employees as $emp)
                                    <option value="{{$emp->id}}">{{$emp->short_name}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <input type="text" class="form-control datepicker" name="dates[]"/>
                              </td>
                            </tr>-->
                            @endfor
                          </table>
                        </div>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Guardar">
                         <a href="{{ URL::previous() }}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        /*
        $(document).ready(function () {
          var date = new Date();
          date.setDate(date.getDate()+2);
          //alert(date);
          $('#date').datetimepicker({  
            format: 'dd/mm/yyyy',
            //defaultDate: "11/1/2013",
            startDate: date
          });
        });
        */
    </script>
@endsection