@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Incidencia del empleado: <strong>{{$employee->name}}</strong>

                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('employees.issues.update', [$employee_id, $issue->id]) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}
                      {{{ method_field('PUT') }}}

                      <div class="form-group">
                          <label for="area" class="control-label">Faltas incurridas *</label>
                          <textarea class="form-control" id="area" name="area" required="required">{{old('area', $issue->area)}}</textarea>
                      </div>

                      <div class="form-group">
                          <label for="date" class="control-label">Fecha *</label>
                          <input type="text" class="form-control datepicker" id="date" name="date" required="required" value="{{old('date', $issue->date)}}">
                      </div>

                      <div class="form-group">
                          <label for="action" class="control-label">Observaciones </label>
                          <textarea class="form-control" id="action" name="action" >{{old('action', $issue->action)}}</textarea>
                      </div>

                      <div class="form-group">
                          <label for="image[]" class="control-label">Fotos </label>
                          <input type="file" class="form-control" id="image[]" name="image[]" multiple>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Agregar">
                        <a href="{{route('employees.edit', $employee_id)}}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Fotos
                </div>

                <div class="panel-body">
                  <div class="row">
                    @foreach($issue->images as $image)
                        @if(!$image->isMp3())
                          <div class="col-md-4">
                            <div class="thumbnail">
                              <img src="{{$image->path}}" alt="">
                              <div class="caption">
                                <form action="{{route('destroy.images', $image->id)}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="col-md-6">
                            <div class="thumbnail">
                              <audio controls>
                                <source src="{{$image->path}}" type="audio/mp3">
                                Tu navegador no soporta audio.
                              </audio>
                              <div class="caption">
                                <form action="{{route('destroy.images', $image->id)}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                              </div>
                            </div>
                          </div>
                        @endif
                    @endforeach
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
