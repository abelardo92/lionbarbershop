<div class="panel panel-default">
    <div class="panel-heading">
        Incapacidades 
        @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'horarios', 'employee', 'cashier-admin'))
            <a class="header-link" href="{{route('employees.disabilities.create', $employee->id)}}">Crear nueva incapacidad</a>
        @endif
    </div>

    <div class="panel-body">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Fecha inicial</th>
                    <th>Fecha final</th>
                    <th>Evidencia</th>
                    <th>Creado por</th>
                    <th>F. creación</th>
                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'horarios'))
                    <th>Opciones</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($employee->disabilities()->get() as $disability)
                    <tr>
                        <td>{{ $disability->start_date }}</td>
                        <td>{{ $disability->end_date }}</td>
                        <td>
                        @if($image = $disability->image()->get()->first())
                            <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                        @endif
                        </td>
                        <td>{{ $disability->user()->get()->first()->name }}</td>
                        <td>{{ $disability->created_at }}</td> 
                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'horarios'))
                        <td class="form-group" width="120px">
                            <a href="{{route('employees.disabilities.edit', [$employee->id, $disability->id])}}" class="btn btn-link col-xs-2">Editar</a>
                            <form action="{{route('employees.disabilities.destroy', [$employee->id, $disability->id])}}" method="post" class="col-xs-2 col-xs-offset-1">
                                {{{csrf_field()}}}
                                {{{method_field('DELETE')}}}
                                <input type="submit" class="btn btn-link" value="Eliminar">
                            </form>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
    </div>
</div>
