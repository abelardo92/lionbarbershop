<div class="panel panel-default">
    <div class="panel-heading">
        Usuario
        <p class="pull-right help-block">Campos con * son obligatorios.</p>
    </div>

    <div class="panel-body">
        @if(!$employee->user)
            <form action="{{route('users.store')}}" method="POST" class="form-horizontal">
                {{{ csrf_field() }}}

                <input type="hidden" name="name" value="{{ $employee->name }}">
                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                <input type="hidden" name="role_id" value="{{ $role->name }}">
                <input type="hidden" name="from_employee" value="1">

                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">Email *</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="email" name="email" required="required" value="{{ old('email', $employee->email) }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-4 control-label">Contraseña *</label>
                    <div class="col-sm-6">
                      <input type="password" class="form-control" id="password" name="password" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-6">
                        <button type="submit" class="btn btn-default">Guardar</button>
                    </div>
                </div>

            </form>
        @else
            <form action="{{route('users.update', $employee->user->id)}}" method="POST" class="form-horizontal">
                {{{ csrf_field() }}}
                {{{ method_field('put') }}}

                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                <input type="hidden" name="from_employee" value="1">
                <div class="form-group">
                    <label for="email" class="col-sm-4 text-right">Email</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="email" name="email" required="required" value="{{ old('email', $employee->user->email) }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-4 text-right">Contraseña</label>
                    <div class="col-sm-6">
                      <input type="password" class="form-control" id="password" name="password" >
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-6">
                        <button type="submit" class="btn btn-default">actualizar</button>
                    </div>
                </div>
            </form>
        @endif
        <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
    </div>
</div>