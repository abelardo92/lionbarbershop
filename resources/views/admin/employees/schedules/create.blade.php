@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Horario del empleado: <strong>{{$employee->name}}</strong>

                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('employees.schedules.store', $employee_id) }}" method="post" autocomplete="off">
                      {{{ csrf_field() }}}

                      <div class="form-group">
                        <label for="day" class="control-label">Dia *</label>
                        <div class="">
                          <select class="form-control" id="day" name="day" required="required">
                            <option value="">Seleccione uno por favor...</option>
                            <option value="Sunday">Domingo</option>
                            <option value="Monday">Lunes</option>
                            <option value="Tuesday">Martes</option>
                            <option value="Wednesday">Miercoles</option>
                            <option value="Thursday">Jueves</option>
                            <option value="Friday">Viernes</option>
                            <option value="Saturday">Sabado</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="subsidiary_id" class="control-label">Sucursal *</label>
                        <div class="">
                          <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required" v-model="subsidiary_id">
                              <option value="">Seleccione uno por favor...</option>
                              @foreach($subsidiaries as $subsidiary)
                                <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="turn_id" class="control-label">Turno *</label>
                        <div class="">
                          <select class="form-control" id="turn_id" name="turn_id" required="required">
                            <option value="">Seleccione uno por favor...</option>
                            @foreach($turns as $turn)
                              <option value="{{$turn->id}}">{{$turn->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="" class="control-label">Semana del *</label>
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control" name="start" value="">
                            <span class="input-group-addon">al</span>
                            <input type="text" class="form-control" name="end" value="">
                        </div>
                      </div>

                      <div class="form-group">
                              <label for="has_rest" class="control-label">¿Tiene hora de descanso? *</label>
                              <select class="form-control" id="has_rest" name="has_rest" required="required">
                                  <option selected="selected" value="0">No</option>
                                  <option value="1">Si</option>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="rest_minutes" class="control-label">Tiempo en minutos</label>
                              <input type="text" class="form-control" id="rest_minutes" name="rest_minutes" value="{{ old('rest_minutes') }}">
                          </div>

                      <div class="form-group">
                        <label for="extra_day" class="control-label">Dia extra*</label>
                        <div class="">
                          <select class="form-control" id="extra_day" name="extra_day" required="required">
                            <option value="1">Si</option>
                            <option value="0" selected="selected">No</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Asignar">
                         <a href="{{route('employees.edit', $employee_id)}}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
