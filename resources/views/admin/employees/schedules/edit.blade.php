@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Horario del empleado: <strong>{{$employee->name}}</strong>
                        <p class="pull-right help-block">Campos con * son obligatorios.</p>
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('employees.schedules.update', [$employee_id, $schedule->id ]) }}" method="post" autocomplete="off">
                          {{{ csrf_field() }}}
                          {{{ method_field('PUT') }}}

                          @if(!Auth::user()->isA('supervisor'))
                          <div class="form-group">
                              <label for="date" class="control-label">Fecha</label>
                              <input type="date" class="form-control" id="date" name="date" value="{{ old('date', $schedule->date) }}">
                          </div>

                          <div class="form-group">
                            <label for="day" class="control-label">Dia *</label>
                            <div class="">
                              <select class="form-control" id="day" name="day" required="required">
                                <option value="">Seleccione uno por favor...</option>
                                <option @if($schedule->day == 'Sunday') selected="selected" @endif value="Sunday">Domingo</option>
                                <option @if($schedule->day == 'Monday') selected="selected" @endif value="Monday">Lunes</option>
                                <option @if($schedule->day == 'Tuesday') selected="selected" @endif value="Tuesday">Martes</option>
                                <option @if($schedule->day == 'Wednesday') selected="selected" @endif value="Wednesday">Miercoles</option>
                                <option @if($schedule->day == 'Thursday') selected="selected" @endif value="Thursday">Jueves</option>
                                <option @if($schedule->day == 'Friday') selected="selected" @endif value="Friday">Viernes</option>
                                <option @if($schedule->day == 'Saturday') selected="selected" @endif value="Saturday">Sabado</option>
                              </select>
                            </div>
                          </div>
                          @endif
                          <div class="form-group">
                            <label for="subsidiary_id" class="control-label">Sucursal *</label>
                            <div class="">
                              <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required" v-model="subsidiary_id">
                                  <option value="">Seleccione uno por favor...</option>
                                  @foreach($subsidiaries as $subsidiary)
                                    <option @if($schedule->subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                          @if(!Auth::user()->isA('supervisor'))
                          <div class="form-group">
                            <label for="turn_id" class="control-label">Turno *</label>
                            <div class="">
                              <select class="form-control" id="turn_id" name="turn_id" required="required">
                                <option value="">Seleccione uno por favor...</option>
                                @foreach($turns as $turn)
                                  <option @if($schedule->turn_id == $turn->id) selected="selected" @endif value="{{$turn->id}}" >{{$turn->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                              <label for="has_rest" class="control-label">¿Tiene hora de descanso? *</label>
                              <select class="form-control" id="has_rest" name="has_rest" required="required">
                                  <option @if(!$schedule->has_rest) selected="selected" @endif value="0">No</option>
                                  <option @if($schedule->has_rest) selected="selected" @endif value="1">Si</option>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="rest_minutes" class="control-label">Tiempo en minutos</label>
                              <input type="text" class="form-control" id="rest_minutes" name="rest_minutes" value="{{ old('rest_minutes', $schedule->rest_minutes) }}">
                          </div>

                          <div class="form-group">
                            <label for="extra_day" class="control-label">Dia extra*</label>
                            <div class="">
                              <select class="form-control" id="extra_day" name="extra_day" required="required">
                                <option value="1" @if($schedule->extra_day) selected="selected" @endif>Si</option>
                                <option value="0" @if(!$schedule->extra_day) selected="selected" @endif>No</option>
                              </select>
                            </div>
                          </div>
                          @endif
                          <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Asignar">
                            <a href="{{route('employees.edit', $employee_id)}}" class="btn btn-danger">Cancelar</a>
                            <a href="{{route('employees.schedules.create', $employee_id)}}" class="btn btn-default">Crear otro</a>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
