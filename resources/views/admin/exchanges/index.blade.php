@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitudes de cambio de horario
                </div>

            <div class="panel-body">
                <table class="table table-striped datatables">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Solicita</th>
                            <th>Horario</th>
                            <th>Intercambia con</th>
                            <th>Horario</th>
                            <th>Auth empleado</th>
                            <th>Auth Admin</th>
                            <th>F. Creación</th>
                            <th>Opciones</th>            
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($exchanges as $exchange)
                        <tr>
                            <td>{{ $exchange->date }}</td>
                            <td>{{ $exchange->employee->short_name }}</td>
                            <td>
                                @if($schedule1 = $exchange->schedule1())
                                    {{ $schedule1->turn->name }}
                                @else
                                    Sin definir
                                @endif
                            </td>
                            <td>{{ $exchange->employee2->short_name }}</td>
                            <td>
                                @if($schedule2 = $exchange->schedule2())
                                    {{ $schedule2->turn->name }}
                                @else
                                    Sin definir
                                @endif
                            </td>
                            <td>
                                @if($exchange->employee2_authorized !== null) 
                                    {{ $exchange->employee2_authorized ? 'Si' : 'No' }}
                                @else
                                    Pendiente
                                @endif
                            </td>
                            <td>
                                @if($exchange->admin_authorized !== null)
                                    {{ $exchange->admin_authorized ? 'Si' : 'No' }}
                                @else
                                    Pendiente
                                @endif
                            </td>
                            <td>{{ $exchange->created_at }}</td>
                            <td>
                                @if($exchange->employee2_authorized && $exchange->admin_authorized === null)
                                    <form action="{{route('employees.exchanges.authorizeadmin', $exchange->id)}}" method="post">
                                    {{{csrf_field()}}}
                                        <input type="submit" class="btn btn-link" value="Autorizar">
                                    </form>
                                    <form action="{{route('employees.exchanges.unauthorizeadmin', $exchange->id)}}" method="post">
                                    {{{csrf_field()}}}
                                        <input type="submit" class="btn btn-link" value="Rechazar">
                                    </form>     
                                @endif
                            </td> 
                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
