@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Erogaciones
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('expenditures.store')}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Fecha</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="date" name="date" value="{{ old('date') }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal*</label>
                            <div class="col-sm-6">
                                <select name="subsidiary_id" id="subsidiary_id" class="form-control" required="required">
                                    @foreach(App\Subsidiary::all() as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account_id" class="col-sm-4 control-label">Cuenta*</label>
                            <div class="col-sm-6">
                                <select name="account_id" id="account_id" class="form-control" required="required">
                                    @foreach(App\Account::all() as $account)
                                        <option value="{{$account->id}}">{{$account->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-4 control-label">Concepto*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="description" name="description" required="required" value="{{ old('description') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="total" class="col-sm-4 control-label">Importe*</label>
                            <div class="col-sm-6">
                              <input type="number" min="0" step="any" class="form-control" id="total" name="total" required="required" value="{{ old('total') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-4 control-label">Archivo</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="image" name="image">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
