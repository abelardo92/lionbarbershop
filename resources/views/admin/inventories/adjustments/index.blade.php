<div class="panel panel-default">
    <div class="panel-heading">Ajustes</div>

    <div class="panel-body">
        <table class="table datatables">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Producto</th>
                    <th>Existencia actual</th>
                    <th>Ajuste</th>
                    <th>Existencia real</th>
                    @if(!$subdomain)
                        <th>Sucursal</th>
                    @endif
                    <th>Usuario</th>
                    <th>Fecha</th>
                    <th>opcion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($adjustments as $adjustment)
                    <tr @if($adjustment->folio % 2 == 0) class="active" @endif>
                        <td>{{$adjustment->folio}}</td>
                        <td>{{$adjustment->product->name}}</td>
                        <td>{{$adjustment->current_existence}}</td>
                        <td>{{$adjustment->qty}}</td>
                        <td>{{$adjustment->real_existence}}</td>
                        @if(!$subdomain)
                            <td>{{$adjustment->subsidiary->name}}</td>
                        @endif
                        <td>{{$adjustment->user->name}}</td>
                        <td>{{$adjustment->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" onclick="window.open('/home/inventories/adjustments/{{$adjustment->folio}}/{{$adjustment->subsidiary_id}}', '_blank')" class="btn btn-link">reimprimir nota</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>