<div class="panel panel-default">
    <div class="panel-heading">Salidas</div>

    <div class="panel-body">
        <table class="table datatables">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>F. Venta</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    @if(!$subdomain)
                        <th>Sucursal</th>
                    @endif
                    <th>Concepto</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                    <th>reimprimir</th>
                </tr>
            </thead>
            <tbody>
                @foreach($departures as $departure)
                    <tr @if($departure->folio % 2 == 0) class="active" @endif>
                        <td>{{$departure->folio}}</td>
                        <td>
                            @if($departure->sale)
                                {{$departure->sale->subsidiary->key}} - {{$departure->sale->folio}}
                            @endif
                        </td>
                        <td>{{$departure->product->name}}</td>
                        <td>{{$departure->qty}}</td>
                        @if(!$subdomain)
                            <td>{{$departure->subsidiary->name}}</td>
                        @endif
                        <td>@if($departure->concept) {{$departure->concept->name}} @endif</td>
                        <td>{{$departure->user->name}}</td>
                        <td>{{$departure->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" onclick="window.open('/home/inventories/departures/{{$departure->folio}}/{{$departure->subsidiary_id}}', '_blank')" class="btn btn-link">salida</button>
                            @if($departure->sale)
                                |
                                <button type="button" onclick="window.open('{{route('sales.print', [$departure->subsidiary->key, $departure->sale->id])}}', '_blank')" class="btn btn-link">venta</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>