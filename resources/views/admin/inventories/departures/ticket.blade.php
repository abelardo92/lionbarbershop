@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$model->subsidiary->name}}
@endsection

@section('content')
    <p><strong>Folio:</strong> {{$model->folio}}</p>
    <p><strong>Concepto:</strong> {{$model->concept->name}}</p>
    <p><strong>Fecha:</strong> {{$model->created_at->format('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{$model->created_at->format('H:i a') }}</p>
    <p><strong>Realizo:</strong> {{$model->user->name}}</p>

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th>Producto</th>
            <th>Cantidad</th>
        </tr>
        @foreach($models as $departure)
            <tr>
                <td align="center">{{$departure->product->key}}</td>
                <td align="center">{{$departure->product->name}}</td>
                <td align="center">{{$departure->qty}}</td>
            </tr>
        @endforeach
    </table>

    <div class="sign text-area">
        @if($model->employee)
            <p>{{$model->employee->name}}</p>
        @else
            <p>Firma</p>
        @endif
    </div>
@endsection