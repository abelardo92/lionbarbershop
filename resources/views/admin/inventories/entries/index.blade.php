<div class="panel panel-default">
    <div class="panel-heading">Entradas</div>

    <div class="panel-body">
        <table class="table datatables">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    @if(!$subdomain)
                        <th>Sucursal</th>
                    @endif
                    <th>Usuario</th>
                    <th>Fecha</th>
                    <th>opcion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($entries as $entry)
                    <tr @if($entry->folio % 2 == 0) class="active" @endif>
                        <td>{{$entry->folio}}</td>
                        <td>{{$entry->product->name}}</td>
                        <td>{{$entry->qty}}</td>
                        @if(!$subdomain)
                            <td>{{$entry->subsidiary->name}}</td>
                        @endif
                        <td>{{$entry->user->name}}</td>
                        <td>{{$entry->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" onclick="window.open('/home/inventories/entries/{{$entry->folio}}/{{$entry->subsidiary_id}}', '_blank')" class="btn btn-link">reimprimir nota</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>