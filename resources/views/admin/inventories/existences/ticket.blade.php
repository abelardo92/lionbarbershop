@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$subsidiary->name}}
@endsection

@section('content')
    <p><strong>Fecha:</strong> {{date('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{date('H:i a') }}</p>

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th>Producto</th>
            <th>Cantidad</th>
        </tr>
        @foreach($products as $product)
            <tr>
                <td align="center">{{$product->key}}</td>
                <td align="center">{{$product->name}}</td>
                <td align="center">{{$product->pivot->existence}}</td>
            </tr>
        @endforeach
    </table>

    <div>
        <p><strong>Entrego:</strong> {{App\Employee::find(session()->get('employee_id'))->name}}</p>
        <p><strong>Recibio:</strong> _________________________________________</p>
    </div>
@endsection