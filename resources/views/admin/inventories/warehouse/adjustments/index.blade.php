@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Ajustes de almacen</div>

                <div class="panel-body">
                    
                    <form method="GET" action="{{ url('home/inventories/warehouse/adjustments') }}">
                        <div class="pull-right">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="filter" placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                    
                    <table class="table striped">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Producto</th>
                    <th>Existencia actual</th>
                    <th>Ajuste</th>
                    <th>Existencia real</th>
                    <th>Usuario</th>
                    <th>Fecha</th>
                    <th>opcion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($adjustments as $adjustment)
                    <tr @if($adjustment->folio % 2 == 0) class="active" @endif>
                        <td>{{$adjustment->folio}}</td>
                        <td>{{$adjustment->product->name}}</td>
                        <td>{{$adjustment->current_existence}}</td>
                        <td>{{$adjustment->qty}}</td>
                        <td>{{$adjustment->real_existence}}</td>
                        <td>{{$adjustment->user->name}}</td>
                        <td>{{$adjustment->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" onclick="window.open('/home/inventories/warehouse/adjustments/{{$adjustment->folio}}/print', '_blank')" class="btn btn-link">reimprimir nota</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if(isset($filter))
            {{ $adjustments->appends(['filter' => $filter])->links() }}
        @else
            {{ $adjustments->links() }}
        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection