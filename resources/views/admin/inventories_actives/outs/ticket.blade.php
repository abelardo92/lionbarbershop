@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$types->first()->subsidiary->name}}
@endsection

@section('content')
    <p><strong>Folio:</strong> {{$types->first()->folio}}</p>
    <p><strong>Concepto:</strong> {{$types->first()->concepto}}</p>
    <p><strong>Fecha:</strong> {{$types->first()->created_at->format('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{$types->first()->created_at->format('H:i a') }}</p>
    {{-- <p><strong>Realizo:</strong> {{$types->first()->user->name}}</p> --}}

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th>Producto</th>
            <th>Cantidad</th>
        </tr>
        @foreach($types as $departure)
            <tr>
                <td align="center">{{$departure->article->key}}</td>
                <td align="center">{{$departure->article->name}}</td>
                <td align="center">{{$departure->qty}}</td>
            </tr>
        @endforeach
    </table>

    {{-- <div class="sign text-area">
        @if($types->first()->employee)
            <p>{{$types->first()->employee->name}}</p>
        @else
            <p>Firma</p>
        @endif
    </div> --}}
@endsection