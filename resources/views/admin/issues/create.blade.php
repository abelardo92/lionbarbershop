@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Escriba un mensaje
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('issues.messages.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}
                      @if(isset($related_issue))
                          <input type="hidden" id="related_issue_id" name="related_issue_id" value = "{{ $related_issue->id }}">
                      @endif
                      @if($user->isA('super-admin', 'supervisor', 'accountant'))
                          @if(isset($related_issue))
                            <input type="hidden" id="destiny_subsidiary_id" name="destiny_subsidiary_id" value = "{{ $related_issue->origin_subsidiary_id }}">
                          @else
                            <div class="form-group">
                                <label for="action" class="control-label">Enviar mensaje a: </label>
                                <select class="form-control" id="destiny_subsidiary_id" name="destiny_subsidiary_id" required="required">
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                          @endif
                      @endif

                      <div class="form-group">
                          <label for="action" class="control-label">Asunto </label>
                          <input type="text" class="form-control" id="subject" name="subject" required="required">
                      </div>

                      <div class="form-group">
                          <label for="action" class="control-label">Mensaje </label>
                          <textarea class="form-control" id="action" rows="10" name="action" ></textarea>
                      </div>

                      <div class="form-group">
                          <label for="image[]" class="control-label">Archivo *</label>
                          <input type="file" class="form-control" id="image[]" name="image[]" multiple>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Enviar">
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
