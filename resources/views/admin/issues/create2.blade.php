@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <p class="pull-right help-block">Campos con * son obligatorios.</p>
                <div class="panel-body">
                    <form action="{{ route('issues.store2') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}

                      <div class="form-group">
                        <label for="employee_id" class="control-label">Empleado *</label>
                        <div class="">
                          <select class="form-control" id="employee_id" name="employee_id" required="required">
                              <option value="">Seleccione uno por favor...</option>
                              @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                          <label for="area" class="control-label">Faltas incurridas *</label>
                          <textarea class="form-control" id="area" name="area" required="required"></textarea>
                      </div>

                      <div class="form-group">
                          <label for="date" class="control-label">Fecha *</label>
                          <input type="text" class="form-control datepicker" id="date" name="date" value="{{ $currentDate }}" required="required">
                      </div>

                      <div class="form-group">
                          <label for="action" class="control-label">Observaciones </label>
                          <textarea class="form-control" id="action" name="action" ></textarea>
                      </div>

                      <div class="form-group">
                          <label for="image[]" class="control-label">Fotos *</label>
                          <input type="file" class="form-control" id="image[]" name="image[]" multiple required="required">
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Agregar">
                        <a href="{{url('/home')}}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
