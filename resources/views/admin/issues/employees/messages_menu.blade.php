
<div class="panel panel-default">
    <div class="panel-heading">Menu</div>
    <div class="panel-body">
        <ul class="list-unstyled">
            <li>
                <a href="{{route('issues.messages.employees.sent')}}">Enviados</a>
            </li>
            <li>
                <a href="{{route('issues.messages.employees.received')}}">Recibidos</a>
            </li>
        </ul>
    </div>
</div>
        