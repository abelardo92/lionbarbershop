@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('admin.issues.employees.messages_menu')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Mensajes recibidos</div>

                    <div class="panel-body">
                        <a href="{{ route('issues.messages.employees.create') }}">Enviar mensaje</a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Enviado por</th>
                                    <th>Asunto</th>
                                    <th>Fecha / hora</th>
                                    <th>Archivo</th>
                                    @if($user->isA('super-admin'))
                                        <th>Visto por</th>
                                    @endif
                                    <th>Ver</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($issues as $issue)
                                    <tr>
                                        <td>
                                            @if($issue->originUser->employee != null)
                                                {{ $issue->originUser->employee->short_name }}
                                            @else
                                                {{ $issue->originUser->name }}
                                            @endif
                                        </td>
                                        <td>{{ $issue->subject }}</td>
                                        <td>{{ $issue->created_at }}</td>
                                        <td>
                                            @if($issue->images != null)
                                                @foreach($issue->images as $image)
                                                    <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                                @endforeach
                                            @endif
                                        </td>
                                        @if($user->isA('super-admin'))
                                            <td>
                                                @if($issue->openedBy != null)
                                                    {{ $issue->openedBy->name }}
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            <a class="btn btn-link" href="{{ route('issues.messages.employees.show', $issue->id) }}">Ver</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $issues->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection