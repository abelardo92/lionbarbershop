@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Agregar servicios de lavanderías anteriores
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <div class="panel-body">
                    <form action="{{route('laundry-services.store-previous',['subsidiary_id' => $subsidiary->id])}}" method="POST" class="form-horizontal" name="sale-form" id="sale-form">
                        {{{ csrf_field() }}}
                        
                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">
                                Sucursal (Cliente)
                            </label>
                            <div class="col-sm-6">
                                <select name="customer_subsidiary_id" id="customer_subsidiary_id" class="select2 form-control">
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="customer_name" class="col-sm-4 control-label">Otro cliente (si aplica)</label>
                            <div class="col-sm-6">
                                <input type="text" name="customer_name" id="customer_name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">
                                Fecha
                            </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control datepicker" name="date" id="date" v-model="date" value="{{ $now }}">  
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">
                                Turno
                            </label>
                            <div class="col-sm-6">
                                <select name="turn_id" id="turn_id" class="select2 form-control">
                                    @foreach($turns as $turn)
                                        <option value="{{$turn->id}}">{{$turn->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-sm-4 control-label">Monto (MXN)</label>
                            <div class="col-sm-6">
                                <input type="number" step="any" name="amount" id="amount" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });
    </script>
@endsection
