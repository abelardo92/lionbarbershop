@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nuevo servicio
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <div class="panel-body">
                    <form action="{{route('laundry-services.update',['id' => $laundry_service->id])}}" method="POST" class="form-horizontal" name="sale-form" id="sale-form">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">
                                Sucursal
                            </label>
                            <div class="col-sm-6">
                                <select name="customer_subsidiary_id" id="customer_subsidiary_id" class="select2 form-control">
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}" @if($subsidiary->id == $laundry_service->customer_subsidiary_id) selected="selected" @endif>{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="comments-group">
                            <label for="comments" class="col-sm-4 control-label">Fecha</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control datepicker" name="date" id="date" v-model="date" value="{{ $laundry_service->date }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-sm-4 control-label">Monto (MXN)</label>
                            <div class="col-sm-6">
                                <input type="number" step="any" name="amount" id="amount" class="form-control" value="{{ $laundry_service->amount }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });
    </script>
@endsection
