@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Paquetes
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('packages.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                <div class="form-group">
                                    <label for="name" class="col-sm-2">Nombre *</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name') }}">
                                    </div>

                                    <label for="sell_price" class="col-sm-2">Precio de venta *</label>
                                    <div class="col-sm-4">
                                      <input type="number" min="0" step="any" class="form-control" id="price" name="price" required="required" value="{{ old('price') }}">
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="service_id" class="col-sm-2">Servicios</label>
                                    <div class="col-sm-3">
                                        <select name="service" id="service" class="select2 form-control">
                                        @foreach($services as $service)
                                            <option value="{{$service->id}}">{{$service->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="button" class="btn btn-success" id="addService" value="Añadir"/>
                                    </div>
                                
                                    <label for="service_id" class="col-sm-2">Productos</label>
                                    <div class="col-sm-3">
                                        <select name="product" id="product" class="select2 form-control">
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="button" class="btn btn-success" id="addProduct" value="Añadir"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <table class="table" id="servicesTable">
                                            <tbody>
                                                <tr>
                                                    <th>Nombre del servicio</th>
                                                    <th>Cantidad</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-6">
                                        <table class="table" id="productsTable">
                                            <tbody>
                                                <tr>
                                                    <th>Nombre del producto</th>
                                                    <th>Cantidad</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-3">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });
        $('#addProduct').on('click', function (evt) {
            if($("#product").val() != null){
                var value = $("#product").val();
                var name = $("#product option:selected").text();
                var idInput = "<input type='hidden' id='product_ids' name='product_ids[]' value='"+value+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeProduct("+value+")'>Borrar</button>";
                $("#productsTable > tbody").append("<tr id='product"+value+"'><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });
        $('#addService').on('click', function (evt) {
            if($("#service").val() != null){
                var value = $("#service").val();
                var name = $("#service option:selected").text();
                var idInput = "<input type='hidden' id='service_ids' name='service_ids[]' value='"+value+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='service_quantities' name='service_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeService("+value+")'>Borrar</button>";
                $("#servicesTable > tbody").append("<tr id='service"+value+"'><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#service option:selected').attr('disabled','disabled');
                $("#service").val("");
                $('#service').select2();
            }
        });

        function removeProduct(id){
            $('#product' + id).remove();
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }

        function removeService(id){
            $('#service' + id).remove();
            $('#service option[value="' + id + '"]').removeAttr('disabled');
            $('#service').select2();
        }
    </script>
@endsection
