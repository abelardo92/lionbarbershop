@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Paquetes</div>

                    <div class="panel-body">
                        <a href="{{ route('packages.create') }}">Agregar un paquete</a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Precio venta</th>
                                    <th>Producto(s)</th>
                                    <th>Servicio(s)</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($packages as $package)
                                    <tr>
                                        <td>{{ $package->name }}</td>
                                        <td>{{ $package->price }}</td>
                                        <td>
                                            @php
                                                foreach($package->packageProducts()->get() as $packageProduct){
                                                    echo $packageProduct->product()->get()->first()->name . " (".$packageProduct->quantity.")<br/>";
                                                }
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                foreach($package->packageServices()->get() as $packageService){
                                                    echo $packageService->service()->get()->first()->name . " (".$packageService->quantity.")<br/>";
                                                }
                                            @endphp
                                        </td>
                                        <td><a href="{{ route('packages.edit', $package->id) }}">Editar</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
