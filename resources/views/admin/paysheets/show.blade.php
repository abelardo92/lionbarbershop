@extends('layouts.app')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nomina {{$paysheets->first()->key}}
                    <p class="pull-right">
                        {{-- <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button> --}}
                    </p>
                </div>

                <div class="panel-body">

                    <div class="">
                        <form action="{{route('paysheets.update', 1)}} " method="post" onkeypress="return event.keyCode != 13;"  class="disable" data-disable="{{$status}}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <input type="hidden" name="start" value="{{$start}}">
                            <input type="hidden" name="end" value="{{$end}}">

                            <a href="/home/paysheets/{{$paysheets->first()->key}}/export/original" class="btn btn-success" target="_blank">Exportar original</a>
                            <a href="/home/paysheets/{{$paysheets->first()->key}}/export/nomina" class="btn btn-success" target="_blank">Exportar para nomina</a>
                            <a href="/home/paysheets/{{$paysheets->first()->key}}/export/banco" class="btn btn-success" target="_blank">Exportar para el banco</a>
                            <div class="pull-right">
                                @if($paysheets->first()->status == 'save')
                                    <a href="/home/paysheets/{{$paysheets->first()->key}}/archive" class="btn btn-warning">Archivar</a>
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                @endif
                            </div>
                            <br><br><br><br><br><br>
                            <div class="clearfix"></div>
                            <fieldset>
                                <table class="table table-striped table-scroll" style="font-size: 0.8em; position: fixed; margin-top: -69px; background: white;">
                                    <thead>
                                        <tr>
                                            <th>
                                                Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </th>
                                            <th>Total</th>
                                            <th colspan="2">Asistencia&nbsp;&nbsp;</th>
                                            <th colspan="2">Puntualidad&nbsp;&nbsp;</th>
                                            <th colspan="2">Productividad</th>
                                            <th colspan="2">Excelencia&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th colspan="2">Dia&nbsp;extra&nbsp;&nbsp;&nbsp;</th>
                                            <th>Otros<br>ingresos&nbsp;</th>
                                            <th>Reparaciones</th>
                                            <th>Propinas</th>
                                            <th>Comision<br>productos</th>
                                            <th>Total<br>ingresos</th>
                                            <th>Prestamo</th>
                                            <th colspan="2">Faltas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Uniforme</th>
                                            <th>Infonavit</th>
                                            <th>Otras</th>
                                            <th>Reparaciones</th>
                                            <th>Total deducciones</th>
                                            <th>Neto a pagar</th>
                                            <th>Depositado</th>
                                            <th>Diferencia</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-striped" style="font-size: 0.8em">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Total</th>
                                            <th colspan="2">Asistencia</th>
                                            <th colspan="2">Puntualidad</th>
                                            <th colspan="2">Productividad</th>
                                            <th colspan="2">Excelencia</th>
                                            <th colspan="2">Dia extra</th>
                                            <th>Otros ingresos</th>
                                            <th>Reparaciones</th>
                                            <th>Propinas</th>
                                            <th>Comision productos</th>
                                            <th>Total ingresos</th>
                                            <th>Prestamo</th>
                                            <th colspan="2">Faltas</th>
                                            <th>Uniforme</th>
                                            <th>Infonavit</th>
                                            <th>Otras</th>
                                            <th>Reparaciones</th>
                                            <th>Total deducciones</th>
                                            <th>Neto a pagar</th>
                                            <th>Depositado</th>
                                            <th>Diferencia</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $tabindex1 = 1;
                                            $tabindex2 = 2;
                                            $tabindex3 = 3;
                                            $tabindex4 = 4;
                                            $tabindex5 = 5;
                                        @endphp
                                        @foreach($paysheets as $paysheet)
                                            <?php $employee = $paysheet->employee;?>
                                            <input type="hidden" name="employee[{{$employee->id}}][repairs]" value="{{ number_format($paysheet->repairs, 2, '.', '') }}" id="repairs-{{$employee->id}}-hidden">
                                            <input type="hidden" name="employee[{{$employee->id}}][discount-repairs]" value="{{ number_format($paysheet->discount_repairs, 2, '.', '') }}" id="discount-repairs-{{$employee->id}}-hidden">
                                            <tr>
                                                <td>
                                                    @if($employee->job == 'Barbero')
                                                        <a href="{{url('/home/reports/weekly/paysheet/barber?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @else
                                                        <a href="{{url('/home/reports/weekly/paysheet/employee?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @endif
                                                    <input type="hidden" name="employee[{{$employee->id}}][id]" value="{{$employee->id}}">
                                                    <input type="hidden" name="employee[{{$employee->id}}][paysheet_id]" value="{{$paysheet->id}}">
                                                </td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total]" value="{{ number_format($paysheet->total, 2, '.', '') }}" id="total-{{$employee->id}}-hidden">
                                                <td> $ {{ number_format($paysheet->total, 2, '.', '') }}</td>
                                                <td id="asistencia-{{$employee->id}}-text">$ {{ number_format($paysheet->asistencia, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="attendance[{{$employee->id}}]" @if($paysheet->asistencia_pay) checked="checked" @endif data-employee-id="{{$employee->id}}" class="check-attendance"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][asistencia]" value="{{ number_format($paysheet->asistencia, 2, '.', '') }}" id="asistencia-{{$employee->id}}-hidden">

                                                <td id="puntualidad-{{$employee->id}}-text">$ {{ number_format($paysheet->puntualidad, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="appoint[{{$employee->id}}]" @if($paysheet->puntualidad_pay) checked="checked" @endif data-employee-id="{{$employee->id}}"  class="check-puntualidad"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][puntualidad]" value="{{ number_format($paysheet->puntualidad, 2, '.', '') }}" id="puntualidad-{{$employee->id}}-hidden">

                                                <td id="productividad-{{$employee->id}}-text"> $ {{ number_format($paysheet->productividad, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="productivity[{{$employee->id}}]" @if($paysheet->productividad_pay) checked="checked" @endif data-employee-id="{{$employee->id}}"  class="check-productividad"></td>
                                                <?php
                                                $porcen_productividad = 0.20;
                                                if($employee->job == 'Cajero') {
                                                    $porcen_productividad = 0.25;
                                                }
                                                ?>
                                                <input type="hidden" value="{{ number_format($porcen_productividad, 2, '.', '') }}" id="porcen-productividad-{{$employee->id}}-hidden">
                                                <input type="hidden" name="employee[{{$employee->id}}][productividad]" value="{{ number_format($paysheet->productividad, 2, '.', '') }}" id="productividad-{{$employee->id}}-hidden">

                                                <td id="excellence-{{$employee->id}}-text"> $ {{ number_format($paysheet->excellence, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][excellence]" value="{{ number_format($paysheet->excellence, 2, '.', '') }}" id="excellence-{{$employee->id}}-hidden" data-val="{{ number_format($paysheet->excellence, 2, '.', '') }}">
                                                <td>
                                                    <input type="checkbox" name="excellence[{{$employee->id}}]" @if($paysheet->excellence_pay) checked="checked" @endif class="check-excellence" data-val="{{ number_format($paysheet->excellence, 2, '.', '') }}" data-employee-id="{{$employee->id}}">
                                                </td>
                                                <input type="hidden" name="employee[{{$employee->id}}][excellence]" value="{{ number_format($paysheet->excellence, 2, '.', '') }}" data-val="{{ number_format($paysheet->excellence, 2, '.', '') }}" id="excellence-{{$employee->id}}-hidden">

                                                <td id="extra-{{$employee->id}}-text"> $ {{ number_format($paysheet->extra, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="extra[{{$employee->id}}]" @if($paysheet->extra_pay) checked="checked" @endif  class="check-extra" data-employee-id="{{$employee->id}}"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][extra]" value="{{number_format($paysheet->extra, 2, '.', '')}}" data-val="{{$paysheet->extra}}" id="extra-{{$employee->id}}-hidden" data-employee-id="{{$employee->id}}">

                                                <td>
                                                    <input type="text" name="employee[{{$employee->id}}][otros_ingresos]" value="{{ number_format($paysheet->otros_ingresos, 2, '.', '') }}" id="otros_ingresos-{{$employee->id}}-hidden" data-employee-id="{{$employee->id}}" class="otros_ingresos" style="width: 50px;" tabindex="{{$tabindex1}}">
                                                </td>
                                                <td id="repairs-{{$employee->id}}-text"> $ {{ number_format($paysheet->repairs, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][repairs]" value="{{ number_format($paysheet->repairs, 2, '.', '') }}" id="repairs-{{$employee->id}}-hidden">


                                                <td id="cardtips-{{$employee->id}}-text"> $ {{ number_format($paysheet->cardtips, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][cardtips]" value="{{ number_format($paysheet->cardtips ?? 0, 2, '.', '') }}" id="cardtips-{{$employee->id}}-hidden">

                                                <td id="products-{{$employee->id}}-text"> $ {{ number_format($paysheet->products, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][products]" value="{{ number_format($paysheet->products ?? 0, 2, '.', '') }}" id="products-{{$employee->id}}-hidden">


                                                <td id="total_ingresos-{{$employee->id}}-text"> $ {{ number_format($paysheet->total_ingreso, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total_ingresos]" value="{{ number_format($paysheet->total_ingreso, 2, '.', '') }}" id="total_ingresos-{{$employee->id}}-hidden">


                                                <td><input type="text" name="employee[{{$employee->id}}][prestamo]" style="width: 50px;" class="prestamo" data-employee-id="{{$employee->id}}" id="prestamo-{{$employee->id}}-hidden" value="{{number_format($paysheet->prestamo, 2, '.', '')}}" tabindex="{{$tabindex2}}"></td>

                                                <td  id="total_faltas-{{$employee->id}}-text"> $ {{ number_format($paysheet->faltas, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="total_faltas[{{$employee->id}}]" @if($paysheet->faltas_pay) checked="checked" @endif data-employee-id="{{$employee->id}}" class="check-faltas"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total_faltas]" value="{{number_format($paysheet->faltas, 2, '.', '')}}" data-val="{{ number_format($paysheet->faltas, 2, '.', '') }}" id="total_faltas-{{$employee->id}}-hidden">
                                                <td><input type="text" name="employee[{{$employee->id}}][uniforme]" style="width: 50px;" class="uniforme" data-employee-id="{{$employee->id}}" id="uniforme-{{$employee->id}}-hidden" value="{{number_format($paysheet->uniforme, 2, '.', '')}}" tabindex="{{$tabindex3}}"></td>
                                                <td> $ {{ number_format($paysheet->infonavit, 2, '.', '') }}</td>

                                                <input type="hidden" name="employee[{{$employee->id}}][monto_infonavit]" value="{{number_format($paysheet->infonavit, 2, '.', '')}}" id="monto_infonavit-{{$employee->id}}-hidden">
                                                <td><input type="text" name="employee[{{$employee->id}}][otras]" style="width: 50px;" class="otras" data-employee-id="{{$employee->id}}" id="otras-{{$employee->id}}-hidden" value="{{number_format($paysheet->otras, 2, '.', '')}}" tabindex="{{$tabindex4}}"></td>

                                                <td id="discount-repairs-{{$employee->id}}-text"> $ {{ number_format($paysheet->discount_repairs, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][discount-repairs]" value="{{ number_format($paysheet->discount_repairs, 2, '.', '') }}" id="discount-repairs-{{$employee->id}}-hidden">

                                                <td id="desctotal-{{$employee->id}}-text"> $ {{ number_format($paysheet->total_deducciones, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][desctotal]" value="{{number_format($paysheet->total_deducciones, 2, '.', '')}}" id="desctotal-{{$employee->id}}-hidden">
                                                <td id="neto-{{$employee->id}}-text"> $ {{ number_format($paysheet->neto, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][neto]" value="{{number_format($paysheet->neto, 2, '.', '') }}" id="neto-{{$employee->id}}-hidden">
                                                <td><input type="text" name="employee[{{$employee->id}}][depositado]"  style="width: 50px;" class="depositado" data-employee-id="{{$employee->id}}" id="depositado-{{$employee->id}}-hidden" value="{{number_format($paysheet->depositado, 2, '.', '')}}" tabindex="{{$tabindex5}}"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][diferencia]" id="diferencia-{{$employee->id}}-hidden" value="{{number_format($paysheet->diferencia, 2, '.', '')}}">
                                                <td id="diferencia-{{$employee->id}}-text"> $ {{number_format($paysheet->diferencia, 2, '.', '')}}
                                                </td>
                                                <td>
                                                    @if($employee->job == 'Barbero')
                                                        <a href="{{url('/home/reports/weekly/paysheet/barber?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @else
                                                        <a href="{{url('/home/reports/weekly/paysheet/employee?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                $tabindex1 += 5;
                                                $tabindex2 += 5;
                                                $tabindex3 += 5;
                                                $tabindex4 += 5;
                                                $tabindex5 += 5;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
