@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Escriba la descripción para finalizar el pendiente
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('pendings.update', $pending->id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}
                      
                      <div class="form-group">
                          <label for="subject" class="control-label">Asunto </label>
                          <p>{{ $pending->subject }}</p>
                      </div>

                      <div class="form-group">
                          <label for="description_finished" class="control-label">Descripción (Finalizado)</label>
                          <textarea class="form-control" id="description_finished" rows="10" name="description_finished" ></textarea>
                      </div>

                      <div class="form-group">
                          <label for="image[]" class="control-label">Archivo *</label>
                          <input type="file" class="form-control" id="image[]" name="image[]" multiple>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Enviar">
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection