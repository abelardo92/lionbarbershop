@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Pendientes enviados</div>

                    <div class="panel-body">
                        <a href="{{ route('pendings.create') }}">Crear pendiente</a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Nombre</th>
                                    <th>Asunto</th>
                                    <th>Estatus</th>
                                    <th>Archivo</th>
                                    <th>Ver</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pendings as $pending)
                                    <tr>
                                        <td>{{ $pending->date }}</td>
                                        <td>
                                            @if($pending->createdBy->employee != null)
                                                {{ $pending->createdBy->employee->short_name }}
                                            @else
                                                {{ $pending->createdBy->name }}
                                            @endif
                                        </td>
                                        <td>{{ $pending->subject }}</td>
                                        <td>{{ $status[$pending->status] }}</td>
                                        <td>
                                            @if($pending->images != null)
                                                @foreach($pending->images as $image)
                                                    <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-link" href="{{ route('pendings.show', $pending->id) }}">Ver</a>
                                            @if($pending->status == 1)  
                                                <a class="btn btn-link" href="{{ route('pendings.edit', $pending->id) }}">Finalizar</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $pendings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection