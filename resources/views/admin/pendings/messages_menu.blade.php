<div class="row">
    <div class="col-md-2">
        @if(isset($is_admin) && $is_admin)
            <a href="{{ route('pendings.admin.create') }}" class="btn btn-success">Crear pendiente</a>
            @else
            <a href="{{ route('pendings.create') }}" class="btn btn-success">Crear pendiente</a>
        @endif
    </div>
    <div class="col-md-5">
        @if(isset($is_admin) && $is_admin)
            <a href="{{route('pendings.admin.nonFinished')}}" class="btn btn-primary">Pendientes</a>
        @else
            <a href="{{route('pendings.nonFinished')}}" class="btn btn-primary">Pendientes</a>
        @endif
        &nbsp;
        @if(isset($is_admin) && $is_admin)
            <a href="{{route('pendings.admin.finished')}}" class="btn btn-primary">Terminados</a>
        @else
            <a href="{{route('pendings.finished')}}" class="btn btn-primary">Terminados</a>
        @endif
    </div>
</div>
