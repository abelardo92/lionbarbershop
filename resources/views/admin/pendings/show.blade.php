@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Información de mensaje:
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                
                <div class="panel-body">
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-3">
                                  <strong>Fecha:</strong> {{ $pending->created_at }}        
                              </div>
                              <div class="col-sm-3">
                                  <strong>Creado por:</strong> 
                                  @if($pending->createdBy != null)
                                      {{ $pending->createdBy->name }}     
                                  @endif  
                              </div>
                              <div class="col-sm-3">
                                  <strong>Sucursal:</strong> 
                                  @if($pending->subsidiary != null)
                                      {{ $pending->subsidiary->name }}   
                                  @endif     
                              </div>
                              @if($pending->finishedBy != null)
                              <div class="col-sm-3">
                                  <strong>Terminado por:</strong> 
                                  {{ $pending->finishedBy->name }}  
                                  @if($pending->finished_at != null)
                                    <br/>({{ $pending->finished_at }})
                                  @endif   
                              </div>
                              @endif
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Asunto:</strong>       
                              </div>
                              <div class="col-sm-10">
                                  {{ $pending->subject }}       
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Descripción (Pendiente):</strong>       
                              </div>
                              <div class="col-sm-10">
                                  {{ $pending->description_pending }}       
                              </div>
                          </div>
                      </div>
                      @if($pending->description_finished != "")
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Descripción (Finalizado):</strong>       
                              </div>
                              <div class="col-sm-10">
                                  {{ $pending->description_finished }}       
                              </div>
                          </div>
                      </div>
                      @endif

                      @if($pending->images != null && !$pending->images->isEmpty())
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Archivo(s):</strong>       
                              </div>
                              <div class="col-sm-10">
                                  @foreach($pending->images as $image)
                                      <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                  @endforeach      
                              </div>
                          </div>
                      </div>
                      @endif

                  <div class="form-group">
                      <div class="col-sm-offset-10 col-sm-2">
                          <button class="btn btn-primary" type="button" onclick="window.history.back()">Regresar</button>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
