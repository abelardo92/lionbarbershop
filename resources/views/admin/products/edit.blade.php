@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($product->image)
                        <img src="{{$product->image->path}}" alt="{{$product->image->name}}" class="img-square img-responsive">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('PUT') }}}

                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Codigo *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key', $product->key) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $product->name) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="image" class="col-sm-4 control-label">Foto</label>
                                    <div class="col-sm-6">
                                      <input type="file" class="form-control" id="image" name="image">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="stock_max" class="col-sm-4 control-label">Maximo por sucursal *</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control" id="stock_max" name="stock_max" required="required" value="{{ old('stock_max', $product->stock_max) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="stock_min" class="col-sm-4 control-label">Minimo por sucursal *</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control" id="stock_min" name="stock_min" required="required" value="{{ old('stock_min', $product->stock_min) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="existence" class="col-sm-4 control-label">Existencia</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control" id="existence" disabled="disabled" value="{{ old('existence', $product->existence) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="sell_price" class="col-sm-4 control-label">Precio de venta *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="sell_price" name="sell_price" required="required" value="{{ old('sell_price', $product->sell_price) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="buy_price" class="col-sm-4 control-label">Precio de compra *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="buy_price" name="buy_price"  disabled="disabled" value="{{ $product->buy_price }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="commission" class="col-sm-4 control-label">Comisión *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="commission" name="commission" required="required" value="{{ old('commission', $product->commission) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="has_commission" class="col-sm-4 control-label">Tiene comision *</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="has_commission" name="has_commission" required="required">
                                        <option value="1" @if($product->has_commission) selected="selected" @endif>Si</option>
                                        <option value="0" @if(!$product->has_commission) selected="selected" @endif>No</option>
                                      </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
