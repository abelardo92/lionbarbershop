@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Menu</div>

                <div class="panel-body">
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{route('products.index')}}">Productos</a>
                            <ul>
                                <li>
                                    <a href="{{route('products.create')}}">Crear productos</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{route('services.index')}}">Servicios</a>
                            <ul>
                                <li>
                                    <a href="{{route('services.create')}}">Crear servicio</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Productos</div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Precio venta</th>
                                <th>Precio de compra</th>
                                <th>Comisión</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->key }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->sell_price }}</td>
                                    <td>{{ $product->buy_price }}</td>
                                    <td>{{ $product->commission }}</td>
                                    <td>
                                        <a href="{{ route('products.edit', $product->id) }}" >Editar</a>
                                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                        |
                                            <a href="#" data-toggle="modal" data-target="#modal-delete-{{$product->id}}">
                                                Dar de baja
                                            </a>
                                            @include('admin.products.partials.delete_modal', compact('product'))
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
