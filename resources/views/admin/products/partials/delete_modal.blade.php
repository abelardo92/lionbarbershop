<div class="modal fade" id="modal-delete-{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-delete-{{$product->id}}Label">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style="width: 400px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-delete-{{$product->id}}Label">Baja del empleado</h4>
      </div>
      <form action="{{route('products.destroy', $product->id)}}" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
      <div class="modal-body">
            <label for="inactived_at">Desea eliminar el producto '{{ $product->name }}'?</label>
          <div class="form-group">
            
          </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-danger" value="Dar de baja">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
      </form>
    </div>
  </div>
</div>