@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas diario acumulado
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/accumulated-daily')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Efectivo</th>
                                <th>Tarjeta</th>
                                <th>Dolares (T.C)</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cash_total = 0;?>
                            <?php $card_total = 0;?>
                            <?php $usd_total = 0;?>
                            <?php $usd_mxn_total = 0;?>
                            <?php $total = 0;?>
                            @foreach($subsidiaries as $subsidiary)
                                <tr>
                                    <td>{{ $subsidiary->name }}</td>
                                    <?php
                                        $sales = $subsidiary->sales()->whereBetween('date', [$start, $end])
                                        ->finished()->notCanceled();
                                        if($subsidiary->is_laundry) {
                                            $sales = $sales->finishedOnSameCashRegister();
                                        }
                                        $sales = $sales->get();
                                        $sale_ids = $sales->pluck('id');
                                        $cash_tot = 0;

                                        if($subsidiary->id == 11) {
                                            $cash_tot = $subsidiary->originLaundryServices()->whereBetween('date', [$start, $end])->get()->sum('amount');
                                        } else {
                                            if($subsidiary->is_laundry) {
                                                $money_change = 0;
                                                $cash_registers = $subsidiary->cashRegisters()->with(['sales','subsidiary'])->byRange($start, $end)->get();

                                                foreach($cash_registers as $cash_register) {
                                                    $cash_tot += $cash_register->totalLaundry();
                                                }
                                            } else {
                                                $cash_tot = App\SalePayment::whereIn('sale_id', $sale_ids)->efectivo()->mxn()->sum('total') - $sales->sum('money_change');
                                            }
                                        }
                                        $cash_total += $cash_tot;
                                    ?>
                                    <td>{{ number_format($cash_tot, 2, '.', ',') }}</td>
                                    @php
                                        $card_tot = 0;
                                        if($subsidiary->id != 11) {
                                            $card_tot = App\SalePayment::whereIn('sale_id', $sale_ids)->card()->sum('total');
                                            $card_total += $card_tot;
                                        }
                                    @endphp
                                    <td>{{ number_format($card_tot, 2, '.', ',') }}</td>
                                    @php
                                    $usd_tot = 0;
                                    $mxn_subtotal = 0;
                                   
                                    $sale_payments = App\SalePayment::whereIn('sale_id', $sale_ids)->efectivo()->usd()->get();
                                    if($subsidiary->id != 11){
                                        foreach($sale_payments as $sale_payment){
                                        $usd_tot += $sale_payment->total / $sale_payment->sale->exchangeRate->rate;
                                        $mxn_subtotal += $sale_payment->total;
                                        }
                                        $usd_mxn_total += $mxn_subtotal;
                                        $usd_total += $usd_tot;
                                    }
                                    @endphp
                                    <td>$ {{ number_format($usd_tot, 2, '.', ',') }} 
                                    @if($usd_tot != 0)
                                    ({{ number_format($mxn_subtotal, 2, '.', ',') }} MXN)
                                    @endif
                                    </td>
                                    <?php
$subtotal = $cash_tot + $card_tot + $mxn_subtotal;
$total += $subtotal;
?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Totales:</th>
                                <td>$ {{ number_format($cash_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($card_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($usd_total, 2, '.', ',') }} ({{ number_format($usd_mxn_total, 2, '.', ',') }} MXN)</td>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
