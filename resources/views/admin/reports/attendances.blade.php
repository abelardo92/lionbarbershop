@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Checadas
                    <p class="pull-right">
                        <button type="button" class="btn-default hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/attendances')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="hide-in-print">
                        <table class="table table-striped datatables">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Nombre corto</th>
                                    <th>Puesto</th>
                                    <th>Hora</th>
                                    <th>Tipo</th>
                                    @if($start == $today)
                                        <th>Opciones</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($attendances as $attendance)
                                    <tr>
                                        <td>{{ $attendance->subsidiary->name }}</td>
                                        <td>{{ $attendance->employee->short_name }}</td>
                                        <td>{{ $attendance->employee->job }}</td>
                                        <td>{{ $attendance->time }}</td>
                                        <td>{{ $attendance->type }}</td>
                                        @if($start == $today)
                                            <td>
                                                <form action="{{ route('delete_attendance', $attendance->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <input type="submit" class="btn btn-link" value="Eliminar">
                                                </form>

                                                @include('modal_time', compact('attendance'))
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="show-in-print">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Nombre corto</th>
                                    <th>Puesto</th>
                                    <th>Hora</th>
                                    <th>Tipo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($attendances as $attendance)
                                    <tr>
                                        <td>{{ $attendance->subsidiary->name }}</td>
                                        <td>{{ $attendance->employee->short_name }}</td>
                                        <td>{{ $attendance->employee->job }}</td>
                                        <td>{{ $attendance->time }}</td>
                                        <td>{{ $attendance->type }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
