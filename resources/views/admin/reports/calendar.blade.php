@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sucursales
                </div>

                <div class="panel-body">
                    <div class="form-group">
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control">
                                <option value="all">Todas</option>
                                @foreach($subsidiaries as $subsidiary)
                                    <option
                                        value="{{$subsidiary->id}}"
                                    >{{$subsidiary->name}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas global calendario
                </div>

                <div class="panel-body" id="calendar">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
