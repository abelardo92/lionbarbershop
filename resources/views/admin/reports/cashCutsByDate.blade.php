@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de movimientos de caja por fecha
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/cashCutsByDate')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <?php 
                        $count_total_general = 0;
                        $cash_register_total_general = 0;
                    ?>
                    @foreach($subsidiaries as $index => $subsidiary)
                    <label for="start">Sucursal: {{ $subsidiary->name }} </label>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Turno</th>
                                <th>Tipo</th>
                                <th>Cajera</th>
                                <th>No.</th>
                                <th>Importe</th>
                                <th>Hora</th>
                            </tr>
                        </thead>

                        <?php 
                            $cash_registers = $subsidiary->cashRegistersByDate($start)->with('movements', 'turn', 'employee')->get(); 
                            $cash_register_total = 0;
                            $count = 1;
                            $count_total = 0;
                        ?>
                        <tbody>
                        @foreach($cash_registers as $index => $cash_register)
                            @foreach($cash_register->movements->where('type', '!=', 'abrir') as $index => $movement)
                                <tr>
                                    <td>{{ $cash_register->turn->name}}</td>
                                    <td>{{ $movement->type }}</td>
                                    <td>{{ $cash_register->employee->name }}</td>
                                    <td>{{ $count }}</td>
                                    <td>{{ $movement->total }}</td>
                                    <td>{{ $movement->created_at }}</td>
                                    @php
                                        $cash_register_total += $movement->total;
                                        $count++;
                                        $count_total++;
                                    @endphp
                                </tr>
                            @endforeach
                            <?php $count = 1; ?>
                        @endforeach
                        <?php 
                            $count_total_general += $count_total;
                            $cash_register_total_general += $cash_register_total; 
                        ?>
                        <tr>
                            <th colspan="2" style="text-align:right">Total:</th>
                            <td>{{ $count_total }}</td>
                            <td>$ {{ number_format($cash_register_total, 2, '.', ',') }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    @endforeach
                    <table class="table">
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <th style="text-align:right">Total general:</th>
                                <td>{{ $count_total_general }}</td>
                                <td>$ {{ number_format($cash_register_total_general, 2, '.', ',') }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
