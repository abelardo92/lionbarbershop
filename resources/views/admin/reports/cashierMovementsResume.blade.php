@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Total de movimientos de caja por sucursal
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/cashierMovementsResume')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Fecha de: </label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control" name="start" value="{{$start}}">
                                        <span class="input-group-addon">al</span>
                                        <input type="text" class="form-control" name="end" value="{{$end}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary hide-in-print" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>    
                            </div>
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Total de movimiento de caja</th>
                            </tr>
                        </thead>

                        <?php 
                            $count_total = 0;
                        ?>
                        <tbody>
                        @foreach($subsidiaries as $index => $subsidiary)
                            <?php 
                                $cash_registers = $subsidiary->cashRegistersByDates($start, $end)->with('movements')->get(); 
                                $count = 0;
                                foreach ($cash_registers as $cash_register) {
                                    $count += $cash_register->movements->where('type', '!=', 'abrir')->count();
                                }
                            ?>
                            
                            @if($count > 0)
                                <tr>
                                    <td>{{ $subsidiary->name}}</td>
                                    <td>{{ $count }}</td>
                                    @php
                                        $count_total += $count;
                                    @endphp
                                </tr>
                            @endif

                        @endforeach
                        <tr>
                            <th style="text-align:right">Total:</th>
                            <td>{{ $count_total }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
