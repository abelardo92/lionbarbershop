@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de propinas
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/commissions/products')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Importe vendido</th>
                                <th>Comisión</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_productos = 0; ?>
                            <?php $total_comision = 0; ?>
                            @foreach($sales as $sales_group)
                                <?php $productos = 0; ?>
                                <?php $comision = 0; ?>
                                @if($sales_group->first()->employee)
                                    <?php
                                    $prods = App\SaleProduct::whereIn('sale_id', $sales_group->pluck('id'))->get();
                                   
                                    foreach ($prods->filter(function($item) {
                                            return $item->product->has_commission;
                                        })->groupBy('product_id') as $group) {
                                        foreach ($group as $sale_prod) {
                                            $productos += $group->count() * $sale_prod->price; 
                                        }
                                    }
                                    ?>
                                    <?php $comision = $productos * 0.10; ?>

                                    <?php $total_productos += $productos; ?>
                                    <?php $total_comision += $comision; ?>

                                    <tr>
                                        <td>{{$sales_group->first()->employee->name}}</td>
                                        <td>{{number_format($productos, 2, '.', ',')}}</td>
                                        <td>{{number_format($comision, 2, '.', ',')}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td>Totales: </td>
                                <td>{{number_format($total_productos, 2, '.', ',')}}</td>
                                <td>{{number_format($total_comision, 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
