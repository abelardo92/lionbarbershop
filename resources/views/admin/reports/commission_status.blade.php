@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Status por de comisiones
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/commission/status')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Empleado:</label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($employees as $employe)
                                    <option
                                        @if($employe->id == $employee->id) selected="selected" @endif
                                        value="{{$employe->id}}"
                                    >{{$employe->short_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="hide-in-print">
                        <table class="table table-striped datatables">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Fecha</th>
                                    <th>Folio</th>
                                    <th>Total</th>
                                    <th>Propinas</th>
                                    <th>Productos</th>
                                    <th>Cliente</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sales as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>{{ $sale->date }}</td>
                                        <td><a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                                {{ $sale->subsidiary->key }} - {{ $sale->folio }}</td>
                                            </a></td>
                                        <td>$ {{ $sale->subtotal }}</td>
                                        <td>$ {{ $sale->tip }}</td>
                                        <?php $productos = 0;?>

                                        @foreach ($sale->products->filter(function($item) {
                                                return $item->product->has_commission;
                                            })->groupBy('product_id') as $group)
                                            @foreach ($group as $sale_prod)
                                                <?php $productos += $group->count() * $sale_prod->price;?>
                                            @endforeach
                                        @endforeach
                                        <td>$ {{ $productos * 0.10 }}</td>
                                        <td>{{ $sale->customer->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
