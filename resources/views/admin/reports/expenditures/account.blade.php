@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Detalle de gastos por cuenta: <strong>{{ $account->name }}</strong>
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Concepto</th>
                                <th>Importe</th>
                                @if($user->isA('super-admin'))
                                    <th>Opciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalGlobal = 0;
                            @endphp
                            @foreach($expenditures as $expenditure)
                                @php
                                    $totalGlobal += $expenditure->total;
                                @endphp
                                <tr>
                                    <td>{{ $expenditure->date }}</td>
                                    <td>{{ $expenditure->subsidiary->name }}</td>
                                    <td>{{ $expenditure->description }}</td>
                                    <td>${{ number_format($expenditure->total, 2, '.', ',')}}</td>
                                    @if($user->isA('super-admin'))
                                        <td><a href="{{ route('expenditures.edit', $expenditure->id) }}">Editar</a></td>
                                    @endif
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3">Total</td>
                                <td>
                                    ${{ number_format($totalGlobal, 2, '.', ',')}}
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
