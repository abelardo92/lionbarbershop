@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Flujo
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/flow')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                @foreach($subsidiaries as $subsidiary)
                                    @php
                                        $totalsExpenditures[$subsidiary->key] = 0;
                                        $totalsSales[$subsidiary->key] = 0;
                                        $totalsTips[$subsidiary->key] = 0;
                                    @endphp
                                    <th>{{$subsidiary->name}}</th>
                                @endforeach
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalGlobal = 0;
                                $totalExpenditures = 0;
                                $totalSales = 0;
                                $totalTips = 0;
                                $totalDifference = 0;
                            @endphp
                            <tr>
                                <td>
                                    <a href="{{route('report.subsidiaries', [
                                        'start' => $start,
                                        'end' => $end,
                                    ])}}">
                                        Ingresos
                                    </a>
                                </td>
                                @foreach($subsidiaries as $subsidiary)
                                    <?php
                                        $total = 0;
                                        if($subsidiary->id != 11) {
                                            if($subsidiary->is_laundry) {
                                                $cash_registers = $subsidiary->cashRegisters()->with(['sales','subsidiary'])->byRange($start, $end)->get();
                                                foreach ($cash_registers as $cr) {
                                                    $total += $cr->totalLaundry();
                                                }
                                            } else {
                                                $sales = $subsidiary->sales()->finished()->notCanceled()
                                                ->dates([$start, $end])->get();
                                                $total += $sales->sum('subtotal');
                                            }
                                        } else {
                                            $total = $subsidiary->originLaundryServices()->whereBetween('date', [$start, $end])->get()->sum('amount');
                                        }
                                        
                                        $totalSales += $total;
                                        $totalsSales[$subsidiary->key] += $total;
                                    ?>
                                    <td>
                                        {{ number_format($total, 2, '.', ',')}}
                                    </td>
                                @endforeach
                                @php
                                    $totalGlobal += $totalSales;
                                @endphp
                                <td>{{ number_format($totalSales, 2, '.', ',')}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('monthly.expenditures', [
                                        'start' => $start,
                                        'end' => $end,
                                    ])}}">
                                        Egresos
                                    </a>
                                </td>
                                @foreach($subsidiaries as $subsidiary)
                                    @php
                                        $total = $subsidiary->expenditures()->dates([$start, $end])->sum('total');
                                        $totalExpenditures += $total;
                                        $totalsExpenditures[$subsidiary->key] += $total;
                                    @endphp
                                    <td>
                                        {{ number_format($total, 2, '.', ',')}}
                                    </td>
                                @endforeach
                                @php
                                    $totalGlobal += $totalExpenditures;
                                @endphp
                                <td>{{ number_format($totalExpenditures, 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Propinas</td>
                                @foreach($subsidiaries as $subsidiary)
                                    @php
                                        $total = $subsidiary->sales()->finished()->notCanceled()->dates([$start, $end])->sum('tip');
                                        $totalsTips[$subsidiary->key] += $total;
                                        $totalTips += $totalsTips[$subsidiary->key];
                                    @endphp
                                    <td>{{ number_format($totalsTips[$subsidiary->key], 2, '.', ',')}}</td>
                                @endforeach
                                <td>{{ number_format($totalTips, 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td>Diferencia</td>
                                @foreach($subsidiaries as $subsidiary)
                                    @php
                                        $totalDifference += $totalsSales[$subsidiary->key] - $totalsExpenditures[$subsidiary->key];
                                    @endphp
                                    <td>{{ number_format($totalsSales[$subsidiary->key] - $totalsExpenditures[$subsidiary->key], 2, '.', ',')}}</td>
                                @endforeach
                                <td>{{ number_format($totalDifference, 2, '.', ',')}}</td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
