@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ajustes de inventario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/inventories/adjustments')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2" required="required">
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="folio">Folio: </label>
                            <input type="number" class="form-control" id="folio" name="folio" value="{{$folio}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Producto</th>
                                <th>Existencia actual</th>
                                <th>Ajuste</th>
                                <th>Existencia real</th>
                                <th>Usuario</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($adjustments as $adjustment)
                                <tr @if($adjustment->folio % 2 == 0) class="active" @endif>
                                    <td>{{$adjustment->folio}}</td>
                                    <td>{{$adjustment->product->name}}</td>
                                    <td>{{$adjustment->current_existence}}</td>
                                    <td>{{$adjustment->qty}}</td>
                                    <td>{{$adjustment->real_existence}}</td>
                                    <td>{{$adjustment->user->name}}</td>
                                    <td>{{$adjustment->created_at->format('d-m-Y')}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Ningun ajuste con este folio.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
