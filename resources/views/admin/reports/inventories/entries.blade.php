@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas de inventario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/inventories/entries')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2" required="required">
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="folio">Folio: </label>
                            <input type="number" class="form-control" id="folio" name="folio" value="{{$folio}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Usuario</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($entries as $entry)
                                <tr @if($entry->folio % 2 == 0) class="active" @endif>
                                    <td>{{$entry->folio}}</td>
                                    <td>{{$entry->product->name}}</td>
                                    <td>{{$entry->qty}}</td>
                                    <td>{{$entry->user->name}}</td>
                                    <td>{{$entry->created_at->format('d-m-Y')}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Ninguna entrada con este folio.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
