@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas de inventario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                @foreach($subsidiaries as $subsidiary)
                                    <th>{{$subsidiary->name}}</th>
                                @endforeach
                                <th>Total</th>
                                <th>Precio venta</th>
                                <th>Neto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_neto = 0; ?>
                            @foreach($products as $product)
                                <?php $total = 0; ?>
                                <tr>                          
                                    <td>{{$product->name}}</td>
                                    
                                    @foreach($subsidiaries as $subsidiary)
                                    <td>
                                        @if($currentProduct = $subsidiary->products->find($product->id))
                                            <?php $total += $currentProduct->pivot->existence; ?>
                                            {{$currentProduct->pivot->existence}}
                                        @else
                                            0
                                        @endif
                                    </td>              
                                    @endforeach
                                    
                                    <td>{{$total}}</td>
                                    <td>$ {{$product->sell_price}}</td>
                                    <?php $total_neto += $product->sell_price * $total; ?>
                                    <td>$ {{$product->sell_price * $total}}</td>
                                    
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="{{$subsidiaries->count() + 3}}">Total</td>
                                    <td>$ {{$total_neto}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
