@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ultimos movimientos por producto
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i>
                        </button>
                    </p>
                </div>
                <div class="panel-body">
                    <form action="{{url('/home/reports/inventories/warehouse/movementsByProduct')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="product_id">Producto: </label>
                            <select name="product_id" id="product_id" class="form-control select2" required="required">
                                @foreach($products as $prod)
                                    <option 
                                        @if($prod->id == $product->id) selected="selected" @endif
                                        value="{{$prod->id}}"
                                    >{{ $prod->name }} ({{ $prod->code }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha / hora</th>
                                <th>Sucursal</th>
                                <th>Folio</th>
                                <th>Tipo</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($movements as $movement)
                            <?php 
                                $movement_product = $movement->products
                                ->where('warehouse_product_id', $product->id)->first(); 
                            ?>
                                <tr>
                                    <td>{{$movement->created_at}}</td>    
                                    <td>{{$movement->subsidiary->name}}</td>                      
                                    <td>{{$movement->folio}}</td>
                                    <td>{{$types[$movement->type]}}</td>
                                    <td>{{$movement_product->quantity}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha / hora</th>
                                <th>Folio</th>
                                <th>Existencia a la fecha</th>
                                <th>Existencia real a la fecha</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($adjustments as $adjustment)
                                <tr>    
                                    <td>{{$adjustment->created_at}}</td>   
                                    <td>{{$adjustment->folio}}</td>                      
                                    <td>{{$adjustment->current_existence}}</td>
                                    <td>{{$adjustment->real_existence}}</td>
                                    <td>{{$adjustment->qty}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
