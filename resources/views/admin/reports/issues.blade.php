@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Incidencias
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/issues')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha inicio: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <label for="end">Fecha final: </label>
                            <input type="text" name="end" id="end" class="form-control datepicker" value="{{$end}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="hide-in-print">
                        <table class="table table-striped datatables">
                            <thead>
                                <tr>
                                    <th>Faltas Incurridas</th>
                                    <th>Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($issues as $issue)
                                    <tr>
                                        <td>
                                            @if($issue->images->count() > 0)
                                                <a href="{{$issue->images->first()->path}}" target="_blank">
                                                    <img src="{{$issue->images->first()->path}}" width="100px">
                                                </a>
                                            @endif
                                            {{$issue->area}}
                                        </td>
                                        <td>{{$issue->action}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
