@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de clientes no frecuentes (ultima venta posterior a 1 mes)
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/notFrequentCustomers')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Fecha de: </label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control" name="start" value="{{$start}}">
                                        <span class="input-group-addon">al</span>
                                        <input type="text" class="form-control" name="end" value="{{$end}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary hide-in-print" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>    
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                                <th>Sucursal donde asistió</th>
                                <th>Barbero que lo atendió</th>
                                <th>Servicio(s) efectuados</th>
                                <th>Ver ticket</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                            <?php $sale = $customer->sales()->with(['subsidiary', 'employee'])->finished()->notCanceled()->where('date', '<=', $end)->orderBy('id', 'desc')->first();?>
                            @if($sale != null)
                                <tr>
                                    <td>
                                        <a href="{{route('reports.sales.byCustomer', ['customer_id' => $customer->id])}}" target="_blank">
                                            {{ $customer->name }}
                                        </a>
                                    </td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>
                                        @if($sale->subsidiary)
                                            {{ $sale->subsidiary->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($sale->employee != null)
                                            {{ $sale->employee->short_name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($sale->services != null)
                                            @foreach($sale->services as $service)
                                                {{ $service->service->name }},
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @if($sale->subsidiary != null && $sale != null)  
                                            <a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                                {{ $sale->subsidiary->key }}-{{ $sale->folio }}
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                    {{ $customers->appends(['start' => $start, 'end' => $end])->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
