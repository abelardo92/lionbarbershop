<a type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#modal-{{$objects}}-{{$employee->id}}">
  Agregar nuevo
</a>
<div class="modal fade" id="modal-{{$objects}}-{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-{{$objects}}-{{$employee->id}}Label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-{{$objects}}-{{$employee->id}}Label">Agregar {{$objects}}.</h4>
      </div>
      <div class="modal-body">
        <form action="{{route($objects.".store")}}" method="post">
          {{{ csrf_field() }}}
          <input type="hidden" id="employee_id" name="employee_id" value="{{$employee->id}}">
          <input type="hidden" id="start" name="start" value="{{ $dates[0] }}">
          <input type="hidden" id="end" name="end" value="{{ $dates[1] }}">

          <div class="form-group">
            <label for="text" class="control-label">Concepto</label>
            <input type="text" class="form-control" id="text" name="text" required="required">
          </div>

          <div class="form-group">
            <label for="amount" class="control-label">Importe</label>
            <input type="text" class="form-control" id="amount" name="amount" required="required">
          </div>

          <div class="form-group">
            <label for="date" class="control-label">Fecha</label>
            <input type="text" class="form-control datepicker" id="date" value="{{ $dates[0] }}" name="date" required="required">
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Guardar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>