<div class="panel panel-default">
    <div class="panel-heading">
        <b>{{ ucfirst($objects) }}</b>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Concepto</th>
                    <th>Importe</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employee->{$objects}()->dates($dates)->get() as $object)
                    <tr>
                        <td>{{ $object->text }}</td>
                        <td>$ {{ number_format($object->amount, 2, '.', ',') }}</td>
                    </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
</div>