@extends('layouts.tickets_internos')

@section('content')
<div style="margin-left: 3px;">
    <div class="panel panel-default">
        <p><b>{{$barber->name}}</b></p>
        <?php 
            $paysheet = \App\Paysheet::where('key', "{$start}-{$end}")->where('employee_id', $barber->id)->first();
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th>Día</th>
                    <th>Fecha</th>
                    <th>Importe</th>
                    <th>P. servicios</th>
                    <th>P. a aplicar</th>
                </tr>
            </thead>
    
            <tbody>
                    @php
                    $total = 0;
                    $salary_total = 0;
                    $garantizado_total = 0;
                    $faltas = [];
                    $no_asistencia = count($dates);
                    $no_puntualidad = count($dates);
                    $tips = $barber->weeklySalesRange($start,$end)->sum('tip');
                @endphp
                @php $date_days = ['Lun','Mar','Mie','Jue','Vie','Sab','Dom']; @endphp
                @foreach($dates as $date)
                    @php
                        $falta = true;
                        $atiempo = false;
                        $schedule = $barber->schedules()->where('date', $date)->first();
                        $descanso = $schedule? $schedule->turn->is_rest: false;
                        $rest_name = $descanso ? $schedule->turn->name : '';
                        $is_payed = $descanso ? $schedule->turn->is_payed : false;
                        $attendances = $barber->attendances()->date($date)->get();

                        $date_day_number = \Carbon\Carbon::parse($date)->format('N');
                        $date_day = $date_days[$date_day_number-1];
                    @endphp
                    @if($attendances->count())
                        <?php $falta = false; ?>
                        <?php $no_asistencia -= 1; ?>
                        <?php $has_punctuality = false; ?>
    
                        @if($schedule)
                            @php
                                $attendanceStart = \Carbon\Carbon::parse($attendances->first()->time);
                                $scheduleStart = \Carbon\Carbon::parse($schedule->turn->start);
                                if($schedule->employee->job != "Cajero") {
                                    $scheduleStart = \Carbon\Carbon::parse($schedule->turn->barber_start);
                                }
                            @endphp
                            @if ($attendanceStart <= $scheduleStart)
                                        
                                @if($attendances->count() >= 3)
                                    @php
                                        $start = \Carbon\Carbon::parse($attendances[1]->time);
                                        $end = \Carbon\Carbon::parse($attendances[2]->time);
                                        $start->second = 0;
                                        $end->second = 0;                                                
                                    @endphp
                                    @if($start->diffInMinutes($end) <= 60)
                                        <?php $no_puntualidad -= 1; ?>
                                        <?php $has_punctuality = true; ?>
                                    @endif
                                @else
                                    <?php $no_puntualidad -= 1; ?>
                                    <?php $has_punctuality = true; ?>
                                @endif
                            @endif
                        @endif
    
                    @endif
                    @if($descanso)
                        <?php $falta = true; ?>
                        <?php $no_asistencia -= 1; ?>
                        <?php $atiempo = true; ?>
                        <?php $no_puntualidad -= 1; ?>
                    @endif
                    
                    <tr @if($falta) class="danger" @endif>
                        <td>{{$date_day}}</td>
                        <td @if($falta && !$descanso) colspan="4" @endif>
                            {{$date}}
                            @if($descanso)
                                <small>{{$rest_name}}</small>
                            @else
                                @if($falta)
                                    <?php $faltas[] = $date; ?>
                                    <small>Falta</small>
                                @endif
                            @endif
                        </td>
                        @if($descanso)
                            <td>$ 0.00</td>
                            <td>$0.00</td>
                            <td>$0.00</td>
                        @endif
                        @if(!$falta)
                            <?php 
                                $salary_by_date = $barber->getSalaryByDate($date, $has_punctuality);
                                $salary_total += $salary_by_date;
                                $garantizado_total += $barber->salary;
                            ?>
                            <td> 
                                $ {{ number_format($barber->salary, 2, '.', ',') }}
                            </td>
                            <?php   
                            $sales = $barber->weeklySales($date);
                            $subtotal = 0;
                            $serviceTotal = $barber->getCommissionByDate($sales);
                            $subtotal += $serviceTotal;
                            $total += $subtotal;
                            ?>
    
                            <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                            
                            <td>
                                $ {{ number_format($salary_by_date, 2, '.', ',') }}
                            </td>
                        @endif
    
                    </tr>
                @endforeach
                
                <tr>
                    <td></td>
                    <td>Total:</td>
                    <td>$ {{ number_format($garantizado_total, 2, '.', ',') }}</td>
                    <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                    <td>$ {{ number_format($salary_total, 2, '.', ',') }}</td>
                </tr>
                <?php 
                        $big_total = $salary_total; 
                        //$total_salarios = $paysheet ? $paysheet->total : $salary_total;
                        $total_salarios = $paysheet ? $paysheet->total : $salary_total;
                    ?>
                    <tr>
                        <td colspan="4">Total salarios:</td>
                        <td>$ <span id="salarios-text">{{ number_format($total_salarios, 2, '.', ',') }}</span></td>
                    </tr>
            </tbody>
        </table>
    </div>
    <br>
    
    <br>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Servicio</th>
                <th>Cantidad</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php $sum = 0;?>
                @php
                    $saless = $barber->weeklySalesRange($start, $end)->get();
                    $sale_ids = $saless->pluck('id');
                    $services = App\SaleService::with('sale')->whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');

                    $kids_promotion_qty = 0;
                    foreach($saless as $sale) {
                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                            $kids_promotion_qty += 1;
                        }
                    } 
                @endphp

                @foreach($services as $service)
                    @if($service->first()->service_id == 1)
                        <?php $qty = $service->sum('qty') - $kids_promotion_qty; ?>
                    @else
                        <?php $qty = $service->sum('qty'); ?>
                    @endif
                    <?php 
                        $subtotal = 0;
                        foreach ($service as $serv) {
                            $subtotal += $serv->getCommission();
                        }
                    ?>
                    @if($qty > 0)
                    <tr>
                        <td>{{ $service->first()->service->name }}</td>
                        <td>{{ $qty }}</td>
                        <?php $sum += $subtotal;?>
                        <td>$ {{ $subtotal }}</td>
                    </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="2"></td>
                    <td>$ {{$sum}}</td>
                </tr>
        </tbody>
    </table>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/nomina-resumen.js') }}"></script>
@endsection