@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de nómina semanal por barbero 
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>
                <?php 
                    $paysheet = \App\Paysheet::where('key', "{$start}-{$end}")->where('employee_id', $barber->id)->first();
                ?>
                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/paysheet/barber')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option 
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            <a href="/home/reports/weekly/services/barbers?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}" class="btn btn-primary">Reporte de servicios</a>
                            <a href="{{ route('reports.weekly.services.barbers.print', [$barber->id, $start, $end]) }}" class="btn btn-default">Imprimir ticket</a>
                            @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                            <input type="button" class="btn btn-warning hide-in-print" onClick="archivePaysheet('{{ $paysheet->id }}')" value="Archivar">
                            <input type="button" class="btn btn-success hide-in-print" onClick="updatePaysheet('{{ $paysheet->id }}')" value="Guardar">
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="alert alert-danger" id="paysheet-error" hidden></div>
                            <div class="alert alert-success" id="paysheet-success" hidden></div>
                        </div>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Día garantizado</th>
                                <th>Pago de servicios</th>
                                <th>Pago a aplicar</th>
                            </tr>
                        </thead>

                        <tbody>
                             @php
                                $total = 0;
                                $salary_total = 0;
                                $garantizado_total = 0;
                                $faltas = [];
                                $no_asistencia = count($dates);
                                $no_puntualidad = count($dates);
                                $tips = $barber->weeklySalesRange($start,$end)->sum('tip');
                            @endphp
                            @foreach($dates as $date)
                                @php
                                    $falta = true;
                                    $atiempo = false;
                                    $schedule = $barber->schedules()->where('date', $date)->first();
                                    $descanso = $schedule? $schedule->turn->is_rest: false;
                                    $rest_name = $descanso ? $schedule->turn->name : '';
                                    $is_payed = $descanso ? $schedule->turn->is_payed : false;
                                    $attendances = $barber->attendances()->date($date)->get();
                                @endphp
                                @if($attendances->count())
                                    <?php $falta = false; ?>
                                    <?php $no_asistencia -= 1; ?>
                                    <?php $has_punctuality = false; ?>

                                    @if($schedule)
                                        @php
                                            $attendanceStart = \Carbon\Carbon::parse($attendances->first()->time);
                                            $scheduleStart = \Carbon\Carbon::parse($schedule->turn->start);
                                            if($schedule->employee->job != "Cajero") {
                                                $scheduleStart = \Carbon\Carbon::parse($schedule->turn->barber_start);
                                            }
                                        @endphp
                                        @if ($attendanceStart <= $scheduleStart)
                                                    
                                            @if($attendances->count() >= 3)
                                                @php
                                                    $start = \Carbon\Carbon::parse($attendances[1]->time);
                                                    $end = \Carbon\Carbon::parse($attendances[2]->time);
                                                    $start->second = 0;
                                                    $end->second = 0;                                                
                                                @endphp
                                                @if($start->diffInMinutes($end) <= 60)
                                                    <?php $no_puntualidad -= 1; ?>
                                                    <?php $has_punctuality = true; ?>
                                                @endif
                                            @else
                                                <?php $no_puntualidad -= 1; ?>
                                                <?php $has_punctuality = true; ?>
                                            @endif
                                        @endif
                                    @endif

                                @endif
                                @if($descanso)
                                    <?php $falta = true; ?>
                                    <?php $no_asistencia -= 1; ?>
                                    <?php $atiempo = true; ?>
                                    <?php $no_puntualidad -= 1; ?>
                                @endif
                                
                                <tr @if($falta) class="danger" @endif>
                                    <td @if($falta && !$descanso) colspan="4" @endif>
                                        {{$date}}
                                        @if($descanso)
                                            <small>{{$rest_name}}</small>
                                        @else
                                            @if($falta)
                                                <?php $faltas[] = $date; ?>
                                                <small>Falta</small>
                                            @endif
                                        @endif
                                    </td>
                                    @if($descanso)
                                        <td>
                                            $ 0.00
                                        </td>
                                        <td>
                                            $0.00
                                        </td>
                                        <td>
                                             $0.00
                                        </td>
                                    @endif
                                    @if(!$falta)
                                        <?php 
                                            $salary_by_date = $barber->getSalaryByDate($date, $has_punctuality);
                                            $salary_total += $salary_by_date;
                                            $garantizado_total += $barber->salary;
                                        ?>
                                        <td> 
                                            $ {{ number_format($barber->salary, 2, '.', ',') }}
                                        </td>
                                        <?php   
                                        $sales = $barber->weeklySales($date);
                                        $subtotal = 0;
                                        $serviceTotal = $barber->getCommissionByDate($sales);
                                        $subtotal += $serviceTotal;
                                        $total += $subtotal;
                                        ?>

                                        <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                        
                                        <td>
                                            $ {{ number_format($salary_by_date, 2, '.', ',') }}
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            
                            <tr>
                                <td>Total:</td>
                                <td>$ {{ number_format($garantizado_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($salary_total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--<form action="{{url('/home/reports/weekly/paysheet/barber')}}" method="post">-->

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php 
                                $big_total = $salary_total; 
                                //$total_salarios = $paysheet ? $paysheet->total : $salary_total;
                                $total_salarios = $paysheet ? $paysheet->total : $salary_total;
                            ?>
                            <tr>
                                <td colspan="3">Total salarios:</td>
                                <td>$ <span id="salarios-text">{{ number_format($total_salarios, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank">
                                        Bono por asistencia:
                                    </a>
                                </td>
                                <?php $ba_money = ($total_salarios * 10) / 100; ?>
                                <?php $ba = 0; ?>
                                <?php $pay_ba = false; ?>
                                @if($no_asistencia == 0)
                                    <?php $pay_ba = true; ?>
                                    <?php $ba = $ba_money; ?>
                                    <?php $big_total += $ba; ?> 
                                @endif
                                <?php $asistencia = $paysheet ? ($paysheet->asistencia > $ba ? $paysheet->asistencia : $ba) : $ba; ?>
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                        <input type="checkbox" class="check-attendance" id="check-attendance" @if($paysheet->asistencia_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="attendance" name="attendance" value="{{ $ba_money }}">
                                    $ <span id="attendance-text">{{ number_format($ba, 2, '.', ',') }}</span>
                                </td>
                            </tr>

                            <?php $bp_money = ($total_salarios * 10) / 100; ?>
                            <?php $bp = 0; ?>
                            <?php $pay_bp = false; ?>
                            @if($no_puntualidad == 0)
                                <?php $pay_bp = true; ?>
                                <?php $bp = $bp_money; ?>
                                <?php $big_total += $bp; ?> 
                            @endif
                            <?php $puntualidad = $paysheet ? ($paysheet->puntualidad > $bp ? $paysheet->puntualidad : $bp) : $bp; ?>
                            <tr>
                                <td colspan="2">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank">
                                        Bono por puntualidad:
                                    </a>
                                </td>
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                    <input type="checkbox" class="check-punctuality" id="check-punctuality" @if($paysheet->puntualidad_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="punctuality" name="punctuality" value="{{ $bp_money }}">
                                    $ <span id="punctuality-text">{{ number_format($puntualidad, 2, '.', ',') }}</span>
                                </td>
                            </tr>

                            <?php $bpr = ($total_salarios * 20) / 100; ?>
                            <?php $big_total += $bpr; ?>
                            <?php $productividad = $paysheet ? ($paysheet->productividad > $bpr ? $paysheet->productividad : $bpr) : $bpr; ?>
                            <tr>
                                <td colspan="2">
                                    <a 
                                        href="/home/reports/issues?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" >
                                        Bono por productividad:
                                    </a>
                                </td>
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                    <input type="checkbox" class="check-productivity" id="check-productivity" @if($paysheet->productividad_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="productivity" name="productivity" value="{{ $bpr }}">
                                    $ <span id="productivity-text">{{ number_format($productividad, 2, '.', ',') }}</span>
                                </td>
                            </tr>

                            <?php $bpe_money = ($salary_total * 40) / 100; ?>
                            <?php $bpe = 0; ?>
                            <?php $pay_bpe = false; ?>
                            @if($barber->has_excellence)
                                <?php $big_total += $bpe; ?> 
                                <?php $pay_bpe = true; ?>
                                <?php $bpe = $bpe_money; ?>
                            @endif
                            <?php $excelencia = $paysheet ? $paysheet->excellence : $bpe; ?>
                            <?php $big_total += $bpe; ?>
                            <tr>
                                <td colspan="2">Bono por excelencia:</td>
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                    <input type="checkbox" class="check-excellence" id="check-excellence" @if($paysheet->excellence_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="excellence" name="excellence" value="{{ $bpe_money }}">
                                    $ <span id="excellence-text">{{ number_format($excelencia, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Dia extra:</td>
                                @php
                                    $extra_day = 0;
                                    $eam = 100;
                                    $extra_count = $barber->schedules()->extraDay()->dates([$start, $end])->count();
                                    $extra_count += $barber->schedules()->TurnMixt()->dates([$start, $end])->count();
                                    $extra_day = (($extra_count - 1) <= 0 ? 0 : ($extra_count - 1)) * $eam;
                                    $big_total += $extra_day;
                                @endphp
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                    <input type="hidden" id="extraday" name="extraday" value="{{ $extra_day }}">
                                    <input type="checkbox" class="check-extraday" id="check-extraday" @if($paysheet->extra_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    $ <span id="extraday-text">{{ number_format($extra_day, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Propinas:</td>
                                <td>
                                    $ <span id="tips-text">{{ number_format($tips, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Comision por productos:</td>
                                <?php $comisionProductos = $paysheet ? $paysheet->products : 0 ?>
                                <td>
                                    $ <span id="commission-text">{{ number_format($comisionProductos, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php
                            $percepciones = $barber->percepciones()->dates([$start, $end])->sum('amount');
                            //$big_total += $paysheet ? ($paysheet->otros_ingresos > $big_total ? $paysheet->otros_ingresos : $percepciones) : $percepciones;
                            $big_total += $percepciones;
                            ?>
                            <tr>
                                <td colspan="3">Percepciones:</td>
                                <td>$ <span id="perceptions-text">{{ number_format($percepciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Gran total:</td>
                                <?php $granTotal = $total_salarios + $asistencia + $puntualidad + $productividad + $excelencia + $extra_day + $tips + $comisionProductos + $percepciones; ?>
                                <td>$ <span id="total-text">{{ number_format($granTotal, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $barber,
                'objects' => 'percepciones',
                'dates' => [$start, $end]
            ])

            <div class="panel panel-default">
                <div class="panel-heading">Descuentos</div>

                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php $desc_total = 0; ?>
                            <?php $prestamo = $paysheet ? $paysheet->prestamo : 0; ?>
                            <tr>
                                <td colspan="3">Prestamo:</td>
                                <td>
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                        <input type="number" name="prestamo" id="prestamo" value="{{ $prestamo }}" min="0" class="form-control">
                                    @else
                                        $ <span id="prestamo-text"> {{ number_format($prestamo, 2, '.', ',') }} </span>
                                    @endif
                                </td>
                            </tr>
                            <?php
                            $total_faltas = count($faltas) * $barber->salary;
                            $desc_total += $total_faltas;
                            $deducciones = $barber->deducciones()->dates([$start, $end])->get();
                            $deducciones = $deducciones->sum('amount');
                            $desc_total += $deducciones;
                            $faltas = $paysheet ? $paysheet->faltas : $total_faltas;
                            //$faltas = $total_faltas; 
                            ?>
                            <tr>
                                <td colspan="2">Faltas:</td>
                                <td style="align:left;"> 
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin','super-admin-restringido'))
                                    <input type="checkbox" class="check-faults" id="check-faults" @if($paysheet->extra_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="faults" name="faults" value="{{ $total_faltas }}">
                                    $ <span id="faults-text">{{ number_format($faltas, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $uniforme = $paysheet ? $paysheet->uniforme : 0; ?>
                            <tr>
                                <td colspan="3">Uniforme:</td>
                                <td>
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin','super-admin-restringido'))
                                        <input type="number" name="uniform" id="uniform" value="{{ $uniforme }}" min="0" class="form-control">
                                    @else
                                        $ <span id="uniform-text">{{ number_format($uniforme, 2, '.', ',') }}</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Infonavit:</td>
                                @php
                                $monto_infonavit = 0;
                                if ($barber->monto_infonavit) {
                                    $monto_infonavit = (float) $barber->monto_infonavit;
                                }
                                $desc_total += $monto_infonavit;
                                $infonavit = $paysheet ? $paysheet->infonavit : $monto_infonavit; 
                                $tipcomision = $tips * 0.03;
                                @endphp
                                <td>
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                        <input type="number" name="infonavit" id="infonavit" value="{{ $infonavit }}" min="0" class="form-control">
                                    @else
                                        $ <span id="infonavit-text">{{ number_format($infonavit, 2, '.', ',') }}</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Comisión bancaria de propinas (3%):</td>
                                <td>$ <span id="commissiontip-text">{{ number_format($tipcomision, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Deducciones:</td>
                                <td>$ <span id="deductions-text">{{ number_format($deducciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <?php $total_descuento = $prestamo + $faltas + $uniforme + $infonavit + $deducciones + $tipcomision; ?>
                            <tr>
                                <td colspan="3">Total:</td>
                                <td>$ <span id="total-deducciones-text">{{ number_format($total_descuento, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $barber,
                'objects' => 'deducciones',
                'dates' => [$start, $end]
            ])
            <?php
             $neto = $granTotal - $total_descuento;
             $depositado = $barber->deposits()->where('start', $start)->where('end', $end)->sum('amount');
             $diferencia = $neto - $depositado;
             ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="3">Neto a pagar:</td>
                                <td>$ <span id="neto-text">{{ number_format($neto, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Depositado:</td>
                                <td>
                                    @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin', 'super-admin-restringido'))
                                        <input type="number" name="depositado" id="depositado" value="{{ $depositado }}" min="0" class="form-control">
                                    @else
                                        $ {{ number_format($depositado, 2, '.', ',') }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Diferencia:</td>
                                <td>$ <span id="diferencia-text">{{ number_format($diferencia, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--</form>-->
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/nomina-resumen.js') }}"></script>
@endsection