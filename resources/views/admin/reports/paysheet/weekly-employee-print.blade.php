@extends('layouts.tickets_internos')

@section('content')
<div class="panel panel-default">
    <p><b>{{$employee->name}}</b></p>

    <div class="panel-body">

        <table class="table">
            <thead>
                <tr>
                    <th>Día</th>
                    <th>Fecha</th>
                    <th>Día garantizado</th>
                </tr>
            </thead>

            <tbody>
                @php
                    $paysheet = \App\Paysheet::where('key', "{$start}-{$end}")->where('employee_id', $employee->id)->first();
                    $total = 0;
                    $upper_total = 0;
                    $salary_total = 0;
                    $faltas = [];
                    $no_asistencias = count($dates);
                    $no_puntualidad = count($dates);
                    $date_days = ['Lun','Mar','Mie','Jue','Vie','Sab','Dom'];
                @endphp
                @foreach($dates as $date)
                    @php
                        $falta = true;
                        $atiempo = false;
                        $schedule = $employee->schedules()->where('date', $date)->first();
                        $descanso = $schedule ? $schedule->turn->is_rest : false;
                        $rest_name = $descanso ? $schedule->turn->name : '';
                        $is_payed = $descanso ? $schedule->turn->is_payed : false;
                        $attendance = $employee->attendances()->date($date)->get();
                        $date_day_number = \Carbon\Carbon::parse($date)->format('N');
                        $date_day = $date_days[$date_day_number-1];
                    @endphp
                    @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                        <?php $attendance = ['2']; ?>
                    @else
                        <?php $attendance = $employee->attendances()->date($date)->get(); ?>
                    @endif
                    @if(count($attendance))
                        <?php $falta = false; ?>
                        <?php $no_asistencias -= 1; ?>
                        <?php $salary_total += $employee->salary; ?>
                        @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                            <?php $atiempo = true; ?>
                            <?php $no_puntualidad -= 1; ?>
                        @elseif($schedule)
                            @if (startltqend($attendance->first()->time, $employee->schedules()->where('date', $date)->first()->turn->start))
                                <?php $atiempo = true;?>
                                <?php $no_puntualidad -= 1;?>
                                @php
                                    $attendances = $employee->attendances()->date($date)->get();
                                @endphp
                                @if($attendances->count() >= 3)
                                    @php
                                        $start1 = \Carbon\Carbon::parse($attendances[1]->time);
                                        $end1 = \Carbon\Carbon::parse($attendances[2]->time);
                                    @endphp
                                    @if($start1->diffInMinutes($end1) <= 60)
                                        <?php $no_puntualidad -= 1;?>
                                    @endif
                                @else
                                    <?php $no_puntualidad -= 1;?>
                                @endif
                            @endif
                        @endif
                    @endif
                    @if($descanso)
                        <?php $falta = true; ?>
                        <?php $no_asistencias -= 1; ?>
                        <?php $atiempo = true; ?>
                        <?php $no_puntualidad -= 1; ?>
                    @endif
                    <tr @if($falta) class="danger" @endif>
                        <td>{{$date_day}}</td>
                        <td @if($falta && !$descanso) colspan="4" @endif>
                            {{$date}}
                            @if($descanso)
                                <small>{{$rest_name}}</small>
                            @else
                                @if($falta)
                                    <?php $faltas[] = $date; ?>
                                    <small>Falta</small>
                                @endif
                            @endif
                        </td>
                        @if($descanso)
                            <td>
                                @if($is_payed)
                                    <?php 
                                        $employee_salary = $employee->salary;
                                        $salary_total += $employee_salary; 
                                        $upper_total += $employee_salary; 
                                    ?>
                                    $ {{$employee_salary}}
                                @else
                                    $0.00
                                @endif
                            </td>
                            <td>
                                $0.00
                            </td>
                            <td>
                                $0.00
                            </td>
                        @endif
                        @if(!$falta)
                            <?php 
                            $employee_salary = $schedule && $schedule->turn->isDouble() ? ($employee->salary * 2) : $employee->salary;
                            $upper_total += $employee_salary; 
                            ?>
                            <td>
                                $ {{ number_format($employee_salary, 2, '.', ',') }}
                            </td>
                        @endif

                    </tr>
                @endforeach
                @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                    <?php $upper_total = count($dates) * $employee->salary; ?>
                @endif
                <tr>
                    <td></td>
                    <td>Total:</td>
                    <td>$ {{ number_format($upper_total, 2, '.', ',') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection