@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de nomina semanal por empleado
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/paysheet/employee')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Empleado: <code>{{$employee->job}}</code></label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($employees as $employe)
                                    <option 
                                        @if($employe->id == $employee->id) selected="selected" @endif
                                        value="{{$employe->id}}"
                                    >{{$employe->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            <a href="{{ route('reports.weekly.services.employees.print', [$employee->id, $start, $end]) }}" 
                                class="btn btn-default" target="_blank">Imprimir ticket</a>
                        </div>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Día garantizado</th>
                            </tr>
                        </thead>

                        <tbody>
                             @php
                                $paysheet = \App\Paysheet::where('key', "{$start}-{$end}")->where('employee_id', $employee->id)->first();
                                $total = 0;
                                $upper_total = 0;
                                $salary_total = 0;
                                $faltas = [];
                                $no_asistencias = count($dates);
                                $no_puntualidad = count($dates);
                            @endphp
                            @foreach($dates as $date)
                                @php
                                    $falta = true;
                                    $atiempo = false;
                                    $schedule = $employee->schedules()->where('date', $date)->first();
                                    $descanso = $schedule ? $schedule->turn->is_rest : false;
                                    $rest_name = $descanso ? $schedule->turn->name : '';
                                    $is_payed = $descanso ? $schedule->turn->is_payed : false;
                                    $attendance = $employee->attendances()->date($date)->get();
                                @endphp
                                @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                    <?php $attendance = ['2']; ?>
                                @else
                                    <?php $attendance = $employee->attendances()->date($date)->get(); ?>
                                @endif
                                @if(count($attendance))
                                    <?php $falta = false; ?>
                                    <?php $no_asistencias -= 1; ?>
                                    <?php $salary_total += $employee->salary; ?>
                                    @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                        <?php $atiempo = true; ?>
                                        <?php $no_puntualidad -= 1; ?>
                                    @elseif($schedule)
                                        @if (startltqend($attendance->first()->time, $employee->schedules()->where('date', $date)->first()->turn->start))
                                            <?php $atiempo = true;?>
                                            <?php $no_puntualidad -= 1;?>
                                             @php
                                                $attendances = $employee->attendances()->date($date)->get();
                                            @endphp
                                            @if($attendances->count() >= 3)
                                                @php
                                                    $start1 = \Carbon\Carbon::parse($attendances[1]->time);
                                                    $end1 = \Carbon\Carbon::parse($attendances[2]->time);
                                                @endphp
                                                @if($start1->diffInMinutes($end1) <= 60)
                                                    <?php $no_puntualidad -= 1;?>
                                                @endif
                                            @else
                                                <?php $no_puntualidad -= 1;?>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                                @if($descanso)
                                    <?php $falta = true; ?>
                                    <?php $no_asistencias -= 1; ?>
                                    <?php $atiempo = true; ?>
                                    <?php $no_puntualidad -= 1; ?>
                                @endif
                                <tr @if($falta) class="danger" @endif>
                                    <td @if($falta && !$descanso) colspan="4" @endif>
                                        {{$date}}
                                        @if($descanso)
                                            <small>{{$rest_name}}</small>
                                        @else
                                            @if($falta)
                                                <?php $faltas[] = $date; ?>
                                                <small>Falta</small>
                                            @endif
                                        @endif
                                    </td>
                                    @if($descanso)
                                        <td>
                                            @if($is_payed)
                                            <?php $salary_total += $employee->salary; ?>
                                            <?php $upper_total += $employee->salary; ?>
                                            $ {{$employee->salary}}
                                            @else
                                                $0.00
                                            @endif
                                        </td>
                                        <td>
                                            $0.00
                                        </td>
                                        <td>
                                            $0.00
                                        </td>
                                    @endif
                                    @if(!$falta)
                                        <?php 
                                        $employee_salary = $schedule && $schedule->turn->isDouble() ? ($employee->salary * 2) : $employee->salary;
                                        $upper_total += $employee_salary; 
                                        ?>
                                        <td>
                                            $ {{ number_format($employee_salary, 2, '.', ',') }}
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                <?php $upper_total = count($dates) * $employee->salary; ?>
                            @endif
                            <tr>
                                <td>Total:</td>
                                <td>$ {{ number_format($upper_total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php $big_total = $upper_total; ?>
                            <tr>
                                <td colspan="3">Total salarios:</td>
                                <td>$ {{ number_format($paysheet ? $paysheet->total : $upper_total, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por asistencia:
                                    </a>
                                </td>
                                <?php $ba_money = ($upper_total * 10) / 100; ?>
                                <?php $ba = 0; ?>
                                <?php $pay_ba = false; ?>
                                @if($no_asistencias == 0)
                                    <?php $pay_ba = true; ?>
                                    <?php $ba = $ba_money; ?>
                                    <?php $big_total += $paysheet ? ($paysheet->asistencia) : $ba; ?> 
                                @endif
                                
                                <td>
                                    $ <span id="asistencia-text">{{ number_format($paysheet ? ($paysheet->asistencia) : $ba, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $bp_money = ($upper_total * 10) / 100; ?>
                            <?php $bp = 0; ?>
                            <?php $pay_bp = false; ?>
                            @if($no_puntualidad == 0)
                                <?php $pay_bp = true; ?>
                                <?php $bp = $bp_money; ?>
                                <?php $big_total += $paysheet ? ($paysheet->puntualidad) : $bp; ?> 
                            @endif
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por puntualidad:
                                    </a>
                                </td>
                                <td>
                                    $ <span id="puntualidad-text">{{ number_format($paysheet ? $paysheet->puntualidad : $bp, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $porcen_productividad = 20; ?>
                            @if($employee->job == 'Cajero')
                                <?php $porcen_productividad = 25; ?>
                            @endif
                            <?php $bpr = ($upper_total * $porcen_productividad) / 100; ?>
                            <?php $big_total += $paysheet ? ($paysheet->productividad) : $bpr; ?>
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/issues?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por productividad:
                                    </a>
                                </td>
                                <td>
                                    $ <span id="productividad-text">{{ number_format($paysheet ? ($paysheet->productividad) : $bpr, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Dia extra:</td>
                                @php
                                    $extra_day = 0;
                                    $eam = 200;
                                    $extra_count = $employee->schedules()
                                                ->extraDay()->dates([$start, $end])->count();
                                    $extra_count += $employee->schedules()
                                                ->TurnMixt()->dates([$start, $end])->count();
                                    $extra_day = (($extra_count - 1) <= 0 ? 0 : ($extra_count - 1)) * $eam;
                                    $big_total += $paysheet ? ($paysheet->extra > $extra_day ? $paysheet->extra : $extra_day) : $extra_day;
                                @endphp
                                <td> $ {{ number_format($paysheet ? ($paysheet->extra > $extra_day ? $paysheet->extra : $extra_day) : $extra_day, 2, '.', ',') }}</td>
                            </tr>
                            <?php
                            $percepciones = $employee->percepciones()->dates([$start, $end])->sum('amount');

                            //$big_total += $paysheet ? ($paysheet->otros_ingresos > $percepciones ? $paysheet->otros_ingresos : $percepciones) : $percepciones;
                            $big_total += $percepciones;
                            ?>
                            <tr>
                                <td colspan="3">Otros ingresos:</td>
                                <td>$ {{ number_format($percepciones, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Gran total:</td>
                                <td>$ <span id="total-text">{{ number_format($paysheet ? ($paysheet->total_ingresos > $big_total ? $paysheet->total_ingresos : $big_total) : $big_total, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $employee,
                'objects' => 'percepciones',
                'dates' => [$start, $end]
            ])

            <div class="panel panel-default">
                <div class="panel-heading">Descuentos</div>

                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php $desc_total = 0; ?>
                            <tr>
                                <td colspan="3">Prestamo:</td>
                                <td>
                                    $ {{ number_format($paysheet ? $paysheet->prestamo : 0, 2, '.', ',') }}
                                </td>
                            </tr>
                            <?php
                            $total_faltas = count($faltas) * 200;
                            $desc_total += $total_faltas;
                            $deducciones = $employee->deducciones()->dates([$start, $end])->sum('amount');
                            // $deducciones = $paysheet ? ($paysheet->otras > $deducciones ? $paysheet->otras : $deducciones) : $deducciones;
                            $desc_total += $deducciones;
                            ?>
                            <tr>
                                <td colspan="3">Faltas:</td>
                                <td>$ {{ number_format($paysheet ? $paysheet->faltas : $total_faltas, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Uniforme:</td>
                                <td>
                                    $ {{ number_format($paysheet ? $paysheet->uniforme : 0, 2, '.', ',') }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Infonavit:</td>
                                @php
                                $monto_infonavit = 0;
                                if ($employee->monto_infonavit) {
                                    $monto_infonavit = (float) $employee->monto_infonavit;
                                }
                                $desc_total += $monto_infonavit;
                                @endphp
                                <td>
                                    $ <input type="text" name="infonavit" id="infonavit" value="{{ number_format($paysheet ? $paysheet->infonavit : $monto_infonavit, 2, '.', ',') }}" form="paysheet">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Deducciones:</td>
                                <td>$ {{ number_format($paysheet ? ($paysheet->otras > $deducciones ? $paysheet->otras : $deducciones) : $deducciones, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Total:</td>
                                <td>$ <span id="total-deducciones-text">{{ number_format($paysheet ? ($paysheet->total_deducciones > $desc_total ? $paysheet->total_deducciones : $desc_total) : $desc_total, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $employee,
                'objects' => 'deducciones',
                'dates' => [$start, $end]
            ])

            <?php
             $neto = $big_total - $desc_total;
             $depositado = $employee->deposits()->where('start', $start)->where('end', $end)->sum('amount');
             $diferencia = $neto - $depositado;
             ?>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="3">Neto a pagar:</td>
                                <td>$ <span id="neto-text">{{ number_format($paysheet ? ($paysheet->neto > $neto ? $paysheet->neto : $neto) : $neto, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Depositado:</td>
                                <td>$ {{ number_format($paysheet ? ($paysheet->depositado > $depositado ? $paysheet->depositado : $depositado) : $depositado, 2, '.', ',') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Diferencia:</td>
                                <td>$ <span id="diferencia-text">{{ number_format($paysheet ? ($paysheet->diferencia > $diferencia ? $paysheet->diferencia : $diferencia) : $diferencia, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="/js/nomina.js"></script>
@endsection
