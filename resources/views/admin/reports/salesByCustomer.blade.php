@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de ventas por cliente
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/byCustomer')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Cliente: </label>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" id="customer_id" name="customer_id">
                                    @foreach($customers as $customerr)
                                        <option @if($customer != null && $customer->id == $customerr->id) selected="selected" @endif value="{{$customerr->id}}">{{$customerr->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary hide-in-print" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>    
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Servicio</th>
                                <th>Sucursal</th>
                                <th>Ticket</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($customer != null)
                            <?php
                                $sales = $customer->sales()->with(['subsidiary', 'services'])->
                                orderBy('id', 'desc')->get();
                            ?>
                            @foreach($sales as $sale)
                                <tr>
                                    <td>{{ $sale->date }}</td>
                                    <td>
                                        @foreach($sale->services as $service)
                                            {{ $service->service->name }},
                                        @endforeach
                                    </td>
                                    <td>{{ $sale->subsidiary->name }}</td>
                                    <td>
                                        @if($sale->subsidiary != null && $sale != null)  
                                            <a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                                {{ $sale->subsidiary->key }}-{{ $sale->folio }}
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
