@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicios todas las sucursales
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/services/subsidiaries')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                @foreach($subsidiaries as $subsidiary)
                                    <th>{{$subsidiary->name}}</th>
                                @endforeach
                                <th>Total</th>
                                <th>Precio venta</th>
                                <th>Neto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_neto = 0; ?>
                            @foreach($services as $service)
                                <?php $total = 0; ?>
                                <tr>
                                    <td>{{$service->name}}</td>
                                    @foreach($subsidiaries as $subsidiary)
                                        @php
                                            $sale_ids = $subsidiary->sales->pluck('id');
                                            $sale_services = App\SaleService::whereIn('sale_id', $sale_ids)->where('service_id', $service->id)->get();
                                        @endphp
                                        <?php $total += $sale_services->count(); ?>
                                        <td>{{$sale_services->count()}}</td>
                                    @endforeach
                                    <td>{{$total}}</td>
                                    <td>$ {{$service->sell_price}}</td>
                                    <?php $total_neto += $service->sell_price * $total; ?>
                                    <td>$ {{$service->sell_price * $total}}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="{{$subsidiaries->count() + 3}}">Total</td>
                                    <td>$ {{$total_neto}}</td>
                                </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                @foreach($subsidiaries as $subsidiary)
                                    <th>{{$subsidiary->name}}</th>
                                @endforeach
                                <th>Total</th>
                                <th>Precio costo</th>
                                <th>Neto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_neto = 0; ?>
                            @foreach($services as $service)
                                <?php $total = 0; ?>
                                <tr>
                                    <td>{{$service->name}}</td>
                                    @foreach($subsidiaries as $subsidiary)
                                        @php
                                            $sale_ids = $subsidiary->sales->pluck('id');
                                            $sale_services = App\SaleService::whereIn('sale_id', $sale_ids)->where('service_id', $service->id)->get();
                                        @endphp
                                        <?php $total += $sale_services->count(); ?>
                                        <td>{{$sale_services->count()}}</td>
                                    @endforeach
                                    <td>{{$total}}</td>
                                    <td>$ {{$service->cost}}</td>
                                    <?php $total_neto += $service->cost * $total; ?>
                                    <td>$ {{$service->cost * $total}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="{{$subsidiaries->count() + 3}}">Total</td>
                                <td>$ {{$total_neto}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
