@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de servicios por sucursal
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/services-subsidiaries')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Numero de servicios</th>
                                <th>Tiempo atendido</th>
                                <th>Tiempo espera</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            <?php $attended_time = 0; ?>
                            <?php $wait_time = 0; ?>
                            @foreach($subsidiaries as $subsidiary)
                                <tr>
                                    <td>{{ $subsidiary->name }}</td>
                                    <td>
                                        <?php
                                            $servicesNumber = $subsidiary->getServicesQuantity($start, $end);
                                            $total += $servicesNumber;
                                        ?>
                                        {{ $servicesNumber }}
                                    </td>
                                    <td>
                                        <?php
                                            $attendedAverage = $subsidiary->getAttendedTimeAverage($start, $end);
                                            $attended_time += $attendedAverage;
                                        ?>
                                        {{ $attendedAverage }} min
                                    </td>
                                    <td>
                                        <?php
                                            $waitAverage = $subsidiary->getWaitTimeAverage($start, $end);
                                            $wait_time += $waitAverage;
                                        ?>
                                        {{ $waitAverage }} min
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total:</th>
                                <td>{{ $total }}</td>
                                <td>{{ number_format($attended_time, 2, '.', ',') }} min</td>
                                <td>{{ number_format($wait_time, 2, '.', ',') }} min</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
