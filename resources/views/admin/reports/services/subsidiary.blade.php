@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de promedio de servicios
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/services-subsidiary')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                <th>Servicios efectuados</th>
                                <th>Promedio tiempo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            <?php $attended_time = 0; ?>
                            @foreach($subsidiary->getServicesReport($start, $end) as $service)
                                <tr>
                                    <td>{{ $service->service->name }}</td>
                                    <td>
                                        <?php $total += $service->services_count; ?>
                                        {{ $service->services_count }}
                                    </td>
                                    <td>
                                    <?php $attended_time += $service->sale->attended_at->diffInMinutes($service->sale->finished_at) / $service->sale->services->count(); ?>
                                        {{ $service->sale->attended_at->diffInMinutes($service->sale->finished_at) / $service->sale->services->count() }} min
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total:</th>
                                <td>{{ $total }}</td>
                                <td>{{ number_format($attended_time, 2, '.', ',') }} min</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
