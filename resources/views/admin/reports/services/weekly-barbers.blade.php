@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas semanal por barbero
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/services/barbers')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Costo</th>
                                <th>Entrada</th>
                                <th>Puntualidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sale)
                                <?php $date = $sale->getOriginal('date'); ?>
                                <tr>
                                    <td>{{ $date }}</td>
                                    <td>{{ $sale->subsidiary->name }}</td>
                                    <?php
                                        //$subtotal = $barber->getSalaryByDate($date, false);
                                        $subtotal = $barber->getSalaryByDateAndSubsidiary($date, $sale->subsidiary_id, false);
                                        $total += $subtotal;
                                    ?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                    <td>
                                        @php
                                            $attendance = $barber->attendances()->where('subsidiary_id', $sale->subsidiary->id)->date($date)->get();
                                        @endphp
                                        @if($attendance->count())
                                            {{$attendance->first()->getTime()}}
                                           
                                        @endif
                                    </td>

                                    <td>
                                    <?php $schedules = $barber->schedules()->get(); ?>
                                        @if($attendance->count())
                                            @if ($schedules->count() > 0)
                                            <?php //dd($schedules); ?>
                                                <?php $dateSchedule = $schedules->where('date', $date)->first();?>
                                                @if($dateSchedule && startltqend($attendance->first()->time, $dateSchedule->turn->start))
                                                    <i class="fa fa-check"></i>
                                                @else
                                                    <i class="fa fa-close"></i> 
                                                    @if(!$dateSchedule) 
                                                        (SIN HORARIO ESE DIA)
                                                    @endif
                                                @endif
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @else
                                            @if (!$schedules->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @php
                                    $saless = $barber->weeklySalesRange($start, $end)->get();
                                    $sale_ids = $saless->pluck('id');
                                    $services = App\SaleService::with('sale')->whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');

                                    $kids_promotion_qty = 0;
                                    foreach($saless as $sale) {
                                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                            $kids_promotion_qty += 1;
                                        }
                                    } 
                                @endphp

                                @foreach($services as $service)
                                    @if($service->first()->service_id == 1)
                                        <?php $qty = $service->sum('qty') - $kids_promotion_qty; ?>
                                    @else
                                        <?php $qty = $service->sum('qty'); ?>
                                    @endif
                                    <?php 
                                        $subtotal = 0;
                                        foreach ($service as $serv) {
                                            $subtotal += $serv->getCommission();
                                        }
                                    ?>
                                    @if($qty > 0)
                                    <tr>
                                        <td>{{ $service->first()->service->name }}</td>
                                        <td>{{ $qty }}</td>
                                        <?php $sum += $subtotal;?>
                                        <td>$ {{ $subtotal }}</td>
                                    </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <th colspan="2">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <tbody>
                            <?php $tot = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sale)
                                <?php $date = $sale->getOriginal('date'); ?>
                                <tr>
                                    <th>{{ $date }}</th>
                                    <th colspan="5">{{ $sale->subsidiary->name }}</th>
                                </tr>
                                <?php
                                    $saless = $barber->weeklySales($date)
                                            ->where('subsidiary_id', $sale->subsidiary->id)
                                            ->finished()->notCanceled()->get();
                                    $sale_ids = $saless->pluck('id');
                                    $services = App\SaleService::with('sale')->whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');
                                    $service_total = 0;

                                    $kids_promotion_qty = 0;
                                    foreach ($saless as $sale) {
                                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                            $kids_promotion_qty += 1;
                                        }
                                    }
                                ?>
                                    @foreach($services as $service_group)
                                        @if($service_group->first()->service_id == 1)
                                            <?php $qty = $service_group->sum('qty') - $kids_promotion_qty;?>
                                        @else
                                            <?php $qty = $service_group->sum('qty'); ?>
                                        @endif
                                        @if($qty > 0)
                                        @php
                                            $service_total += ($service_group->first()->service->cost * $qty);
                                        @endphp
                                        <tr>
                                            <td>{{ $qty }}</td>
                                            <td>{{ $service_group->first()->service->name }}</td>
                                            <td>$ {{ $service_group->first()->service->cost}}</td>
                                            <td>$ {{ $service_group->first()->service->cost * $qty}}</td>
                                            <td>
                                                @if($loop->last)
                                                    $ {{$service_total}}
                                                    <?php $tot += $service_total;?>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endforeach
                            <tr>
                                <th colspan="3">Total:</th>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Reparaciones = {{\App\Sale::where('repaired_by', $barber->id)->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->count()}}</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Fecha</th>
                                <th>Reparacion de</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @foreach(\App\Sale::where('repaired_by', $barber->id)->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->get() as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>{{ $sale->getOriginal('date') }}</td>
                                        <td>{{ $sale->employee->short_name }}</td>
                                        <?php $sum += $sale->services->first()->service->cost; ?>
                                        <td>{{ $sale->services->first()->service->cost }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th colspan="3">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Reparaciones en contra = {{$barber->sales()->whereBetween('date', [$start, $end])->finished()->notCanceled()->repaireds()->count()}}</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Fecha</th>
                                <th>Reparacion por</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @foreach($barber->sales()->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->get() as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>{{ $sale->getOriginal('date') }}</td>
                                        <td>
                                        @if($sale->repaired != null)
                                        {{ $sale->repaired->short_name }}
                                        @endif
                                        </td>
                                        <?php $sum += $sale->services->first()->service->cost; ?>
                                        <td>{{ $sale->services->first()->service->cost }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th colspan="3">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
