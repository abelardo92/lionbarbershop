@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de promociones de ventas
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/promotions')}}" method="get">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Fecha de: </label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control" name="start" value="{{$start}}">
                                        <span class="input-group-addon">al</span>
                                        <input type="text" class="form-control" name="end" value="{{$end}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                                </div>    
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Cajer@</th>
                                <th>Foto adjunta</th>
                                <th>Promoción</th>
                                <th>Cliente</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sales as $sale)
                                <tr>
                                    <td>
                                        <a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                            {{ $sale->subsidiary->key }} - {{ $sale->folio }}
                                        </a>
                                    </td>
                                    <td>{{ $sale->date }}</td>
                                    <td>{{ $sale->subsidiary->name }}</td>
                                    <td>
                                        @if($sale->cashRegister != null)
                                            {{ $sale->cashRegister->employee->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($sale->image == null)
                                            NO
                                        @else
                                            SI
                                        @endif
                                    </td>
                                    <td>
                                        @if($sale->has_birthday)
                                            CUMPLEAÑOS
                                        @else
                                            MONEDERO
                                        @endif
                                    </td>
                                    <td>{{ $sale->customer->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $sales->appends(['start' => $start, 'end' => $end])->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
