@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pedidos no surtidos
                </div>

                <div class="panel-body">
                    <form method="GET" action="{{ url('home/reports/warehouse/requests/unsupplied_orders') }}">
                        <div class="row">
                            <label for="code" class="col-sm-1 control-label">Sucursal</label>
                            <div class="col-sm-3">
                                <select class="form-control" id="subsidiary_id" name="subsidiary_id">
                                    <option value="">TODOS</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option @if($subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="start" class="col-sm-1">Fecha: </label>
                            <div class="input-group input-daterange col-sm-5">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Folio</th>
                                <th>Solicitado por</th>
                                <th>Fecha</th>
                                <th>Productos faltantes</th>
                                <!--<th>Ver información</th>-->
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($filteredRequests as $index => $request)
                                <tr>
                                    <td>{{ $request->subsidiary->name }}</td>
                                    <td>{{ $request->folio }}</td>
                                    <td>
                                        @if($request->employee != null)
                                            {{ $request->employee->short_name }}    
                                        @endif
                                    </td>
                                    <td>{{ $request->created_at }}</td>
                                    <td>
                                        <?php
                                        $movement = $request->movements->first();
                                        ?>
                                        @foreach ($request->products as $product)
                                            @if($movementProduct = $movement->products->where('warehouse_product_id', $product->product->id)->first()) 
                                                @if($product->quantity > $movementProduct->quantity)
                                                    {{ $product->product->name }} ({{ $product->quantity - $movementProduct->quantity}}) <br/>
                                                @endif
                                            @else
                                                {{ $product->product->name }} ({{ $product->quantity }})  <br/>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @if($request->movements != null && count($request->movements) > 0)
                                            @php
                                                $movement = $request->movements->first();
                                            @endphp
                                            <a href="{{ route('warehouse.movements.print-unsupplied', $movement->id) }}">Imprimir</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
