@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(Auth::user()->isA('super-admin'))
                        <p class="pull-right">
                            <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                        </p>
                    @endif
                    @if(Auth::user()->isA('employee'))
                        Reporte de servicios
                    @else
                        Reporte de ventas semanal por barbero
                    @endif
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/barbers')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                         @if(Auth::user()->isA('employee'))
                            <input type="hidden" name="employee_id" value="{{$barber->id}}">
                         @else
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            @if(Auth::user()->isA('employee'))
                                <a href="{{ route('issues.messages.employees.create') }}" class="btn btn-default hide-in-print">Enviar mensaje</a>
                                <a href="{{ route('issues.messages.employees.received') }}" class="btn btn-default hide-in-print">Ver mensajes</a>
                            @endif
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                @if(Auth::user()->isA('super-admin'))
                                <th>Venta</th>
                                @endif
                                <th>Entrada</th>
                                <th>Puntualidad</th>
                                @if(Auth::user()->isA('super-admin'))
                                <th>Comisión</th>
                                @endif
                                <th>Reparaciones</th>
                                <th>Descuento reparaciones</th>
                                <th>Propinad TC</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;?>
                            <?php $total_comision = 0;?>
                            <?php $total_discount_repaired = 0;?>
                            <?php $total_repaired = 0;?>
                            <?php $total_tips = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sale)

                                <tr>
                                    <td>{{ $sale->date }}</td>
                                    <td>{{ $sale->subsidiary->name }}</td>

                                    <?php
                                        $saless = $barber->weeklySales($sale->date)
                                            ->where('subsidiary_id', $sale->subsidiary->id)
                                            ->finished()->notCanceled()->get();
                                        $sale_ids = $saless->pluck('id');
                                        $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                            ->get()->groupBy('service_id');
                                        $subtotal = 0;
                                        $subtotal_comision = 0;
                                        $discount_repaired = 0;
                                        $repaired = 0;
                                        $tips = 0;

                                        $kids_promotion_qty = 0;
                                        foreach ($saless as $sale) {
                                            if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                                $kids_promotion_qty += 1;
                                            }
                                        }
                                    ?>
                                    <?php //dd($services); ?>
                                     @foreach($services as $service_group)
                                        @if($service_group->first()->service_id == 1)
                                            <?php $qty = $service_group->sum('qty') - $kids_promotion_qty;?>
                                        @else
                                            <?php $qty = $service_group->sum('qty'); ?>
                                        @endif
                                        @php
                                            $subtotal += $service_group->first()->price * $qty;
                                        @endphp
                                    @endforeach
                                    @php
                                    $total += $subtotal;
                                    @endphp
                                    @if(Auth::user()->isA('super-admin'))
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                    @endif
                                    <td>
                                        @php
                                            $attendance = $barber->attendances()->where('subsidiary_id', $sale->subsidiary->id)->date($sale->date)->get();
                                        @endphp
                                        @if($attendance->count())
                                            {{$attendance->first()->getTime()}}
                                        @endif
                                    </td>

                                    <td>
                                        @if($attendance->count())
                                            @if($sale->date == '2016-12-11')
                                            {{-- {{dd($barber->schedules()->where('date', $sale->date)->first()->turn->start)}} --}}
                                            @endif
                                            <?php 
                                                $schedule = $barber->schedules()->with(['turn'])->where('date', $sale->date)->first(); 
                                            ?>
                                            @if ($schedule != null && $schedule->turn != null && startltqend($attendance->first()->time, $schedule->turn->start))
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @else
                                            0
                                        @endif
                                    </td>
                                    @if(Auth::user()->isA('super-admin'))
                                    <td>
                                        @if($attendance->count())
                                            @if (startltqend($attendance->first()->time, $barber->schedules()->where('date', $sale->date)->first()->turn->start))
                                                <?php $subtotal_comision = $subtotal * $barber->commission;?>
                                            @else
                                                <?php $subtotal_comision = $subtotal * $barber->commission_two;?>
                                            @endif
                                                $ {{ $subtotal_comision }}
                                        @else
                                            <i class="fa fa-close"></i>
                                        @endif
                                        <?php $total_comision += $subtotal_comision;?>
                                    </td>
                                    @endif
                                    <?php
                                $sale_ids = $barber->weeklySales($sale->date)->repaireds()->get()->pluck('id');
                                $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                    ->get()->groupBy('service_id');
                                ?>
                                    @foreach($services as $service_group)
                                        @php
                                            $repaired += ($service_group->first()->price * $service_group->sum('qty'));
                                        @endphp
                                    @endforeach
                                    @php
                                        $total_repaired += $repaired;
                                    @endphp
                                    <td>$ {{ number_format($repaired, 2, '.', ',') }}</td>

                                    <?php
                                $sale_ids = $barber->sales()->where('date', $sale->date)
                                    ->where('subsidiary_id', $sale->subsidiary->id)
                                    ->finished()->notCanceled()->repaireds()->get()->pluck('id');
                                $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                    ->get()->groupBy('service_id');
                                ?>
                                    @foreach($services as $service_group)
                                        @php

                                            $discount_repaired += ($service_group->first()->price * $service_group->sum('qty'));
                                        @endphp
                                    @endforeach
                                    @php
                                        $total_discount_repaired += $discount_repaired;
                                    @endphp
                                    <td>$ {{ number_format($discount_repaired, 2, '.', ',') }}</td>
                                    @php 
                                    $tips = $barber->sales()->where('date', $sale->date)
                                ->where('subsidiary_id', $sale->subsidiary->id)
                                ->finished()->notCanceled()->where('tip_in', 'tarjeta')->sum('tip');
                                $total_tips += $tips;
                                @endphp
                                    <td>$ {{ number_format($tips, 2, '.', ',') }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                @if(Auth::user()->isA('super-admin'))
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                                @endif
                                <td></td>
                                <td></td>
                                @if(Auth::user()->isA('super-admin'))
                                <td>$ {{ number_format($total_comision, 2, '.', ',') }}</td>
                                @endif
                                <td>$ {{ number_format($total_repaired, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($total_discount_repaired, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($total_tips, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                <th>Cantidad</th>
                                @if(Auth::user()->isA('super-admin'))
                                <th>Total</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @php
                                    $saless = $barber->weeklySalesRange($start, $end)->get();
                                    $sale_ids = $saless->pluck('id');
                                    $services = App\SaleService::whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');

                                    $kids_promotion_qty = 0;
                                    foreach ($saless as $sale) {
                                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                            $kids_promotion_qty += 1;
                                        }
                                    }
                                    
                                @endphp
                                @foreach($services as $service)
                                    @if($service->first()->service_id == 1)
                                        <?php $qty = $service->sum('qty') - $kids_promotion_qty;?>
                                    @else
                                        <?php $qty = $service->sum('qty'); ?>
                                    @endif
                                    @if($qty > 0)
                                    <tr>
                                        <?php $sum += $service->first()->price * $qty;?>
                                        <td>{{ $service->first()->service->name }}</td>
                                        <td>{{ $qty }}</td>
                                        @if(Auth::user()->isA('super-admin'))
                                        <td>$ {{ $service->first()->price * $qty }}</td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                                @if(Auth::user()->isA('super-admin'))
                                <tr>
                                    <th colspan="2">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <tbody>
                            <?php $tot = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sale)
                                <tr>
                                    <td>{{ $sale->date }}</td>
                                    <td colspan="3">{{ $sale->subsidiary->name }}</td>
                                </tr>
                                @php
                                    $saless = $barber->weeklySales($sale->date)
                                        ->where('subsidiary_id', $sale->subsidiary->id)
                                        ->finished()->notCanceled()->get();
                                    $sale_ids = $saless->pluck('id');
                                    $services = App\SaleService::whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');
                                    $service_total = 0;

                                    $kids_promotion_qty = 0;
                                    foreach ($saless as $sale) {
                                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                            $kids_promotion_qty += 1;
                                        }
                                    }

                                @endphp
                                    @foreach($services as $service_group)
                                        @if($service_group->first()->service_id == 1)
                                            <?php $qty = $service_group->sum('qty') - $kids_promotion_qty;?>
                                        @else
                                            <?php $qty = $service_group->sum('qty'); ?>
                                        @endif
                                        @if($qty > 0)
                                        @php
                                            $service_total += ($service_group->first()->price * $qty);
                                        @endphp
                                        <tr>
                                            <td>{{ $qty }}</td>
                                            <td>{{ $service_group->first()->service->name }}</td>
                                            @if(Auth::user()->isA('super-admin'))
                                            <td>$ {{ $service_group->first()->price * $qty}}</td>
                                            <td>
                                                @if($loop->last)
                                                    $ {{$service_total}}
                                                    <?php $tot += $service_total;?>
                                                @endif
                                            </td>
                                            @endif
                                        </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endforeach
                            @if(Auth::user()->isA('super-admin'))
                            <tr>
                                <th colspan="2">Total:</th>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
