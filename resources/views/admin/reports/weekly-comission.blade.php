@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    REPORTE DE COMISIONES POR VENTAS RESUMEN
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/sales/commision/resume')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="">
                        <form action="{{url('/home/reports/weekly/sales/commision/resume')}} " method="post" onkeypress="return event.keyCode != 13;">
                            {{ csrf_field() }}

                            <input type="hidden" name="start" value="{{$start}}">
                            <input type="hidden" name="end" value="{{$end}}">
                            <div class="pull-right">
                                <input type="submit" class="btn btn-success" value="Exportar">
                            </div>
                            <div class="clearfix"></div>
                            <fieldset>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Comision</th>
                                            <th colspan="2">Puntualidad</th>
                                            <th colspan="2">Comision venta de productos</th>
                                            <th colspan="2">Propinas</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $tc = 0;?>
                                            <?php $tp = 0;?>
                                            <?php $tvp = 0;?>
                                            <?php $tpp = 0;?>
                                            <?php $tt = 0;?>
                                        @foreach($employees as $employee)
                                            <?php $total_comision = 0;?>
                                            <?php $total_productos = 0;?>
                                            <?php $productos = 0;?>
                                            <?php $total_tip = 0;?>
                                            <?php $subtotal_comision = 0;?>
                                            <?php $no_puntualidad = 0;?>
                                            <?php $sales = [];?>
                                            @foreach($dates as $date)
                                                <?php $total_comision = 0;?>
                                                @php
                                                $salequery = $employee->sales()->select(DB::raw('id, subsidiary_id, date, SUM(subtotal) as sumtotal'))->where('date', $date);
                                                $user = Auth::user();
                                                if($user->access_subsidiary_id != null){
                                                    $salequery = $salequery->where('subsidiary_id',$user->access_subsidiary_id);
                                                }
                                                $sales[] = $salequery->finished()->notCanceled()->groupBy('subsidiary_id')->get();
                                                @endphp
                                                @foreach($sales as $group_sale)
                                                    @foreach($group_sale as $sale)
                                                        @php
                                                            $sale_ids = $employee->sales()->where('date', $sale->date)
                                                                ->where('subsidiary_id', $sale->subsidiary->id)
                                                                ->finished()->notCanceled()->get()->pluck('id');
                                                            $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                                                        ->get()->groupBy('service_id');
                                                            $subtotal = 0;
                                                            $subtotal_comision = 0;
                                                        @endphp
                                                        @foreach($services as $service_group)
                                                            {{-- @if(!$service_group->first()->service)
                                                                {{dd($service_group)}}
                                                            @endif --}}
                                                            @php
                                                                $subtotal += ($service_group->first()->price * $service_group->count());
                                                            @endphp
                                                        @endforeach

                                                        @php
                                                            $attendance = $employee->attendances()->where('subsidiary_id', $sale->subsidiary->id)->date($sale->date)->get();
                                                        @endphp
                                                        @if($attendance->count())
                                                            @if (startltqend($attendance->first()->time, $employee->schedules()->where('date', $sale->date)->first()->turn->start))
                                                                <?php $subtotal_comision = $subtotal * "0.{$employee->commission}";?>
                                                            @else
                                                                <?php $subtotal_comision = $subtotal * "0.{$employee->commission_two}";?>
                                                            @endif
                                                                <?php $total_comision += $subtotal_comision;?>
                                                        @endif
                                                    @endforeach
                                                @endforeach

                                                <?php $total_tip += $employee->sales()->where('date', $date)->sum('tip');?>
                                            @endforeach

                                            @php
                                                $sales = $employee->sales()->whereBetween('date', [$start, $end])
                                                    ->finished()->notCanceled()->orderBy('employee_id')
                                                    ->get();
                                            @endphp
                                            <?php $productos = 0;?>
                                            <?php $comision = 0;?>

                                            <?php
                                            $prods = App\SaleProduct::whereIn('sale_id', $sales->pluck('id'))->get();

                                            foreach ($prods->filter(function ($item) {
                                                return $item->product->has_commission;
                                            })->groupBy('product_id') as $group) {
                                                foreach ($group as $sale_prod) {
                                                    $productos += $group->count() * $sale_prod->product->sell_price;
                                                }
                                            }
                                            ?>
                                            <?php $comision = $productos * 0.10;?>
                                            <?php $total_productos += $comision;?>
                                            <?php $big_total = 0;?>
                                            <tr>
                                                <td>
                                                    <a href="{{url('/home/reports/weekly/barbers?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank" class="hide-in-print">{{$employee->short_name}}</a>
                                                    <input type="hidden" name="employee[{{$employee->id}}][id]" value="{{$employee->id}}">

                                                    <span class="show-in-print">{{$employee->short_name}}</span>
                                                </td>
                                                <td> $ {{ number_format($total_comision, 2, '.', '') }}</td>
                                                <?php $tc += $total_comision;?>
                                                <input type="hidden" name="employee[{{$employee->id}}][commission]" value="{{number_format($total_comision, 2, '.', '')}}" id="hidden-commision-{{$employee->id}}">

                                                <?php $big_total += $total_comision;?>
                                                @if($no_puntualidad == 0)
                                                    <?php $bp = ($total_comision * 10) / 100;?>
                                                    <?php $big_total += $bp;?>
                                                @else
                                                    <?php $bp = 0?>
                                                @endif
                                                <td id="text-appoint-{{$employee->id}}">$ {{ number_format($bp, 2, '.', '') }}</td>
                                                <?php $tp += $bp;?>
                                                <input type="hidden" name="employee[{{$employee->id}}][appoint]" value="{{number_format($bp, 2, '.', '')}}" id="hidden-appoint-{{$employee->id}}">

                                                <td><input type="checkbox" name="appoint[{{$employee->id}}]" @if($no_puntualidad == 0) checked="checked" @endif data-employee-id="{{$employee->id}}"  class="check-puntu"></td>

                                                <td>
                                                    <a href="{{url('/home/reports/sales/commission/status?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank" class="hide-in-print"  id="text-products-{{$employee->id}}">
                                                        $ {{ number_format(($productos * 0.10), 2, '.', '') }}
                                                    </a>
                                                </td>
                                                <?php $tvp += ($productos * 0.10);?>
                                                <input type="hidden" name="employee[{{$employee->id}}][products]" value="{{number_format($total_productos, 2, '.', '')}}" id="hidden-products-{{$employee->id}}">
                                                <td><input type="checkbox" name="prod[{{$employee->id}}]" checked="checked" data-employee-id="{{$employee->id}}" class="check-prod" data-total="{{number_format($total_productos, 2, '.', '')}}"></td>

                                                <?php $big_total += ($total_productos);?>
                                                <td id="text-tip-{{$employee->id}}">$ {{ number_format($total_tip, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][tip]" value="{{number_format($total_tip, 2, '.', '')}}" id="hidden-tip-{{$employee->id}}">
                                                <td><input type="checkbox" name="tip[{{$employee->id}}]" checked="checked" data-employee-id="{{$employee->id}}" data-total="{{number_format($total_tip, 2, '.', '')}}" class="check-tip"></td>
                                                <?php $tpp += $total_tip;?>

                                                <?php $big_total += $total_tip;?>
                                                <td id="text-bigtotal-{{$employee->id}}">$ {{ number_format($big_total, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][bigtotal]" value="{{number_format($big_total, 2, '.', '')}}" id="hidden-bigtotal-{{$employee->id}}">
                                                <?php $tt += $big_total;?>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td>Totales:</td>
                                            <td>{{$tc}}</td>
                                            <td>{{$tp}}</td>
                                            <td></td>
                                            <td>{{$tvp}}</td>
                                            <td></td>
                                            <td>{{$tpp}}</td>
                                            <td></td>
                                            <td>{{$tt}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
