@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="panel-customer" class="panel panel-default display-none" style="position: absolute; left: 0px; top: -30px;">
                <div class="panel-heading danger-animation">
                    Feliz cumpleaños <span id="customer-name"></span>
                </div>
                <div class="panel-body" id="text-service" style="padding: 8px 10px;">
                    Hoy Lion Barbershop le regala un servicio gratis
                </div>
                <div class="panel-body display-none" id="text-no-service">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nuevo servicio
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <div class="panel-body">
                    <form action="{{route('sales.store', $subsidiary->key)}}" method="POST" class="form-horizontal" name="sale-form" id="sale-form" enctype="multipart/form-data">
                        {{{ csrf_field() }}}
                        <input type="hidden" name="has_birthday" id="has_birthday" value="0">
                        <input type="hidden" name="suggested_employee_id" id="suggested_employee_id" value="{{$suggestedBarber->id}}">
                        <div class=row>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="diary_id" class="col-sm-3 control-label">Cita</label>
                                    <div class="col-sm-7">
                                        <select name="diary_id" id="diary_id" class="select2 form-control">
                                            <option value="">Seleccionar uno por favor...</option>
                                            @foreach($diaries as $diary)
                                                <option value="{{$diary->id}}">{{$diary->customer->name}} 
                                                @if($diary->comments)
                                                    ({{$diary->comments}}) 
                                                @endif
                                                - {{$diary->time}} ({{$diary->confirmation_code}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="customer_id" class="col-sm-3 control-label">
                                        Cliente
                                    </label>
                                    <div class="col-sm-7">
                                        <span id="customer_name" hidden></span>
                                        <input type="button" class="btn btn-default" id="show_customer_name" value="Seleccionar cliente" onclick="showCustomerFilter();" />
                                        <div id="customer_id_container">
                                            <select name="customer_id" id="customer_id" class="customer_id form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{url('/home/customers/create?redirect=/home/sales/create')}}">Nuevo cliente</a>
                                    </div>
                                </div>
                                <div class="form-group" id="comments-group">
                                    <label for="comments" class="col-sm-3 control-label">Nombre de Referencia</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="comments" id="comments" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="employee_id" class="col-sm-3 control-label">Barbero</label>
                                    <div class="col-sm-7">
                                        <select name="employee_id" id="employee_id" class="select2 form-control">
                                            <option value="0">Seleccionar uno por favor...</option>
                                            @foreach($barbers as $barber)
                                                <option value="{{$barber->id}}" @if($barber->id == $suggestedBarber->id) selected="selected" @endif>{{$barber->short_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="service_id" class="col-sm-3 control-label">Servicio(s)</label>
                                    <div class="col-sm-7">
                                        <select name="service_id[]" id="service_id" multiple="multiple" class="select2 form-control">
                                            <option value="0">Seleccionar uno o mas por favor...</option>
                                            @foreach($services as $service)
                                                <option value="{{$service->id}}">{{$service->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="employee2-group">
                                    <label for="employee2_id" class="col-sm-3 control-label">Barbero 2</label>
                                    <div class="col-sm-7">
                                        <select name="employee2_id" id="employee2_id" class="select2 form-control">
                                            <option value="0">Seleccionar uno por favor...</option>
                                            @foreach($barbers as $barber)
                                                <option value="{{$barber->id}}">{{$barber->short_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="customer2-group" hidden>
                                    <label for="customer2" class="col-sm-3 control-label">Nombre del segundo cliente</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="customer2" id="customer2" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--
                        <div class="form-group">
                            <label for="service_id" class="col-sm-4 control-label">Imagen evidencia de promoción</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="file" id="image" name="image">
                            </div>
                        </div>
                        -->
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                </div>

                <div class="panel-body">
                    <table class="table table-striped datatables">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th>Marcar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->key}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->sell_price}}</td>
                                    <td><input type="number" class="form-control" name="product_qty[{{$product->id}}]" form="sale-form" value="0"></td>
                                    <td><input type="checkbox" name="product_id[]" form="sale-form" value="{{$product->id}}"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/customer-filter.js') }}"></script>
    <script>

        $(function() {  
            $("#employee2-group").hide();
            $("#add_fields_placeholderValue").hide();
            $("#show_customer_name").hide();
            $("#customer_name").hide();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });

        $('#comments-group').hide();
        $('#customer_id').on('change', function (evt) {
            if ($(this).val() == 7) {
                $('#comments-group').show();
            } else {
                $('#comments-group').hide();
                checkBirthday();
            }
        });

        $('#service_id').on('change', function (evt) {
            //alert($('#service_id').val());
            var arrObj = $('#service_id').select2("data");
            //alert(arrObj);
            $("#customer2-group").hide();
            $("#employee2-group").hide();
            $.each(arrObj, function(key,value){
                if(value.text.includes("2x1") || value.text.includes("2X1") || value.text.includes("NIÑO")){
                    $("#customer2-group").show();
                    $("#employee2-group").show();
                }
            });
            $("#service_id:selected").each(function(){
                alert($(this).val()); 
            });
        });

        $('#diary_id').on('change', function (evt) {
          $.ajax({
            type: 'get',
            dataType: 'json',
            url: '/home/diary/'+$(this).val(),
            success: function(res) {

                var opt = "<option value='" + res.data.customer_id + "' selected ='selected'>" + res.data.customer.name+ " </option>";
                $('#customer_id').html(opt);
                $('#customer_id').val("" + res.data.customer_id); //este pone el valor correcto.

                if(res.data.customer_id == 7) {
                    $('#comments').val(res.data.comments);
                    $('#comments-group').show();
                } else {
                    $('#comments-group').hide();
                    checkBirthday();
                }

                $("#customer_name").html(res.data.customer.name);
                hideCustomerFilter();

                $('#employee_id').val(res.data.employee_id);
                $('#employee_id').trigger('change.select2');
            }
          });
        });

        function showCustomerFilter() {
            $("#customer_id_container").show();
            $("#customer_name").hide();
            $("#show_customer_name").hide();
        }

        function hideCustomerFilter() {
            $("#customer_id_container").hide();
            $("#customer_name").show();
            $("#show_customer_name").show();   
        }

        function checkBirthday() {        
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/home/customers/'+$('#customer_id').val()+'/birthday',
                success: function(res){
                    if (res.success) {
                        $('#customer-name').text(res.customer.name);
                        $('#panel-customer').removeClass('display-none');
                        $('#text-service').addClass('display-none');
                        $('#text-no-service').addClass('display-none');
                        if (res.able_for_discount) {
                            $('#has_birthday').val(1);
                            $('#text-service').removeClass('display-none');
                        } else {
                            $('#text-no-service').removeClass('display-none');
                            $('#text-no-service').text(res.message)
                        }
                    } else {
                        $('#panel-customer').addClass('display-none');
                        $('#has_birthday').val(0);
                    }
                }
            });
        }
    </script>
@endsection
