@extends('layouts.app')

@section('content')
<div class="container" style="padding-left:0px; padding-right:0px;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nuevo servicio
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('sales.storeLaundry')}}" method="POST" class="form-horizontal" name="sale-form" id="sale-form">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="employee_id" class="col-sm-1">Lavador</label>
                            <div class="col-sm-3">
                                <select name="employee_id" id="employee_id" class="select2 form-control">
                                    <option value="0">Seleccionar uno por favor...</option>
                                    @foreach($washers as $washer)
                                        <option value="{{$washer->id}}">{{$washer->short_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="customer_id" class="col-sm-1">
                                Cliente (Sucursal)
                            </label>
                            <div class="col-sm-3">
                                <select name="customer_subsidiary_id" id="customer_subsidiary_id" class="select2 form-control">
                                    <option value="0">Otro tipo de cliente</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="customer_id" class="col-sm-1 customer-group">
                                Cliente
                            </label>
                            <div class="col-sm-3 customer-group">
                                <select name="customer_id" id="customer_id" class="select2 form-control">
                                    <option value="0">Publico en general</option>
                                    @foreach($customers as $customer)
                                        <option @if($customer_id == $customer->id) selected="selected" @endif value="{{$customer->id}}">{{$customer->name}} ({{$customer->wallet_number}})</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="comments" class="col-sm-1 comments-group">Nombre Cliente</label>
                            <div class="col-sm-3 comments-group">
                                <input type="text" name="comments" id="comments" class="form-control">
                            </div>
                            <div class="col-sm-4 createCustomer">
                                <a href="{{url('/home/customers/create?redirect=/home/sales/create')}}">Nuevo cliente</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="service_id" class="col-sm-1">Servicios</label>
                            <div class="col-sm-2">
                                <select name="service" id="service" class="select2 form-control ">
                                @foreach($services as $service)
                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <input type="button" class="btn btn-success" id="addService" value="Añadir"/>
                            </div>

                            <label for="product" class="col-sm-1">Productos</label>
                            <div class="col-sm-2">
                                <select name="product" id="product" class="select2 form-control ">
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <input type="button" class="btn btn-success" id="addProduct" value="Añadir"/>
                            </div>

                            <label for="package" class="col-sm-1">Paquetes</label>
                            <div class="col-sm-2">
                                <select name="package" id="package" class="select2 form-control ">
                                @foreach($packages as $package)
                                    <option value="{{$package->id}}">{{$package->name}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <input type="button" class="btn btn-success" id="addPackage" value="Añadir"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <table class="table" id="servicesTable">
                                    <tbody>
                                        <tr>
                                            <th>Nombre del servicio</th>
                                            <th>Cantidad</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <table class="table" id="productsTable">
                                    <tbody>
                                        <tr>
                                            <th>Nombre del producto</th>
                                            <th>Cantidad</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>  
                            <div class="col-sm-4">
                                <table class="table" id="packagesTable">
                                    <tbody>
                                        <tr>
                                            <th>Nombre del paquete</th>
                                            <th>Cantidad</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>          
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>

        $(function() {  
            $("#employee2-group").hide();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });

        $('.comments-group').hide();
        $('.customer-group').hide();
        $('.createCustomer').hide();
        $('#customer_subsidiary_id').val("");
        $('#customer_id').val("");

        $('#customer_subsidiary_id').on('change', function (evt) {
            $('.comments-group').hide();
          if ($("#customer_subsidiary_id").val() == 0) {
            $('.customer-group').show();
            $('.createCustomer').show();
            handleComments();
            $('#customer_id').select2();
          }else {
            $('.customer-group').hide();
            $('.createCustomer').hide();
          }
        });

        $('#customer_id').on('change', function (evt) {
          handleComments();
        });

        function handleComments(){
            if ($("#customer_id").val() == 0) {
            $('.comments-group').show();
          }else {
            $('.comments-group').hide();
          }
        }

        $('#addProduct').on('click', function (evt) {
            if($("#product").val() != null){
                var value = $("#product").val();
                var name = $("#product option:selected").text();
                var idInput = "<input type='hidden' id='product_ids' name='product_ids[]' value='"+value+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeProduct("+value+")'>Borrar</button>";
                $("#productsTable > tbody").append("<tr id='product"+value+"'><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });

        $('#addService').on('click', function (evt) {
            if($("#service").val() != null){
                var value = $("#service").val();
                var name = $("#service option:selected").text();
                var idInput = "<input type='hidden' id='service_ids' name='service_ids[]' value='"+value+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='service_quantities' name='service_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeService("+value+")'>Borrar</button>";
                $("#servicesTable > tbody").append("<tr id='service"+value+"'><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#service option:selected').attr('disabled','disabled');
                $("#service").val("");
                $('#service').select2();
            }
        });

        $('#addPackage').on('click', function (evt) {
            if($("#package").val() != null) {
                var value = $("#package").val();
                var name = $("#package option:selected").text();
                var idInput = "<input type='hidden' id='package_ids' name='package_ids[]' value='"+value+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='package_quantities' name='package_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removePackage("+value+")'>Borrar</button>";
                $("#packagesTable > tbody").append("<tr id='package"+value+"'><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#package option:selected').attr('disabled','disabled');
                $("#package").val("");
                $('#package').select2();
            }
        });

        function removeProduct(id){
            $('#product' + id).remove();
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }

        function removeService(id){
            $('#service' + id).remove();
            $('#service option[value="' + id + '"]').removeAttr('disabled');
            $('#service').select2();
        }

        function removePackage(id){
            $('#package' + id).remove();
            $('#package option[value="' + id + '"]').removeAttr('disabled');
            $('#package').select2();
        }
    </script>
@endsection
