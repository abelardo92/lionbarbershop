@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicio {{$sale->id}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('sales.update', [$subsidiary->key, $sale->id])}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="customer_id" class="col-sm-4 control-label">Cliente *</label>
                            <div class="col-sm-6">
                                <select name="customer_id" id="customer_id" class="select2 form-control" required="required">
                                    <option value="">Seleccionar uno por favor...</option>
                                    @foreach($customers as $customer)
                                        <option @if($sale->customer_id == $customer->id) selected="selected" @endif value="{{$customer->id}}">{{$customer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employee_id" class="col-sm-4 control-label">Barbero</label>
                            <div class="col-sm-6">
                                <select name="employee_id" id="employee_id" class="select2 form-control">
                                    <option value="0">Seleccionar uno por favor...</option>
                                    @foreach($attendances as $attendance)
                                        @if($attendance->employee->job == 'Barbero')
                                            <option @if($sale->employee_id == $attendance->employee->id) selected="selected" @endif value="{{$attendance->employee->id}}">{{$attendance->employee->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="service_id[]" class="col-sm-4 control-label">Servicio *</label>
                            <div class="col-sm-6">
                                <select name="service_id[]" id="service_id[]" multiple="multiple" class="select2 form-control" required>
                                    <option value="0">Seleccionar uno o mas por favor...</option>
                                    @foreach($services as $service)
                                        <option value="{{$service->id}}">{{$service->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
