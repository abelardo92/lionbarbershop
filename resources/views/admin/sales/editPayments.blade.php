@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicio {{$sale->id}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('sales.updatePayments', [$sale->id])}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        <sale-payments-form
                        :sale="{{$sale->toJson()}}"
                        :barbers="{{$barbers->toJson()}}"
                        ></sale-payments-form>
                        <!--
                        <div class="form-group">
                            <div class="col-sm-4">
                                <strong>Folio:</strong> {{ $sale->folio }}       
                            </div>
                            <div class="col-sm-4">
                                <strong>Fecha y hora:</strong> {{ $sale->created_at }}        
                            </div>
                            <div class="col-sm-4">
                                <strong>Cliente:</strong> {{ $sale->customer->name }}        
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <strong>Total:</strong> $ {{ $sale->total }}        
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <strong>Propina:</strong>        
                            </div>
                            <div class="col-sm-4">
                                <input class='form-control' type='number' step='any' min='0' name="tip" id="tip" value="{{ $sale->tip }}"/> 
                            </div>
                            <div class="col-sm-2">
                                <strong>Barbero:</strong>        
                            </div>
                            <?php //dd($barbers); ?>
                            <div class="col-sm-4">
                                <select name="employee_id" id="employee_id" class="select2 form-control">
                                    <option value="0">Seleccionar uno por favor...</option>
                                    @foreach($barbers as $barber)
                                        <option value="{{$barber->id}}" @if($barber->id == $sale->employee_id) selected="selected" @endif>{{$barber->short_name}}</option>
                                    @endforeach
                                </select>        
                            </div>
                        </div>
                        <br/>
                        <table class="table">
                            <tr>
                                <th>Tipo de pago</th>
                                <th>Moneda</th>
                                <th>Cantidad</th>
                            </tr>
                            <?php //dd($sale->payments()->get()); ?>
                            @foreach($sale->payments()->get() as $payment)
                            <tr>
                            <input type="hidden" name="payment_ids[]" value="{{ $payment->id }}"/>
                                <td>
                                    <select class="form-control" name="methods[]">
                                        <option value="Efectivo" @if($payment->method == "Efectivo") selected="selected" @endif>Efectivo</option>
                                        <option value="Tarjeta" @if($payment->method == "Tarjeta") selected="selected" @endif>Tarjeta</option>
                                        <option value="Monedero" @if($payment->method == "Monedero") selected="selected" @endif>Monedero</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" name="coins[]">
                                        <option value="mxn" @if($payment->coin == "mxn") selected="selected" @endif >mxn</option>
                                        <option value="usd" @if($payment->coin == "usd") selected="selected" @endif >usd</option>
                                    </select>
                                </td>
                                <td><input type="number" min="0" step="any" name="totals[]" class="form-control" value="{{ $payment->total }}"></td>
                            </tr>
                            @endforeach
                        </table>
                        -->
                        <div class="form-group">
                            <div class="col-sm-offset-8 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
