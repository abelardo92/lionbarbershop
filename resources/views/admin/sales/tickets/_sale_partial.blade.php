<p><strong>Fecha:</strong> {{ $sale->date }}</p>
<p><strong>Hora:</strong> {{ $sale->pay_at->format('h:i a') }}</p>
<p>
    <strong>Cliente(s):</strong> 
    @if($sale->subsidiary->is_laundry)
        <?php $wash_quantity = $sale->washQuantity(); ?>
        @if($sale->customer_subsidiary_id != null)
            {{ $sale->customerSubsidiary->name }}
        @else
            @if($sale->customer_id != null)
                {{ $sale->customer->name }}
            @else
                {{ $sale->comments }}
            @endif
        @endif
    @else
        {{ $sale->customer->name}}
        @if($sale->customer2 != "")
            , {{ $sale->customer2}}
        @endif
        @if($sale->customer->id == 7)
            ({{ $sale->comments}})
        @endif
    @endif
</p>

<table width="100%">
    <tr>
        <th>Cantidad</th>
        <th>Producto / Servicio</th>
        <th>Importe</th>
    </tr>
    <?php 
        $hasPromotion = false;
        $subtotal = 0;
    ?>
    @foreach($sale->services as $sale_service)
        <tr>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale->subsidiary->is_laundry && $sale_service->service_id == 23 && $wash_quantity <= 5)
                    1-5 kg
                @else
                    {{$sale_service->qty}}
                @endif
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_service->service->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale_service->has_promotion)
                    {{$sale_service->service->sell_price * $sale_service->qty}}
                @else
                    @if($sale->is_courtesy)
                        {{$sale_service->original_price * $sale_service->qty}}
                    @else
                        @if($sale_service->service_id == 23 && $wash_quantity <= 5)
                            {{$sale_service->price * 5}}
                        @else
                            {{$sale_service->price * $sale_service->qty}}
                        @endif
                    @endif
                @endif
                </span>
            </td>
        </tr>
        @php 
            $subtotal += $sale_service->original_price;
        @endphp
        @if($sale_service->has_promotion)
            <?php $hasPromotion = true;?>
        <tr>
            <td></td>
            <td align="center">
                Desc. promoción
            </td>
            <td align="center">
                - {{($sale_service->service->sell_price - $sale_service->price) * $sale_service->qty}}
            </td>
        </tr>    
        @endif
    @endforeach
    @foreach($sale->products as $sale_product)
        <tr>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->qty}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->product->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->product->sell_price * $sale_product->qty}}
                </span>
            </td>
        </tr>
        @if($sale_product->price < $sale_product->product->sell_price)
            <?php $hasPromotion = true;?>
        <tr>
            <td></td>
            <td align="center">
                Desc. promoción
            </td>
            <td align="center">
                - {{($sale_product->product->sell_price - $sale_product->price) * $sale_product->qty}}
            </td>
        </tr>    
        @endif
        @php 
            $subtotal += $sale_product->original_price;
        @endphp
    @endforeach
    @foreach($sale->packages as $sale_package)
        <tr>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->quantity}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->price * $sale_package->quantity}}
                </span>
            </td>
        </tr>
        @php 
            $subtotal += $sale_package->price;
        @endphp
    @endforeach
    <tr>
        <td colspan="2" align="right"><strong>Subtotal</strong></td>
        <td> $ 
        @if($sale->is_courtesy)
            {{$subtotal}}
        @else
            {{$sale->subtotal}}
        @endif
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right"><strong>Propina</strong></td>
        <td>$ {{$sale->tip}}</td>
    </tr>
    @if(!$hasPromotion && $sale->has_birthday && $sale->kids_promotion == 0)
        <tr>
            <td colspan="2" align="right"><strong>Descuento (cumpleaños)</strong></td>
            <td>$ {{number_format($sale->servicesMaxPrice(), 2, '.', '')}}</td>
        </tr>
    @endif

    @if(!$hasPromotion && !$sale->payments()->where('method','Monedero')->first() && $sale->has_discount && $sale->kids_promotion == 0)
        <tr>
            <td colspan="2" align="right"><strong>Descuento (10%)</strong></td>
            <td>$ {{number_format(($sale->servicesTotalPrice() * 10) / 100, 2, '.', '')}}</td>
        </tr>
    @endif
    @if(!$hasPromotion && $sale->kids_promotion > 0)
        <tr>
            <td colspan="2" align="right"><strong>Promocion dia del niño</strong></td>
            @if($sale->kids_promotion == 50)
                <?php $amount = (($sale->getKidsPromotionService()->price * 50) / 100) * $sale->getKidsPromotionService()->qty; ?>
            @else
                <?php $amount = (($sale->getKidsPromotionService()->price * 100) / 100) * $sale->getKidsPromotionService()->qty; ?>
            @endif
            <td>$ {{number_format($amount, 2, '.', '')}}</td>
        </tr>
    @endif
    <tr>
        <td colspan="2" align="right"><strong>A pagar</strong></td>
        <td>$ 
        @if($sale->is_courtesy)
            {{$subtotal + $sale->tip}}
        @else
            {{$sale->total}}
        @endif
        </td>
    </tr>
    <?php $total = 0; ?>
    @foreach($sale->payments as $payment)
    <tr>
        <td colspan="2" align="right">
            <strong>{{$payment->method}}
            @if($payment->type == 1)
                (Anticipo)
            @endif
            </strong>
        </td>
        <td>$ {{$payment->total}} {{$payment->coin}}</td>
        <?php $total += $payment->total; ?>
    </tr>
    @endforeach

    @if($sale->is_courtesy)
    <tr>
        <td colspan="2" align="right"><strong>Cortesía</strong></td>
        <td>$ 
        @if($sale->is_courtesy)
            {{$subtotal}}
        @else
            {{$sale->subtotal}}
        @endif
        </td>
    </tr>
    @endif

    <tr>
        <td colspan="2" align="right"><strong>Tipo de cambio</strong></td>
        <td>$ {{$sale->exchangeRate->rate}}</td>
    </tr>

    <tr>
        <td colspan="2" align="right"><strong>Cambio</strong></td>
        <td>$ 
        @if($sale->is_courtesy)
            {{$total - $sale->tip}}
        @else
            {{$total - $sale->total}}
        @endif
        </td>
    </tr>
</table>

<p>
    <strong>Formas de pago:</strong>
    @foreach($sale->payments as $payment)
        {{$payment->method}} {{$payment->coin}} ,
    @endforeach
</p>
@if($sale->payments()->where('method', 'Tarjeta')->first() )
    <p><strong>Numero de tarjeta:</strong> **** **** **** {{ $sale->last_four }}</p>
@endif
<p><strong>Cajero:</strong> @if($sale->cashRegister && $sale->cashRegister->employee){{ $sale->cashRegister->employee->short_name }} @endif</p>
@if($sale->employee)
    <p><strong>Atendido por:</strong> {{ $sale->employee->short_name }}
    @if($sale->employee2 != null)
        ,{{ $sale->employee2->short_name }}
    @endif
    </p>
@endif
@if($sale->customer != null)
    <p><strong>Usted lleva:</strong> {{ $sale->customer->visits }} visitas</p>
@endif
<h2><strong>Folio:</strong> {{$sale->subsidiary->key}} - {{ $sale->folio }}</h2>
<p><strong>{{ $text }}</strong></p>
@if($sale->canceled_by)
    <p><strong>Cancelado</strong></p>
    <p><strong>Fecha de cancelacion:</strong> {{ $sale->canceled_at->format('d-m-Y h:i a') }}</p>
    <p><strong>Cancelado por:</strong> {{ $sale->canceler->short_name }} ({{ $sale->canceler->job }})</p>
@endif

@if($sale->repair)
    <p><strong>Reparado</strong></p>
    <p><strong>Fecha de reparación:</strong> {{ $sale->repaired_at->format('d-m-Y h:i a') }}</p>
    <p><strong>Reparado por:</strong> {{ $sale->repaired->short_name }}</p>
@endif

@if($sale->image != null && $user->isA('super-admin')) 
    <img src="{{$sale->image->path}}" alt="{{$sale->image->name}}" style="width: 80%;">
@endif

<p>{{ $configuration->text_footer }}</p>
<p>SUGERENCIAS EN www.lionbarbershop.com/sugerencias</p>

<hr>
