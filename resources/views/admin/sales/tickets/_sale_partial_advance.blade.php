<p><strong>Fecha:</strong> {{ $sale->created_at->format('d-m-Y') }}</p>
<p><strong>Hora:</strong> {{ $sale->created_at->format('h:i a') }}</p>
<p><strong>Comprobante de anticipos</strong><br/>
    <strong>Cliente(s):</strong> 
    @if($sale->customer_subsidiary_id != null)
        {{ $sale->customerSubsidiary->name }}
    @else
        @if($sale->customer_id != null)
            {{ $sale->customer->name }}
        @else
            {{ $sale->comments }}
        @endif
    @endif
</p>
<?php $wash_quantity = $sale->washQuantity(); ?>
<table width="100%">
    <tr>
        <th>Cantidad</th>
        <th>Producto / Servicio</th>
        <th>Importe</th>
    </tr>
    <?php 
        $subtotal = 0;
    ?>
    @foreach($sale->services as $sale_service)
        <tr>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale_service->service_id == 23 && $wash_quantity <= 5)
                    1-5 kg
                @else
                    {{$sale_service->qty}}
                @endif
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_service->service->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale_service->has_promotion)
                    {{$sale_service->service->sell_price * $sale_service->qty}}
                @else
                    @if($sale->is_courtesy)
                        {{$sale_service->original_price * $sale_service->qty}}
                    @else
                        @if($sale_service->service_id == 23 && $wash_quantity <= 5)
                            {{$sale_service->price * 5}}
                        @else
                            {{$sale_service->price * $sale_service->qty}}
                        @endif
                    @endif
                @endif
                </span>
            </td>
        </tr>
        @php 
            $subtotal += $sale_service->original_price;
        @endphp
    @endforeach
    @foreach($sale->products as $sale_product)
        <tr>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->qty}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->product->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->product->sell_price * $sale_product->qty}}
                </span>
            </td>
        </tr>
        @php 
            $subtotal += $sale_product->original_price;
        @endphp
    @endforeach
    @foreach($sale->packages as $sale_package)
        <tr>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->quantity}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->price * $sale_package->quantity}}
                </span>
            </td>
        </tr>
        @php 
            $subtotal += $sale_package->price;
        @endphp
    @endforeach
    <tr>
        <td colspan="2" align="right"><strong>Subtotal</strong></td>
        <td> $ {{$sale->subtotal}}</td>
    </tr>
    @if($sale->tip != null && $sale->tip > 0)
        <tr>
            <td colspan="2" align="right"><strong>Propina</strong></td>
            <td>$ {{$sale->tip}}</td>
        </tr>
    @endif
    
    <?php $total = 0; ?>
    @foreach($sale->payments as $payment)
        @if($payment->type == 1)
        <tr>
            <td colspan="2" align="right"><strong>{{$payment->method}}</strong></td>
            <td>$ {{$payment->total}} {{$payment->coin}}</td>
            <?php $total += $payment->total; ?>
        </tr>
        @endif
    @endforeach

    <tr>
        <td colspan="2" align="right"><strong>Tipo de cambio</strong></td>
        <td>$ {{$sale->exchangeRate->rate}}</td>
    </tr>
</table>

<p>
    <strong>Formas de pago:</strong>
    @foreach($sale->payments as $payment)
        {{$payment->method}} {{$payment->coin}} ,
    @endforeach
</p>
@if($sale->payments()->where('method', 'Tarjeta')->first())
    <p><strong>Numero de tarjeta:</strong> **** **** **** {{ $sale->last_four }}</p>
@endif
<p><strong>Cajero:</strong> {{ $sale->cashRegister->employee->short_name }}</p>
@if($sale->employee)
    <p><strong>Atendido por:</strong> {{ $sale->employee->short_name }}</p>
@endif
<h2><strong>Folio:</strong> {{$sale->subsidiary->key}} - {{ $sale->folio }}</h2>
<p><strong>{{ $text }}</strong></p>

<p>{{ $configuration->text_footer }}</p>
<p>SUGERENCIAS EN www.lionbarbershop.com/sugerencias</p>

<hr>