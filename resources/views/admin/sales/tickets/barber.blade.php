@extends('layouts.tickets')


@section('content')
    @if($sale->employee)
    @section('barber')
        <p><strong>Fecha y hora de llegada del cliente:</strong> {{ $sale->created_at->format('d-m-Y h:i a') }}</p>
        <p><strong>Fecha y hora que se le comenzo a atende:</strong> {{ $sale->attended_at->format('d-m-Y h:i a') }}</p>
        <p><strong>Fecha y hora que termino el servicio:</strong> {{ $sale->finished_at->format('d-m-Y h:i a') }}</p>
        <p><strong>Tiempo que duro en espera:</strong> {{ $sale->created_at->diffInMinutes($sale->attended_at) }} minutos</p>
        <p><strong>Tiempo que duro el servicio:</strong> {{ $sale->attended_at->diffInMinutes($sale->finished_at) }} minutos</p>

        <table width="100%">
            <tr>
                <td align="center" colspan="2">Servicios efectuados</td>
            </tr>
            @foreach($sale->services as $sale_service)
                <tr>
                    <td align="right"><strong>{{$sale_service->service->name}}</strong></td>
                    <td>{{$sale_service->price * $sale_service->qty}}</td>
                </tr>
            @endforeach
            <tr>
                <td align="center" colspan="2">Productos vendidos</td>
            </tr>
            @foreach($sale->products as $sale_product)
                <tr>
                    <td align="right"><strong>{{$sale_product->product->name}}</strong></td>
                    <td>{{$sale_product->price * $sale_product->qty}}</td>
                </tr>
            @endforeach
            <tr>
                <td align="right"><strong>Propina</strong></td>
                <td>{{$sale->tip}}</td>
            </tr>
            <tr>
                <td align="right"><strong>A pagar</strong></td>
                <td>{{$sale->total}}</td>
            </tr>
        </table>
        
        <p><strong>Cajero:</strong> {{ $sale->cashRegister->employee->short_name }}</p>
        <p><strong>Barbero:</strong> {{ $sale->employee->short_name }}</p>
        <p><strong>Cliente:</strong> {{ $sale->customer->name }}</p>
        <p><strong>Folio:</strong> {{$sale->subsidiary->key}} - {{ $sale->folio}}</p>
        @if($sale->canceled_by)
            <p><strong>Cancelado</strong></p>
            <p><strong>Fecha de cancelacion:</strong> {{ $sale->canceled_at->format('d-m-Y h:i a') }}</p>
            <p><strong>Cancelado por:</strong> {{ $sale->canceler->short_name }} ({{ $sale->canceler->job }})</p>
        @endif
        <div class="sign text-area">
            <p>Firma</p>
        </div>
    @endsection
@endif
@endsection

@section('sign')
    <div class="sign text-area">
        <h4>Firma</h4>
    </div>
@endsection