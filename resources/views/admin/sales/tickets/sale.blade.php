@php
$is_customer_print = isset($customer_print) && $customer_print == true;
$extends = $is_customer_print ? 'layouts.tickets_customer' : 'layouts.tickets';
$text = $is_customer_print ? '' : 'Original';
@endphp
@extends($extends)

@section('subsidiary')
    {{ $sale->subsidiary->name }}
@endsection

@section('fecha')
    {{ $sale->created_at->format('d-m-Y h:i a')}}
@endsection

@section('address')
    {{ $sale->subsidiary->address }}
@endsection

@section('content')
    @php
        if($sale->subsidiary->configuration_id != null) {
            $configuration = App\Configuration::find($sale->subsidiary->configuration_id);
        } else {
            $configuration = App\Configuration::first();
        }
        $imagepath = $configuration->image->path;
    @endphp

    @if(isset($is_advance) && $is_advance)
        @include('admin.sales.tickets._sale_partial_advance', ['text' => ''])
    @else
        @include('admin.sales.tickets._sale_partial', ['text' => ''])
    @endif

@endsection
