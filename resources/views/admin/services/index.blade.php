@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Menu</div>

                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{route('products.index')}}">Productos</a>
                                <ul>
                                    <li>
                                        <a href="{{route('products.create')}}">Crear productos</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{route('services.index')}}">Servicios</a>
                                <ul>
                                    <li>
                                        <a href="{{route('services.create')}}">Crear servicio</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Servicios</div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Precio venta</th>
                                    <th>Comisión</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($services as $service)
                                    <tr>
                                        <td>{{ $service->key }}</td>
                                        <td>{{ $service->name }}</td>
                                        <td>{{ $service->sell_price }}</td>
                                        <td>{{ $service->commission }}</td>
                                        <td><a href="{{ route('services.edit', $service->id) }}">Editar</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $services->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
