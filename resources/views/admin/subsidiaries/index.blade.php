@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Menu</div>

                    <div class="panel-body">
                        <ul class="list_unstyled">
                        <li>
                            <a href="{{route('subsidiaries.create')}}">Agregar sucursal</a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Sucursales</div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Razón social</th>
                                    <th>Estatus</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subsidiaries as $subsidiary)
                                    <tr>
                                        <td>{{ $subsidiary->key }}</td>
                                        <td>{{ $subsidiary->name }}</td>
                                        <td>{{ $subsidiary->razon_social }}</td>
                                        <td>
                                            <?php 
                                                if($subsidiary->is_active){
                                                    echo "Activo";
                                                } else {
                                                    echo "Inactivo";
                                                }
                                            ?>
                                        </td>
                                        <td><a href="{{ route('subsidiaries.edit', $subsidiary->id) }}">Editar</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $subsidiaries->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
