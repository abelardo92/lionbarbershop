<li><a href="#" data-toggle="modal" data-target="#modal-employees">Asignar empleados</a></li>
<div class="modal fade" id="modal-employees" tabindex="-1" role="dialog" aria-labelledby="modal-employeesLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-employeesLabel">Asignar empleados</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('subsidiaries.employees', $subsidiary->id) }}" method="post">
          {{{ csrf_field() }}}

          @foreach($employees as $employee)
            <div class="checkbox">
              <label>
                <input type="checkbox" value="{{ $employee->id }}" name="employees[]">
                {{ $employee->name }}
              </label>
            </div>
          @endforeach

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Asignar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>