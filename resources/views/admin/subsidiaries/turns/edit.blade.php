@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Turnos
                    <a href="{{route('subsidiaries.edit', $subsidiary_id)}}" class="btn btn-primary btn-xs">regresar a sucursal</a>
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('subsidiaries.turns.update', [$subsidiary_id, $turn->id])}}" method="POST" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('PUT') }}}
                                <div class="form-group">
                                    <label for="identifier" class="col-sm-4 control-label">Identificador *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="identifier" name="identifier" required="required" value="{{ old('identifier', $turn->identifier) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $turn->name) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="start" class="col-sm-4 control-label">Inicio</label>
                                    <div class="col-sm-6">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text" class="form-control timeinput" id="start" name="start" value="{{ old('start', $turn->start) }}">
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="end" class="col-sm-4 control-label">Fin</label>
                                    <div class="col-sm-6">
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text" class="form-control timeinput" id="end" name="end" value="{{ old('end', $turn->end) }}">
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="is_rest" class="col-sm-4 control-label">Descanso</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="is_rest" name="is_rest">
                                        <option @if(!$turn->is_rest) selected="selected" @endif value="0">No</option>
                                        <option @if($turn->is_rest) selected="selected" @endif value="1">Si</option>
                                      </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
