@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Preguntas > Agregar
                    <p class="pull-right">
                        <a href="{{ route('questions.index') }}" class="btn btn-default btn-xs">Cancelar</a>
                    </p>
                </div>

                <div class="panel-body">
                    <form method="post" action="{{route('questions.store')}}" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 text-right">Texto</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="title" id="title" value="{{old('title')}}">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('input_type') ? ' has-error' : '' }}">
                            <label for="input_type" class="col-md-4 text-right">Tipo de pregunta</label>
                            <div class="col-md-8">
                                <select class="form-control js-input-type-select" name="input_type" id="input_type">
                                    @foreach(\App\Surveys\Question::INPUT_TYPES as $inputType)
                                        <option
                                            @if(old('input_type') == $inputType) selected="selected" @endif
                                            value="{{$inputType}}"
                                        >
                                            {{trans('bocsi.input_types.'.$inputType)}}
                                        </option>
                                    @endforeach 
                                </select>
                                @if ($errors->has('input_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('input_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-md-4 text-right">Sucursal</label>
                            <div class="col-md-8">
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    <option value="0">Todas</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option
                                            @if(old('subsidiary_id') == $subsidiary->id) selected="selected" @endif
                                            value="{{$subsidiary->id}}"
                                        >
                                            {{ $subsidiary->name }}
                                        </option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="required" class="col-md-4 text-right">¿Requerido?</label>
                            <div class="col-md-8">
                                <input type="hidden" name="required" id="required" value="0">
                                <input type="checkbox" name="required" id="required" value="1" @if(old('required', 1) == 1) checked="checked" @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="active" class="col-md-4 text-right">Se muestra en la encuesta</label>
                            <div class="col-md-8">
                                <input type="hidden" name="active" id="active" value="0">
                                <input type="checkbox" name="active" id="active" value="1" @if(old('active', 1) == 1) checked="checked" @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="for_barber" class="col-md-4 text-right">Para los barberos</label>
                            <div class="col-md-8">
                                <input type="hidden" name="for_barber" id="for_barber" value="0">
                                <input type="checkbox" name="for_barber" id="for_barber" value="1" @if(old('for_barber', 1) == 1) checked="checked" @endif>
                            </div>
                        </div>

                        <div class="js-options @if(!in_array(old('input_type', 'text'), ['checkbox', 'radio'])) display-none @endif">
                            <hr>
                            <div class="col-md-8 col-md-offset-4">
                                <h4>
                                    Opciones <button type="button" class="btn btn-info btn-xs js-add-option">Agregar</button>
                                </h4>
                                @if ($errors->has('options'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('options') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="js-options-container">
                                @foreach(old('options', []) as $key => $text)
                                    <div class="form-group" id="option-{{$key}}">
                                        <label for="{{$key}}" class="col-md-4 text-right">
                                            <button type="button" class="btn btn-link btn-xs js-remove-option" data-id="option-{{$key}}">x</button>
                                            Opción
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="options[]" id="{{$key}}" value="{{$text}}" required>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <a href="{{route('questions.index')}}" class="btn btn-default" >Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="/js/questions_handler.js"></script>
    <script>
        $(function () {
            QuestionsHandler.init();
        });
    </script>
@endsection
