@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Encuestas
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Sucursal</th>
                                <th>Folio de venta</th>
                                <th>Cajera</th>
                                <th>barbero</th>
                                <th>Completada</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($surveys as $survey)
                                <tr>
                                    <td>{{ $survey->code}}</td>
                                    <td>{{ $survey->subsidiary->name}}</td>
                                    <td>{{ $survey->sale->folio }}</td>
                                    <td>{{ $survey->cashier->short_name}}</td>
                                    <td>{{ $survey->employee->short_name}}</td>
                                    {{-- <td>{{ $survey->orden }}</td> --}}
                                    <td><span class="label label-success">{{ $survey->completed ? 'Si' : 'No' }}</span></td>
                                    <td><a href="{{ route('admin.surveys.show', $survey->id) }}">Detalles</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $surveys->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
