<div class="panel panel-default">
    <div class="panel-heading">
        {{$question->title}}
        @if(!$question->required) <small>(Opcional)</small> @endif
    </div>
    <div class="panel-body">
        @foreach($question->options() as $option)
            <div class="well well-sm">
                <div class="checkbox">
                    <label>
                        <input 
                            type="checkbox" 
                            @if(in_array($option, explode('|', $answer))) checked="checked" @endif
                            disabled 
                        >
                        {{$option}}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
</div>