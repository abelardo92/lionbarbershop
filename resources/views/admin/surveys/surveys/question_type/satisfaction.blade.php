<div class="panel panel-default">
    <div class="panel-heading">
        {{$question->title}}
        @if(!$question->required) <small>(Opcional)</small> @endif
    </div>

    <div class="panel-body flex-center">
        @foreach([ 'excelente', 'bueno', 'regular', 'malo', 'muy-malo'] as $option)
            <div class="emoji @if(ucwords(str_replace('-', ' ', $option)) == $answer) checked @endif ">
                <label for="question-{{$question->id}}-option-{{$option}}" class="check" style="position: relative;">
                    @if(ucwords(str_replace('-', ' ', $option)) == $answer)
                        <p class="checkmark" style="position: absolute; font-size: 200px; top: -105px; width: 100%;">&times;</p>
                    @endif
                    <img src="{{asset("images/survey/{$option}.jpg")}}" width="100%">
                    <span>{{ucwords(str_replace('-', ' ', $option))}}</span>
                </label>
            </div>
        @endforeach
    </div>
</div>