@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Encuestas > Detalles
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <p>Codigo: <small>{{ $survey->code}}</small></p>
                            <p>Sucursal: <small>{{ $survey->subsidiary->name}}</small></p>
                            <p>Folio de venta: <small>{{ $survey->sale->folio}}</small></p>
                        </div>
                        <div class="col-md-3">
                            <p>Cajera: <small>{{ $survey->cashier->short_name}}</small></p>
                            <p>Barbero: <small>{{ $survey->employee->short_name}}</small></p>
                        </div>
                        <div class="col-md-5">
                            <p>Fecha/Hora inicio: <small>{{ $survey->started_at !== null ? $survey->started_at->format('d/m/Y h:i a') : '' }}</small></p>
                            @if($survey->finished_at !== null)
                                <p>Fecha/Hora fin: <small>{{ $survey->finished_at->format('d/m/Y h:i a')}}</small></p>
                                <p>Tiempo total (minutos): <small>{{ $survey->finished_at->diffInMinutes($survey->started_at)}}</small></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <h4>Preguntas y respuestas</h4>

            @foreach($survey->questions as $question)
                @include("admin.surveys.surveys.question_type.{$question->input_type}", [
                    'question' => $question, 
                    'answer' => $question->pivot->answer
                ])
            @endforeach
        </div>
    </div>
</div>
@endsection
