@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Usuarios
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('users.update', $user->id)}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre *</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="name" name="name" required="required" value="{{old('name', $user->name)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo *</label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" id="email" name="email" required="required" value="{{ old('email', $user->email) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-sm-4 control-label">Contraseña</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="subsidiary_id" name="subsidiary_id">
                                    <option value="">Seleccione uno por favor...</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option @if($user->subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role_id" class="col-sm-4 control-label">Rol *</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="role_id" name="role_id" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    @foreach($roles as $role)
                                        <option @if($user->roles->first()->id == $role->id) selected="selected" @endif value="{{$role->name}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="show_auxiliar_contable">
                            <label for="role_id" class="col-sm-4 control-label">Clientes permitidos *</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="allowed_accounting_customers" name="allowed_accounting_customers[]" multiple="multiple" class="select2 form-control">
                                    <option value="">Seleccione uno por favor...</option>
                                    @foreach($accounting_customers as $customer)
                                        <option @if($user->containsAccountingCustomer($customer->id)) selected="selected" @endif value="{{$customer->id}}">{{$customer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="{{route('users.create')}}" class="btn btn-default">Crear otro</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#allowed_accounting_customers').select2();

            if($("#role_id").val() == "auxiliar-contable") {
                $("#show_auxiliar_contable").show();
            } else {
                $("#show_auxiliar_contable").hide();
            }

            $('#role_id').on('change', function (evt) {
                if($("#role_id").val() == "auxiliar-contable") {
                    $("#show_auxiliar_contable").show();
                } else {
                    $("#show_auxiliar_contable").hide();
                }
            });

        })
    </script>
@endsection
