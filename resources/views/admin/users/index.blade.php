@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Menu</div>

                <div class="panel-body">
                    <ul class="list_unstyled">
                        <li>
                            <a href="{{route('users.create')}}">Agregar usuario</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Usuarios</div>

                <div class="panel-body">
                    <form method="GET" action="{{ url('home/users') }}">
                        <div class="pull-right">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="filter" placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Sucursal</th>
                                <th>Rol</th>
                                @if($currentUser->isA('super-admin', 'manager', 'super-admin-restringido'))
                                <th>Opciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <?php $role = $user->roles->first() ? $user->roles->first()->name : '' ?>
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->subsidiary ? $user->subsidiary->name : ''}}</td>
                                    <td>{{ $role }}</td>
                                    <td>
                                        @if($currentUser->isA('super-admin', 'manager', 'super-admin-restringido'))
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-link">Editar</a>
                                        @endif
                                        @if(Auth::user()->isA('super-admin'))
                                        <form action="{{route('users.deactivate', $user->id)}}" method="post">
                                            {{{csrf_field()}}}
                                            <input type="submit" class="btn btn-link" value="Eliminar">
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    @if(isset($filter))
                        {{ $users->appends(['filter' => $filter])->links() }}
                    @else
                        {{ $users->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
