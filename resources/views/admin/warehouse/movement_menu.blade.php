
<div class="panel panel-default">
    <div class="panel-heading">Menu</div>
    <div class="panel-body">
        <ul class="list-unstyled">
            <li>
                <a href="{{route('warehouse.movements.index')}}">Entradas</a>
                <ul>
                    <li>
                        <a href="{{route('warehouse.movements.createEntry')}}">Crear Entrada</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('warehouse.products.index')}}">Productos</a>
                <ul>
                    <li>
                        <a href="{{route('warehouse.products.create')}}">Crear Producto</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="/home/inventories/warehouse/adjustments">Ajustes</a>
                @if(Auth::user()->isA('super-admin'))
                <ul>
                    <li>
                        <a href="/home/inventories/warehouse/adjustments/create">Crear Ajuste</a>
                    </li>
                </ul>
                @endif
            </li>
            
            <li>
                <a href="{{route('warehouse.requests.pending')}}">Pedidos pendientes</a>
            </li>
            <li>
                <a href="{{route('warehouse.requests.finished')}}">Pedidos finalizados</a>
            </li>
        </ul>
    </div>
</div>
        