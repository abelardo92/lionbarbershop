@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form class="form-horizontal" action="{{route('warehouse.movements.store')}}" method="POST">
                                {{{ csrf_field() }}}
                                <input type="hidden" name="request_id" value="{{$warehouseRequest->id}}">
                                <div class="form-group">
                                    <label class="control-label col-md-offset-1"> Lista de productos de pedido (Folio {{ $warehouseRequest->folio }}, Sucursal {{ $warehouseRequest->subsidiary->name }})</label>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio unitario</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach($warehouseRequest->products as $warehouseProduct)
                                            <tr>
                                                <td>{{ $warehouseProduct->product->code }}</td>
                                                <td>{{ $warehouseProduct->product->name }}</td>
                                                <td>{{ $warehouseProduct->quantity }}</td>
                                                <td>{{ $warehouseProduct->amount }}</td>
                                                <td>{{ $warehouseProduct->amount * $warehouseProduct->quantity }}</td>
                                            </tr>
                                            @php
                                                $total += $warehouseProduct->amount * $warehouseProduct->quantity;
                                            @endphp
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Total:</b></td>
                                                <td>{{ $total }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-6"> Agrega los productos a entregar (click en botón "Agregar")</label>
                                    <div class="col-sm-4">
                                        <select name="product" id="product" class="select2 form-control">
                                        @foreach($warehouseProducts as $warehouseProduct)
                                            <option value="{{$warehouseProduct->id}}" data-value="{{$warehouseProduct->amount}}" data-name="{{$warehouseProduct->name}}" data-code="{{$warehouseProduct->code}}" 
                                            @if($warehouseRequest->containsProduct($warehouseProduct->id))
                                                disabled="disabled"
                                            @endif
                                            >{{$warehouseProduct->name}} ({{$warehouseProduct->code}})</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="button" class="btn btn-success" id="addProduct" v-on:click="addProduct" value="Añadir"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table class="table" id="productsTable">
                                            <tbody>
                                                <tr>
                                                    <th>Código</th>
                                                    <th>Nombre del producto</th>
                                                    <th>Cantidad</th>
                                                    <th>Opciones</th>
                                                </tr>
                                                @foreach($warehouseRequest->products as $warehouseProduct)
                                                <tr id='product{{$warehouseProduct->product->id}}'>
                                                    <td>{{ $warehouseProduct->product->code }}</td>
                                                    <td>
                                                        <input type='hidden' id='product_ids' name='product_ids[]' value='{{$warehouseProduct->product->id}}' />
                                                        <input type='hidden' id='product_amounts' name='product_amounts[]' value='{{$warehouseProduct->product->amount}}' />
                                                        {{ $warehouseProduct->product->name }}
                                                    </td>
                                                    <td><input class='form-control' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='{{ $warehouseProduct->quantity }}'/></td>
                                                    <td><input type="button" class='btn btn-danger' onClick='removeProduct({{$warehouseProduct->product->id}})' value="Borrar"></input></td>
                                                </tr>   
                                                @endforeach     
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-10 col-sm-2">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js"></script>
    <script>
        
        $('#addProduct').on('click', function (evt) {
            if($("#product").val() != null){
                var value = $("#product").val();
                var name = $("#product option:selected").data('name');
                var code = $("#product option:selected").data('code');
                var amountValue = $("#product option:selected").data('value');
                var idInput = "<input type='hidden' id='product_ids' name='product_ids[]' value='"+value+"' /><input type='hidden' id='product_amounts' name='product_amounts[]' value='"+amountValue+"' />";
                var quantityInput = "<input class='form-control' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeProduct("+value+")'>Borrar</button>";
                $("#productsTable > tbody").append("<tr id='product"+value+"'><td>"+code+"</td><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+removeButton+"</td></tr>");
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });
        
        function removeProduct(id){
            $('#product' + id).remove();
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }
        
    </script>
@endsection