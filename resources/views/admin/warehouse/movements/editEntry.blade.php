@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('warehouse.movements.updateEntry', $warehouseMovement->id)}}" method="POST" enctype="multipart/form-data">
                        {{{ csrf_field() }}}
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label"> Descuento:</label>
                            </div>
                            <div class="col-md-4">
                                <input class='form-control' type='number' min='0' id='discount' name='discount' step="0.01" value="{{ $warehouseMovement->discount }}"/>
                            </div>
                            <div class="col-md-2">
                                <label class="control-label"> Proveedor:</label>
                            </div>
                            <div class="col-md-4">
                                <input class='form-control' type='text' id='provider' name='provider' value="{{ $warehouseMovement->provider }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label class="control-label"> Folio de nota:</label>
                            </div>
                            <div class="col-md-4">
                                <input class='form-control' type='text' id='note_folio' name='note_folio' value="{{ $warehouseMovement->note_folio }}" />
                            </div>
                            <div class="col-md-2">
                                <label class="control-label"> Imagen nota:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="file" name="image" id="image" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-10">
                                @if($image = $warehouseMovement->image()->get()->first())
                                    <img src="{{$image->path}}" alt="{{$image->name}}" style="width: 200px;">
                                @endif
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
