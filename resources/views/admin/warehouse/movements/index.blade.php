@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('admin.warehouse.movement_menu')
            </div>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Entradas a almacen</div>

                    <div class="panel-body">
                        <a href="{{ route('warehouse.movements.createEntry') }}">Agregar entrada</a>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Fecha / hora</th>
                                    <th>Proveedor</th>
                                    <th>No. de nota</th>
                                    <th>Ver información</th>
                                    <th>Acciones</th>
                                    <th>Imagen de nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($movements as $movement)
                                    <tr>
                                        <td>{{ $movement->folio }}</td>
                                        <td>{{ $movement->created_at }}</td>
                                        <td>{{ $movement->provider }}</td>
                                        <td>{{ $movement->note_folio }}</td>
                                        <td><a class="btn btn-link" href="{{ route('warehouse.movements.show', $movement->id) }}">Ver información</a></td>
                                        <!-- Acceso a egresos y gerardo reyes a editar entradas -->
                                        @if($user->isA('super-admin') || $user->id == 77 || $user->id == 87) 
                                        <td>
                                            <a class="btn btn-link" href="{{ route('warehouse.movements.editEntry', $movement->id) }}">Editar</a>
                                            @if($user->isA('super-admin'))
                                            <form action="{{route('warehouse.movements.destroy', $movement->id)}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <input type="submit" class="btn btn-link" value="Cancelar">
                                            </form>
                                            @endif
                                        </td>
                                        @endif
                                        <td>
                                            @if($image = $movement->image()->get()->first())
                                                <a class="btn btn-link" href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
