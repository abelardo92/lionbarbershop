@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de productos de entrada
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form class="form-horizontal">
                                {{{ csrf_field() }}}
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"> Lista de producto de entrada (Folio {{ $warehouseMovement->folio }})</label>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio unitario</th>
                                            <th>Precio</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach($warehouseMovement->products as $warehouseProduct)
                                            <?php 
                                                $amount = $warehouseProduct->amount;
                                                if($warehouseProduct->provider_price != null){
                                                    $amount = $warehouseProduct->provider_price;
                                                }
                                            ?>
                                            <tr>
                                                <td>{{ $warehouseProduct->product->code }}</td>
                                                <td>{{ $warehouseProduct->product->name }}</td>
                                                <td>{{ $warehouseProduct->quantity }}</td>
                                                <td>{{ $amount }}</td>
                                                <td>{{ $amount * $warehouseProduct->quantity }}</td>
                                            </tr>
                                            @php
                                                $total += $amount * $warehouseProduct->quantity;
                                            @endphp
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Subtotal:</b></td>
                                                <td>{{ $total }}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Descuento:</b></td>
                                                <td>{{ $warehouseMovement->discount }}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Total:</b></td>
                                                <td>{{ $total - $warehouseMovement->discount }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <a href="{{route('warehouse.movements.index')}}" class="btn btn-default">Regresar</a>
                                    <a href="{{route('warehouse.movements.print', $warehouseMovement->id)}}" class="btn btn-default">Imprimir</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
