@extends('layouts.tickets_blank')

@section('subsidiary')
    @if($warehouseMovement->subsidiary != null)
        {{ $warehouseMovement->subsidiary->name }}
    @endif
@endsection

@section('fecha')
    {{ $warehouseMovement->created_at->format('d-m-Y h:i a')}}
@endsection

@section('address')
    @if($warehouseMovement->subsidiary != null)
        {{ $warehouseMovement->subsidiary->address }}
    @endif
@endsection

@section('content')
    @include('admin.warehouse.movements.tickets._movement_unsupplied_partial', ['text' => 'Original'])
    <br/>
    @include('admin.warehouse.movements.tickets._movement_unsupplied_partial', ['text' => 'Copia'])
@endsection
