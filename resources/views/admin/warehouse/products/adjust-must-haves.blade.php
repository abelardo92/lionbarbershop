@extends('layouts.app')

@section('css')
<style type="text/css">
    @page {
        size: 25cm 35.7cm;
        margin: 5mm 5mm 5mm 5mm; /* change the margins as you want them to be. */
    }
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding-left: 3px;
        padding-right: 3px;
        padding-top: 6px;
        padding-bottom: 6px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ajuste de productos en existencia
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>
                <div class="panel-body">
                    <form action="{{route('warehouse.products.storeMustHaves')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>PRODUCTO</th>
                                    <th>ALMACEN</th>
                                    @foreach($subsidiaries as $subsidiary)
                                    <th>{{ $subsidiary->name }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($warehouseProducts as $product)
                                    <tr>
                                        <td>{{ $product->code }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>
                                            <input type="number" min="0" class="form-control" name="existence[{{$product->id}}]" value="{{$product->warehouse_desired_existence}}" />
                                        </td>
                                        @foreach($subsidiaries as $subsidiary)
                                        <td>
                                            <?php $value = 0; ?>
                                            @if($existence = $product->existences->where('subsidiary_id', $subsidiary->id)->first())
                                                <?php $value = $existence->must_have; ?>
                                            @endif
                                            <input type="number" min="0" class="form-control" name="mustHave[{{$product->id}}][{{$subsidiary->id}}]" value="{{$value}}" />
                                        </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>          
                        </table>
                        <div class="form-group hide-in-print">
                            <div class="col-sm-offset-9 col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
