@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('warehouse.products.update', $product->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}

                                <div class="form-group">
                                    <label for="code" class="col-sm-2 control-label">Codigo *</label>
                                    <div class="col-sm-3">
                                      <input type="hidden" class="form-control" id="prev_code" name="prev_code" required="required" value="{{ $product->code }}">
                                      <input type="text" class="form-control" id="code" name="code" required="required" value="{{ $product->code }}" @if($user->id == 88 || $user->id == 75 || $user->id == 87 || $user->id == 77) disabled @endif>
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre *</label>
                                    <div class="col-sm-3">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ $product->name }}" @if($user->id == 88 || $user->id == 75 || $user->id == 87 || $user->id == 77) disabled @endif>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="use" class="col-sm-2 control-label">Uso *</label>
                                    <div class="col-sm-3">
                                      <select type="text" class="form-control" id="use" name="use" >
                                        @foreach( App\WarehouseProduct::getEnum('uses') as $key => $value)
                                           <option value="{{ $key }}" @if($key == $product->use) selected @endif>{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                    <label for="amount" class="col-sm-2 control-label">Monto</label>
                                    <div class="col-sm-3">
                                      <input type="number" min="0" step="any" class="form-control" id="amount" name="amount" value="{{ $product->amount }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="accounting" class="col-sm-2 control-label">Contabilidad *</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="accounting" name="accounting">
                                        @foreach( App\WarehouseProduct::getEnum('accountings') as $key => $value)
                                           <option value="{{ $key }}" @if($key == $product->accounting) selected @endif >{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                    
                                    <label for="product_id" class="col-sm-2 control-label">Producto correspondiente</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="product_id" name="product_id">
                                        <option value="">Sin producto</option>
                                        @foreach( $products as $prod)
                                           <option value="{{ $prod->id }}" 
                                           @if($product->product_id == $prod->id)
                                            selected="selected"
                                           @endif
                                           >{{ $prod->name }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="type" class="col-sm-2 control-label">Produco para *</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="type" name="type">
                                        @foreach( App\WarehouseProduct::getEnum('types') as $key => $value)
                                           <option value="{{ $key }}" @if($key == $product->type) selected @endif>{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                    <label for="in_storage" class="col-sm-2 control-label">En inventario</label>
                                    <div class="col-sm-3">
                                      <input type="number" min="0" step="any" class="form-control" id="in_storage" name="in_storage" value="{{ $product->in_storage }}">
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-2">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
