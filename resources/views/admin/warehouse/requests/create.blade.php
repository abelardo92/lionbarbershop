@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('warehouse.requests.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        {{{ csrf_field() }}}          
                        <div class="form-group">
                            <div class="col-sm-12">                             
                                <warehouse-request-table
                                    :warehouse_products="{{$warehouseProducts->toJson()}}"
                                    :uses="{{json_encode($uses)}}"
                                    :subsidiary="{{$subsidiary->toJson()}}"
                                    :action="'create'"
                                ></warehouse-request-table>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
