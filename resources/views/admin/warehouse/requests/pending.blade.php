@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pedidos pendientes
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Folio</th>
                                <th>Solicitado por</th>
                                <th>F. confirmación</th>
                                <th>Ver información</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->subsidiary->name }}</td>
                                    <td>{{ $request->folio }}</td>
                                    <td>
                                        @if($request->employee != null)
                                            {{ $request->employee->short_name }}    
                                        @endif
                                    </td>
                                    <td>{{ $request->confirmed_at }}</td>
                                    <td><a href="{{ route('warehouse.requests.show', $request->id) }}">Ver información</a></td>
                                    <td>
                                        @if($request->movements == null || count($request->movements) == 0)
                                            <a href="{{ route('warehouse.movements.create', $request->id) }}">Realizar pedido</a>
                                        @else
                                            @php
                                                $movement = $request->movements->first();
                                            @endphp
                                            @if($movement != null && !$movement->is_confirmed)
                                                <a href="{{ route('warehouse.movements.edit', $movement->id) }}">Editar pedido</a><br/>
                                            @endif
                                            <a href="{{ route('warehouse.movements.print', $movement->id) }}">Imprimir</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
