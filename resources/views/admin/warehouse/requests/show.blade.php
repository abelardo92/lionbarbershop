@extends('layouts.app_blank_print_header')

@section('content')
<div class="show-in-print">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <p>@yield('title', 'Lion Barbershop')</p>
            <p>Sucursal: {{ $warehouseRequest->subsidiary->name }}</p>
            <p>Fecha de elaboración: {{ $warehouseRequest->created_at->formatLocalized('%d/%B/%Y') }}</p>
            @if($warehouseRequest->createdBy != null)
                <p>Elaboró: {{ $warehouseRequest->createdBy->name }}</p>
            @endif
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right">
                        @if($subdomain)
                            @if(!$warehouseRequest->movements->isEmpty())
                                @php
                                $movement = $warehouseRequest->movements->first();
                                @endphp
                                <a href="{{ route('warehouse.movements.printSubsidiary', $movement->id) }}" class="btn btn-primary btn-sm hide-in-print" title="imprimir"><i class="fa fa-print"></i></a>
                            @else
                                <a href="{{ route('warehouse.requests.printSubsidiary', $warehouseRequest->id) }}" class="btn btn-primary btn-sm hide-in-print" title="imprimir"><i class="fa fa-print"></i></a>
                            @endif
                        @else
                            <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                        @endif
                    </p>
                </div>
                <div class="panel-body">
                    <div class="">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label"> Lista de productos de pedido (Folio {{ $warehouseRequest->folio }})</label>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Uso</th>
                                            <th>Cantidad</th>
                                            @if(!$subdomain)
                                            <th>Precio unitario</th>
                                            <th>Precio</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach($warehouseRequest->products as $warehouseProduct)
                                            <tr>
                                                <td>{{ $warehouseProduct->product->code }}</td>
                                                <td>{{ $warehouseProduct->product->name }}</td>
                                                <td>{{ $uses[$warehouseProduct->product->use] }}</td>
                                                <td>{{ $warehouseProduct->quantity }}</td>
                                                @if(!$subdomain)
                                                <td>{{ $warehouseProduct->amount }}</td>
                                                <td>{{ $warehouseProduct->amount * $warehouseProduct->quantity }}</td>
                                                @endif
                                            </tr>
                                            @php
                                                $total += $warehouseProduct->amount * $warehouseProduct->quantity;
                                            @endphp
                                            @endforeach
                                            @if(!$subdomain)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Total:</b></td>
                                                <td>{{ $total }}</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @if(!$warehouseRequest->movements->isEmpty())
                            @php
                                $movement = $warehouseRequest->movements->first();
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-4 control-label"> Lista de productos a entregar (Folio {{ $movement->folio }})</label>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Uso</th>
                                            <th>Cantidad</th>
                                            @if(!$subdomain)
                                            <th>Precio unitario</th>
                                            <th>Precio</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $total = 0;
                                            @endphp
                                            @foreach($movement->products as $warehouseProduct)
                                            <tr>
                                                <td>{{ $warehouseProduct->product->code }}</td>
                                                <td>{{ $warehouseProduct->product->name }}</td>
                                                <td>{{ $uses[$warehouseProduct->product->use] }}</td>
                                                <td>{{ $warehouseProduct->quantity }}</td>
                                                @if(!$subdomain)
                                                <td>{{ $warehouseProduct->amount }}</td>
                                                <td>{{ $warehouseProduct->amount * $warehouseProduct->quantity }}</td>
                                                @endif
                                            </tr>
                                            @php
                                                $total += $warehouseProduct->amount * $warehouseProduct->quantity;
                                            @endphp
                                            @endforeach
                                            @if(!$subdomain)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Total:</b></td>
                                                <td>{{ $total }}</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                @if(!$warehouseRequest->is_confirmed)
                                    <div class="col-sm-8">
                                        <p>Debe confirmar el pedido para enviar al almacen (No se podrá editar una vez confirmado)</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <form action="{{ route('warehouse.requests.confirm', [$warehouseRequest->id]) }}" method="POST">
                                        {{{csrf_field()}}}
                                        <button type="submit" class="btn btn-success">Confirmar</button>
                                        </form>
                                    </div>
                                @endif
                            </div>            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
