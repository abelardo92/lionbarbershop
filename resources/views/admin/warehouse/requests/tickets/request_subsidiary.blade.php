@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$warehouseRequest->subsidiary->name}}
@endsection

@section('content')
    <p><strong>Fecha:</strong> {{date('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{date('H:i a') }}</p>

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th>Producto</th>
            <th>Cantidad</th>
        </tr>
        @foreach($warehouseRequest->products as $index => $product)
            <tr>
                <td align="center">{{$product->product->code}}</td>
                <td align="center">{{$product->product->name}}</td>
                <td align="center">{{$product->quantity}}</td>
            </tr>
        @endforeach
    </table>

    <div>
        <p><strong>Entregó:</strong> _________________________________________</p>
        <p><strong>Recibió:</strong> _________________________________________</p>
    </div>
@endsection