<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
</head>
<body>
    <h4>Hola {{$diary->customer->name}}</h4>
    <p>Te enviamos este correo para notificarte que hoy tienes una cita en Lion Barbershop a las {{$diary->time}}</p>
    <p>Tu codigo de confirmación es: <strong>{{$diary->confirmation_code}}</strong></p>
</body>
</html>