@php
$user = Auth::user();
$employee = $user->employee();
@endphp
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                Información basica
            </div>
            <div class="panel-body">
                @if($employee != null && 
                $image = $employee->getProfileimage())
                    <img src="{{$image->path}}" alt="{{$image->name}}" class="img-square img-responsive">
                @endif

                <div class="form-group">
                    <label for="name" class="control-label">Nombre</label>
                    <p class="form-text-control">{{$employee->name}}</p>
                </div>

                <div class="form-group">
                    <label for="short_name" class="control-label">Nombre corto</label>
                    <p class="form-text-control">{{$employee->short_name}}</p>
                </div>

                <div class="form-group">
                    <label for="email" class="control-label">Correo</label>
                    <p class="form-text-control">{{$employee->email}}</p>
                </div>

                <div class="form-group">
                    <label for="phone" class="control-label">Telefono</label>
                    <p class="form-text-control">{{$employee->phone}}</p>
                </div>

                <div class="form-group">
                    <label for="address" class="control-label">Dirección</label>
                    <p class="form-text-control">{{$employee->address}}</p>
                </div>

                <div class="form-group">
                    <label for="nss" class="control-label">NSS</label>
                    <p class="form-text-control">{{$employee->nss}}</p>
                </div>

                <div class="form-group">
                    <label for="rfc" class="control-label">RFC</label>
                    <p class="form-text-control">{{$employee->rfc}}</p>
                </div>

                <div class="form-group">
                    <label for="curp" class="control-label">CURP</label>
                    <p class="form-text-control">{{$employee->curp}}</p>
                </div>

                <div class="form-group">
                    <label for="marital_status" class="control-label">Estado Civil</label>
                    <p class="form-text-control">{{$employee->marital_status}}</p>
                </div>

                <ul class="list-unstyled">
                        @foreach(App\Employee::DOCUMENTS as $document)
                            <li>
                              @if($doc = $employee->hasDocument($document))
                                <a href="{{$doc->path}}" target="_blank">
                                  <i class="fa fa-check"></i> {{$document}}
                                </a>
                              @else
                                <i class="fa fa-close"></i> {{$document}}
                              @endif
                            </li>
                        @endforeach
                </ul>

            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                Reportes
            </div>
            <div class="panel-body">
                @if($employee->job == 'Barbero')
                    <form action="{{url('/home/reports/weekly/barbers')}}" method="post" id="serviceform">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">
                    </form>
                    
                    <a href="javascript:void(0);" onclick="document.getElementById('serviceform').submit()" class="btn btn-default hide-in-print">Servicios</a>
                @endif
                <a href="/home/operations-manual?type={{$employee->job}}" class="btn btn-default hide-in-print">Manual de operaciones</a>
                <a href="/home/operations-manual?type=Reglamento" class="btn btn-default hide-in-print">Reglamento</a>
                @if($user->isA('cashier'))
                <a href="{{ route('issues.messages.employees.create') }}" class="btn btn-default hide-in-print">Enviar mensaje</a>
                @endif
            </div>
        </div>
        @if($employee->job == 'Barbero')
            @include('admin.employees.partials.dates')
        @endif

        @include('admin.employees.partials.issues_dashboard')
        
        @include('admin.employees.partials.scheluds')

        @include('admin.employees.partials.exchanges')

        @include('admin.employees.partials.disabilities')
        
        @if($employee->job == 'Barbero')
            @php
                $start = request('start', Carbon\Carbon::now()->format('Y-m-d'));
                $end = request('end', Carbon\Carbon::now()->format('Y-m-d'));
            @endphp
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tarjeta de asistencia 
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <input type="hidden" name="employee_id" value="{{$employee->id}}">

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Hora entrada</th>
                                {{-- <th>Puntualidad</th>
                                <th>Asistencia</th>
                                <th>Extra</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employee->schedules()->dates([$start, $end])->orderBy('date', 'asc')->get() as $schedule)
                                <tr>
                                    <td>{{$schedule->date}}</td>
                                    <td>{{$schedule->subsidiary->name}}</td>
                                    <td>
                                        @if($schedule->employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get()->count())
                                            @php
                                                $attendance = $schedule->employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get()->first();
                                            @endphp
                                            {{$attendance->getTime()}}
                                        @endif
                                    </td>
                                    {{-- <td>
                                        @if($schedule->employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get()->count())
                                        
                                            @if (startltqend($attendance->time, $schedule->turn->start))
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @else
                                            <i class="fa fa-close"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if($schedule->employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get()->count())
                                            <i class="fa fa-check"></i>
                                        @else
                                            <i class="fa fa-close"></i>
                                        @endif
                                    </td>
                                    <td></td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
        
    </div>
</div>

@if($user->isA('super-admin')) 
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Horarios por sucursal
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%A %d %B') }} -
                    @if(Carbon\Carbon::now()->format('H:i:s') <= '15:45:00')
                        Turno Matutino
                    @else
                        Turno Vespertino
                    @endif
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Puesto</th>
                            <th>Nombre</th>
                            <th>Asistencia</th>
                            <th>Hora entrada</th>
                            <th>Doble</th>
                            <th>¿Acceso?</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $current_subsidiary = 0; ?>
                        @foreach($subsidiaries as $subsidiary)
                            <?php $schedules = $subsidiary->schedules()->today()->currentTurn($subsidiary->id)->get();?>
                            @if($schedules->count() > 0)
                                @foreach($schedules as $schedule)
                                    @if(!$schedule->employee->active)
                                        @continue
                                    @endif
                                    <tr
                                    @if($current_subsidiary % 2 == 0)
                                        class="active"
                                    @endif
                                    >
                                        <td>{{$subsidiary->name}}</td>
                                        <td>{{$schedule->employee->job}}</td>
                                        <td>{{$schedule->employee->short_name}}</td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                {{$schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->first()->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->turn->identifier == 3)
                                                <i class="fa fa-check"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                <i class="fa fa-unlock"></i>
                                            @else
                                                @if($schedule->can_check)
                                                    <i class="fa fa-unlock"></i>
                                                @else
                                                    <i class="fa fa-lock"></i>
                                                @endif
                                            @endif
                                        </td>
                                   </tr>
                                @endforeach
                                <?php $current_subsidiary++; ?>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif