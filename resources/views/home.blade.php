@extends('layouts.app')

@section('content')
    <?php 
        $show_content = $user->isA('super-admin') ? true : false;
    ?>
    @if($subdomain && (!$user->isA('supervisor', 'Barbero', 'employee')))

        @if($subsidiary->is_laundry)
            <laundry-cash-register 
            imprest="{{$subsidiary->imprest}}"
            :services="{{$services->toJson()}}"
            :products="{{$subsidiary->products()->with('image')->get()->toJson()}}"
            :packages="{{$packages->toJson()}}"
            <?php $minicut = $subsidiary->miniCutPetitions()->latest()->first(); ?>
            @if($minicut != null)
                minicut="{{$minicut->datetime}}"
            @else
                minicut=""
            @endif
            :turns="{{$turns->toJson()}}"
            :barbers="{{$employees->toJson()}}"
            :sales_own="{{$sales->toJson()}}"
            exchange_rate_id="{{$exchange_rate->id}}"
            exchange_rate="{{$exchange_rate->rate}}"
            subsidiary="{{$subsidiary->toJson()}}"
            user="{{$user->toJson()}}"
            :cuts="{{$cuts->toJson()}}"
            >
            </laundry-cash-register>
        @else
            <cash-register 
            imprest="{{$subsidiary->imprest}}"
            :services="{{$services->toJson()}}"
            :products="{{$subsidiary->products()->with('image')->get()->toJson()}}"
            <?php $minicut = $subsidiary->miniCutPetitions()->latest()->first(); ?>
            @if($minicut != null)
                minicut="{{$minicut->datetime}}"
            @else
                minicut=""
            @endif
            :turns="{{$turns->toJson()}}"
            :barbers="{{$employees->toJson()}}"
            :sales_own="{{$sales->toJson()}}"
            :show_content="{{json_encode($show_content)}}"
            exchange_rate_id="{{$exchange_rate->id}}"
            exchange_rate="{{$exchange_rate->rate}}"
            subsidiary_str="{{$subsidiary->toJson()}}"
            subsidiaries_str="{{$subsidiaries->toJson()}}"
            user_str="{{$user->toJson()}}"
            time_average="{{$time_average}}"
            @if($laundry_services != null)
                laundry_services="{{$laundry_services->toJson()}}"
            @endif
            @if($subsidiary->id != 11)
                :cuts="{{$cuts->toJson()}}"
            @endif
            >
            </cash-register>
        @endif
    @endif
    @if(!$user->isA('supervisor', 'Barbero', 'employee'))
        @if(!$user->isA('super-admin'))

        @if($subdomain)
        <div class="modal fade" id="modal-employees-turn" tabindex="-1" role="dialog" aria-labelledby="modal-employees-turn">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modal-finished-services">
                            Empleados del turno
                            @if(Carbon\Carbon::now()->format('H:i:s') <= '16:00:00')
                                matutino
                            @else
                                vespertino
                            @endif
                        </h4>
                    </div>
                    @include('layouts.partials.employees_turn', ['show_header' => false])
                </div>
            </div>
        </div>
        @endif
        <div class="modal fade" id="modal-daily-sales" tabindex="-1" role="dialog" aria-labelledby="modal-daily-sales">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Ventas del día</h4>
                    </div>
                    @include('layouts.partials.daily_sales', ['show_header' => false])
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-sales-turn" tabindex="-1" role="dialog" aria-labelledby="modal-sales-turn">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Ventas por turno</h4>
                    </div>
                    @include('layouts.partials.sales_turn', ['show_header' => false])
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-pendings" tabindex="-1" role="dialog" aria-labelledby="modal-pendings">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Pendientes</h4>
                    </div>
                    @include('layouts.partials.pendings', ['show_header' => false])
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-schedules-subsidiary" tabindex="-1" role="dialog" aria-labelledby="modal-schedules-subsidiary">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Horarios por sucursal</h4>
                    </div>
                    @include('layouts.partials.schedules_subsidiary', ['show_header' => false])
                </div>
            </div>
        </div>

        <reprint-cuts-modal 
            :cuts="{{$cuts->toJson()}}"
            :show_header="false"
        ></reprint-cuts-modal>
        @if($subdomain)
        <repair-service-modal 
            :barbers="{{$employees->toJson()}}"
            :show_header="false"
        ></repair-service-modal>
        @endif
        <reprint-out-ticket-modal
            :show_header="false"
        ></reprint-out-ticket-modal>
        <see-issues-modal
            :show_header="false"
        ></see-issues-modal>
        <reprint-cash-cut-modal
            :subsidiaries="{{$subsidiaries->toJson()}}"
            :turns="{{$turns->toJson()}}"
            :exchange_rate="{{$exchange_rate->rate}}"
            :show_header="false"
        ></reprint-cash-cut-modal>
        <log-in-to-subsidiary-modal
            :subsidiaries="{{$subsidiaries->toJson()}}"
            :show_header="false"
        ></log-in-to-subsidiary-modal>
        <cancel-admin-sale-modal
            :subsidiaries="{{$subsidiaries->toJson()}}"
            :show_header="false"
        ></cancel-admin-sale-modal>
        
        @else
            <div class="container">
                <div class="row">
                    @if($subdomain)
                        <div class="col-md-4">
                        @include('layouts.partials.employees_turn', ['show_header' => true])
                        </div>
                        @if($subsidiary->id != 11)
                            <reprint-cuts 
                                :cuts="{{$cuts->toJson()}}"
                                :show_header="true"
                            ></reprint-cuts>
                            <repair-service 
                                :barbers="{{$employees->toJson()}}"
                                :show_header="true"
                            ></repair-service>
                            <reprint-out-ticket
                                :show_header="true"
                            ></reprint-out-ticket>
                            <see-issues
                                :show_header="true"
                            ></see-issues>
                        @endif
                    @endif
                </div>
            </div>
        @endif
    @endif
    
    @if($user->isA('supervisor', 'cashier-admin'))
        <div class="container">
            @include('supervisor.dashboard')
        </div>
    @endif

    @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
        <div class="container">
            @include('admin.dashboard')
        </div>
    @endif
    
    @if($user->isA('subsidiary-admin'))
        <div class="container">
            @include('subsidiary-admin.dashboard')
        </div>
    @endif

    @if((!$subdomain && $user->isA('employee', 'cashier', 'Barbero')) || ($subdomain && $user->employee() != null && !$user->isA('cashier'))))
        <div class="container">
            @include('employee_dashboard')
        </div>
    @endif

@endsection
