@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($subsidiaries->chunk(2) as $subsidiaries_group)
            <div class="row">
                @foreach($subsidiaries_group as $subsidiary)
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-4">
                                        {{$subsidiary->name}}
                                    </div>
                                    <div class="col-md-5">
                                        <span>Espera estimada: {{ $subsidiary->getTranscurredTimeAverage() }} m</span>
                                    </div>
                                    <div class="col-md-3">
                                        @if($cash_register = $subsidiary->cashRegisters()->open()->today()->first())    
                                            {{$cash_register->turn->name}}
                                        @else
                                            No hay cajas abiertas
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if($cash_register)
                                <?php 
                                    $sales = $cash_register->sales()->with('servicesWithoutWaiting.service')->today()->active()->get();
                                    $subsidiary_id = $subsidiary->id;

                                    $employee_ids = $sales->map(function ($sale) {
                                        return $sale->employee_id;
                                    })->toArray();

                                    if($subsidiary->is_laundry) {
                                        $attendances = App\Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
                                        ->where('subsidiary_id', $subsidiary->id)
                                        ->where('type', '!=', 'salida')
                                        ->today()
                                        ->groupBy('employee_id')
                                        ->get()
                                        ->whereIn('count_attend', [1, 3]);

                                        $employees = $attendances->where('employee.job', 'Lavador');
                                        $employees = $employees->map(function ($key) {
                                        return $key->employee;
                                        })->filter(function ($employee) use ($employee_ids) {
                                        return !in_array($employee->id, $employee_ids);
                                        });
                                    } else {  

                                        $employees = App\Employee::with('todayAttendances')->where('job', 'Barbero')->get();

                                        $employees = $employees->filter(function ($employee) use ($subsidiary_id) {
                                            return $employee->todayAttendances->count() > 0 && 
                                            $employee->todayAttendances->first()->subsidiary_id == $subsidiary_id &&
                                            !$employee->todayAttendances->where('type', 'salida')->first();
                                        });
                                        $employees = $employees->filter(function ($employee) use ($employee_ids) {
                                            return !in_array($employee->id, $employee_ids);
                                        });

                                    }
                                ?>
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Hora</th>
                                                <th>Cliente</th>
                                                <th>Barbero</th>
                                                <th>Servicios</th>
                                                <th>Status</th>
                                                <th>Tiempo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sales->where('status', 'en espera') as $sale)
                                                <tr>
                                                    <td>{{$loop->index}}</td>
                                                    <td>{{$sale->created_at->format('h:i a')}}</td>
                                                    <td>
                                                        {{$sale->customer->name}}
                                                        @if($sale->customer2 != "")
                                                            <br/>{{$sale->customer2}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($sale->employee)
                                                            {{$sale->employee->short_name}}
                                                        @endif
                                                        @if($sale->employee2)
                                                            <br/>{{$sale->employee2->short_name}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @foreach($sale->servicesWithoutWaiting as $service)
                                                            {{ $service->service->name }} ({{ $service->qty }}) <br>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <span class="label label-warning">{{strtoupper($sale->status)}}</span>
                                                    </td>
                                                    <td>
                                                        00:00 m
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($sales->where('status', 'atendiendo') as $sale)
                                                <tr>
                                                    <td>{{$loop->index}}</td>
                                                    <td>{{$sale->created_at->format('h:i a')}}</td>
                                                    <td>
                                                        {{$sale->customer->name}}
                                                        @if($sale->customer2 != "")
                                                            <br/>{{$sale->customer2}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($sale->employee)
                                                            {{$sale->employee->short_name}}
                                                        @endif
                                                        @if($sale->employee2)
                                                            <br/>{{$sale->employee2->short_name}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @foreach($sale->servicesWithoutWaiting as $service)
                                                            {{ $service->service->name }} ({{ $service->qty }}) <br>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <span class="label label-success">{{strtoupper($sale->status)}}</span>
                                                    </td>
                                                    <input class="sale_seconds" type="hidden" id="{{$sale->id}}" value="{{$sale->transcurred_time}}" />
                                                    <td id="sale_timer_{{$sale->id}}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach($sales->where('status', 'finalizado') as $sale)
                                                <tr>
                                                    <td>{{$loop->index}}</td>
                                                    <td>{{$sale->created_at->format('h:i a')}}</td>
                                                    <td>
                                                        {{$sale->customer->name}}
                                                        @if($sale->customer2 != "")
                                                            <br/>{{$sale->customer2}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($sale->employee)
                                                            {{$sale->employee->short_name}}
                                                        @endif
                                                        @if($sale->employee2)
                                                            <br/>{{$sale->employee2->short_name}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @foreach($sale->servicesWithoutWaiting as $service)
                                                            {{ $service->service->name }} ({{ $service->qty }}) <br>
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <span class="label label-danger">{{strtoupper($sale->status)}}</span>
                                                    </td>
                                                    <td>
                                                        {{ $sale->getTranscurredTimeFormatted() }} m
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div style="padding-left: 10px; padding-bottom: 10px;">
                                    Barberos sin servicio:
                                    @foreach($employees as $barber)
                                        <br> 
                                        <span> {{$barber->short_name}} @if($barber->isEating()) (Comiendo) @endif </span>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {  
            this.interval = setInterval(() => {
                $(".sale_seconds").each(function() {
                    var id = $(this).attr('id');
                    var value = $(this).val();

                    var totalSeconds = parseInt(value);
                    var minutes = Math.floor(totalSeconds / 60);
                    var seconds = totalSeconds - (minutes * 60);
                    seconds = seconds < 10 ? "0" + seconds : seconds;  
                    $("#sale_timer_" + id).html(minutes + ":" + seconds + " m");
                    $(this).val(parseInt(value) + 1);
                });
            }, 1000);
        });
    </script>
@endsection