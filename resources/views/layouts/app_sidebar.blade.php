<?php
$configuration = App\Configuration::first();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/lionbarbershop_logo.jpeg') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="/css/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.css" />
    <link href="/css/sidebar.css" rel="stylesheet" />
    <link href="/css/app.css?v={{strtotime('now')}}" rel="stylesheet" />
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'subsidiaries' => [7, 10, 5, 6, 9, 4]
        ]); ?>
    </script>
</head>
<body style="padding-top: 55px;">
    <div id="wrapper">
        <div class="overlay"></div>
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand" style="font-size: 18px;">
                    <a href="{{ url('/home') }}">
                        @if($subdomain)
                            {{$subsidiary->name}}
                        @else
                            LIONBARBERSHOP
                        @endif
                    </a>
                </li>
                @if (!Auth::guest())
                    @include("layouts.partials.nav")
                @endif

                <!-- Right Side Of Navbar -->
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    {{-- <li><a href="{{ url('/register') }}">Register</a></li> --}}
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                <li><a href="{{route('subsidiaries.index')}}">Sucursales</a></li>
                                <li><a href="{{route('users.index')}}">Usuarios</a></li>
                            @endif
                            @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'accountant', 'manager'))
                                <li><a href="{{ route('configurations.index') }}">Configuraciones</a></li>
                            @endif
                            @if(!$subdomain && Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                <li><a href="/home/operations-manual?type=Cajero">Manual de operaciones para cajeros</a></li>
                                <li><a href="/home/operations-manual?type=Barbero">Manual de operaciones para barberos</a></li>
                                <li><a href="/home/operations-manual?type=Supervisor">Manual de operaciones para supervisor</a></li>
                            @endif
                            @if(!$subdomain && !Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                <li><a href="/home/operations-manual?type=Cajero">Manual de operaciones</a></li>
                            @endif
                            <li><a href="/home/operations-manual?type=Reglamento">Reglamento</a></li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </nav>

        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
            <div class="container">
                <div class="row">
                    <!--<div class="col-lg-10 col-lg-offset-1">-->
                    <div class="col-lg-12" style="padding:0px;">
                        @yield('content')
                        <!--
                        <h1>Fancy Toggle Sidebar Navigation</h1>
                        <p>Bacon ipsum dolor sit amet tri-tip shoulder tenderloin shankle. Bresaola tail pancetta ball tip doner meatloaf corned beef. Kevin pastrami tri-tip prosciutto ham hock pork belly bacon pork loin salami pork chop shank corned beef tenderloin meatball cow. Pork bresaola meatloaf tongue, landjaeger tail andouille strip steak tenderloin sausage chicken tri-tip. Pastrami tri-tip kielbasa sausage porchetta pig sirloin boudin rump meatball andouille chuck tenderloin biltong shank </p>
                        <p>Pig meatloaf bresaola, spare ribs venison short loin rump pork loin drumstick jowl meatball brisket. Landjaeger chicken fatback pork loin doner sirloin cow short ribs hamburger shoulder salami pastrami. Pork swine beef ribs t-bone flank filet mignon, ground round tongue. Tri-tip cow turducken shank beef shoulder bresaola tongue flank leberkas ball tip.</p>
                        <p>Filet mignon brisket pancetta fatback short ribs short loin prosciutto jowl turducken biltong kevin pork chop pork beef ribs bresaola. Tongue beef ribs pastrami boudin. Chicken bresaola kielbasa strip steak biltong. Corned beef pork loin cow pig short ribs boudin bacon pork belly chicken andouille. Filet mignon flank turkey tongue. Turkey ball tip kielbasa pastrami flank tri-tip t-bone kevin landjaeger capicola tail fatback pork loin beef jerky.</p>
                        <p>Chicken ham hock shankle, strip steak ground round meatball pork belly jowl pancetta sausage spare ribs. Pork loin cow salami pork belly. Tri-tip pork loin sausage jerky prosciutto t-bone bresaola frankfurter sirloin pork chop ribeye corned beef chuck. Short loin hamburger tenderloin, landjaeger venison porchetta strip steak turducken pancetta beef cow leberkas sausage beef ribs. Shoulder ham jerky kielbasa. Pig doner short loin pork chop. Short ribs frankfurter rump meatloaf.</p>
                        <p>Filet mignon biltong chuck pork belly, corned beef ground round ribeye short loin rump swine. Hamburger drumstick turkey, shank rump biltong pork loin jowl sausage chicken. Rump pork belly fatback ball tip swine doner pig. Salami jerky cow, boudin pork chop sausage tongue andouille turkey.</p>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session()->has('success'))
        <div class="alert alert-info alert-dismissible" role="alert" style="right: 70px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{session('success')}}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" style="right: 70px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
            {!! session('error') !!}
        </div>
    @endif

    <div class="container show-in-print">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <p>@yield('title', 'Lion')</p>
                <p>{{ $configuration->razon_social }}</p>
                <p>{{ $configuration->rfc }}</p>
                <p>{{ $configuration->address }}</p>
                <p>Fecha de impresión: {{ Carbon\Carbon::now()->formatLocalized('%d de %B del %Y') }}</p>
            </div>
        </div>
    </div>
    <div class="hide-in-print">
        <br><br>
    </div>

    @include('admin.auth.modal_auth')
    @include('admin.auth.modal_admin_auth')

    <attendance></attendance>
    <attendance-device></attendance-device>
    <schedules></schedules>
    <schedules-barber
        :barbers="{{$schedules_barbers->toJson()}}"
    ></schedules-barber>


    <!-- Scripts -->
    <script src="{{asset('js/socket.io.js')}}"></script>
    <script src="/js/app.js?v={{strtotime('now')}}"></script>
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.es.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js"></script>
    <script src="/js/enter2tab.js"></script>
    <script src="/js/sidebar.js"></script>


    <script>
        $(function () {
            $("fieldset").enableEnterToTab({ captureTabKey: true });
            $('.timeinput').timepicker({
                icons: {
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down'
                }
            });
            $('.input-daterange').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.datepicker').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.select2').select2({placeholder: "Selecciona uno por favor..."});
            $('[data-toggle="tooltip"]').tooltip();
            $.extend( true, $.fn.dataTable.defaults, {
                "ordering": true
            } );
            $('.datatables').DataTable();
            $('#calendar').fullCalendar({
                lang: 'es',
                header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay'
                },
                events: {
                    url: '/home/reports/calendars',
                    type: 'GET',
                    textColor: '#ffffff',
                    data: function() { // a function that returns an object
                        return {
                            subsidiary_id: $('#subsidiary_id').val()
                        };
                    }
                }
            });
        });
    </script>
    @yield('scripts')
</body>
</html>
