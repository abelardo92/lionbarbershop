
<body>
    <nav class="navbar navbar-default navbar-fixed-top" id="bocsiyow-menu">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if($subdomain)
                       <li class="active">
                           <a href="/home">{{$subsidiary->name}}</a>
                       </li>
                    @endif
                    @if (!Auth::guest())
                        @include("layouts.partials.nav")
                    @endif
               <!-- </ul> -->

                <!-- Right Side Of Navbar -->
               <!-- <ul class="nav navbar-nav navbar-right"> -->
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        {{-- <li><a href="{{ url('/register') }}">Register</a></li> --}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                    <li><a href="{{route('subsidiaries.index')}}">Sucursales</a></li>
                                    <li><a href="{{route('users.index')}}">Usuarios</a></li>
                                @endif
                                @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'accountant', 'manager'))
                                    <li><a href="{{ route('configurations.index') }}">Configuraciones</a></li>
                                @endif
                                @if(!$subdomain && Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                    <li><a href="/home/operations-manual?type=Cajero">Manual de operaciones (cajeros)</a></li>
                                    <li><a href="/home/operations-manual?type=Barbero">Manual de operaciones (barberos)</a></li>
                                    <li><a href="/home/operations-manual?type=Supervisor">Manual de operaciones (supervisor)</a></li>
                                @endif
                                @if(!$subdomain && !Auth::user()->isA('super-admin', 'super-admin-restringido', 'manager'))
                                    <li><a href="/home/operations-manual?type=Cajero">Manual de operaciones</a></li>
                                @endif
                                <li><a href="/home/operations-manual?type=Reglamento">Reglamento</a></li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if(session()->has('success'))
        <div class="alert alert-info alert-dismissible" role="alert" style="right: 70px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {{session('success')}}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" style="right: 70px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
            {!! session('error') !!}
        </div>
    @endif

    <div class="container show-in-print">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <p>@yield('title', 'Lion')</p>
                <p>{{ $configuration->razon_social }}</p>
                <p>{{ $configuration->rfc }}</p>
                <p>{{ $configuration->address }}</p>
                <p>Fecha de impresión: {{ Carbon\Carbon::now()->formatLocalized('%d de %B del %Y') }}</p>
            </div>
        </div>
    </div>
    <div class="hide-in-print">
        <br><br>
    </div>
    <div id="app">
        @yield('content')

        @include('admin.auth.modal_auth')
        @include('admin.auth.modal_admin_auth')

        <attendance></attendance>
        <attendance-device></attendance-device>
        <schedules></schedules>
        <schedules-barber
            :barbers="{{$schedules_barbers->toJson()}}"
        ></schedules-barber>
    </div>


    <!-- Scripts -->
    <script src="{{asset('js/socket.io.js')}}"></script>
    <script src="/js/app.js?v={{strtotime('now')}}"></script>
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.es.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js"></script>
    <script src="/js/enter2tab.js"></script>

    <script>
        $(function () {
            $("fieldset").enableEnterToTab({ captureTabKey: true });
            $('.timeinput').timepicker({
                icons: {
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down'
                }
            });
            $('.input-daterange').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.datepicker').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.select2').select2({placeholder: "Selecciona uno por favor..."});
            $('[data-toggle="tooltip"]').tooltip();
            $.extend( true, $.fn.dataTable.defaults, {
                "ordering": true
            } );
            $('.datatables').DataTable();
            $('#calendar').fullCalendar({
                lang: 'es',
                header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay'
                },
                events: {
                    url: '/home/reports/calendars',
                    type: 'GET',
                    textColor: '#ffffff',
                    data: function() { // a function that returns an object
                        return {
                            subsidiary_id: $('#subsidiary_id').val()
                        };
                    }
                }
            });
        });
    </script>
    @yield('scripts')
</body>
