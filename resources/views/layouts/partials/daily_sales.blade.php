<div class="panel panel-default">
    @if($show_header)
    <div class="panel-heading">
        Ventas del dia
        <p class="pull-right">
            {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
        </p>
    </div>
    @endif

    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Sucursal</th>
                    <th>Importe</th>
                    <th>Servicios</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total = 0;
                $total_services = 0;
                ?>
                @foreach($subsidiaries as $subsidiary)
                    <tr>
                        <td>{{$subsidiary->name}}</td>
                        <td>$ 
                        @if($subsidiary->id == 11)
                            <?php 
                                $subtotal = $subsidiary->originLaundryServices()->today()->notCanceled()->sum('amount');
                            ?>
                        @else
                            <?php $subtotal = 0; ?>
                            @if($subsidiary->is_laundry)
                            <?php
                                $cash_registerss = $subsidiary->cashRegisters()->today()->get();
                                foreach ($cash_registerss as $cr) {
                                    $subtotal += $cr->totalLaundry();
                                }
                            ?>
                            @else
                                <?php 
                                $subsidiary_sales = $subsidiary->todaySales;
                                $subtotal += $subsidiary_sales->sum('subtotal');
                                ?>
                            @endif
                        @endif
                        {{ $subtotal }}
                        <?php $total += $subtotal; ?>
                        </td>
                        <td>
                            @if($subtotal > 0)
                                <?php  
                                $today_services = $subsidiary->getServicesToday();
                                $total_services += $today_services; 
                                ?>
                                @if($today_services > 0)
                                    <a href="#" class="total-services-link" data-toggle="modal" data-target="#modal-subsidiary-sales-{{$subsidiary->id}}">
                                        {{ $today_services }}
                                    </a>
                                @else 
                                    0 
                                @endif
                            @else
                                0
                            @endif
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-subsidiary-sales-{{$subsidiary->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-subsidiary-sales-{{$subsidiary->id}}-Label">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="modal-subsidiary-sales-{{$subsidiary->id}}-Label">Resumen de ventas de {{$subsidiary->name}}</h4>
                                </div>
                                <div class="modal-body services-table">
                                    <div class="row">
                                        <div class="col-sm-1"><b>Ticket#</b></div>
                                        <div class="col-sm-4"><b>Cliente</b></div>
                                        <div class="col-sm-4"><b>Barbero</b></div>
                                        <div class="col-sm-3"><b>Servicios</b></div>
                                    </div>
                                    @foreach ($subsidiary_sales as $subsidiary_sale)
                                        @if($subsidiary_sale->services)
                                        <div class="row">
                                            <div class="col-sm-1">
                                                {{$subsidiary_sale->folio}}
                                            </div>
                                            <div class="col-sm-4">
                                                @if($subsidiary_sale->customer)
                                                    {{$subsidiary_sale->customer->name}}
                                                @endif
                                            </div>
                                            <div class="col-sm-4">
                                                @if($subsidiary_sale->employee)
                                                    {{$subsidiary_sale->employee->name}}
                                                @endif
                                            </div>
                                            <div class="col-sm-3">
                                                @foreach ($subsidiary_sale->services as $subsidiary_service)
                                                    {{-- @if($subsidiary_service->service_id != 19) --}}
                                                        {{$subsidiary_service->service->name}} <b>({{$subsidiary_service->qty}})</b><br>
                                                    {{-- @endif --}}
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <tr>
                    <td>Total:</td>
                    <td>$ {{ $total }}</td>
                    <td>{{ $total_services }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>