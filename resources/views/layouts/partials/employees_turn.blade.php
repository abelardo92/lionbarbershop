<div class="panel panel-default">
    @if($show_header)
    <div class="panel-heading">
        Empleados del turno
        @if(Carbon\Carbon::now()->format('H:i:s') <= '16:00:00')
            matutino
        @else
            vespertino
        @endif
    </div>
    @endif

    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Puesto</th>
                    <th>Nombre</th>
                    <th>Hora entrada</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subsidiary->schedules()->today()->currentTurn($subsidiary->id)->get() as $schedule)
                    <?php $attendances = $schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get(); ?>
                    @if(!$attendances->count())
                        <?php continue; ?>
                    @endif
                    @if($schedule->employee->has_rest)
                        @if($attendances->count() == 4)
                            <?php continue; ?>
                        @endif
                    @else
                        @if($attendances->count() == 2)
                            <?php continue; ?>
                        @endif
                    @endif
                    <tr>
                        <td>{{$schedule->employee->job}}</td>
                        <td>{{$schedule->employee->short_name}}</td>
                        <td>
                            @if($attendances->count())
                                {{$attendances->first()->getTime()}}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>