@php
    $user = Auth::user();
@endphp
@if(!$user->isA('employee', 'subsidiary-admin', 'supervisor', 'cashier-admin', 'auxiliar-contable') && ($subdomain || $user->isA('super-admin', 'super-admin-restringido', 'manager')))
<li><a href="{{route('customers.index')}}">Clientes</a></li>
@endif

@if(!$user->isA('employee', 'subsidiary-admin', 'supervisor', 'auxiliar-contable') && ($subdomain || $user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin')))
<li><a href="{{route('inventories.index')}}">Inventarios</a></li>
@endif

@if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
    <li><a href="{{url('/home/admin/sales')}}">Ventas activas</a></li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'nomina', 'manager', 'cashier-admin'))
    <li><a href="{{route('employees.index')}}">Empleados</a></li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Catálogos
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{url('/home/catalogs/employees')}}">Empleados</a></li>
            <li><a href="{{url('/home/issues/create2')}}">Crear incidencia</a></li>
        </ul>
    </li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'nomina', 'subsidiary-admin', 'accountant', 'supervisor', 'cashier-admin', 'manager'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Reportes
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu" style="max-height: 400px; overflow: scroll;">
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/attendances')}}">Checadas del dia</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/attendance-card')}}">Tarjeta de asistencia</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/issues')}}">Incidencias</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'supervisor', 'cashier-admin', 'manager'))
            <li><a href="{{url('/home/reports/schedules')}}">Horarios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/weekly/paysheet/barber')}}">Semanal por barbero</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/weekly/paysheet/employee')}}">Semanal por empleado</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager'))
            <li><a href="{{url('/home/reports/weekly/paysheet/resume')}}">Resumen</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/paysheets')}}">Nominas</a></li>
            <li><a href="{{url('/home/deposits')}}">Depósitos</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido'))
            <li><a href="{{url('/home/reports/cashCutsByDate')}}">Movimientos de caja (fecha)</a></li>
            <li><a href="{{url('/home/reports/cashierMovementsResume')}}">Resumen movimientos (caja)</a></li>
        @endif

        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
            <li class="active"><a href="javascript:void(0);">Reporte de ventas</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/flow')}}">Flujo</a></li>
            <li><a href="{{url('/home/reports/daily')}}">Diario</a></li>
            <li><a href="{{url('/home/reports/accumulated-daily')}}">Diario acumulado</a></li>
            <li><a href="{{url('/home/reports/daily-turns')}}">Diario por turno</a></li>
            @if($user->isA('super-admin', 'super-admin-restringido'))
            <!-- <li><a href="{{url('/home/reports/daily-laundry-services')}}">Diario por servicios (lavandería)</a></li> -->
            @endif
            <li><a href="{{url('/home/reports/subsidiary')}}">Por sucursal</a></li>
            <li><a href="{{url('/home/reports/subsidiaries')}}">Por sucursales</a></li>
            <li><a href="{{url('/home/reports/barber')}}">Por barbero</a></li>
            <li><a href="{{url('/home/reports/weekly/barbers')}}">Semanal por barbero</a></li>
            <li><a href="{{url('/home/reports/subsidiaries/turns')}}">Por sucursales por turno</a></li>
            <li><a href="{{url('/home/reports/sales/date')}}">Status por fecha</a></li>
            <li><a href="{{url('/home/reports/sales/commission/status')}}">Status de comisiones</a></li>
            <li><a href="{{url('/home/reports/sales/commissions/products')}}">Comisión por productos</a></li>
            <li><a href="{{url('/home/reports/sales/tips')}}">Propinas</a></li>
            <li><a href="{{url('/home/reports/weekly/sales/commision/resume')}}">Comisiones por ventas</a></li>
            <li><a href="{{url('/home/reports/diaries')}}">Citas</a></li>
            <li><a href="{{url('/home/reports/promos')}}">Promociones</a></li>
        @endif
            @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/sales/promotions')}}">Promociones por fecha</a></li>
            @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/calendar')}}">Calendario</a></li>
            <li><a href="{{url('/home/reports/sales/repairs')}}">Descuento por reparaciones</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <!--
            <li class="active"><a href="javascript:void(0);">Reporte de pedidos</a></li>
            <li><a href="{{url('/home/reports/warehouse/requests/unsupplied_orders')}}">Pedidos no surtidos</a></li>
            -->
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin', 'manager'))
            <li class="active"><a href="javascript:void(0);">Rep. de productos y servicios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/services-subsidiaries')}}">Servicios por sucursal</a></li>
            <li><a href="{{url('/home/reports/services-employees')}}">Promedio servicios (barbero)</a></li>
            <li><a href="{{url('/home/reports/weekly/services/barbers')}}">Semanal por barbero</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/services-subsidiary')}}">Promedio servicios (sucursal)</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/products-subsidiary')}}">Productos vendidos (sucursal)</a></li>
            <li><a href="{{url('/home/reports/services/subsidiaries')}}">Resumen de servicios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager'))
            <li class="active"><a href="javascript:void(0);">Reporte de inventarios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/inventories/entries')}}">Entradas</a></li>
            <li><a href="{{url('/home/reports/inventories/departures')}}">Salidas</a></li>
            <li><a href="{{url('/home/reports/inventories/adjustments')}}">Ajustes</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager'))
            <li><a href="{{url('/home/reports/inventories/stocks')}}">Existencias</a></li>
        @endif
        <!-- Acceso a Gerardo Reyes y Brisa -->
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <!--
            <li class="active"><a href="javascript:void(0);">Reporte de inventarios (Almacen)</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/stocks')}}">Existencias</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/productsToOrder')}}">Mercancía a comprar</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/movementsByProduct')}}">Movimientos por producto</a></li>
            -->
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <!--
            <li class="active"><a href="javascript:void(0);">Marketing</a></li>
            <li><a href="{{url('/home/reports/sales/notFrequentCustomers')}}">Clientes no frecuentes</a></li>
            <li><a href="{{url('/home/reports/sales/byCustomer')}}">Ventas por cliente</a></li>
            <li><a href="{{url('/home/reports/customers/byBarber')}}">Clientes por barbero</a></li>

            <li class="active"><a href="javascript:void(0);">Encuestas</a></li>
            <li><a href="{{url('/home/reports/survey/subsidiary')}}">Por Sucursal</a></li>
            <li><a href="{{url('/home/reports/survey/barber')}}">Por Barbero</a></li>
            -->
        @endif
        </ul>
    </li>
@endif

@if(!$user->isA('employee', 'supervisor', 'auxiliar-contable') && ($user->isA('super-admin', 'super-admin-restringido', 'agenda', 'cashier-admin', 'subsidiary-admin', 'cashier-admin') || $subdomain))
<li><a href="{{route('diary.index')}}">Agenda</a></li>
<!-- <li><a href="{{route('admin.sales.activeSales')}}">Pantalla</a></li> -->
@endif
@if($user->isA('super-admin', 'super-admin-restringido', 'accountant'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Erogaciones
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('expenditures.index')}}">Listado</a></li>
            <li><a href="{{route('accounts.index')}}">Cuentas</a></li>

            <li class="active"><a href="javascript:void(0);">Reportes</a></li>
            <li><a href="{{url('/home/reports/expenditures/subsidiary')}}">Gastos por Sucursal</a></li>
            <li><a href="{{url('/home/reports/expenditures/monthly')}}">Gastos mensuales</a></li>
        </ul>
    </li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin', 'auxiliar-contable'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Administracion
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('accounting_customers.index')}}">Ver clientes (Contabilidad)</a></li>
            <li><a href="{{route('accounting_customers.create')}}">Agregar clientes (Contabilidad)</a></li>
        </ul>
    </li>
@endif

<?php $dontShow = false ?>
    @if(!$dontShow)
    <?php
        //$issues_total = App\Issue::unreadedCashierMessagesBySubsidiary($subsidiary->id, $user)->count();
        $issues_total = 0;
        $pendings_total = App\Pending::unreadedMessages($user)->notAdmin()->count();
        $pendings_admin_total = App\Pending::unreadedMessages($user)->admin()->count();
    ?>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Notificaciones
            @if($issues_total > 0 || $pendings_total > 0)
                <span class="badge badge-error">{{ $issues_total + $pendings_total }}</span>
            @endif
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            {{-- <li>
                <a href="{{route('issues.messages.received')}}">Mensajes
                @if($issues_total > 0)
                    <span class="badge badge-error">{{ $issues_total }}</span>
                @endif
                </a>
            </li> --}}
            <li>
                <a href="{{route('pendings.nonFinished')}}">Pendientes
                @if($pendings_total > 0)
                    <span class="badge badge-error">{{ $pendings_total }}</span>
                @endif
                </a>
            </li>
            @if($user->isA('super-admin', 'super-admin-restringido'))
            <li>
                <a href="{{route('pendings.admin.nonFinished')}}">Pendientes (Oficina)
                    @if($pendings_admin_total > 0)
                        &nbsp;<span class="badge badge-error">{{ $pendings_admin_total }}</span>
                    @endif
                </a>
            </li>
            @endif
        </ul>
    </li>
@endif

<?php // $user->isA('super-admin', 'super-admin-restringido', 'supervisor', 'agenda', 'cashier-admin', 'accountant') ?>
@if(false)
    <?php
        $cashier_messages_total = App\Issue::unreadedCashierMessagesByAdmon($user)->count();
        $employee_messages_total = App\Issue::unreadedEmployeeMessagesByAdmon($user)->count();
        $pendings_total = App\Pending::unreadedMessages($user)->count();
        if($user->isA('super-admin', 'super-admin-restringido', 'agenda', 'cashier-admin')) {
            $complains_total = App\Complain::unreadedComplains()->count();
        } else {
            $complains_total = 0;
        }
    ?>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Notificaciones
            @if($cashier_messages_total > 0 || $complains_total > 0 || $pendings_total > 0)
                <span class="badge badge-error">{{ $cashier_messages_total + $employee_messages_total + $complains_total + $pendings_total }}</span>
            @endif
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{route('issues.messages.received')}}">Mensajes (Caja)
                    @if($cashier_messages_total > 0)
                        <span class="badge badge-error">{{ $cashier_messages_total }}</span>
                    @endif
                </a>
            </li>

            <li>
                <a href="{{route('issues.messages.employees.received')}}">Mensajes (Empleados)
                    @if($employee_messages_total > 0)
                        <span class="badge badge-error">{{ $employee_messages_total }}</span>
                    @endif
                </a>
            </li>

            @if(Auth::user()->isA('super-admin', 'super-admin-restringido','supervisor', 'cashier-admin'))
                <li>
                    <a href="{{route('exchanges.index')}}">Intercambio de horarios</a>
                </li>
                <li>
                    <a href="{{route('pendings.nonFinished')}}">Pendientes
                        @if($pendings_total > 0)
                            <span class="badge badge-error">{{ $pendings_total }}</span>
                        @endif
                    </a>
                </li>
            @endif
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido','agenda', 'cashier-admin'))
                <li>
                    <a href="{{route('complains.index')}}">Quejas y sugerencias
                        @if($complains_total > 0)
                            <span class="badge badge-error">{{ $complains_total }}</span>
                        @endif
                    </a>
                </li>
            @endif
        </ul>
    </li>
@endif

@if($subdomain && !$user->isA('super-admin', 'supervisor', 'employee'))
    <li class="dropdown" id="cashier-information" style="display:none">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Información de caja<span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="#" data-toggle="modal" data-target="#modal-reprint-cash-movement">Reimprimir movimientos</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-finished-services">Servicios terminados</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-cancel-sale">Cancelar nota</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-reprint-turn-sale">Reimprimir nota</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-employees-turn">Empleados del turno</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-reprint-cut">Reimprimir cortes</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-repair-service">Reparar servicio</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-reprint-out-ticket">Imprimir ticket de salida</a></li>
        </ul>
    </li>
@endif
<?php //dd("hola" . Request::url()); ?>
@if(!$user->isA('super-admin', 'supervisor', 'employee', 'auxiliar-contable') && isset($showModals))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Acciones<span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="#" data-toggle="modal" data-target="#modal-see-issues">Ver incidencias</a></li>
            @if($user->isA('super-admin-restringido', 'subsidiary-admin'))
            <li><a href="#" data-toggle="modal" data-target="#modal-daily-sales">Ventas del día</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-sales-turn">Ventas por turno</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-cancel-admin-sale">Cancelar/Editar nota</a></li>
            @if(!$user->isA('subsidiary-admin'))
            <li><a href="#" data-toggle="modal" data-target="#modal-pendings">Pendientes</a></li>
            @endif
            <li><a href="#" data-toggle="modal" data-target="#modal-schedules-subsidiary">Horarios por sucursal</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-reprint-cash-cut">Reimprimir cortes de caja</a></li>
            <li><a href="#" data-toggle="modal" data-target="#modal-login-to-subsidiary">Acceder a sucursal</a></li>
            @endif
        </ul>
    </li>
@endif

@if($subdomain && !$user->isA('supervisor', 'employee', 'cashier-admin'))
    <!-- <li><a href="{{route('eats.index', $subsidiary->key)}}">Comidas</a></li> -->
    <li>
        @if($user->isA('super-admin'))
            @if($subsidiary->devices->count() == 0)
                <a href="#" data-toggle="modal" data-target="#modal-clock-attendance">
                    <i class="fa fa-clock-o fa-2x"></i>
                </a>
            @else
                <a href="#" data-toggle="modal" data-target="#modal-clock-attendance-device">
                    <i class="fa fa-clock-o fa-2x"></i>
                </a>
            @endif
        @else
            @if($subsidiary->devices->count() == 0)
                <a href="#" data-toggle="modal" data-target="#modal-clock-attendance"><b>Checar asistencia</b></a>
            @else
                <a href="#" data-toggle="modal" data-target="#modal-clocsk-attendance-device"><b>Checar asistencia</b></a>
            @endif
        @endif
    </li>

    <li>
        @if($user->isA('super-admin'))
        <a href="/home/sales/create" title="Agregar nuevo servicio">
            <i class="fa fa-cart-plus fa-2x"></i>
        </a>
        @else
        <a href="/home/sales/create"><b>Agregar servicio</b></a>
        @endif
    </li>
    <li>
        @if($user->isA('super-admin'))
        <a href="#" data-toggle="modal" data-target="#modal-schedules" title="Imprimir horarios del empleado">
            <i class="fa fa-calendar fa-2x"></i>
        </a>
        @else
        <a href="#" data-toggle="modal" data-target="#modal-schedules"><b>Imprimir horarios</b></a>
        @endif
    </li>
    <li>
        @if($user->isA('super-admin'))
        <a href="#" data-toggle="modal" data-target="#modal-schedules-barber" title="Imprimir horarios del empleado">
            <i class="fa fa-calendar-check-o fa-2x"></i>
        </a>
        @else
        <a href="#" data-toggle="modal" data-target="#modal-schedules-barber"><b>Imprimir horarios (empleado)</b></a>
        @endif
    </li>
@endif
