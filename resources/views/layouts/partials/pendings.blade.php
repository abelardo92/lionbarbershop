<div class="panel panel-default">
    @if($show_header)
    <div class="panel-heading">
        Pendientes
    </div>
    @endif
    <div class="table-wrapper-scroll-y">
        <table class="table">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Sucursal</th>
                    <th>Asunto</th>
                    <th>Ver</th>
                </tr>
            </thead>
            <tbody>
                <?php $total = 0; ?>
                @foreach($pendings as $pending)
                    <tr>
                        <td>{{$pending->date}}</td>
                        <td>
                            @if($pending->subsidiary != null)
                                {{ $pending->subsidiary->name }}
                            @endif
                        </td>
                        <td>{{$pending->subject}}</td>
                        <td>
                            <a class="btn btn-link" href="{{ route('pendings.show', $pending->id) }}">Ver</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>