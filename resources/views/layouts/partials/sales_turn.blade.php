<div class="panel panel-default">
    @if($show_header)
    <div class="panel-heading">
        Ventas por turno
        <p class="pull-right">
            {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
        </p>
    </div>
    @endif
    <div class="table-wrapper-scroll-y">
        <table class="table">
            <thead>
                <tr>
                    <th>Sucursal</th>
                    <th>Importe</th>
                    <th>Turno</th>
                </tr>
            </thead>
            <tbody>
                <?php $total = 0; ?>
                @foreach($cash_registers as $cash_register)
                    <tr>
                        <td>{{$cash_register->subsidiary->name}}</td>
                        <td>
                        <?php
                        if($cash_register->subsidiary->id != 11) {
                            $subtotal = 0;
                            if($cash_register->subsidiary->is_laundry) {
                                $subtotal += $cash_register->totalLaundry();
                            } else {
                                $subtotal += $cash_register->sales()->today()
                                ->finished()->notCanceled()->sum('total');
                            }    
                        } else {
                            $subtotal = $cash_register->laundryServices()->get()->sum('amount');
                        }
                        ?>
                        $ {{$subtotal}}
                        </td>
                        <td>{{$cash_register->turn->name}}</td>
                        
                        <?php 
                            $total += $subtotal;
                        ?>
                    </tr>
                @endforeach
                <tr>
                    <td>Total:</td>
                    <td>$ {{ $total }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>