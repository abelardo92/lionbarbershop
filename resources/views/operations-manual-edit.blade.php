@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Texto
                    </div>
                    <div class="panel-body">
                        <form action="/home/operations-manual/{{$manual->id}}" method="post">
                            {{ csrf_field() }}
                            {{-- <input type="hidden" name="type" value="{{$manual->type}}"> --}}
                            <div class="form-group">
                                <textarea name="text" id="text" cols="30" rows="10" class="form-control" required="required">{{$manual->text}}</textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
