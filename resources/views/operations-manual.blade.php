@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($type == 'Reglamento')
                            {{ucfirst($type)}}
                        @else
                            Manual de procedimientos de {{ucfirst($type)}}
                        @endif
                        @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                            <p class="pull-right">
                                <a href="#do-content">Agregar contenido</a>
                            </p>
                        @endif
                    </div>
                    <div class="panel-body">
                        @foreach($creators as $creator)
                            {{-- <h4>
                                Contenido por
                                <strong> --}}
                                    @if($creator->user)
                                        {{-- {{ $creator->user->name }} --}}
                                        @php
                                            $person = $creator->user;
                                        @endphp
                                    @else
                                        {{-- {{ $creator->cashier->short_name }} --}}
                                        @php
                                            $person = $creator->cashier;
                                        @endphp
                                    @endif
                               {{--  </strong>
                            </h4> --}}
                            <p>
                                @foreach($person->manuals()->where('type', $type)->get() as $manual)
                                    {!! nl2br($manual->text) !!}
                                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                                        <form action="{{url('/home/operations-manual')}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="id" value="{{$manual->id}}">
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-danger btn-xs" value="Eliminar">
                                                <a href="{{url('/home/operations-manual/'.$manual->id)}}" class="btn btn-default btn-xs">Editar</a>
                                            </div>
                                        </form>
                                        <hr>
                                    @endif
                                @endforeach
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                <div class="col-md-8 col-md-offset-2" id="do-content">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Texto
                        </div>
                        <div class="panel-body">
                            <form action="" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="type" value="{{$type}}">
                                <div class="form-group">
                                    <textarea name="text" id="text" cols="30" rows="10" class="form-control" required="required"></textarea>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
