@if($show_content)
<div class="row">
    <div class="col-md-4">
        @include('layouts.partials.daily_sales', ['show_header' => true])
    </div>
    <div class="col-md-4">
        @include('layouts.partials.sales_turn', ['show_header' => true])
    </div>
    <log-in-to-subsidiary
    :subsidiaries="{{$subsidiaries->toJson()}}"
    :show_header="true"
    ></log-in-to-subsidiary>
</div>
<div class="row">
    <div class="col-md-8">
        @include('layouts.partials.schedules_subsidiary', ['show_header' => true])
    </div>

    <reprint-cash-cut
    :subsidiaries="{{$subsidiaries->toJson()}}"
    :turns="{{$turns->toJson()}}"
    :exchange_rate="{{$exchange_rate->rate}}"
    :show_header="true"
    ></reprint-cash-cut>
</div>
@endif