<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ventas del dia
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Importe</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        ?>
                        @foreach($subsidiaries as $subsidiary)
                            <tr>
                                <td>{{$subsidiary->name}}</td>
                                <td>$ 
                                @if($subsidiary->id == 11)
                                    <?php 
                                        $subtotal = $subsidiary->originLaundryServices()->today()->sum('amount');
                                    ?>
                                @else
                                    <?php 
                                        $subtotal = $subsidiary->sales()->today()->finished()->notCanceled()->sum('subtotal');
                                    ?>
                                @endif
                                {{ $subtotal }}
                                <?php $total += $subtotal; ?>
                                </td>
                                
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total:</td>
                            <td>$ {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ventas por turno
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Importe</th>
                            <th>Turno</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($cash_registers as $cash_register)
                            <tr>
                                <td>{{$cash_register->subsidiary->name}}</td>
                                <td>
                                <?php
                                if($cash_register->subsidiary->id != 11){
                                    $subtotal = $cash_register->sales()->today()->finished()->notCanceled()->sum('subtotal');
                                } else {
                                    $subtotal = $cash_register->laundryServices()->get()->sum('amount');
                                }
                                ?>
                                $ {{$subtotal}}
                                </td>
                                <td>{{$cash_register->turn->name}}</td>
                                
                                <?php 
                                    $total += $subtotal;
                                ?>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total:</td>
                            <td>$ {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <log-in-to-subsidiary
    :subsidiaries="{{$subsidiaries->toJson()}}"
    ></log-in-to-subsidiary>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Horarios por sucursal
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%A %d %B') }} -
                    @if(Carbon\Carbon::now()->format('H:i:s') <= '15:45:00')
                        Turno Matutino
                    @else
                        Turno Vespertino
                    @endif
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Puesto</th>
                            <th>Nombre</th>
                            <th>Asistencia</th>
                            <th>Hora entrada</th>
                            <th>Doble</th>
                            <th>¿Acceso?</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $current_subsidiary = 0; ?>
                        @foreach($subsidiaries as $subsidiary)
                            <?php $schedules = $subsidiary->schedules()->today()->currentTurn($subsidiary->id)->get();?>
                            @if($schedules->count() > 0)
                                @foreach($schedules as $schedule)
                                    @if(!$schedule->employee->active)
                                        @continue
                                    @endif
                                    <tr
                                    @if($current_subsidiary % 2 == 0)
                                        class="active"
                                    @endif
                                    >
                                        <td>{{$subsidiary->name}}</td>
                                        <td>{{$schedule->employee->job}}</td>
                                        <td>{{$schedule->employee->short_name}}</td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                {{$schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->first()->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->turn->identifier == 3)
                                                <i class="fa fa-check"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count())
                                                <i class="fa fa-unlock"></i>
                                            @else
                                                @if($schedule->can_check)
                                                    <i class="fa fa-unlock"></i>
                                                @else
                                                    <form action="{{route('employees.schedules.unlock', $schedule->id)}}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                        <button class="btn btn-link">
                                                            <i class="fa fa-lock"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('employees.edit', $schedule->employee->id)}}">Editar</a>
                                        </td>
                                   </tr>
                                @endforeach
                                <?php $current_subsidiary++; ?>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <reprint-cash-cut
    :subsidiaries="{{$subsidiaries->toJson()}}"
    :turns="{{$turns->toJson()}}"
    ></reprint-cash-cut>
</div>