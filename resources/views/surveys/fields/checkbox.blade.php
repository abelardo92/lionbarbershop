<div class="panel panel-default js-question-container @if(!$first) display-none @endif" data-index="{{$index}}" data-aswered="0"  data-type="checkbox">
    <div class="panel-heading">
        <label for="question-{{$question->id}}">
            <h2>
                {{$question->title}}
                @if(!$question->required) <small>(Opcional)</small> @endif
            </h2>
        </label>
    </div>
    <div class="panel-body">
        <input 
            type="hidden" 
            class="form-control js-checkbox-change" 
            name="question[{{$question->id}}]" 
            value="{{$question->id}}"
            data-index="{{$index}}"
        >
        @foreach($question->options() as $option)
            <div class="well well-sm">
                <div class="checkbox">
                    <label>
                        <input 
                            type="checkbox" 
                            name="question[{{$question->id}}][answer][]" 
                            id="question-{{$question->id}}"
                            value="{{$option}}"
                        >
                        {{$option}}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
</div>