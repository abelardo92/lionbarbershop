<div class="panel panel-default js-question-container @if(!$first) display-none @endif" data-index="{{$index}}"  data-aswered="0"  data-type="satisfaction">
    <div class="panel-heading">
        <label for="question-{{$question->id}}">
            <h2>
                {{$question->title}}
                @if(!$question->required) <small>(Opcional)</small> @endif
            </h2>
        </label>
    </div>
    <input 
        type="hidden" 
        class="form-control" 
        name="question[{{$question->id}}]" 
        value="{{$question->id}}"
    >
    <div class="panel-body flex-center">
        @foreach([ 'excelente', 'bueno', 'regular', 'malo', 'muy-malo'] as $option)
            <div class="emoji">
                <input 
                    type="radio" 
                    class="hide js-radio-change"
                    name="question[{{$question->id}}][answer]" 
                    id="question-{{$question->id}}-option-{{$option}}"
                    data-index="{{$index}}"
                    @if($question->required) required @endif
                    value="{{ucwords(str_replace('-', ' ', $option))}}"
                >
                <label for="question-{{$question->id}}-option-{{$option}}">
                    <img src="{{asset("images/survey/{$option}.jpg")}}" width="100%">
                    <span>{{ucwords(str_replace('-', ' ', $option))}}</span>
                </label>
            </div>
        @endforeach
    </div>
</div>