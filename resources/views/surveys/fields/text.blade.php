<div class="panel panel-default js-question-container @if(!$first) display-none @endif" data-index="{{$index}}"  data-aswered="1"  data-type="text">
    <div class="panel-heading">
        <label for="question-{{$question->id}}">
            <h2>
                {{$question->title}}
                @if(!$question->required) <small>(Opcional)</small> @endif
            </h2>
        </label>
    </div>
    <div class="panel-body text-center">
        <input 
            type="hidden" 
            class="form-control" 
            name="question[{{$question->id}}]" 
            value="{{$question->id}}"
        >
        <input 
            type="text" 
            class="form-control" 
            name="question[{{$question->id}}][answer]" 
            id="question-{{$question->id}}"
            @if($question->required) required @endif
            @if($focus) autofocus @endif
        >
    </div>
</div>