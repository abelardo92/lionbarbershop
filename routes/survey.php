<?php

Route::group([ 'middleware' => 'auth', 'prefix' => 'home'], function () {
    Route::resource('questions', 'QuestionController', [
        'except' => ['show']
    ]);

    Route::resource('surveys', 'SurveyController', [
        'only' => ['index', 'show'],
        'names' => [
            'index' => 'admin.surveys.index',
            'show' => 'admin.surveys.show',
        ]
    ]);
});

Route::group([
    'domain' => "{subsidiary_code}." . env('DOMAIN'),
], function () {
    Route::resource('surveys', 'Web\SurveyController', [
        'only' => ['index', 'edit', 'store', 'update']
    ]);
});