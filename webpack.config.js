const path = require('path');
const webpack = require('webpack');
// const VueLoaderPlugin = require('vue-loader/lib/plugin');
const VueLoaderPlugin = require('vue-loader');
// const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
    entry: ["./resources/assets/js/app.js"],
    output: {
        path: __dirname + "/assets/js",
        filename: 'bullets.js'
    },
    devtool: "sourcemap",
    externals: {
        "jquery": "jQuery"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules\/(?!bullets-js)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            }
            , {
                test: /\.vue$/,
                // use: {
                    loader: 'vue-loader',
                // }
            }
            // ,
            // {
            //     test: /\.vue$/,
            //     // include: /src/,
            //     loader: 'vue-loader',
            //     // options: {
            //     //     loaders: {
            //     //         js: 'awesome-typescript-loader?silent=true'
            //     //     }
            //     // }
            // }
        ]
    },
    plugins: [
        // make sure to include the plugin!
        // new webpack(),
        new VueLoaderPlugin()
    ]
}